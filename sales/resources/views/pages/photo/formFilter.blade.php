<div class="box-content">
	<div class="form-horizontal">
		 <fieldset>
		 	<div class="span12">
				<div class="control-group span4">
				 	<label class="control-label">From</label>
				  	<div class="controls">
					{!! Form::text('dateFrom', null, array('id' => 'datepicker1')) !!}
				  	</div>
				</div>

				<div class="control-group span4">
				 <label class="control-label">To</label>
				  	<div class="controls">
						{!! Form::text('dateTo', null, array('id' => 'datepicker2')) !!}
				  	</div>
				</div>
		 	</div>
		 	<div class="span12" style="margin-left: 0px" >
				<div class="control-group span1">	
					<div class="controls">	
					{!! Form::submit('Show', array('class' => 'btn btn-primary')) !!}
					</div>
				</div>
			</div>
		</fieldset>
	</div>
</div>

             
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script>
    $(function() {
      $('#datepicker1').datepicker({ dateFormat: 'yy-mm-dd' }).val();
      $('#datepicker2').datepicker({ dateFormat: 'yy-mm-dd' }).val();
    });
 </script>




