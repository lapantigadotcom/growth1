@extends('layouts.app')

@section('content')
@section('title', 'Photo Activity')
<div id="content" class="span10">
	<ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="{{url('/home/dashboard')}}">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">Take Order</a>  
            <i class="icon-angle-right"></i>             
        </li>    
         <li>
            <a href="#">Take Order List Filter</a>                          
        </li>    
    </ul>

	<div class="row-fluid sortable">
		<div class="box span12">
			<div class="box-header" data-original-title>
				<h2><i class="halflings-icon edit"></i><span class="break"></span>Filter Data Date Take Photo</h2>
			</div>
			    {!! Form::open(array('route' => 'admin.photoAct.result', 'method' => 'POST' )) !!}
			    	@include('pages.photo.formFilter')
			    {!! Form::close() !!}
		</div>
	</div>

    <div class="row-fluid sortable">        
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon shopping-cart"></i><span class="break"></span>Take Order 1 Minggu Terakhir</h2>
            </div>
            <br>
                <div class="box-content">                                 
                    <table class="table table-striped table-bordered bootstrap-datatable datatable">
                         <thead>
                                <tr>
                                    
                                    <th>Nama Outlet</th>
                                    <th>Tipe</th>
                                    <th>Sales Force </th>
                                    <th>Nama Foto </th>
                                    <th>Note</th>
                                    <th>Date Take Foto</th>
                                    <th>Date Upload</th>
                                    <th>Foto</th>
                                    @if(Auth::user()->kd_role != 3)   
                                    <th>Action</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $row)
                                <tr>
                                    
                                    <td>{{ $row->nm_outlet }}</td>
                                    <td>{{ $row->nama_tipe }}</td>
                                    <td>{{ $row->nama }}</td>
                                    <td>{{ $row->nm_photo }}</td>
                                    <td>{{ $row->keterangan }}</td>
                                    <td>{{ date('d-F-Y H:i:s', strtotime($row->date_take_photo)) }}</td>
                                    <td>{{ date('d-F-Y H:i:s', strtotime($row->date_upload_photo)) }}</td>
                                    @if(File::exists('image_upload/outlet_photoact/'.$row->path_photo) and $row->path_photo != '')
                                    <td> <a href="{!! url('image_upload/outlet_photoact/'.$row->path_photo) !!}" target="_blank" title="{{ $row->nm_photo }}">
                                        <img style="max-height: 60px; max-width: 60px;" src="{!! url('image_upload/outlet_photoact/'.$row->path_photo) !!}" class="img-responsive" /></a>
                                    </td>
                                    @else
                                       <td><img style="max-height: 60px; max-width: 60px;" src="{!! url('/img/no_image_outlet_upload.png') !!}" class="img-responsive" /></td>
                                    @endif

                                    @if(Auth::user()->kd_role != 3)   
                                    <td>         
                                    @if(Auth::user()->hasAccess('admin.photo.edit'))
                                        <a href="{!! route('admin.photo.edit',[$row->id]) !!}" class="icon-pencil"></a>
                                    @endif
                                        &nbsp;
                                    @if(Auth::user()->hasAccess('admin.photo.delete'))
                                        <a href="{!! route('admin.photo.delete',[$row->id]) !!}" onclick="return confirm('Are You Sure')" class="icon-trash"></a>   
                                    @endif           
                                    </td>
                                    @endif
                                </tr>
                                @endforeach
                            </tbody>
                    </table>
                </div>
            </div>
        </div>
</div>	    

@endsection

@section('custom-head')
    
@endsection

@section('custom-footer')
    
@endsection
