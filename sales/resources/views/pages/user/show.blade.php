<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>GROWTH | Profil: {!! $data['content']->nama !!}</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        @include('partials.head')
        <script src="//cdn.ckeditor.com/4.5.2/standard/ckeditor.js"></script>

    </head>
<body>
<div class="container-fluid-full">
    <div class="row-fluid"> 
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="left-side sidebar-offcanvas">
        @include('partials.sidebar')
        </aside>
<!-- Main content -->
<div id="content" class="span9">
  <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="{{url('/home/dashboard')}}">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">User Manager</a>
            <i class="icon-angle-right"></i>
            
        </li>
        <li>
            <a href="#">Non SF</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">Detail User</a>
        </li>
    </ul>
        <div class="row-fluid sortable">        
                <div class="box span12">
                    <div class="box-header">
                        <h2><i class="halflings-icon th-list"></i><span class="break"></span>{!! $data['content']->nama !!}</h2>
                    </div>
                    
                    <div class="box-content">
                        <div class="row-fluid">            
                            <div class="span4">
                                @if(File::exists('userphoto/'.$data['content']->foto) and $data['content']->foto != '')
                                <img src="{!! asset('userphoto/'.$data['content']->foto) !!}" class="img-responsive" />
                                @else
                                <img src="{!! url('/img/user.png') !!}" class="img-responsive img-thumbnail">
                                @endif
                              </div>
                              <div class="span7">  <br>  
                                <p><i class=" halflings-icon barcode"></i> NIK : {!! $data['content']->nik !!}</p>
                                <p><i class=" halflings-icon user"></i> Nama User: {!! $data['content']->nama !!}</p>
                                <p><i class=" halflings-icon home"></i> Alamat  : {!! $data['content']->alamat !!}</p>
                                <p><i class=" halflings-icon th"></i> Area : {!! $data['content']->nm_area !!}</p>
                                <p><i class=" halflings-icon phone"></i> Telepon : {!! $data['content']->telepon !!}</p>
                                <p><i class=" halflings-icon user"></i> Username : {!! $data['content']->username !!}</p>       
                                <p><i class=" halflings-icon envelope"></i> Email : {!! $data['content']->email_user !!}</p>                 
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>

            <div class="row-fluid sortable">        
                    <div class="box span12">
                        <div class="box-header" data-original-title>
                            <h2><i class="halflings-icon list-alt"></i><span class="break"></span>Log User {{$data['content']->nama}}</h2>
                        </div>
                        <div class="box-content">                                 
                            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                                <thead>
                                    <tr>
                                        <th>Time</th>
                                        <th>Description</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data['logs'] as $row)
                                    <tr>
                                        <td style="color:blue">{{$row->log_time}}</td>
                                        <td style="color:blue">{{$row->description}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                              </table>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>
@include('partials.footer')
    </body>
</html>