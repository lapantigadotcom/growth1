<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>GROWTH | Working Time</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        @include('partials.head')
        <script src="//cdn.ckeditor.com/4.5.2/standard/ckeditor.js"></script>

    </head>
<body>
<div class="container-fluid-full">
    <div class="row-fluid"> 
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="left-side sidebar-offcanvas">
        @include('partials.sidebar')
        </aside>

        <!-- Right side column. Contains the navbar and content of the page -->
        <div id="content" class="span10">
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="{{url('/home/dashboard')}}">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="#">Report</a>   
                    <i class="icon-angle-right"></i>            
                </li>     
                <li>
                    <a href="#">Working Time Sales</a>   
                    <i class="icon-angle-right"></i>            
                </li>     
            </ul>
            <div class="row-fluid sortable">        
                <div class="box span12">
                    <div class="box-header" data-original-title>
                        <h2><i class="halflings-icon edit"></i><span class="break"></span>Input Data</h2>
                        <div class="box-icon">
                            <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                            <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                            <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                        </div>
                    </div>
                        {!! Form::open(array('route' => 'admin.report.resultWorkTime', 'method' => 'store' )) !!}
                            @include('pages.workTime.form',array('submit' => 'Show'))
                        {!! Form::close() !!}
                </div>
            </div>    
        </div>  
    </div>
</div>
<!-- add new calendar event modal -->
@include('partials.footer')

    </body>
</html>