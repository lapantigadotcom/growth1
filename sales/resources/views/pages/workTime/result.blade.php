<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>GROWTH | Working Time Report</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        @include('partials.head')
        <script src="//cdn.ckeditor.com/4.5.2/standard/ckeditor.js"></script>

    </head>
<body>
<div class="container-fluid-full">
    <div class="row-fluid"> 
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="left-side sidebar-offcanvas">
        @include('partials.sidebar')
        </aside>

        <!-- Right side column. Contains the navbar and content of the page -->
        <div id="content" class="span10">
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="{{url('/home/dashboard')}}">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="#">Report</a>  
                    <i class="icon-angle-right"></i>             
                </li>    
                 <li>
                    <a href="#">Visiting Outlet Report</a>                          
                </li>    
            </ul>

            <div class="row-fluid sortable">       
               <div class="box span12">
                <div class="box-header" data-original-title>
                    <h2><i class="halflings-icon user"></i><span class="break"></span>Laporan Working Time Per Sales Force</h2>
                </div>
                    <div class="box-content">    
                        <div class="span12">
                             @foreach($data['sales'] as $val)
                                <h1>Sales Force : {!! $val->nama !!}</h1>
                             @endforeach
                             @foreach($data['sales'] as $val)
                                <div class="priority low"><span>NIK: {{$val->nik}}</span></div>
                             @endforeach
                            <div class="task high">
                                 @foreach($data['area'] as $val)
                                 <div class="span3">
                                    <div class="title">Area</div>
                                    <div> {!! $val->nm_area !!}</div>
                                </div>
                                 @endforeach  
                                <div class="span3">
                                    <div class="title">Periode</div>
                                    <div>{{$data['periode']}}</div>
                                </div>
                                 
                            </div>
                        </div>
                
                    
                        <table class="table table-striped table-bordered bootstrap-datatable">
                            <thead>
                                <tr>               
                                   
                                    <th style="text-align:center;" rowspan="2" colspan="1">Waktu Check In</th>
                                    <th style="text-align:center;" rowspan="2" colspan="1">Waktu Check Out</th>
                                    <th style="text-align:center;" rowspan="2" colspan="1">Working Time/ Day</th>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data['workingTime'] as $row)
                                <tr>                         
                                    <td style="text-align:center;">{{ date('d-F-Y H:i:s', strtotime($row->CheckIn ))  }}</td>
                                    <td style="text-align:center;">{{ date('d-F-Y H:i:s', strtotime($row->CheckOut)) }}</td>
                                    <td style="text-align:center;">{{ $row->WorkingHours }}</td>                                 
                               </tr>
                               @endforeach  
                                <tr>
                                    <td colspan="2" style="text-align:center;"> Total Working Time / Month</td>
                                    <td style="text-align:center;font-size: 18pt"> {{$data['totalWorkTimeMonth']}}</td>
                                </tr>
                            </tbody>
                        </table>
                        
                        </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div> 

        </div>  
    </div>
</div>

<!-- add new calendar event modal -->
@include('partials.footer')
    </body>
</html>