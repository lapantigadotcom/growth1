<div class="box-content">
	<div class="form-horizontal">
		 <fieldset>
		 	<div class="control-group">
				<label class="control-label">Periode</label>
				  	<div class="controls">
					{!! Form::text('periode', null, array('id' => 'datepicker0')) !!}
				</div>
			</div>
						
			<div class="control-group ">
				<label class="control-label" >Kode Area</label>
				  	<div class="controls">
					{!! Form::select('kd_area',$areas, null, array('id'=>'area','class' => 'form-control')) !!}	
				</div>
			</div>
			
			<div class="control-group ">
				<label class="control-label" >Nama Sales</label>
				  	<div class="controls">
					<select id="sales" name="sales">
					  <option>Select Sales</option>
					</select>
				</div>
			</div>
				
			<div class="control-group ">
				  	<div class="controls">
					{!! Form::submit('Show', array('class' => 'btn btn-primary', 'name' => 'show')) !!}
					{!! Form::submit('Download', array('class' => 'btn btn-primary', 'name' => 'down')) !!}
				</div>
			</div>

		</fieldset>
	</div>
</div>

             
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script>
function standardPeriod() {
				var CurrentDate = new Date();
				CurrentDate.setMonth(CurrentDate.getMonth());
				
				var day = CurrentDate.getDate();
				var year = CurrentDate.getFullYear();
				var month = new Array();
				month[0] = "January";
				month[1] = "February";
				month[2] = "March";
				month[3] = "April";
				month[4] = "May";
				month[5] = "June";
				month[6] = "July";
				month[7] = "August";
				month[8] = "September";
				month[9] = "October";
				month[10] = "November";
				month[11] = "December";
				var n = month[CurrentDate.getMonth()]; 
				var today = n + "-" + year;
				
				return today;
}

$(document).ready(
  function () {
			$('#initialdate').val(standardPeriod());
  }
);
</script>
 <script>
    $(function() {
      $('#datepicker0').datepicker({ dateFormat: 'mm-yy'	}).val();
      $('#datepicker1').datepicker({ dateFormat: 'yy-mm-dd' }).val();
      $('#datepicker2').datepicker({ dateFormat: 'yy-mm-dd' }).val();
      $('#datepicker3').datepicker({ dateFormat: 'yy-mm-dd' }).val();
      $('#datepicker4').datepicker({ dateFormat: 'yy-mm-dd' }).val();
      $('#datepicker5').datepicker({ dateFormat: 'yy-mm-dd' }).val();
      $('#datepicker6').datepicker({ dateFormat: 'yy-mm-dd' }).val();
      $('#datepicker7').datepicker({ dateFormat: 'yy-mm-dd' }).val();
      $('#datepicker8').datepicker({ dateFormat: 'yy-mm-dd' }).val();
    });
  </script>
 <script>
	$('#area').on('change', function(e) {
        console.log(e);
        var id = e.target.value;
        //ajax
        $.getJSON('{{url('/ajax-sales?id=')}}'+id, function (data) {
			//console.log(data);
            $('#sales').empty();
          	$.each(data, function(index, salesObj){
            	 $('#sales').append('<option value="'+salesObj.id+'">'+salesObj.nama+'</option>');
				});
        });
    });
</script>





