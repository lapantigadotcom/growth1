<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Hisamitsu Panel</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        @include('partials.head')
        <script src="//cdn.ckeditor.com/4.5.2/standard/ckeditor.js"></script>

    </head>
<body>

<div class="container-fluid-full">
    <div class="row-fluid"> 
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="left-side sidebar-offcanvas">
        @include('partials.sidebar')
        </aside>

        <!-- Right side column. Contains the navbar and content of the page -->
        <div id="content" class="span10">
        <ul class="breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="{{url('/home/dashboard')}}">Home</a> 
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a href="#">User</a>
                <i class="icon-angle-right"></i>
                
            </li>
            <li>
                <a href="#">Change Password</a>
            </li>
        </ul>

		<div class="row-fluid sortable">        
            
                <div class="box-content">
                        @if(Session::has('errorMessage'))
                        	{!! Session::get('errorMessage') !!}
                        @elseif(Session::has('successMessage'))
                        	{!! Session::get('successMessage') !!}
                        @endif
                        {!! Form::open(array('route' => 'admin.postChangePassword', 'method' => 'POST')); !!}
                            <div class="row">
                                <div class="col-md-12">
                               	  {!! Form::password('old_password',array('class' => 'form-control', 'placeholder' => 'Old Password')) !!}
                                  <br></br>
                               	  {!! Form::password('new_password',array('class' => 'form-control', 'placeholder' => 'New Password')) !!}
                                  <br></br>
                               	  {!! Form::password('retype_password',array('class' => 'form-control', 'placeholder' => 'Re-Type Password')) !!} <br></br>                              	                                 
                              	  <div class="form-group">
                                      {!! Form::submit('Simpan', array('class' => 'btn btn-primary')) !!}
            	                  </div>
                                </div><!-- /.col -->
                            </div> <!-- /.row -->
                        {!! Form::close() !!}
                </div>
           </div>
        </div>

    </div>
</div>

@include('partials.footer')
    </body>
</html>
