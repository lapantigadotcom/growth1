<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>GROWTH | Competitor Activity</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        @include('partials.head')
        <script src="//cdn.ckeditor.com/4.5.2/standard/ckeditor.js"></script>

    </head>
<body>

<div class="container-fluid-full">
    <div class="row-fluid"> 
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="left-side sidebar-offcanvas">
        @include('partials.sidebar')
        </aside>
  
        <!-- Main content -->
        <div id="content" class="span10">
          <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="{{url('/home/dashboard')}}">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="#">Photo Activity</a>
                    <i class="icon-angle-right"></i>
                    
                </li>
                <li>
                    <a href="#">Photo Display</a>
                </li>
            </ul>
            <div class="row-fluid">     
                <div class="box span12">
                    <div class="box-header">
                       <h2><i class="halflings-icon user"></i><span class="break"></span>Photo List</h2>
                    </div>
                    <div class="box-content">
                        <ul class="nav tab-menu nav-tabs" id="myTab">
                            <li class="active"><a href="#areaCity">Competitor Photo</a></li>
                            <li><a href="#areaOnly">Competitor List</a></li>
                        </ul>
                         
                        <div id="myTabContent" class="tab-content">             
                            <div class="tab-pane active" id="areaCity">
                                <div class="box-content">              
                                    @if(Auth::user()->hasAccess('admin.competitor.create'))      
                                    <a href="{!! route('admin.competitor.create') !!}" class="btn btn-primary">Tambah Photo Competitor </a>
                                    @endif                   
                                     <table class="table table-striped table-bordered bootstrap-datatable datatable">
                                    <thead>
                                        <tr>
                                            <th>Kode Outlet</th>
                                            <th>Nama Outlet</th>
                                            <th>Nama Competitor</th>
                                            <th>Sales Force </th>
                                            <th>Nama Foto </th>
                                            <th>Date Take Foto</th>
                                            <th>Date Upload</th>
                                            <th>Foto</th>
                                            <th>Note</th>
                                            @if(Auth::user()->kd_role != 3) 
                                              <th style="text-align:center;">Action</th>
                                            @else
                                              <th style="text-align:center;"></th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($data['competitorAct'] as $row)
                                        <tr>
                                            <td>{{ $row->kd_outlet }}</td>
                                            
                                            <!--
                                             @if($row->almt_comp_activity != '')
                                                <td>{{ $row->almt_comp_activity }}</td>
                                            @else
                                                <td><span class="label label-warning">Unknown</span></td>
                                            @endif
                                            -->
                                            <td>{{ $row->nm_outlet }}</td>
                                            <td>{{ $row->nm_competitor }}</td>
                                            <td>{{ $row->nama }}</td>
                                            <td>{{ $row->nm_photo }}</td>
                                            <td>{{ date('d-F-Y H:i:s', strtotime($row->date_take_photo)) }}</td>
                                            <td>{{ date('d-F-Y H:i:s', strtotime($row->date_upload_photo)) }}</td>
                                            @if(File::exists('image_upload/competitor/'.$row->path_photo) and $row->path_photo != '')
                                            <td>
                                               <a target="_blank" href="{!! url('image_upload/competitor/'.$row->path_photo) !!}"> <img style="max-height: 60px; max-width: 60px"  src="{!! url('image_upload/competitor/'.$row->path_photo) !!}" class="img-responsive" /></a>
                                            </td>

                                            @else
                                            <td><img style="max-height: 60px; max-width: 60px" src="{!! url('/img/no_image_outlet_upload.png') !!}" class="img-responsive" />
                                            </td>                                    

                                            @endif
                                            <td> {{ $row->keterangan }}</td> 
                                            <td>  
                                                @if(Auth::user()->hasAccess('admin.competitor.edit'))         
                                                    <a href="{!! route('admin.competitor.edit',[$row->id]) !!}" class="icon-pencil"></a>
                                                @endif
                                                    &nbsp;
                                                @if(Auth::user()->hasAccess('admin.competitor.delete'))   
                                                    <a href="{!! route('admin.competitor.delete',[$row->id]) !!}" onclick="return confirm('Are You Sure')" class="icon-trash"></a>       
                                                @endif   
                                            </td>
                                            
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                </div><!-- /.box-body -->
                            </div>
                            <div class="tab-pane" id="areaOnly">
                                  <div class="box-content">          
                                    @if(Auth::user()->hasAccess('admin.comp.create'))  
                                        <a href="{!! route('admin.comp.create') !!}" class="btn btn-primary">Tambah Daftar Competitor </a>
                                    @endif                  
                                    <table class="table table-striped table-bordered bootstrap-datatable datatable">
                                        <thead>
                                            <tr>                
                                                <th>Nama Competitor</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($data['competitor'] as $row)
                                                <tr>
                                                    <td>{{ $row->nm_competitor }}</td>
                                                    <td>             
                                                    @if(Auth::user()->hasAccess('admin.comp.edit')) 
                                                        <a href="{!! route('admin.comp.edit',[$row->id]) !!}" class="icon-pencil"></a>
                                                    @endif   
                                                        &nbsp;
                                                    @if(Auth::user()->hasAccess('admin.comp.delete'))   
                                                        <a href="{!! route('admin.comp.delete',[$row->id]) !!}" onclick="return confirm('Are You Sure?')" class="icon-trash"> </a>
                                                    @endif   
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>       
            </div>    
        </div>
    </div>
</div>
        @include('partials.footer')
    </body>
</html>