<div class="span3">
	<div class="box-body">
		<div class="form-group">
			{!! Form::label('kd_outlet','Nama Outlet') !!}
			{!! Form::select('kd_outlet',$outlets, null, array('class' => 'form-control')) !!}
			{!! Form::label('kd_competitor','Nama Kompetitor') !!}
			{!! Form::select('kd_competitor',$competitors, null, array('class' => 'form-control')) !!}	
			{!! Form::label('nm_photo','Nama Foto') !!}
			{!! Form::text('nm_photo',null, array('class' => 'form-control')) !!}
			{!! Form::label('jenis_photo','Jenis Photo') !!}
			{!! Form::text('jenis_photo',null, array('class' => 'form-control')) !!}
			
</div></div></div>
<div class="span3">
	<div class="box-body">
		<div class="form-group">
			{!! Form::label('almt_comp_acitivity','Alamat Kompetitor') !!}
			{!! Form::text('almt_comp_activity', null, array('class' => 'form-control')) !!}
			{!! Form::label('date_take_photo','Tanggal Take Photo') !!}
			{!! Form::text('date_take_photo', null, array('id' => 'datepicker')) !!}
			{!! Form::label('keterangan','Keterangan') !!}
			{!! Form::text('keterangan',null, array('class' => 'form-control')) !!}
			<div class="control-group error">
			<div class="controls">
				{!! Form::label('path_photo','Upload Foto') !!}
				{!! Form::file('path_photo',null, array('class' => 'form-control')) !!}
			 	<span class="help-inline">*) Required</span>
			</div>
			</div>

			<div class="form-group">
				@if ($errors->has())
					<div class="alert alert-error">
					  @foreach ($errors->all() as $error)
					    {!! $error !!}<br />		
					  @endforeach
					</div>
				@endif
				{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
			</div>
		
</div></div></div>		

<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
  <script>
    $(function() {
      $('#datepicker').datepicker({ dateFormat: 'yy-mm-dd' }).val();
    });
  </script>





