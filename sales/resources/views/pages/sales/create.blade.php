@extends('layouts.app')

@section('content')

<div id="content" class="span10">  
 <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="{{url('/home/dashboard')}}">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">User Manager</a>
            <i class="icon-angle-right"></i>
            
        </li>
        <li>
            <a href="#">Sales Force</a>
             <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">Create Sales Force</a>
        </li>
    </ul>
      <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Note !</strong> Harap cek data sebelum melakukan Submit.
        </div>

    {!! Form::open(array('route' => 'admin.sales.store', 'method' => 'POST','files' => true)) !!}
        @include('pages.sales.form',array('submit' => 'Simpan'))
    {!! Form::close() !!}
</div>
               
@endsection

@section('custom-head')
    
@endsection

@section('custom-footer')
    
@endsection