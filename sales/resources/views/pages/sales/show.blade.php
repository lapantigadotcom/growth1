<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>GROWTH| Profile : {!! $data['content']->nama !!}</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        @include('partials.head')
        <script src="//cdn.ckeditor.com/4.5.2/standard/ckeditor.js"></script>

    </head>
<body>
<div class="container-fluid-full">
    <div class="row-fluid"> 
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="left-side sidebar-offcanvas">
        @include('partials.sidebar')
        </aside>

<!-- Main content -->
<div id="content" class="span9">
  <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="{{url('/home/dashboard')}}">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">User Manager</a>
            <i class="icon-angle-right"></i>
            
        </li>
        <li>
            <a href="#">Sales Force</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">Detail Sales</a>
        </li>
    </ul>
        <div class="row-fluid sortable">        
                <div class="box span12">
                    <div class="box-header">
                        <h2><i class="halflings-icon user"></i><span class="break"></span>{!! $data['content']->nama !!}</h2>        
	                    <div class="box-icon">
	                   		<h2> <a href="{{ route('admin.download.salesDetail',[$data['content']->id]) }}"> Download
	                   		 <span class="break"></span><i class="icon-download-alt"></i>
	                   		</a></h2>
	                    </div>
                    </div>
                    <div class="box-content">
                        <div class="row-fluid">            
                            <div class="span3">
                                @if(File::exists('userphoto/'.$data['content']->foto) and $data['content']->foto != '')
                                    <img src="{!! url('userphoto/'.$data['content']->foto) !!}" class="img-responsive" />
                                @else
                                    <img src="{!! url('/img/user.png') !!}" class="img-responsive img-thumbnail">
                                @endif
                              </div>
                              <div class="span9">    
                                <p><i class=" halflings-icon barcode"></i> NIK : {!! $data['content']->nik !!}</p>
                                <p><i class=" halflings-icon user"></i> Nama Sales : {!! $data['content']->nama !!}</p>
                                <p><i class=" halflings-icon home"></i> Alamat Sales : {!! $data['content']->alamat !!}</p>
                                <p><i class=" halflings-icon th"></i> Area : {!! $data['content']->nm_area !!}</p>   
                                <p><i class=" halflings-icon calendar"></i> Join date : {!! $data['content']->join_date !!}</p>
                                <p><i class=" halflings-icon phone"></i> Telepon Sales : {!! $data['content']->telepon !!}</p>
                                <p><i class=" halflings-icon user"></i> Username : {!! $data['content']->username !!}</p>       
                                <p><i class=" halflings-icon envelope"></i> Email : {!! $data['content']->email_user !!}</p>    
                            </div>
                        </div>
                    </div><!-- /.box-body -->                
                </div><!-- /.box -->
         </div>
         <div class="row-fluid sortable"> 
            <div class="box span12">
                <div class="box-header">
                    <h2><i class="halflings-icon th-list"></i><span class="break"></span>Report</h2>
                </div>
                
                    <div class="box-content">
                        <div class="row-fluid"> 
                            <a class="quick-button metro blue span3">
                                <i class="icon-truck"></i>
                                <p>Outlet visit</p>
                                <span class="badge">{{$data['outletVisit']}}</span>
                            </a>                       
                            <a class="quick-button metro red span3">
                                <i class="icon-group"></i>
                                <p>Competitor Activity</p>
                                <!--
                                 @foreach($data['workingTime'] as $row)
                                    @if($row->working == 0)
                                        <span class="badge">0</span>
                                    @else
                                        <span class="badge">{{ $row->working}}</span>
                                    @endif
                                @endforeach -->
 
                            </a>    
                            <a class="quick-button metro green span3">
                                <i class="icon-shopping-cart"></i>
                                <p>Take Order</p>
                                <span class="badge">{{$data['takeOrder']}}</span>
                            </a>   
                            <a class="quick-button metro orange span3">
                                <i class="icon-camera"></i>
                                <p>Take Photo</p>
                                <span class="badge">{{$data['takePhoto']}}</span>
                            </a>                       
                        </div>
                    </div><!-- /.box-body -->                
            </div><!-- /.box -->
        </div>

        <div class="row-fluid sortable">        
            <div class="box span12">
                <div class="box-header" data-original-title>
                    <h2><i class="halflings-icon list-alt"></i><span class="break"></span>Log Sales Force</h2>
                </div>

                <div class="box-content">                                 
                    <table class="table table-striped table-bordered bootstrap-datatable datatable ">
                        <thead>
                            <tr>
                                <th>Time</th>
                                <th>Description</th>
                                <th>Detail Akses</th>
                            </tr>
                        </thead>
                       <tbody>
                            @foreach($data['logs'] as $row)
                            <tr>
                                <td style="color:blue">{{$row->log_time}}</td>
                                 <td style="color:blue">{{$row->description}}</td>
                                 <td style="color:blue">{{$row->detail_akses}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                      </table>
                </div>
            </div>
        </div>

        </div><!-- /.box -->
    </div>
</div><!-- /.row (main row) -->


<!-- add new calendar event modal -->
@include('partials.footer')
    </body>
</html>