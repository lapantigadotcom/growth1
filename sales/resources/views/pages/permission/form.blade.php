<div class="box-body">
	<div class="form-group">

		{!! Form::label('nm_permission','Route / Permission') !!}
		{!! Form::text('nm_permission',null, array('class' => 'form-control')) !!}
		
		{!! Form::label('enable','Availability') !!}
		{!! Form::select('enable',[
					   '1' => 'Enable',
					   '0' => 'Disable',
					   ]) !!}		
	</div>
	<div class="form-group">
		{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
	</div>
</div>