<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>GROWTH | Setup Permission</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        @include('partials.head')
        <script src="//cdn.ckeditor.com/4.5.2/standard/ckeditor.js"></script>
    </head>
<body>
<div class="container-fluid-full">
    <div class="row-fluid"> 
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="left-side sidebar-offcanvas">
        @include('partials.sidebar')
        </aside>

        <!-- Right side column. Contains the navbar and content of the page -->
        <div id="content" class="span10">
        <ul class="breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="{{url('/home/dashboard')}}">Home</a> 
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a href="#">Configuration</a>
                <i class="icon-angle-right"></i>
            </li>
             <li>
                <a href="#">Permission</a>
            </li>
        </ul>
        <div class="row-fluid sortable">        
            <div class="box span12">
                <div class="box-header" data-original-title>
                    <h2><i class="halflings-icon user"></i><span class="break"></span>Permission List</h2>
                </div>
                <br>
                @if(Auth::user()->hasAccess('admin.permission.create')) 
                    <a href="{!! route('admin.permission.create') !!}" class="btn btn-primary">Tambah Permission</a>
                @endif
                    <div class="box-content">                                 
                        <table class="table table-striped table-bordered bootstrap-datatable datatable">
                            <thead>
                                <tr>               
                                    <th>Route</th>
                                    <th>Role Pengakses</th>
                                    <th>Enable</th>
                                    @if(Auth::user()->kd_role != 3) 
                                        <th style="text-align:center;">Action</th>
                                    @else
                                        <th style="text-align:center;"></th>
                                    @endif

                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $row)
                                <tr>                          
                                    <td>{{ $row->nm_permission }}</td>
                                    <td>
                                     <?php $tmp = array(); ?>
                                    	@foreach($row->permissionRoles as $val)
                                    		 <?php array_push($tmp, $val->type_role);  ?>
                                    	@endforeach
                                    {!! implode(', ', $tmp) !!}
                                    </td>
                                    @if($row->enable == 1)
                                        <td>YES</td> 
                                    @else
                                        <td>NO</td> 
                                    @endif                       
                                    <td>     
                                    @if(Auth::user()->hasAccess('admin.permission.edit'))    
                                        <a href="{!! route('admin.permission.edit',[$row->id]) !!}" class="icon-pencil"></a>  
                                    @endif
                                        &nbsp;    
                                    @if(Auth::user()->hasAccess('admin.permission.delete')) 
                                        <a href="{!! route('admin.permission.delete',[$row->id]) !!}"  onclick="Are you sure?" class="icon-trash"></a>
                                    @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div><!-- /.row (main row) -->
    </div>
</div>

@include('partials.footer')
    </body>
</html>