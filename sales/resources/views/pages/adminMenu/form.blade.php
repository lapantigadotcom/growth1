<div class="box-body">
	<div class="form-group">

		{!! Form::label('nm_menu','Nama Menu') !!}
		{!! Form::text('nm_menu',null, array('class' => 'form-control')) !!}

		{!! Form::label('route','Route') !!}
		{!! Form::text('route',null, array('class' => 'form-control')) !!}

		{!! Form::label('icon','Icon') !!}
		{!! Form::text('icon',null, array('class' => 'form-control')) !!}

		{!! Form::label('parent_menu','Parent Menu') !!}
		{!! Form::select('parent_menu',$data['adminmenu'],null,array('class' => 'form-control')) !!}	
		
		{!! Form::label('enable','Availability') !!}
		{!! Form::select('enable',[
					   '1' => 'Enable',
					   '0' => 'Disable',
					   ]) !!}		
	</div>
	<div class="form-group">
		{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
	</div>
</div>