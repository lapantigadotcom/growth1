@extends('layouts.app')

@section('content')
<div id="content" class="span10">  

	<ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="{{url('/home/dashboard')}}">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">Configuration</a>
            <i class="icon-angle-right"></i>           
        </li>
         <li>
            <a href="#">Role Management</a>
            <i class="icon-angle-right"></i>
            
        </li>
        <li>
            <a href="#">Edit Role Previlage</a>
        </li>
    </ul>               
    <div class="row-fluid sortable">
        <div class="span12">
                {!! Form::model($data['content'],array('route' => ['admin.role.update',$data['content']->kd_role], 'method' => 'PUT')) !!}
            <div class="box-body">
                    {!! Form::label('type_role','Nama Role') !!}
                    {!! Form::text('type_role',null, array('class' => 'form-control')) !!}
            
                <div class="row text-center">
                    <h1><b>Admin Menu</b></h1>
                </div>

                <div class="span12" style="margin-left: 0px">                                  
                    @foreach($data['adminmenu'] as $key => $value)
                    <div class="span4" style="margin-left: 0px">
                        <div class="form-group">
                              <input type="checkbox" id="menuadmin" name="adminmenu[]" value="{{ $key }}" {{ in_array($key, $data['adminmenu_t'])?'checked':'' }}  class="flat-red"/>
                              &nbsp;
                              {!! $value !!}
                        </div>
                    </div>
                    @endforeach
                </div>

                <div class="row text-center">
                    <h1><b>Permission</b></h1>
                </div>

                <div class="span12" style="margin-left: 0px">                                  
                    @foreach($data['permission'] as $key => $value)
                    <div class="span4" style="margin-left: 0px">
                        <div class="form-group">
                              <input type="checkbox" id="routeadmin" name="permission[]" value="{{ $key }}" {{ in_array($key, $data['permission_t'])?'checked':'' }}  class="flat-red"/>
                              &nbsp;
                              {!! $value !!}
                        </div>
                    </div>
                    @endforeach
                </div>

                <p>
                    <input type="button" id="check" value="Check All" />
                    <input type="button" id="uncheck" value="UnCheck All" />
                </p> 

                <div class="span12" style="margin-left: 0px">
                    <div class="form-actions">
                        {!! Form::submit('Perbarui', array('class' => 'btn btn-primary')) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>   
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script> 
<script>
    $(document).ready( function() {
        $('#check').on('click', function () {
            $(':checkbox').attr("checked", true);
        });
        $('#uncheck').on('click', function () {
            $(':checkbox').attr("checked", false);
        });
    });
</script>
@endsection

@section('custom-head')
     
@endsection

@section('custom-footer')

@endsection