<div class="box-body">
	<div class="form-group">

		{!! Form::label('kd_outlet','Nama Outlet') !!}
		{!! Form::select('kd_outlet', $outlet, null, ['class' => 'form-control']) !!}

		{!! Form::label('status_visit','Status Visiting') !!}
		{!! Form::select('status_visit', [
				   '' => 'Select Status Visiting',
				   '1' => 'YES',
				   '0' => 'NO'], null, ['class' => 'form-control']
				) !!} 
                {!! Form::label('date_visit','Tanggal Visit Plan') !!}
		{!! Form::text('date_visit', date('d-F-Y H:i:s', strtotime($data['content']->date_visit)), array('disabled' => 'disabled')) !!}

                <div class="control-group success">
		<div class="controls">
		         {!! Form::label('date_visiting','Tanggal Visiting (Aktual)') !!}
		         {!! Form::text('date_visiting', null, array('id' => 'datepicker')) !!}
                          <span class="help-inline">Di isi jika Status Visit "YES"</span>
                </div>
		</div>
	
		<div class="control-group error">
		<div class="controls">
			{!! Form::label('skip_reason','Alasan Skip Visit') !!}
			{!! Form::text('skip_reason', null, array('class' => 'form-control')) !!}
		 	<span class="help-inline">Di isi jika Status Visit "NO"</span>
		</div>
		</div>
	</div>

	<div class="form-group">
		{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
	</div>
</div>

<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script>
	$(function() {
	  $('#datepicker').datepicker({ dateFormat: 'yy-mm-dd' }).val();
	});
</script>




