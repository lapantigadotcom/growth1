<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>GROWTH | Export - Import Data System</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        @include('partials.head')
        <script src="//cdn.ckeditor.com/4.5.2/standard/ckeditor.js"></script>

    </head>
<body>
<div class="container-fluid-full">
    <div class="row-fluid"> 
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
    @include('partials.sidebar')
    </aside>

<!-- Main content -->
<div id="content" class="span10">
  <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="{{url('/home/dashboard')}}">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">Export/Import File</a>
        </li>
    </ul>
    <div class="row-fluid sortable">        
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon download"></i><span class="break"></span>Download List</h2>
            </div>
                <br>
                    <div class="box-content">                                 
                       <a href="{{url('admin/download/area')}}" class="quick-button-small span1">
                            <i class="icon-globe"></i>
                            <p>Area</p>                           
                        </a>
                        <a href="{{url('admin/download/outlet')}}" class="quick-button-small span1">
                            <i class="icon-home"></i>
                            <p>Outlet</p>
                        </a>
                        <a href="{{url('admin/download/distributor')}}" class="quick-button-small span1">
                            <i class="icon-truck"></i>
                            <p>Distributor</p>
                        </a>
                        <a href="{{url('admin/download/produk')}}" class="quick-button-small span1">
                            <i class="icon-barcode"></i>
                            <p>Produk</p>
                        </a>
                        <a href="{{url('admin/download/visitPlan')}}" class="quick-button-small span1">
                            <i class="icon-calendar"></i>
                            <p>Visit Plan</p>
                        </a>
                        <a href="{{url('admin/download/visitMonitor')}}" class="quick-button-small span1">
                            <i class="icon-check"></i>
                            <p>Visit Monitor</p>
                        </a>
                        <a href="{{url('admin/download/takeOrder')}}" class="quick-button-small span1">
                            <i class="icon-shopping-cart"></i>
                            <p>Take Order</p>                       
                        </a>
                        <a href="{{url('admin/download/salesForce')}}" class="quick-button-small span1">
                            <i class="icon-group"></i>
                            <p>Sales Force</p>
                        </a>
                        <a href="{{url('admin/download/nonSF')}}" class="quick-button-small span1">
                            <i class="icon-user"></i>
                            <p>Non SF</p>
                        </a>
                        <div class="clearfix"></div>
                    </div><!-- /.box-content -->
                </div><!-- /.box -->
            </div> <!-- end row -->

    <div class="row-fluid">
        
        <div class="box span12">
            <div class="box-header">
                <h2><i class="halflings-icon upload"></i><span class="break"></span>Upload Data</h2>
            </div>
            <div class="box-content">
                <ul class="nav tab-menu nav-tabs" id="myTab">
                    <li><a href="#area">Area</a></li>
                    <li><a href="#produk">Produk</a></li>
                    <li><a href="#distributor">Distributor</a></li>
                    <li><a href="#user">User</a></li>
                    <li class="active"><a href="#outlet">Outlet</a></li>
                </ul>
                 
                <div id="myTabContent" class="tab-content">
                    <div class="tab-pane active" id="outlet">
                        {!! Form::open(array('route' => 'admin.upload.outlet', 'method' => 'POST',  'files' => true)) !!}
                            {!! Form::label('fileUpload','Upload Outlet.xls') !!}
                            <input type="file" name="fileUpload"></input>
                            <button class="btn btn-small btn-info">Upload</button>
                        {!! Form::close() !!}
                    </div>
                    <div class="tab-pane" id="user">
                         {!! Form::open(array('route' => 'admin.upload.user', 'method' => 'POST',  'files' => true)) !!}
                            {!! Form::label('fileUpload','Upload User.xls') !!}
                            <input type="file" name="fileUpload"></input>
                            <button class="btn btn-small btn-info">Upload</button>
                        {!! Form::close() !!}
                    </div>
                    <div class="tab-pane" id="distributor">
                         {!! Form::open(array('route' => 'admin.upload.distributor', 'method' => 'POST',  'files' => true)) !!}
                            {!! Form::label('fileUpload','Upload Distributor.xls') !!}
                            <input type="file" name="fileUpload"></input>
                            <button class="btn btn-small btn-info">Upload</button>
                        {!! Form::close() !!}
                    </div>
                    <div class="tab-pane" id="produk">
                         {!! Form::open(array('route' => 'admin.upload.produk', 'method' => 'POST',  'files' => true)) !!}
                            {!! Form::label('fileUpload','Upload Produk.xls') !!}
                            <input type="file" name="fileUpload"></input>
                            <button class="btn btn-small btn-info">Upload</button>
                        {!! Form::close() !!}
                    </div>
                    <div class="tab-pane" id="area">
                         {!! Form::open(array('route' => 'admin.upload.area', 'method' => 'POST',  'files' => true)) !!}
                            {!! Form::label('fileUpload','Upload Area.xls') !!}
                            <input type="file" name="fileUpload"></input>
                            <button class="btn btn-small btn-info">Upload</button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div><!--/span-->
    </div><!--/row-->   
        </div> <!-- end content -->
   </div> <!-- end row -->
</div><!-- end content -->

<script type="text/javascript">
$(document).ready(function() {
    $('#upload').bind("click",function() 
    { 
        var upVal = $('#fileUp').val(); 
        if(upVal=='') 
        { 
            alert("Empty input file");
        } 
    }); 
});
</script> 

@include('partials.footer') 
    </body>
</html>