<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>GROWTH | Outlet List</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        @include('partials.head')
        <script src="//cdn.ckeditor.com/4.5.2/standard/ckeditor.js"></script>

    </head>
<body>

<div class="container-fluid-full">
    <div class="row-fluid"> 
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="left-side sidebar-offcanvas">
        @include('partials.sidebar')
        </aside>
  
        <!-- Main content -->
        <div id="content" class="span10">
          <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="{{url('/home/dashboard')}}">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="#">Master Data</a>
                    <i class="icon-angle-right"></i>
                    
                </li>
                <li>
                    <a href="#">Outlet</a>
                </li>
            </ul>
        <div class="row-fluid sortable">        
            <div class="box span12">
                <div class="box-header" data-original-title>
                    <h2><i class="halflings-icon user"></i><span class="break"></span>Outlet List</h2>
                    <div class="box-icon">
                      @if(Auth::user()->hasAccess('admin.download.outlet'))        
                       <h2> 
                        <a href="{{ route('admin.download.outlet') }}"> Download <span class="break"></span><i class="icon-download-alt"></i></a>
                       </h2>
                      @endif
                    </div>
                </div>
                    <br>
                    @if(Auth::user()->hasAccess('admin.outlet.create'))   
                        <a href="{!! route('admin.outlet.create') !!}" class="btn btn-primary">Tambah Outlet </a>
                    @endif
                    <div class="box-content">                                 
                        <table class="table table-striped table-bordered bootstrap-datatable datatable">
                            <thead>
                                <tr>
                                    <th>View</th>
                                    <th>Nama Outlet</th>
                                    <th>Alamat </th>
                                    <th>Kodepos </th>
                                    <th>Kota</th>
                                    <th>Area</th>
                                    <th>Tipe</th>
                                    <th>Nama PIC</th>
                                    <th>Tlp PIC</th>
                                    @if(Auth::user()->kd_role != 3) 
                                        <th style="text-align:center;">Action</th>
                                    @else
                                        <th style="text-align:center;"></th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $row)
                                <tr>
                                    <td><a href="{!! route('admin.outlet.show',[$row->kd_outlet]) !!}"  data-toggle="tooltip" data-placement="bottom" title="Lihat Detail Outlet"><i class="icon-zoom-in"></a></td>
                                    <td>{{ $row->nm_outlet }}</td>
                                    <td>{{ $row->almt_outlet }}</td>
                                    <td>{{ $row->kodepos }}</td>
                                    <td>{{ $row->nm_kota }}</td>
                                    <td>{{ $row->kd_area }}</td>
                                    <td>{{ $row->nm_tipe }}</td>
                                     <td>{{ $row->nm_pic }}</td>
                                     <td>{{ $row->tlp_pic }}</td>
                                    @if(Auth::user()->kd_role != 3)   
                                    <td>    
                                        @if(Auth::user()->hasAccess('admin.outlet.edit'))   
                                        <a href="{!! route('admin.outlet.edit',[$row->kd_outlet]) !!}" class="icon-pencil"></a>
                                        @endif
                                        &nbsp;
                                        @if(Auth::user()->hasAccess('admin.outlet.delete'))   
                                        <a href="{!! route('admin.outlet.delete',[$row->kd_outlet]) !!}" onclick="return confirm('Are You Sure')" class="icon-trash"></a> 
                                        @endif        
                                    </td>
                                    @endif
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
        @include('partials.footer')
    </body>
</html>