<div class="box-body">
	<div class="form-group">

		{!! Form::label('kd_visitplan','Kode Visit Plan') !!}
		{!! Form::select('kd_visitplan',$data['visit'], null, ['class' => 'form-control']) !!}

		{!! Form::label('kd_produk','Nama Produk') !!}
		{!! Form::select('kd_produk', $data['produk'], null, array('class' => 'form-control')) !!}

		{!! Form::label('qty_order','Jumlah Order') !!}
		{!! Form::text('qty_order', null, ['class' => 'form-control']) !!}

		{!! Form::label('satuan','Satuan') !!}
		{!! Form::select('satuan', [
					   'Pack' => 'Pack',
					   'Lusin' => 'Lusin',
					   'Kodi' => 'Kodi',
					   'Kardus' => 'Kardus',
					   ], null ) !!}
		
		{!! Form::label('date_order','Tanggal dan Waktu Order') !!}
		{!! Form::text('date_order', null, array('id' => 'datepicker')) !!}

		{!! Form::label('status_order','Status Order') !!}
		{!! Form::select('status_order',[
					   '1' => 'YES',
					   '0' => 'NO',
					   ]) !!}
	</div>
		<div class="form-group">
			{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
		</div>

		<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
		<script>
		  $(function() {
		    $('#datepicker').datepicker({ dateFormat: 'yy-mm-dd' }).val();
		  });
		</script>
</div>