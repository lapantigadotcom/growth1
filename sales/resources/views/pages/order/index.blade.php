<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>GROWTH | Order Page </title>

        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        @include('partials.head')
        <script src="//cdn.ckeditor.com/4.5.2/standard/ckeditor.js"></script>
    </head>
<body>
<div class="container-fluid-full">
    <div class="row-fluid"> 
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="left-side sidebar-offcanvas">
        @include('partials.sidebar')
        </aside>
        <!-- Right side column. Contains the navbar and content of the page -->
        <div id="content" class="span10">
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="{{url('/home/dashboard')}}">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="#">Visit Plan</a>
                    <i class="icon-angle-right"></i>
                </li>
                 <li>
                    <a href="#">Take Order List</a>
                </li>
            </ul>
        <div class="row-fluid sortable">        
            <div class="box span12">
                <div class="box-header" data-original-title>
                    <h2><i class="halflings-icon user"></i><span class="break"></span>Take Order List</h2>
                </div>
                <br>
                    @if(Auth::user()->hasAccess('admin.order.create'))  
                        <a href="{!! route('admin.order.create') !!}" class="btn btn-primary">Tambah Take Order</a>
                    @endif
                    <div class="box-content">                                 
                        <table class="table table-striped table-bordered bootstrap-datatable datatable">
                            <thead>
                                       <tr>               <th style="text-align:center;">Nama SF</th>
                                    <th style="text-align:center;">Kode Order</th>
                                    <th style="text-align:center;">Kode Visit</th>
                                    <th style="text-align:center;">Nama Outlet</th>
                                    <th style="text-align:center;">Kota Outlet</th>
                                    <th style="text-align:center;">Nama Produk</th>
                                    <th style="text-align:center;">Jumlah Order</th>
                                    <th style="text-align:center;">Satuan</th>
                                    <th style="text-align:center;">Date Order</th>
                                    <th style="text-align:center;">Action</th>

                                    <!-- <th style="text-align:center;">Status Order</th>
                                    @if(Auth::user()->kd_role != 3) 
                                        <th style="text-align:center;">Action</th>
                                    @else
                                        <th style="text-align:center;"></th>
                                    @endif -->

                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $row)
                                <tr>                                   <td>{{ $row->nama }}</td>

                                <td>{{ $row->kd_to }}
 
                                       <!-- <a href="{!! route('admin.order.show',[$row->id]) !!}"  data-toggle="tooltip" data-placement="bottom" title="Lihat Detail Order"><i class="icon-zoom-in"></a>
                                    -->  </td>
                                   <td>{{ $row->kd_visitplan }}</td>
                                    <td>{{ $row->nm_outlet }}</td>
                                    <td>{{ $row->nm_kota }}</td>
                                    <td>{{ $row->nm_produk}}</td>
                                    <td>{{ $row->qty_order }}</td>   
                                    <td>{{ $row->satuan }}</td>
                                    <td>{{ date('d-F-Y H:i:s', strtotime($row->date_order)) }}</td>
                                   
                                   <!-- @if($row->status_order == 1)
                                        <td class="center">
                                            <span class="label label-success">Ordered</span>
                                        </td>
                                    @elseif($row->status_order == 0)
                                        <td class="center">
                                            <span class="label label-important">Not Order</span>
                                        </td>
                                    @else
                                    <td class="center">
                                        <span class="label label-warning">Unknown</span>
                                    </td> 
                                    @endif      -->               
                                    <td>      
                                    @if(Auth::user()->hasAccess('admin.order.edit'))                              
                                    <!--    <a href="{!! route('admin.order.edit',[$row->id]) !!}" class="icon-pencil"></a> -->
                                    @endif
                                        &nbsp;
                                    @if(Auth::user()->hasAccess('admin.order.delete'))                            
                                        <a href="{!! route('admin.order.delete',[$row->id]) !!}" onclick="return confirm('Are you sure?')"  class="icon-trash"></a>
                                    @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div><!-- /.row (main row) -->
    </div>
</div>

@include('partials.footer')
    </body>
</html>