@extends('layouts.app')

@section('content')
@section('title', 'Take Order List')
<div id="content" class="span10">
	<ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="{{url('/home/dashboard')}}">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">Take Order</a>  
            <i class="icon-angle-right"></i>             
        </li>    
         <li>
            <a href="#">Take Order List Filter</a>                          
        </li>    
    </ul>

	<div class="row-fluid sortable">
		<div class="box span12">
			<div class="box-header" data-original-title>
				<h2><i class="halflings-icon edit"></i><span class="break"></span>Filter Data Date Order</h2>
			</div>
			    {!! Form::open(array('route' => 'admin.takeOrder.result', 'method' => 'POST' )) !!}
			    	@include('pages.order.formFilter')
			    {!! Form::close() !!}
		</div>
	</div>

    <div class="row-fluid sortable">        
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon shopping-cart"></i><span class="break"></span>Take Order 1 Minggu Terakhir</h2>
            </div>
            <br>
                <div class="box-content">                                 
                    <table class="table table-striped table-bordered bootstrap-datatable datatable">
                         <thead>
                                <tr><th style="text-align:center;">NAMA SF</th>

                                    <th style="text-align:center;">Kode Order</th>
                                    <th style="text-align:center;">Kode Visit</th>
                                    <th style="text-align:center;">Nama Outlet</th>
                                    <th style="text-align:center;">Kota Outlet</th>
                                    <th style="text-align:center;">Area Outlet</th>
                                     <th style="text-align:center;">Nama Produk</th>
                                    <th style="text-align:center;">Jumlah Order</th>
                                    <th style="text-align:center;">Satuan</th>
                                    <th style="text-align:center;">Date Order</th>
                                    <th style="text-align:center;">Action</th>

                                    <!-- <th style="text-align:center;">Status Order</th>
                                    @if(Auth::user()->kd_role != 3) 
                                        <th style="text-align:center;">Action</th>
                                    @else
                                        <th style="text-align:center;"></th>
                                    @endif -->

                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $row) 
                                <tr>
                                                                    <td>{{ $row->nama }}

                                <td>{{ $row->kd_to }}

                                       <!-- <a href="{!! route('admin.order.show',[$row->id]) !!}"  data-toggle="tooltip" data-placement="bottom" title="Lihat Detail Order"><i class="icon-zoom-in"></a>
                                    -->  </td>
                                   <td>{{ $row->kd_visitplan }}</td>
                                    <td>{{ $row->nm_outlet }}</td>
                                    <td>{{ $row->nm_kota }}
                                        <td>{{ $row->nm_area }}
                                     <td>{{ $row->nm_produk}}</td>
                                    <td>{{ $row->qty_order }}</td>   
                                    <td>{{ $row->satuan }}</td>
                                    <td>{{ date('d-F-Y H:i:s', strtotime($row->date_order)) }}</td>
                                   
                                   <!-- @if($row->status_order == 1)
                                        <td class="center">
                                            <span class="label label-success">Ordered</span>
                                        </td>
                                    @elseif($row->status_order == 0)
                                        <td class="center">
                                            <span class="label label-important">Not Order</span>
                                        </td>
                                    @else
                                    <td class="center">
                                        <span class="label label-warning">Unknown</span>
                                    </td> 
                                    @endif      -->               
                                    <td>      
                                    @if(Auth::user()->hasAccess('admin.order.edit'))                              
                                    <!--    <a href="{!! route('admin.order.edit',[$row->id]) !!}" class="icon-pencil"></a> -->
                                    @endif
                                        &nbsp;
                                    @if(Auth::user()->hasAccess('admin.order.delete'))                            
                                        <a href="{!! route('admin.order.delete',[$row->id]) !!}" onclick="return confirm('Are you sure?')"  class="icon-trash"></a>
                                    @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                    </table>
                </div>
            </div>
        </div>
</div>	    

@endsection

@section('custom-head')
    
@endsection

@section('custom-footer')
    
@endsection
