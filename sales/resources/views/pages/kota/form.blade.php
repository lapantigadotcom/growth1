<div class="box-body">
	<div class="form-group">

		{!! Form::label('kd_area', 'Kode Area') !!}

	     <select name="kd_area" id="area">
		    <option>Select Kode Area</option>
		    @foreach ($data['area']as $kodeArea)
		      <option value="{{ $kodeArea->id }}">{{ $kodeArea->kd_area}}</option>
		    @endforeach
	    </select>
	    <br>
	    	{!! Form::label('nm_area','Nama Area : ') !!}
	    	<div id="namaArea" name="namaArea"> </div>
		<br> 


		{!! Form::label('nm_kota','Nama Kota') !!}
		{!! Form::text('nm_kota',null, array('class' => 'form-control')) !!}

	</div>
	<div class="form-group">
		{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
	</div>

<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script>
	$('#area').on('change', function(e) {
        console.log(e);
        var id = e.target.value;
        //ajax
        $.getJSON('{{url('/ajax-area?id=')}}'+id, function (data) {
			//console.log(data);
            $('#namaArea').empty();
          	$.each(data, function(index, areaObj){
            	 $('#namaArea').append('<div>'+areaObj.nm_area+'</div>');
				});
        });
    });
</script>

</div>