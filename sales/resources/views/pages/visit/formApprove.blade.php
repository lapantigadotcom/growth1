<div class="box-body">
	<div class="form-group">
	 <table class="table table-striped table-bordered bootstrap-datatable datatable">
            <thead>
                <tr>         
                    <th style="text-align:center;">Nama Outlet</th>
                    <th style="text-align:center;">Alamat Outlet</th>
                    <th style="text-align:center;">Kota Outlet</th>
                    <th style="text-align:center;">Date Create Visit Plan</th>
                    <th style="text-align:center;">Date Visit Plan</th>
                    <th style="text-align:center;">Status Approval </th>
                    <th style="text-align:center;">Sales Force</th>
                    <th style="text-align:center;">Approval</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $row)
                <tr> 
                    <td>{{ $row->nm_outlet }}</td>
                    <td>{{ $row->almt_outlet }}</td>
                    <td>{{ $row->nm_kota}}</td>
                    <td>{{ date('d-F-Y H:i:s', strtotime($row->date_create_visit))  }}</td>
                    <td>{{ date('d-F-Y H:i:s', strtotime($row->date_visit)) }}</td>
                    @if($row->approve_visit == 1)
                        <td class="center">
                            <span class="label label-success">Approved</span>
                        </td>
                    @elseif($row->approve_visit == 0)
                        <td class="center">
                            <span class="label label-important">Denied</span>
                        </td>
                    @elseif($row->approve_visit == 2)  
                        <td class="center">
                            <span class="label label-warning">Pending</span>
                        </td>
                    @else
                    <td class="center">
                        <span class="label label-warning">Pending</span>
                    </td>
                    @endif                             
                    <td>{{ $row->nama}}</td>                          
                    <td>
                    	<input type="checkbox" name="approve_visit[]" id="approved" value="{{$row->id}}" class="flat-red">
                    </td>

                </tr>
                @endforeach
            </tbody>
        </table>
		<p>
              <input type="button" id="check" value="Check All" />
              <input type="button" id="uncheck" value="UnCheck All" />
        </p> 
	<div class="form-group">
		{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script> 
<script>
	 $(document).ready( function() {
        $('#check').on('click', function () {
            $(':checkbox').attr("checked", true);
        });
        $('#uncheck').on('click', function () {
            $(':checkbox').attr("checked", false);
        });
    });
</script>




