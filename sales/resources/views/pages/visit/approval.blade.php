<div class="box-body">
	<div class="form-group">

		{!! Form::label('kd_outlet','Nama Outlet') !!}
		{!! Form::select('kd_outlet', $outlet, null, ['class' => 'form-control']) !!}
		
		{!! Form::label('date_visit','Tanggal dan Waktu Visit') !!}
		{!! Form::text('date_visit', null, array('id' => 'datepicker')) !!}

		{!! Form::label('approve_visit','Status Approval') !!}
		{!! Form::select('approve_visit', [
				   '' => 'Select Approve Visit',
				   '1' => 'YES',
				   '0' => 'NO'], null, ['class' => 'form-control']
				) !!} 

		
	</div>
	<div class="form-group">
		{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
	</div>

	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	<script>
	  $(function() {
	    $('#datepicker').datepicker({ dateFormat: 'yy-mm-dd' }).val();
	  });
	</script>
</div>