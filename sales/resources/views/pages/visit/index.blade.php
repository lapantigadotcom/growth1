<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>GROWTH | Visit Plan List</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        @include('partials.head')
        <script src="//cdn.ckeditor.com/4.5.2/standard/ckeditor.js"></script>
    </head>
<body>
<div class="container-fluid-full">
    <div class="row-fluid"> 
        <aside class="left-side sidebar-offcanvas">
            @include('partials.sidebar')
        </aside>

        <div id="content" class="span10">
        <ul class="breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="{{url('/home/dashboard')}}">Home</a> 
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a href="#">Visit Plan</a>
            </li>
        </ul>
        <div class="row-fluid sortable">        
            <div class="box span12">
                <div class="box-header" data-original-title>
                    <h2><i class="halflings-icon user"></i><span class="break"></span>Visit Plan List</h2>
                </div>
                <br>
                    <div class="box-content">    
                     <a href="{!! route('admin.visit.editApprove') !!}" class="btn btn-primary">Approval</a>                             
                        <table class="table table-striped table-bordered bootstrap-datatable datatable">
                            <thead>
                                <tr>
                                    <th style="text-align:center;">Sales Force</th>
                                    <th style="text-align:center;">Nama Outlet</th>
                                    <th style="text-align:center;">Alamat Outlet</th>
                                    <th style="text-align:center;">Kota Outlet</th>
                                    <th style="text-align:center;">Date Create Visit Plan</th>
                                    <th style="text-align:center;">Date Visit Plan</th>
                                    <th style="text-align:center;">Status Approval </th>
                                    @if(Auth::user()->kd_role != 3)   
                                    <th style="text-align:center;">Action</th>
                                    @else
                                    <th></th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $row)
                                <tr><td>{{ $row->nama}}</td>
                                    <td>{{ $row->nm_outlet }}</td>
                                    <td>{{ $row->almt_outlet }}</td>
                                    <td>{{ $row->nm_kota}}</td>
                                    <td>{{ date('d-F-Y H:i:s', strtotime($row->date_create_visit))  }}</td>
                                    <td>{{ date('d-F-Y H:i:s', strtotime($row->date_visit)) }}</td>
                                    @if($row->approve_visit == 1)
                                        <td class="center">
                                            <span class="label label-success">Approved</span>
                                        </td>
                                    @elseif($row->approve_visit == 0)
                                        <td class="center">
                                            <span class="label label-important">Denied</span>
                                        </td>
                                    @elseif($row->approve_visit == 2)  
                                        <td class="center">
                                            <span class="label label-warning">Pending</span>
                                        </td>
                                    @else
                                    <td class="center">
                                        <span class="label label-warning">Pending</span>
                                    </td>
                                    @endif                             
                                    <td>
                                        @if(Auth::user()->hasAccess('admin.visit.edit'))                                  
                                        <a href="{!! route('admin.visit.edit',[$row->id]) !!}" class="icon-pencil"></a>
                                        @endif   
                                        &nbsp;         
                                        @if(Auth::user()->hasAccess('admin.visit.delete'))                   
                                        <a href="{!! route('admin.visit.delete',[$row->id]) !!}" onclick="return confirm('Are You Sure?')" class="icon-trash"></a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('partials.footer')
    </body>
</html>