@extends('layouts.app')

@section('content')
@section('title', 'Create Visit Plan')
<div id="content" class="span10">  
  <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="{{url('/home/dashboard')}}">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">Visit Plan</a>
            <i class="icon-angle-right"></i>
            
        </li>
        <li>
            <a href="#">Create Visit</a>
        </li>
    </ul>
  {!! Form::open(array('route' => 'admin.visit.store', 'method' => 'POST')) !!}
  @include('pages.visit.form',array('submit' => 'Simpan'))
  {!! Form::close() !!}
</div>
                    
@endsection

@section('custom-head')
    
@endsection

@section('custom-footer')
    
@endsection