<div class="box-body">
	<div class="form-group">

		{!! Form::label('kd_outlet','Nama Outlet') !!}
		 <select name="kd_outlet" id="outlet">
		    <option>Select Outlet</option>
		    @foreach ($outlet as $outlets)
		      <option value="{{ $outlets->kd_outlet }}">{{ $outlets->nm_outlet}}</option>

		    @endforeach
	    </select>
 
 
		    	    <br>
	    	{!! Form::label('username','Nama Sales Force : ') !!}
	    	<div id="user" name="user"> </div> 	    	<div id="distributor" name="distributor"> </div> 

            <br> 
		
		{!! Form::label('date_visit','Tanggal dan Waktu Visit') !!}
		{!! Form::text('date_visit', '', array('id' => 'datepicker')) !!}

                {!! Form::hidden('approve_visit', '2', array('class' => 'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
	</div>

	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	<script>
	  $(function() {
	    $('#datepicker').datepicker({ dateFormat: 'yy-mm-dd' }).val();
	  });
	</script>
       <script>
	  $('#outlet').on('change', function(e) {
            console.log(e);
            var kd_outlet = e.target.value;
            //ajax
            $.getJSON('{{url('/ajax-call?kd_outlet=')}}'+kd_outlet, function (data) {
				//console.log(data);
                $('#user').empty();
              	$.each(data, function(index, userObj){
                	$('#user').append('<div>'+userObj.nama+'</div>');
 
 				});
            });
        });

	  $('#distributor').on('change', function(e) {
            console.log(e);
            var kd_dist = e.target.value;
            //ajax
            $.getJSON('{{url('/ajax-call?kd_dist=')}}'+kd_dist, function (data) {
				//console.log(data);
                $('#distributor').empty();
              	$.each(data, function(index, userObj){
 	               	$('#distributor').append('<div>'+userObj.nm_dist+'</div>');

 				});
            });
        });
	</script>
</div>