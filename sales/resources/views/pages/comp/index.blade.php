<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>GROWTH | Competitor</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        @include('partials.head')
        <script src="//cdn.ckeditor.com/4.5.2/standard/ckeditor.js"></script>

    </head>
<body>

<div class="container-fluid-full">
    <div class="row-fluid"> 
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="left-side sidebar-offcanvas">
        @include('partials.sidebar')
        </aside>
  
        <!-- Main content -->
        <div id="content" class="span10">
          <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="{{url('/home/dashboard')}}">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="#">Photo Activity</a>
                    <i class="icon-angle-right"></i>
                    
                </li>
                <li>
                    <a href="#">Photo Display</a>
                </li>
            </ul>
        <div class="row-fluid sortable">        
            <div class="box span12">
                <div class="box-header" data-original-title>
                    <h2><i class="halflings-icon user"></i><span class="break"></span>Photo List</h2>
                </div>
                    <br>
                    @if(Auth::user()->hasAccess('admin.competitor.create'))    
                        <a href="{!! route('admin.competitor.create') !!}" class="btn btn-primary">Tambah Photo Competitor </a>
                    @endif
                    @if(Auth::user()->hasAccess('admin.comp.create'))
                        <a href="{!! route('admin.comp.create') !!}" class="btn btn-primary">Tambah Daftar Competitor </a>
                    @endif
                    <div class="box-content">                                 
                        <table class="table table-striped table-bordered bootstrap-datatable datatable">
                            <thead>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>Nama Competitor</th>
                                    <th>Alamat Competitor</th>
                                    <th>Sales Force </th>
                                    <th>Nama Foto </th>
                                    <th>Date Take Foto</th>
                                    <th>Date Upload</th>
                                    <th>Foto</th>
                                    @if(Auth::user()->kd_role != 3) 
                                        <th style="text-align:center;">Action</th>
                                    @else
                                        <th style="text-align:center;"></th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $row)
                                <tr>
                                    <td><a href="#"  data-toggle="tooltip" data-placement="bottom" title="Lihat Detail Photo"><i class="icon-zoom-in"></a></td>
                                    <td>{{ $row->nm_competitor }}</td>
                                     @if($row->almt_comp_activity != '')
                                        <td>{{ $row->almt_comp_activity }}</td>
                                    @else
                                        <td><span class="label label-warning">Unknown</span></td>
                                    @endif

                                    <td>{{ $row->nama }}</td>
                                    <td>{{ $row->nm_photo }}</td>
                                    <td>{{ $row->date_take_photo }}</td>
                                    <td>{{ $row->date_upload_photo }}</td>
                                    @if(File::exists('image_upload/competitor/'.$row->path_photo) and $row->path_photo != '')
                                    <td>
                                        <img src="{!! url('image_upload/competitor/'.$row->path_photo) !!}" class="img-responsive" />
                                    </td>
                                    @endif

                                  <td>         
                                     @if(Auth::user()->hasAccess('admin.competitor.edit'))    
                                    <a href="{!! route('admin.competitor.edit',[$row->id]) !!}" class="icon-pencil"></a>
                                    @endif
                                    &nbsp;
                                     @if(Auth::user()->hasAccess('admin.competitor.delete'))    
                                    <a href="{!! route('admin.competitor.delete',[$row->id]) !!}" onclick="return confirm('Are You Sure')" class="icon-trash"></a> 
                                     @endif         
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div><!-- /.row (main row) -->
    </div>
</div><!-- /.row (main row) -->
        @include('partials.footer')
    </body>
</html>