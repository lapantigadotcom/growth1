<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>GROWTH | Distributor List</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        @include('partials.head')
        <script src="//cdn.ckeditor.com/4.5.2/standard/ckeditor.js"></script>
    </head>
<body>
<div class="container-fluid-full">
    <div class="row-fluid"> 
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="left-side sidebar-offcanvas">
        @include('partials.sidebar')
        </aside>
        <!-- Right side column. Contains the navbar and content of the page -->
        <div id="content" class="span10">
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="{{url('/home/dashboard')}}">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="#">Master Data</a>
                    <i class="icon-angle-right"></i> 
                </li>
                <li>
                    <a href="#">Distributor</a>
                </li>
            </ul>
            <div class="row-fluid sortable">        
                <div class="box span12">
                    <div class="box-header" data-original-title>
                        <h2><i class="halflings-icon user"></i><span class="break"></span>Distributor List</h2>
                    </div>
                    <br>
                    @if(Auth::user()->hasAccess('admin.distributor.create'))  
                        <a href="{!! route('admin.distributor.create') !!}" class="btn btn-primary">Tambah Distributor </a>
                    @endif   
                    <div class="box-content">  
                       <table class="table table-striped table-bordered bootstrap-datatable datatable ">                             
                            <thead>
                                <tr>                           
                                    <th>Kode Distributor</th>
                                    <th>Nama</th>
                                    <th>Alamat</th>
                                    <th>Tipe</th>
                                    <th>Kota</th>
                                    <th>Telepon</th>
                                    @if(Auth::user()->kd_role != 3) 
                                        <th style="text-align:center;">Action</th>
                                    @else
                                        <th style="text-align:center;"></th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $row)
                                <tr>
                                    <td>{{ $row->kd_dist }}</td>
                                    <td>{{ $row->nm_dist }}</td>
                                    <td>{{ $row->almt_dist }}</td>
                                    <td>{{ $row->kd_tipe }}</td>
                                    <td>{{ $row->nm_kota }}</td>
                                    <td>{{ $row->telp_dist }}</td>
                                    <td>         
                                        @if(Auth::user()->hasAccess('admin.distributor.edit'))                            
                                            <a href="{!! route('admin.distributor.edit',[$row->id]) !!}" class="icon-pencil"></a>    
                                        @endif
                                            &nbsp;                    
                                        @if(Auth::user()->hasAccess('admin.distributor.delete'))  
                                            <a href="{!! route('admin.distributor.delete',[$row->id]) !!}" onclick="return confirm('Are You Sure')" class="icon-trash"></a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </div>
</div>
<!-- add new calendar event modal -->
@include('partials.footer')
    </body>
</html>