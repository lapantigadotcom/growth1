@extends('layouts.app')

@section('content')

<div id="content" class="span10">   
      <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="{{url('/home/dashboard')}}">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">Master Data</a>
            <i class="icon-angle-right"></i>   
        </li>
        <li>
            <a href="#">Distributor</a>
        </li>
         <li>
            <a href="#">Edit Distributor</a>
        </li>
    </ul>              
    {!! Form::model($data['content'],array('route' => ['admin.distributor.update',$data['content']->id], 'method' => 'PUT')) !!}
    @include('pages.distributor.form',array('submit' => 'Perbarui'))
    {!! Form::close() !!}
</div>

@endsection


@section('custom-head')
    
@endsection

@section('custom-footer')
    
@endsection