@extends('layouts.app')

@section('content')
<div id="content" class="span10">                  
    {!! Form::model($data['content'],array('route' => ['admin.area.update',$data['content']->id], 'method' => 'PUT')) !!}
        @include('pages.area.form',array('submit' => 'Perbarui'))
    {!! Form::close() !!}
</div>
@endsection

@section('custom-head')
    
@endsection

@section('custom-footer')
    
@endsection