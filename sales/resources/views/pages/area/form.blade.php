<div class="box-body">
	<div class="form-group">
		{!! Form::label('kd_area','Kode Area') !!}
		{!! Form::text('kd_area',null, array('class' => 'form-control')) !!}
		{!! Form::label('nm_area','Nama Area') !!}
		{!! Form::text('nm_area',null, array('class' => 'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
	</div>
</div>