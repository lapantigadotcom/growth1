<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>GROWTH | Tipe Photo</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        @include('partials.head')
        <script src="//cdn.ckeditor.com/4.5.2/standard/ckeditor.js"></script>

    </head>
<body>
<div class="container-fluid-full">
    <div class="row-fluid"> 
<!-- Left side column. contains the logo and sidebar -->
<aside class="left-side sidebar-offcanvas">
@include('partials.sidebar')
</aside>

<div id="content" class="span10">
  <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="{{url('/home/dashboard')}}">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">Configuration</a>
            <i class="icon-angle-right"></i>
            
        </li>
        <li>
            <a href="#">Tipe Photo</a>
        </li>
    </ul>
    <div class="row-fluid sortable">        
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>Tipe Photo List</h2>
            </div>
                <br>
                @if(Auth::user()->hasAccess('admin.tipephoto.create')) 
                    <a href="{!! route('admin.tipephoto.create') !!}" class="btn btn-primary">Tambah Tipe Photo </a>
                @endif
                    <div class="box-content">                                 
                       <table class="table table-striped table-bordered bootstrap-datatable datatable">
                                <thead>
                                    <tr>                
                                        <th>Tipe Photo</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data as $row)
                                    <tr>                 
                                        <td>{{ $row->nama_tipe }}</td>
                                        <td>         
                                            <a href="{!! route('admin.tipephoto.edit',[$row->id]) !!}" class="icon-pencil"></a>
                                            &nbsp;
                                            <a href="{!! route('admin.tipephoto.delete',[$row->id]) !!}" onclick="return confirm('Are You Sure')" class="icon-trash"></a>          
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody> 
                            </table>
                    </div>
                </div>
            </div> 
        </div> 
    </div> 
</div> 


@include('partials.footer')
    </body>
</html>