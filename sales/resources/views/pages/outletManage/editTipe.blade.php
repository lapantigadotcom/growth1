@extends('layouts.app')

@section('content')
@section('title', 'Outlet Management')
<div id="content" class="span10">  
	<ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="{{url('/home/dashboard')}}">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">Outlet Management</a>
            <i class="icon-angle-right"></i> 
        </li>
         <li>
            <a href="#">Edit Tipe Outlet</a>
        </li>
    </ul>

    {!! Form::model($data,array('route' => ['admin.tipe.update',$data->id], 'method' => 'PUT')) !!}
         <div class="form-group">
            {!! Form::label('nm_tipe','Nama Tipe Outlet') !!}
            {!! Form::text('nm_tipe',null, array('class' => 'form-control')) !!}

            <div class="form-group">
                {!! Form::submit('Perbarui', array('class' => 'btn btn-primary')) !!}
            </div>
        </div>
    {!! Form::close() !!}
</div>
@endsection

@section('custom-head')
    
@endsection

@section('custom-footer')
    
@endsection