<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>GROWTH | Report Dashboard</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        @include('partials.head')
        <script src="//cdn.ckeditor.com/4.5.2/standard/ckeditor.js"></script>

    </head>
<body>
<div class="container-fluid-full">
    <div class="row-fluid"> 
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="left-side sidebar-offcanvas">
        @include('partials.sidebar')
        </aside>

        <!-- Right side column. Contains the navbar and content of the page -->
        <div id="content" class="span10">
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="{{url('/home/dashboard')}}">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="#">Report</a>               
                </li>      
            </ul>

                <div class="row-fluid ">       
                      <a  href="{!! route('admin.report.outletVisit') !!}" class="quick-button metro green span3">
                        <i class="icon-truck"></i>
                       <p> <center>Laporan Outlet Visit Per SF</center></p>
                     </a>

                     <a  href="{!! route('admin.report.workingTime') !!}" class="quick-button metro orange span3">
                        <i class="icon-time"></i>
                        <p><center>Laporan Working Time Per SF</center></p>
                     </a>

                     <a  href="{!! route('admin.report.effectiveCall') !!}" class="quick-button metro red span3">
                        <i class="icon-check"></i>
                        <p><center>Laporan Effective Call Sales</center></p>
                     </a>

                    <a  href="{!! route('admin.report.photoActivity') !!}" class="quick-button metro blue span3">
                        <i class="icon-camera"></i>
                        <p><center>Laporan Photo Activity</center></p>
                     </a>

                   
                </div>   <br><br> 
<div class="row-fluid "> 
                  <a  href="{!! route('admin.report.competitorActivity') !!}" class="quick-button metro purple span3">
                        <i class="icon-search"></i>
                        <p><center>Laporan Competitor Activity</center></p>
                     </a>     </div> 

            </div>    
        </div>  
    </div>
</div>
<!-- add new calendar event modal -->
@include('partials.footer') 
    </body>
</html>