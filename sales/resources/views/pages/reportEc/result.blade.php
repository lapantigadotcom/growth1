<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>GROWTH | Result Order Report</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        @include('partials.head')
        <script src="//cdn.ckeditor.com/4.5.2/standard/ckeditor.js"></script>

    </head>
<body>
<div class="container-fluid-full">
    <div class="row-fluid"> 
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="left-side sidebar-offcanvas">
        @include('partials.sidebar')
        </aside>

        <!-- Right side column. Contains the navbar and content of the page -->
        <div id="content" class="span10">
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="{{url('/home/dashboard')}}">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="#">Report</a>  
                    <i class="icon-angle-right"></i>             
                </li>    
                 <li>
                    <a href="#">Visiting Outlet Report</a>                          
                </li>    
            </ul>

            <div class="row-fluid sortable">       
               <div class="box span12">
                <div class="box-header" data-original-title>
                    <h2><i class="halflings-icon user"></i><span class="break"></span>Laporan Effective Call Per Sales Force</h2>
                </div>
                    <div class="box-content">    
                        <div class="span12">
                             @foreach($data['sales'] as $val)
                                <h1>Sales Force : {!! $val->nama !!}</h1>
                             @endforeach
                             @foreach($data['area'] as $val)
                                <div class="priority high"><span>Area : {!! $val->nm_area !!}</span></div>
                             @endforeach  
                            <div class="task high">
                                <div class="span2">
                                    <div class="title">M1</div>
                                    <div>{!! $data['dateFrom1'] !!} - {!! $data['dateTo1'] !!}</div>
                                </div>
                                <div class="span2">
                                    <div class="title">M2</div>
                                    <div>{!! $data['dateFrom2'] !!} - {!! $data['dateTo2'] !!}</div>
                                </div>
                                 <div class="span2">
                                    <div class="title">M3</div>
                                    <div>{!! $data['dateFrom3'] !!} - {!! $data['dateTo3'] !!}</div>
                                </div>
                                 <div class="span2">
                                    <div class="title">M4</div>
                                    <div>{!! $data['dateFrom4'] !!} - {!! $data['dateTo4'] !!}</div>
                                </div>
                                 <div class="span4">
                                    <div class="title">M5</div>
                                    <div>{!! $data['dateFrom5'] !!} - {!! $data['dateTo5'] !!}</div>
                                </div>
                            </div>
                        </div>
                        <div class="span12" style="margin-left:0px">
                            <div class="priority medium"><span>Keterangan</span></div>
                    
                            <div class="task medium">
                                <div class="span3">
                                    <div class="title">Periode</div>
                                    <div>{!! $data['periode'] !!}</div>
                                </div>
                            </div>
                        </div>
                
                    
                        <table class="table table-striped table-bordered bootstrap-datatable">
                            <thead>
                                <tr>               
                                    <th style="text-align:center;" rowspan="2" colspan="1">Kode Outlet</th>
                                    <th style="text-align:center;" rowspan="2" colspan="1">Nama Outlet</th>
                                    <th style="text-align:center;" rowspan="2" colspan="1">Alamat</th>
                                    <th style="text-align:center;" rowspan="2" colspan="1">Kode Pos</th>
                                    <th style="text-align:center;" rowspan="2" colspan="1">Kota</th>
                                    <th style="text-align:center;" rowspan="2" colspan="1">Rank</th>
                                    <th style="text-align:center;" rowspan="2" colspan="1">Tipe</th>
                                    <th style="text-align:center;" rowspan="2" colspan="1">Distributor</th>
                                    <th  style="text-align:center;" colspan="5">Effective Call</th>
                                </tr>
                                <tr>               
                                    <th style="text-align:center;">M1</th>
                                    <th style="text-align:center;">M2</th>
                                    <th style="text-align:center;">M3</th>
                                    <th style="text-align:center;">M4</th>
                                    <th style="text-align:center;">M5</th>
                                </tr>
                            </thead>
                            <tbody>
                                 @foreach($data['result'] as $row)
                                <tr>                         
                                    <td>{{ $row->kode }}</td>
                                    <td>{{ $row->nm_outlet }}</td>
                                    <td>{{ $row->almt_outlet }}</td>
                                    <td>{{ $row->kodepos }}</td>
                                    <td>{{ $row->nm_kota }}</td>
                                    <td>{{ $row->rank_outlet }}</td>
                                    <td>{{ $row->nm_tipe }}</td>
                                    <td>{{ $row->nm_dist }}</td>
                                    <td>{{ $row->T1 }}</td>
                                    <td>{{ $row->T2 }}</td>
                                    <td>{{ $row->T3 }}</td>
                                    <td>{{ $row->T4 }}</td>
                                    <td>{{ $row->T5 }}</td>
                                </tr>
                                @endforeach
                                <tr>
                                    <td colspan="8" style="text-align:center;"> Total</td>
                                    <td >{{ $data['e1'] }}</td>
                                    <td >{{ $data['e2'] }}</td>
                                    <td >{{ $data['e3'] }}</td>
                                    <td >{{ $data['e4'] }}</td>
                                    <td >{{ $data['e5'] }}</td>
                                </tr>
                                <tr>
                                    <td colspan="8" style="text-align:center;"> Grand Total</td>
                                    <td colspan="5" style="text-align:center;font-size: 18pt;">{{ $data['grandTotalEc'] }} </td>
                                </tr>
                            </tbody>
                        </table>
                          
            
                        </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div> 

        </div>  
    </div>
</div>

<!-- add new calendar event modal -->
@include('partials.footer')
    </body>
</html>