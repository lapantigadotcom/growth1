<div id="sidebar-left" class="span2">
    <div class="nav-collapse sidebar-nav">
        <ul class="nav nav-tabs nav-stacked main-menu">
           @foreach(Auth::user()->getEffectiveMenu() as $menu)
                @if(count($menu->_child) > 0)           
                <li>
                    <a class="dropmenu" href="#"><i class="{{ $menu->icon }}"></i> <span>{{ $menu->nm_menu}}</span></a>
                        <ul>
                            @foreach($menu->_child as $child)
                            <li>
                            <a class="submenu" href="{!! $child->route==''?'':route($child->route) !!}"><i class="{{ $child->icon }}"></i> <span>{{ $child->nm_menu}}
                            </span></a>
                            </li>
                            @endforeach
                        </ul>   
                </li>
                @else
                <li>
                    <a href="{{ $menu->route==''?'':route($menu->route) }}">
                        <i class="{{ $menu->icon }}"></i> <span>{{ $menu->nm_menu }}</span>
                    </a>
                </li>
                @endif
            @endforeach
        </ul>
    </div>
</div>   