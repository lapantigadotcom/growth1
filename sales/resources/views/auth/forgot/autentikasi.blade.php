@extends('layouts.auth')
<!-- start: CSS -->
    <link id="bootstrap-style" href="{{url('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{url('css/bootstrap-responsive.min.css')}}" rel="stylesheet">
    <link id="base-style" href="{{url('css/style.css')}}" rel="stylesheet">
    <link id="base-style-responsive" href="{{url('css/style-responsive.css')}}" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
<!-- end: CSS -->
    <style type="text/css">
        body { background:  #091769;}
    </style>    
@section('content')
<div class="container-fluid-full">
    <div class="row-fluid">
        <div class="login-box">
            <h3>Autentikasi Forgot Password</h3>

            {!! Form::open(['route'=>'admin.reset.password', 'method' => 'store', 'class'=>'form-horizontal']) !!}
            <div class="input-prepend" title="Email">
                <span class="add-on"><i class="halflings-icon envelope"></i></span>
                {!! Form::text('email_user', null, array('class' => 'input-large span10', 'placeholder' => 'Email')) !!}
            </div>

            <div class="input-prepend" title="NIK">
                <span class="add-on"> <i class="halflings-icon qrcode"></i></span>
                {!! Form::text('nik',null, array('class' => 'input-large span10', 'placeholder' => 'NIK')) !!}
            </div>

            <div class="input-prepend" title="Telepon">
                <span class="add-on"><i class="halflings-icon bell"></i></span>
                {!! Form::text('telepon', null,array('class' => 'input-large span10', 'placeholder' => 'Telepon')) !!}
            </div>

     
            <div class="button-login">  
                <button type="submit" class="btn btn-success">Send to Email</button>
            </div>
            <div class="clearfix"></div>
        </div>
         <span style="color:#fff">
            <center>&copy; 2016. PT. Hisamitsu Pharma Indonesia. All rights reserved. </center>
        </span>
    </div>
</div>
@endsection