<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Permission;

class Role extends Model
{
    //
    protected $table = 'role';
    protected $guarded = ['kd_role'];
    protected $primaryKey = 'kd_role';
    public $timestamps = false;

    public function users()
    {
    	return $this->hasMany('App\User', 'kd_role', 'kd_role');
    }

    public function permission()
    {
        return $this->belongsToMany('App\Permission', 'permission_role', 'kd_role', 'kd_permission');
    }

    public function adminmenu()
    {
        return $this->belongsToMany('App\AdminMenu', 'admin_menu_role', 'kd_role', 'kd_admin_menu');
    }
 
    //Untuk Delete
    public function permissions()
    {
        return $this->hasMany('App\PermissionRole', 'kd_role', 'kd_role');
    }

    public function adminmenus()
    {
        return $this->hasMany('App\AdminMenuRole',  'kd_role', 'kd_role');
    }
}
