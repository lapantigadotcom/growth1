<?php
/*
    PT. Trikarya Teknologi Indonesia
    Tenggilis raya 127
    Office Complex Apartment Metropolis MKB 206
    Surabaya, Jawa timur, Indonesia
    Phone : +6231-8420384 / +6281235537717
    Code By : Novita Nata Wardanie
*/
namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Input;
use Validator;
use App\PhotoActivity;
use App\Outlet;
use App\Competitor;
use App\User;
use App\TipePhoto;
use File;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AdminController;
use App\Http\Requests\PhotoRequest;
use Illuminate\Http\Request;

class PhotoActivityController extends Controller
{
public function formPhoto()
    {
        if(Auth::user()->kd_area == 100){
           $data = PhotoActivity::select('photo_activity.*',
            'outlet.nm_outlet',
            'tp_photo.nama_tipe',
            'user.nama'
            )
          ->join('outlet','outlet.kd_outlet','=','photo_activity.kd_outlet')
          ->join('user','user.id','=','outlet.kd_user')
          ->leftJoin('tp_photo','tp_photo.id','=','photo_activity.jenis_photo')
          ->where('photo_activity.kd_competitor' , '=', 0)
          ->whereBetween('photo_activity.date_upload_photo',[date('Y-m-d',strtotime("-7 day")),date('Y-m-d',strtotime("+1 day"))])
          ->get();
        }
        else if(Auth::user()->kd_role == 3){
            $id =Auth::user()->id;
            $data= PhotoActivity::select('photo_activity.*',
            'outlet.nm_outlet',
            'tp_photo.nama_tipe',
            'user.nama'
            )
          ->join('outlet','outlet.kd_outlet','=','photo_activity.kd_outlet')
          ->join('user','user.id','=','outlet.kd_user')
          ->leftJoin('tp_photo','tp_photo.id','=','photo_activity.jenis_photo')
          ->where('photo_activity.kd_competitor' , '=', 0)
          ->where('outlet.kd_user','=',$id)
          ->whereBetween('photo_activity.date_upload_photo',[date('Y-m-d',strtotime("-7 day")),date('Y-m-d',strtotime("+1 day"))])
          ->get();
        }
        else {
           $area =Auth::user()->kd_area;
           $data= PhotoActivity::select('photo_activity.*',
            'outlet.nm_outlet',
            'tp_photo.nama_tipe',
            'user.nama'
            )
          ->join('outlet','outlet.kd_outlet','=','photo_activity.kd_outlet')
          ->join('user','user.id','=','outlet.kd_user')
          ->leftJoin('tp_photo','tp_photo.id','=','photo_activity.jenis_photo')
          ->where('photo_activity.kd_competitor' , '=', 0)
          ->where('outlet.kd_area','=',$area)
          ->whereBetween('photo_activity.date_upload_photo',[date('Y-m-d',strtotime("-7 day")),date('Y-m-d',strtotime("+1 day"))])
          ->get();
        }
        return view('pages.photo.photo')->with('data',$data);
    }

    public function index(PhotoRequest $request)
    {
      $data['dateFrom'] = $request->input('dateFrom');
      $data['dateTo'] = $request->input('dateTo');

      if(Auth::user()->kd_area == 100){
        $data = PhotoActivity::select('photo_activity.*',
            'outlet.nm_outlet',
            'tp_photo.nama_tipe',
            'user.nama'
          )
        ->join('outlet','outlet.kd_outlet','=','photo_activity.kd_outlet')
        ->join('user','user.id','=','outlet.kd_user')
        ->leftJoin('tp_photo','tp_photo.id','=','photo_activity.jenis_photo')
        ->where('photo_activity.kd_competitor' , '=', 0)
        ->whereBetween('photo_activity.date_upload_photo',[$data['dateFrom'],$data['dateTo']])
        ->groupBy('photo_activity.id')
        ->get();
      }
        else if(Auth::user()->kd_role == 3){
          $id =Auth::user()->id;
          $data = PhotoActivity::select('photo_activity.*',
          'outlet.nm_outlet',
          'tp_photo.nama_tipe',
          'user.nama'
          )
        ->join('outlet','outlet.kd_outlet','=','photo_activity.kd_outlet')
        ->join('user','user.id','=','outlet.kd_user')
        ->leftJoin('tp_photo','tp_photo.id','=','photo_activity.jenis_photo')
        ->where('photo_activity.kd_competitor' , '=', 0)
        ->where('outlet.kd_user','=',$id)
         ->whereBetween('photo_activity.date_upload_photo',[$data['dateFrom'],$data['dateTo']])
        ->groupBy('photo_activity.id')
        ->get();
      }
      else
       {
         $area =Auth::user()->kd_area;
         $data= PhotoActivity::select('photo_activity.*',
          'outlet.nm_outlet',
          'tp_photo.nama_tipe',
          'user.nama'
          )
        ->join('outlet','outlet.kd_outlet','=','photo_activity.kd_outlet')
        ->join('user','user.id','=','outlet.kd_user')
        ->leftJoin('tp_photo','tp_photo.id','=','photo_activity.jenis_photo')
        ->where('photo_activity.kd_competitor' , '=', 0)
        ->where('outlet.kd_area','=',$area)
        ->whereBetween('photo_activity.date_upload_photo',[$data['dateFrom'],$data['dateTo']])
        ->groupBy('photo_activity.id')
        ->get();
       }
        $jenis = TipePhoto::all();
        return view('pages.photo.index')->with('data',$data)->with('jenis',$jenis);
    }


    public function create()
    {
        $role = Auth::user()->kd_role;
        $area = Auth::user()->kd_area;
        $id = Auth::user()->id;
        if($area == 100){
          $outlets = [''=>''] + Outlet::lists('nm_outlet', 'kd_outlet')->toArray();
        }
        else if ($role == 3) {
          $outlets = [''=>''] + Outlet::where('kd_user','=',$id)
                    ->lists('nm_outlet', 'kd_outlet')
                    ->toArray();
        }
        else {
          $outlets = [''=>''] + Outlet::where('kd_area','=',$area)
                    ->lists('nm_outlet', 'kd_outlet')
                    ->toArray();
        }
        $competitors = [''=>''] + Competitor::lists('nm_competitor', 'id')->toArray();
        $users = [''=>''] + User::where('kd_role','=','3')->lists('nama', 'id')->toArray();
        $tipe = [''=>''] + TipePhoto::lists('nama_tipe', 'id')->toArray();
        return view('pages.photo.create')->with('outlets',$outlets)->with('competitors',$competitors)->with('users',$users)->with('tipe',$tipe);
    }

    public function store(PhotoRequest $request)
    {
        $rules = array(
            'nm_photo'         => 'required',    
            'path_photo'	=> 'required', 
        );

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {

            $messages = $validator->messages();

            $outlets = [''=>''] + Outlet::lists('nm_dist', 'id')->toArray();
            $competitors = [''=>''] + Competitor::lists('kd_area', 'id')->toArray();
            $users = [''=>''] + User::where('kd_role','=','3')->lists('nama', 'id')->toArray();

            return view('pages.photo.create')->with('outlets',$outlets)
            ->with('competitors',$competitors)
            ->with('users',$users)
            ->withErrors($validator);

        } else {
            $photoAct = new PhotoActivity;

            $file = $request->file('path_photo');
            $destinationPath = 'image_upload/outlet_photoact';
            $filename = str_random(6).'_'.$file->getClientOriginalName();
            $file->move($destinationPath, $filename);

            $photoAct->kd_outlet = $request->input('kd_outlet');
            $photoAct->nm_photo = $request->input('nm_photo');
            $photoAct->jenis_photo = $request->input('jenis_photo');
            $photoAct->date_take_photo = $request->input('date_take_photo');
            $photoAct->date_upload_photo = $request->input('date_upload_photo');
            $photoAct->keterangan = $request->input('keterangan');
            $photoAct->path_photo = $filename;
            $photoAct->save();        
            $log = new AdminController;
            $log->getLogHistory('Make New Photo Display');
           return redirect()->route('admin.photoAct.form');
        } 
    }

    public function edit($id)
    {
        $data['content'] = PhotoActivity::find($id);
        
        $role = Auth::user()->kd_role;
        $area = Auth::user()->kd_area;
        $id = Auth::user()->id;
        if($area == 100){
          $outlets = [''=>''] + Outlet::lists('nm_outlet', 'kd_outlet')->toArray();
        }
        else if ($role == 3) {
          $outlets = [''=>''] + Outlet::where('kd_user','=',$id)
                    ->lists('nm_outlet', 'kd_outlet')
                    ->toArray();
        }
        else {
          $outlets = [''=>''] + Outlet::where('kd_area','=',$area)
                    ->lists('nm_outlet', 'kd_outlet')
                    ->toArray();
        }

        $competitors = [''=>''] + Competitor::lists('nm_competitor', 'id')->toArray();
        $users = [''=>''] + User::where('kd_role','=','3')->lists('nama', 'id')->toArray();
        $tipe = [''=>''] + TipePhoto::lists('nama_tipe', 'id')->toArray();
        return view('pages.photo.edit')->with('data',$data)
        ->with('outlets',$outlets)
        ->with('competitors',$competitors)
        ->with('users',$users)
        ->with('tipe',$tipe);
    }

    public function update(PhotoRequest $request, $id)
    {
      $photoAct = PhotoActivity::find($id); 
        
        if(Input::hasFile('path_photo')){
            File::delete('image_upload/outlet_photoact/'.$photoAct->path_photo); 
            $file = $request->file('path_photo');
            $destinationPath = 'image_upload/outlet_photoact';
            $filename = str_random(6).'_'.$file->getClientOriginalName();
            $file->move($destinationPath, $filename);

            $photoAct->kd_outlet = $request->input('kd_outlet');
            $photoAct->nm_photo = $request->input('nm_photo');
            $photoAct->jenis_photo = $request->input('jenis_photo');
            $photoAct->date_take_photo = $request->input('date_take_photo');
            $photoAct->date_upload_photo = $request->input('date_upload_photo');
            $photoAct->keterangan = $request->input('keterangan');
            $photoAct->path_photo = $filename;
            $photoAct->save();        
            $log = new AdminController;
            $log->getLogHistory('Update Photo Display with ID'.$id);
            return redirect()->route('admin.photoAct.form');
        }
        
        else
        {
            $photoAct->update($request->all());
            $log = new AdminController;
            $log->getLogHistory('Update Photo Display with ID'.$id);
            return redirect()->route('admin.photoAct.form');
        }
    }

    public function destroy($id)
    {
        $data = PhotoActivity::find($id);
        $data->delete();
        $log = new AdminController;
       	$log->getLogHistory('Delete Photo Display with ID'.$id);
        return redirect()->route('admin.photoAct.form');       
    }

    
}
