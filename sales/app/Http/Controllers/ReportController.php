<?php
/*
    PT. Trikarya Teknologi Indonesia
    Tenggilis raya 127
    Office Complex Apartment Metropolis MKB 206
    Surabaya, Jawa timur, Indonesia
    Phone : +6231-8420384 / +6281235537717
    Code By : Novita Nata Wardanie
*/
namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Validator;
use Input;
use Excel;
use App\User;
use App\Area;
use App\Role;
use App\Outlet;
use App\VisitPlan;
use App\TakeOrder;
use App\PhotoActivity;
use App\Competitor;

use App\TipePhoto;
use App\Http\Controllers\Controller;
use App\Http\Requests\ReportRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getReport()
    {
        return view('pages.report.index');
    }

    public function outletVisit()
    {
        $role = Auth::user()->kd_role;
        $area = Auth::user()->kd_area;
        if($area==100)
        {
           $areas = [''=>''] + Area::lists('kd_area', 'id')->toArray();
        } 
        else
        {
            $areas = [''=>''] + Area::where('id','=',$area)->lists('kd_area', 'id')->toArray();
        }
        return view('pages.report.outletVisit')->with('areas',$areas);
    }

    public function resultReport(ReportRequest $request)
    {
        if(Input::get('calc')) {

        $rules = array(
            'targetVisit'      => 'required',    
            'targetEc'         => 'required',                        
        );
        $validator = Validator::make(Input::all(), $rules);

        // check if the validator failed -----------------------
        if ($validator->fails()) {

            // get the error messages from the validator
            $messages = $validator->messages();

            $areas = [''=>''] + Area::lists('kd_area', 'id')->toArray();
            $users = [''=>''] + User::where('kd_role','=','3')->lists('nama', 'id')->toArray();
            return view('pages.report.outletVisit')->with('areas',$areas)->with('users',$users)->withErrors($validator);

        } else {
            $page = 'result';
            $data['kd_area'] = $request->input('kd_area');
            $data['kd_sales'] = $request->input('sales');
            $data['periode'] = $request->input('periode');
            $data['targetVisiting'] = $request->input('targetVisit');
            $data['targetEc'] = $request->input('targetEc');
            $data['sales'] = User::select('nama')->where('id','=', $data['kd_sales'])->groupBy('id')->get();
            $data['area']= Area::select('nm_area')->where('id','=', $data['kd_area'])->get();

            $data['dateFrom1']= $request->input('dateFrom');
            $data['dateTo1'] = $request->input('dateTo');
            $data['dateFrom2'] = $request->input('dateFrom2');
            $data['dateTo2'] = $request->input('dateTo2');
            $data['dateFrom3'] = $request->input('dateFrom3');
            $data['dateTo3'] = $request->input('dateTo3');
            $data['dateFrom4'] = $request->input('dateFrom4');
            $data['dateTo4'] = $request->input('dateTo4');
            $data['dateFrom5'] = $request->input('dateFrom5');
            $data['dateTo5'] = $request->input('dateTo5');

             $data['result'] = DB::select(DB::raw("SELECT user.nama, outlet.*, area.kd_area,
                kota.nm_kota,
                distributor.nm_dist,tipe.nm_tipe,
                SUM(visit_plan.date_visiting BETWEEN :dateFrom1 AND :dateTo1 AND visit_plan.status_visit = '1') AS `M1`,
                SUM(visit_plan.date_visiting BETWEEN :dateFrom2 AND :dateTo2 AND visit_plan.status_visit = '1') AS `M2`,
                SUM(visit_plan.date_visiting BETWEEN :dateFrom3 AND :dateTo3 AND visit_plan.status_visit = '1') AS `M3`,
                SUM(visit_plan.date_visiting BETWEEN :dateFrom4 AND :dateTo4 AND visit_plan.status_visit = '1') AS `M4`,
                SUM(visit_plan.date_visiting BETWEEN :dateFrom5 AND :dateTo5 AND visit_plan.status_visit = '1') AS `M5`,
                SUM(take_order.date_order BETWEEN :dateFrom6 AND :dateTo6 AND take_order.status_order = 1) AS `T1`,
                SUM(take_order.date_order BETWEEN :dateFrom7 AND :dateTo7 AND take_order.status_order = 1) AS `T2`,
                SUM(take_order.date_order BETWEEN :dateFrom8 AND :dateTo8 AND take_order.status_order = 1) AS `T3`,
                SUM(take_order.date_order BETWEEN :dateFrom9 AND :dateTo9 AND take_order.status_order = 1) AS `T4`,
                SUM(take_order.date_order BETWEEN :dateFrom0 AND :dateTo0 AND take_order.status_order = 1) AS `T5`
                FROM outlet LEFT JOIN distributor ON (outlet.kd_dist = distributor.id), user, area, kota, tipe, visit_plan
                LEFT JOIN take_order ON (visit_plan.id = take_order.kd_visitplan)
                WHERE outlet.kd_user=user.id 
                AND outlet.kd_area = area.id 
                AND outlet.kd_kota = kota.id
                AND outlet.kd_outlet=visit_plan.kd_outlet
                AND outlet.kd_tipe = tipe.id
                AND visit_plan.status_visit = '1'
                AND user.id = :kodeSales
                AND outlet.kd_area = :kodeArea
                GROUP BY outlet.kd_outlet"), 
                array('kodeSales' => $data['kd_sales'], 'kodeArea' =>$data['kd_area'], 'dateFrom1' => $data['dateFrom1'], 'dateTo1' => $data['dateTo1'], 'dateFrom2' => $data['dateFrom2'], 'dateTo2' => $data['dateTo2'], 'dateFrom3' => $data['dateFrom3'], 'dateTo3' => $data['dateTo3'], 'dateFrom4' => $data['dateFrom4'], 'dateTo4' => $data['dateTo4'], 'dateFrom5' => $data['dateFrom5'], 'dateTo5' => $data['dateTo5'],'dateFrom6' => $data['dateFrom1'], 'dateTo6' => $data['dateTo1'], 'dateFrom7' => $data['dateFrom2'], 'dateTo7' => $data['dateTo2'],'dateFrom8' => $data['dateFrom3'], 'dateTo8' => $data['dateTo3'], 'dateFrom9' => $data['dateFrom4'], 'dateTo9' => $data['dateTo4'], 'dateFrom0' => $data['dateFrom5'], 'dateTo0' => $data['dateTo5'],));
           

            $data['t1'] =  0; $data['t2'] =  0; $data['t3'] =  0; $data['t4'] =  0; $data['t5'] =  0;
            $data['e1'] =  0; $data['e2'] =  0; $data['e3'] =  0; $data['e4'] =  0; $data['e5'] =  0;
            foreach ($data['result'] as $value) {
                $data['t1'] += $value->M1;
                $data['t2'] += $value->M2;
                $data['t3'] += $value->M3;
                $data['t4'] += $value->M4;
                $data['t5'] += $value->M5;
                $data['e1'] += $value->T1;
                $data['e2'] += $value->T2;
                $data['e3'] += $value->T3;
                $data['e4'] += $value->T4;
                $data['e5'] += $value->T5;
            }
        
            //Nilai Grand Total Visit
            $data['grandTotalVisit'] = $data['t1']+$data['t2']+$data['t3']+$data['t4']+$data['t5'];
            //Nilai Grand Total Ec
            $data['grandTotalEc'] = $data['e1']+$data['e2']+$data['e3']+$data['e4']+$data['e5'];
            //Prosentase Visiting
            $data['prosentaseVisit'] = number_format($data['grandTotalVisit']/$data['targetVisiting'] * 100,2,',','');
            //Prosentase Effective Call
            $data['prosentaseEc'] = number_format($data['grandTotalEc']/$data['targetEc'] * 100,2,',','');

            return view('pages.report.'.$page)->with('data',$data);
        }
        } elseif(Input::get('down')) {
            $this->downloadOutletVisit($request);
        }
    }
    public function downloadOutletVisit(ReportRequest $request)
    {
        $rules = array(
            'targetVisit'      => 'required',    
            'targetEc'         => 'required',                        
        );

        // do the validation ----------------------------------
        // validate against the inputs from our form
        $validator = Validator::make(Input::all(), $rules);

        // check if the validator failed -----------------------
        if ($validator->fails()) {

            // get the error messages from the validator
            $messages = $validator->messages();

            $areas = [''=>''] + Area::lists('kd_area', 'id')->toArray();
            $users = [''=>''] + User::where('kd_role','=','3')->lists('nama', 'id')->toArray();
            return view('pages.report.outletVisit')->with('areas',$areas)->with('users',$users)->withErrors($validator);

        } else {
            $page = 'result';
            $data['kd_area'] = $request->input('kd_area');
            $data['kd_sales'] = $request->input('sales');
            $data['periode'] = $request->input('periode');
            $data['targetVisiting'] = $request->input('targetVisit');
            $data['targetEc'] = $request->input('targetEc');
            $data['sales'] = User::select('nama')->where('id','=', $data['kd_sales'])->groupBy('id')->get();
            $data['area']= Area::select('nm_area')->where('id','=', $data['kd_area'])->get();

            $data['dateFrom1']= $request->input('dateFrom');
            $data['dateTo1'] = $request->input('dateTo');
            $data['dateFrom2'] = $request->input('dateFrom2');
            $data['dateTo2'] = $request->input('dateTo2');
            $data['dateFrom3'] = $request->input('dateFrom3');
            $data['dateTo3'] = $request->input('dateTo3');
            $data['dateFrom4'] = $request->input('dateFrom4');
            $data['dateTo4'] = $request->input('dateTo4');
            $data['dateFrom5'] = $request->input('dateFrom5');
            $data['dateTo5'] = $request->input('dateTo5');

            $data['result'] = DB::select(DB::raw("SELECT user.nama, outlet.*, area.kd_area,
                kota.nm_kota,
                distributor.nm_dist,tipe.nm_tipe,
                SUM(visit_plan.date_visiting BETWEEN :dateFrom1 AND :dateTo1) AS `M1`,
                SUM(visit_plan.date_visiting BETWEEN :dateFrom2 AND :dateTo2) AS `M2`,
                SUM(visit_plan.date_visiting BETWEEN :dateFrom3 AND :dateTo3) AS `M3`,
                SUM(visit_plan.date_visiting BETWEEN :dateFrom4 AND :dateTo4) AS `M4`,
                SUM(visit_plan.date_visiting BETWEEN :dateFrom5 AND :dateTo5) AS `M5`,
                SUM(take_order.date_order BETWEEN :dateFrom6 AND :dateTo6 AND take_order.status_order = 1) AS `T1`,
                SUM(take_order.date_order BETWEEN :dateFrom7 AND :dateTo7 AND take_order.status_order = 1) AS `T2`,
                SUM(take_order.date_order BETWEEN :dateFrom8 AND :dateTo8 AND take_order.status_order = 1) AS `T3`,
                SUM(take_order.date_order BETWEEN :dateFrom9 AND :dateTo9 AND take_order.status_order = 1) AS `T4`,
                SUM(take_order.date_order BETWEEN :dateFrom0 AND :dateTo0 AND take_order.status_order = 1) AS `T5`
                FROM outlet   LEFT JOIN distributor ON (outlet.kd_dist = distributor.id), user, area, kota, tipe, visit_plan
                LEFT JOIN take_order ON (visit_plan.id = take_order.kd_visitplan)
                WHERE outlet.kd_user=user.id AND outlet.kd_area = area.id AND outlet.kd_kota = kota.id
                AND outlet.kd_outlet=visit_plan.kd_outlet AND outlet.kd_tipe = tipe.id
                AND visit_plan.status_visit = '1' 
                AND user.id = :kodeSales
                AND outlet.kd_area = :kodeArea
                GROUP BY outlet.kd_outlet"), 
                array('kodeSales' => $data['kd_sales'], 'kodeArea' =>$data['kd_area'], 'dateFrom1' => $data['dateFrom1'], 'dateTo1' => $data['dateTo1'], 'dateFrom2' => $data['dateFrom2'], 'dateTo2' => $data['dateTo2'], 'dateFrom3' => $data['dateFrom3'], 'dateTo3' => $data['dateTo3'], 'dateFrom4' => $data['dateFrom4'], 'dateTo4' => $data['dateTo4'], 'dateFrom5' => $data['dateFrom5'], 'dateTo5' => $data['dateTo5'],'dateFrom6' => $data['dateFrom1'], 'dateTo6' => $data['dateTo1'], 'dateFrom7' => $data['dateFrom2'], 'dateTo7' => $data['dateTo2'],'dateFrom8' => $data['dateFrom3'], 'dateTo8' => $data['dateTo3'], 'dateFrom9' => $data['dateFrom4'], 'dateTo9' => $data['dateTo4'], 'dateFrom0' => $data['dateFrom5'], 'dateTo0' => $data['dateTo5'],));
           

            $data['t1'] =  0; $data['t2'] =  0; $data['t3'] =  0; $data['t4'] =  0; $data['t5'] =  0;
            $data['e1'] =  0; $data['e2'] =  0; $data['e3'] =  0; $data['e4'] =  0; $data['e5'] =  0;
            foreach ($data['result'] as $value) {
                $data['t1'] += $value->M1;
                $data['t2'] += $value->M2;
                $data['t3'] += $value->M3;
                $data['t4'] += $value->M4;
                $data['t5'] += $value->M5;
                $data['e1'] += $value->T1;
                $data['e2'] += $value->T2;
                $data['e3'] += $value->T3;
                $data['e4'] += $value->T4;
                $data['e5'] += $value->T5;
            }
        
            //Nilai Grand Total Visit
            $data['grandTotalVisit'] = $data['t1']+$data['t2']+$data['t3']+$data['t4']+$data['t5'];
            //Nilai Grand Total Ec
            $data['grandTotalEc'] = $data['e1']+$data['e2']+$data['e3']+$data['e4']+$data['e5'];
            //Prosentase Visiting
            $data['prosentaseVisit'] = number_format($data['grandTotalVisit']/$data['targetVisiting'] * 100,2,',','');
            //Prosentase Effective Call
            $data['prosentaseEc'] = number_format($data['grandTotalEc']/$data['targetEc'] * 100,2,',','');
            ob_end_clean();
            ob_start();
            date_default_timezone_set("Asia/Jakarta");
            $d= strtotime("now");
            $time =date("d-m-Y H:i:s", $d);
            foreach($data['sales'] as $val){
                $namaSales = $val->nama;
            }
            $filename = $time."_".$namaSales."_Outlet_Visit";
            Excel::create($filename, function($excel) use($data){
            $excel->sheet('Sheet 1', function($sheet) use($data) {
                    $sheet->loadView('pages.report.resultDownload')->with('data',$data);
                    $sheet->fromArray($data);
                    $sheet->setOrientation('landscape');
                });
            })->download('xls');
        }
    }

    public function workingTime()
    {
        $role = Auth::user()->kd_role;
        $area = Auth::user()->kd_area;
        if($area==100)
        {
           $areas = [''=>''] + Area::lists('kd_area', 'id')->toArray();
        } 
        else
        {
           $areas = [''=>''] + Area::where('id','=',$area)->lists('kd_area', 'id')->toArray();
        }
        return view('pages.workTime.workingTime')->with('areas',$areas);
    }

      public function resultWorkTime(ReportRequest $request)
    {
        if(Input::get('show')) {

            $page = 'result';
            $data['kd_area'] = $request->input('kd_area');
            $data['kd_sales'] = $request->input('sales');
            $data['periode'] = $request->input('periode');
            $periode = substr($data['periode'], 1,2);
            $tahun = substr($data['periode'], 3,4);
            $data['sales'] = User::select('nama','nik')->where('id','=', $data['kd_sales'])->groupBy('id')->get();
            $data['area']= Area::select('nm_area')->where('id','=', $data['kd_area'])->get();

            $data['workingTime'] = DB::select(DB::raw("SELECT MIN(visit_plan.date_visiting) as `CheckIn`, MAX(visit_plan.date_visiting) as `CheckOut`,
            HOUR(TIMEDIFF( MAX(visit_plan.date_visiting),MIN(visit_plan.date_visiting))) AS `WorkingHours`
            FROM visit_plan, user, outlet 
            WHERE visit_plan.kd_outlet=outlet.kd_outlet
            AND MONTH(visit_plan.date_visiting) = :monthPeriod
            AND YEAR(visit_plan.date_visiting) = :yearPeriod
            AND user.id = outlet.kd_user 
            AND user.id= :id 
            GROUP BY DATE(visit_plan.date_visiting)"), 
            array('id' => $data['kd_sales'], 'monthPeriod' => $periode, 'yearPeriod' => $tahun, ));

            $data['totalWorkTimeMonth'] = 0;
            foreach ($data['workingTime'] as $value) {
                $data['totalWorkTimeMonth'] += $value->WorkingHours;
            }

            return view('pages.workTime.'.$page)->with('data',$data);
        }
        elseif(Input::get('down')) {
            $this->downloadWorkTime($request);
        }

    }

    public function downloadWorkTime(ReportRequest $request)
    {
        $data['kd_area'] = $request->input('kd_area');
        $data['kd_sales'] = $request->input('sales');
        $data['periode'] = $request->input('periode');
        $periode = substr($data['periode'], 1,2);
        $tahun = substr($data['periode'], 3,4);
        $data['sales'] = User::select('nama','nik')->where('id','=', $data['kd_sales'])->groupBy('id')->get();
        $data['area']= Area::select('nm_area')->where('id','=', $data['kd_area'])->get();

        $data['workingTime'] = DB::select(DB::raw("SELECT MIN(visit_plan.date_visiting) as `CheckIn`, MAX(visit_plan.date_visiting) as `CheckOut`,
        HOUR(TIMEDIFF( MAX(visit_plan.date_visiting),MIN(visit_plan.date_visiting))) AS `WorkingHours`
        FROM visit_plan, user, outlet 
        WHERE visit_plan.kd_outlet=outlet.kd_outlet
        AND MONTH(visit_plan.date_visiting) = :monthPeriod
        AND YEAR(visit_plan.date_visiting) = :yearPeriod
        AND user.id = outlet.kd_user 
        AND user.id= :id 
        GROUP BY DATE(visit_plan.date_visiting)"), 
        array('id' => $data['kd_sales'], 'monthPeriod' => $periode, 'yearPeriod' => $tahun, ));

        $data['totalWorkTimeMonth'] = 0;
        foreach ($data['workingTime'] as $value) {
            $data['totalWorkTimeMonth'] += $value->WorkingHours;
        }
        ob_end_clean();
        ob_start();
        date_default_timezone_set("Asia/Jakarta");
        $d= strtotime("now");
        $time =date("d-m-Y H:i:s", $d);
        foreach($data['sales'] as $val){
            $namaSales = $val->nama;
        }
        $filename = $time."_".$namaSales."_Working_Time";
        Excel::create($filename, function($excel) use($data){
        $excel->sheet('Sheet 1', function($sheet) use($data) {
                $sheet->loadView('pages.workTime.resultDownload')->with('data',$data);
                $sheet->fromArray($data);
                $sheet->setOrientation('landscape');
            });
        })->download('xls');
    }
    public function effectiveCall()
    {
        $role = Auth::user()->kd_role;
        $area = Auth::user()->kd_area;
        if($area==100)
        {
           $areas = [''=>''] + Area::lists('kd_area', 'id')->toArray();
        } 
        else
        {
           $areas = [''=>''] + Area::where('id','=',$area)->lists('kd_area', 'id')->toArray();
        }
        return view('pages.reportEc.effectiveCall')->with('areas',$areas);
    }

    public function resultEffectiveCall(ReportRequest $request)
    {
        $page = 'result';
        $data['kd_area'] = $request->input('kd_area');
        $data['kd_sales'] = $request->input('sales');
        $data['periode'] = $request->input('periode');
        $data['targetVisiting'] = $request->input('targetVisit');
        $data['targetEc'] = $request->input('targetEc');
        $data['sales'] = User::select('nama')->where('id','=', $data['kd_sales'])->groupBy('id')->get();
        $data['area']= Area::select('nm_area')->where('id','=', $data['kd_area'])->get();

        $data['dateFrom1']= $request->input('dateFrom');
        $data['dateTo1'] = $request->input('dateTo');
        $data['dateFrom2'] = $request->input('dateFrom2');
        $data['dateTo2'] = $request->input('dateTo2');
        $data['dateFrom3'] = $request->input('dateFrom3');
        $data['dateTo3'] = $request->input('dateTo3');
        $data['dateFrom4'] = $request->input('dateFrom4');
        $data['dateTo4'] = $request->input('dateTo4');
        $data['dateFrom5'] = $request->input('dateFrom5');
        $data['dateTo5'] = $request->input('dateTo5');

        $data['result'] = DB::select(DB::raw("SELECT user.nama, outlet.*, area.kd_area,
            kota.nm_kota,
            distributor.nm_dist,tipe.nm_tipe,
            SUM(take_order.date_order BETWEEN :dateFrom1 AND :dateTo1) AS `T1`,
            SUM(take_order.date_order BETWEEN :dateFrom2 AND :dateTo2) AS `T2`,
            SUM(take_order.date_order BETWEEN :dateFrom3 AND :dateTo3) AS `T3`,
            SUM(take_order.date_order BETWEEN :dateFrom4 AND :dateTo4) AS `T4`,
            SUM(take_order.date_order BETWEEN :dateFrom5 AND :dateTo5) AS `T5`
            FROM outlet   LEFT JOIN distributor ON (outlet.kd_dist = distributor.id), user, area, kota, tipe, take_order
            LEFT JOIN visit_plan ON (visit_plan.id = take_order.kd_visitplan)
            WHERE outlet.kd_user=user.id AND outlet.kd_area = area.id AND outlet.kd_kota = kota.id
            AND outlet.kd_outlet=visit_plan.kd_outlet AND outlet.kd_tipe = tipe.id
            AND take_order.status_order = '1' 
            AND user.id = :kodeSales
            AND outlet.kd_area = :kodeArea
            GROUP BY outlet.kd_outlet"), 
            array('kodeSales' => $data['kd_sales'], 'kodeArea' =>$data['kd_area'], 'dateFrom1' => $data['dateFrom1'], 'dateTo1' => $data['dateTo1'], 'dateFrom2' => $data['dateFrom2'], 'dateTo2' => $data['dateTo2'], 'dateFrom3' => $data['dateFrom3'], 'dateTo3' => $data['dateTo3'], 'dateFrom4' => $data['dateFrom4'], 'dateTo4' => $data['dateTo4'], 'dateFrom5' => $data['dateFrom5'], 'dateTo5' => $data['dateTo5'],));

         $data['e1'] =  0; $data['e2'] =  0; $data['e3'] =  0; $data['e4'] =  0; $data['e5'] =  0;
            foreach ($data['result'] as $value) {
                $data['e1'] += $value->T1;
                $data['e2'] += $value->T2;
                $data['e3'] += $value->T3;
                $data['e4'] += $value->T4;
                $data['e5'] += $value->T5;
            }
        
            //Nilai Grand Total Ec
            $data['grandTotalEc'] = $data['e1']+$data['e2']+$data['e3']+$data['e4']+$data['e5'];

         return view('pages.reportEc.'.$page)->with('data',$data);
    }
  
    public function photoActivity()
    {
        $role = Auth::user()->kd_role;
        $area = Auth::user()->kd_area;
        if($area==100)
        {
           $areas = [''=>''] + Area::lists('kd_area', 'id')->toArray();
        } 
        else
        {
           $areas = [''=>''] + Area::where('id','=',$area)->lists('kd_area', 'id')->toArray();
        }
        return view('pages.reportPhoto.photoActivity')->with('areas',$areas);
    }

    // competitor Report

    public function competitorActivity()
    {
        $role = Auth::user()->kd_role;
        $area = Auth::user()->kd_area;
        if($area==100)
        {
           $areas = [''=>''] + Area::lists('kd_area', 'id')->toArray();
        } 
        else
        {
           $areas = [''=>''] + Area::where('id','=',$area)->lists('kd_area', 'id')->toArray();
        }
        return view('pages.reportCompetitor.competitorActivity')->with('areas',$areas);
    }



    // HASIL CARI REPORT competitor photo //
    
    public function resultCompetitorActivity(ReportRequest $request)
    {
        if(Input::get('show')) {
            $page = 'result';
            $data['kd_area'] = $request->input('kd_area');
            $data['kd_sales'] = $request->input('sales');
            $data['periode'] = $request->input('periode');
            $periode = substr($data['periode'], 1,2);
            $tahun = substr($data['periode'], 3,4);
            $data['sales'] = User::select('nama','nik')->where('id','=', $data['kd_sales'])->groupBy('id')->get();
            $data['area']= Area::select('nm_area')->where('id','=', $data['kd_area'])->get();

             $data['competitorAct'] = PhotoActivity::select('photo_activity.*',
                'competitor.*',
                'user.nama'
                )

            ->join('competitor','competitor.id','=','photo_activity.kd_competitor')
            ->join('user','user.id','=','photo_activity.kd_user')
            ->leftJoin('tp_photo','tp_photo.id','=','photo_activity.jenis_photo')
            ->where('photo_activity.kd_outlet' , '=', 0)
            ->where('photo_activity.kd_user','=', $data['kd_sales'])
             ->where(DB::raw('MONTH(photo_activity.date_upload_photo)'),'=',$periode)
            ->where(DB::raw('YEAR(photo_activity.date_upload_photo)'),'=',$tahun)
            ->orderBy('tp_photo.id','desc')
            ->get();
             return view('pages.reportCompetitor.'.$page)->with('data',$data);
        }
        elseif(Input::get('down')) {
            $this->downloadcompetitorActivity($request);
        }
    }



    public function resultPhotoActivity(ReportRequest $request)
    {
        if(Input::get('show')) {
            $page = 'result';
            $data['kd_area'] = $request->input('kd_area');
            $data['kd_sales'] = $request->input('sales');
            $data['periode'] = $request->input('periode');
            $periode = substr($data['periode'], 1,2);
            $tahun = substr($data['periode'], 3,4);
            $data['sales'] = User::select('nama','nik')->where('id','=', $data['kd_sales'])->groupBy('id')->get();
            $data['area']= Area::select('nm_area')->where('id','=', $data['kd_area'])->get();

             $data['photoAct'] = PhotoActivity::select('photo_activity.*',
                'outlet.*',
                'tp_photo.nama_tipe'
              )
            ->join('outlet','outlet.kd_outlet','=','photo_activity.kd_outlet')
            ->join('user','user.id','=','outlet.kd_user')
            ->leftJoin('tp_photo','tp_photo.id','=','photo_activity.jenis_photo')
            ->where('photo_activity.kd_competitor' , '=', 0)
            ->where('outlet.kd_user','=', $data['kd_sales'])
            ->where(DB::raw('MONTH(photo_activity.date_upload_photo)'),'=',$periode)
            ->where(DB::raw('YEAR(photo_activity.date_upload_photo)'),'=',$tahun)
            ->orderBy('tp_photo.id','desc')
            ->get();
             return view('pages.reportPhoto.'.$page)->with('data',$data);
        }
        elseif(Input::get('down')) {
            $this->downloadPhotoActivity($request);
        }
    }







    public function downloadPhotoActivity(ReportRequest $request)
    {
        $page = 'result';
        $data['kd_area'] = $request->input('kd_area');
        $data['kd_sales'] = $request->input('sales');
        $data['periode'] = $request->input('periode');
        $periode = substr($data['periode'], 1,2);
        $tahun = substr($data['periode'], 3,4);
        $data['sales'] = User::select('nama','nik')->where('id','=', $data['kd_sales'])->groupBy('id')->get();
        $data['area']= Area::select('nm_area')->where('id','=', $data['kd_area'])->get();

         $data['photoAct'] = PhotoActivity::select('photo_activity.*',
            'outlet.*',
            'tp_photo.nama_tipe'
          )
        ->join('outlet','outlet.kd_outlet','=','photo_activity.kd_outlet')
        ->join('user','user.id','=','outlet.kd_user')
        ->leftJoin('tp_photo','tp_photo.id','=','photo_activity.jenis_photo')
        ->where('photo_activity.kd_competitor' , '=', 0)
        ->where('outlet.kd_user','=', $data['kd_sales'])
        ->where(DB::raw('MONTH(photo_activity.date_upload_photo)'),'=',$periode)
        ->where(DB::raw('YEAR(photo_activity.date_upload_photo)'),'=',$tahun)
        ->orderBy('tp_photo.id','desc')
        ->get();

        ob_end_clean();
        ob_start();
        date_default_timezone_set("Asia/Jakarta");
        $d= strtotime("now");
        $time =date("d-m-Y H:i:s", $d);
        foreach($data['sales'] as $val){
            $namaSales = $val->nama;
        }
        $filename = $time."_".$namaSales."_Photo_Activity";
        Excel::create($filename, function($excel) use($data){
        $excel->sheet('Sheet 1', function($sheet) use($data) {
                $sheet->loadView('pages.reportPhoto.resultDownload')->with('data',$data);
                $sheet->fromArray($data);
                $sheet->setOrientation('landscape');
            });
        })->download('xls');
    }
    public function downloadCompetitorActivity(ReportRequest $request)
    {
        $data['kd_area'] = $request->input('kd_area');
        $data['kd_sales'] = $request->input('sales');
        $data['periode'] = $request->input('periode');
        $periode = substr($data['periode'], 1,2);
        $tahun = substr($data['periode'], 3,4);
        $data['sales'] = User::select('nama','nik')->where('id','=', $data['kd_sales'])->groupBy('id')->get();
        $data['area']= Area::select('nm_area')->where('id','=', $data['kd_area'])->get();

         $data['competitorAct'] = PhotoActivity::select('photo_activity.*',
            'competitor.*',
            'user.nama'
          )

        ->join('competitor','competitor.id','=','photo_activity.kd_competitor')
        ->join('user','user.id','=','photo_activity.kd_user')
        ->leftJoin('tp_photo','tp_photo.id','=','photo_activity.jenis_photo')
        ->where('photo_activity.kd_outlet' , '=', 0)
        ->where('photo_activity.kd_user','=', $data['kd_sales'])
         ->where(DB::raw('MONTH(photo_activity.date_upload_photo)'),'=',$periode)
        ->where(DB::raw('YEAR(photo_activity.date_upload_photo)'),'=',$tahun)
        ->orderBy('tp_photo.id','desc')
        ->get();

        ob_end_clean();
        ob_start();
        date_default_timezone_set("Asia/Jakarta");
        $d= strtotime("now");
        $time =date("d-m-Y H:i:s", $d);
        $filename = $time."_"."Competitor_Photo_Activity";
        Excel::create($filename, function($excel) use($data){
        $excel->sheet('Sheet 1', function($sheet) use($data) {
                $sheet->loadView('pages.reportCompetitor.resultDownload')->with('data',$data);
                $sheet->fromArray($data);
                $sheet->setOrientation('landscape');
            });
        })->download('xls');
    }

    
}
