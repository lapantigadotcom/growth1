<?php
/*
    PT. Trikarya Teknologi Indonesia
    Tenggilis raya 127
    Office Complex Apartment Metropolis MKB 206
    Surabaya, Jawa timur, Indonesia
    Phone : +6231-8420384 / +6281235537717
    Code By : Novita Nata Wardanie
*/
namespace App\Http\Controllers;

use DB;
use Auth;
use Validator;
use Hash;
use Session;
use Redirect;
use Mail;
use App\User;
use App\VisitPlan;
use App\TakeOrder;
use App\Outlet;
use App\Logging;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Khill\Lavacharts\Lavacharts;
use Lava;

class AdminController extends Controller
{
    
    public function getLogHistory($action)
    {
        $logging = new Logging;
        $logging->kd_user = Auth::user()->id;
        $logging->description = $action;
        $logging->log_time = DB::raw('CURRENT_TIMESTAMP');
        $logging->detail_akses = 'Backend';
        $logging->save();   
    }
    
    public function getLogin()
    {
        return view('auth.login');
    }

    public function postLogin(Request $request)
    {
        $username = $request->input('username');
        $password = $request->input('password');
        $remember_me=false;

        $tmp = $request->input('remember_me');
        if(!empty($tmp))
            $remember_me=true;
        if (Auth::attempt(['username' => $username, 'password' => $password],$remember_me))
        {
            $this->getLogHistory('LOGIN');
            return redirect()->intended('admin/dashboard'); 
          
        }
        else
        {
            return redirect()->back()->withInput()->with('message', 'Login Failed');;
        }
    }

    public function getDashboard()
    {
        if(Auth::user()->kd_area== 100)
        {
            /* Kecuali Count Untuk outlet semua di count satu bulan */
             $data['finishedVisit']= DB::table('visit_plan')
                                ->where('approve_visit', '=', 1)
                                ->where('status_visit', '=', 1)
                                ->where(DB::raw('MONTH(date_visit)'), '=', date('m'))

                                ->count();
             $data['pendingVisit'] = DB::table('visit_plan')
                                ->where('approve_visit', '=', 2)
                                ->where(DB::raw('MONTH(date_visit)'), '=', date('m'))

                                ->count();
             $data['effectiveCall'] = DB::table('take_order')->where('status_order', '=', 1)->where(DB::raw('MONTH(date_order)'), '=', date('m'))->count();

             $data['competitorAct'] = DB::table('photo_activity')->where('jenis_photo', 0)->where(DB::raw('MONTH(date_upload_photo)'), '=', date('m'))->count();
             $data['salesForce'] = DB::table('user')->where('kd_role', '=', 3)->count();
             $data['photoOutlet'] = DB::table('photo_activity')->where('jenis_photo', 2)->where(DB::raw('MONTH(date_upload_photo)'), '=', date('m'))->count();
             
             $data['regOutlet'] = DB::table('outlet')->where('reg_status', '=', 'YES')->count();

              $data['top']= DB::table('user')
              ->select('user.nama','area.nm_area')
              ->where('user.kd_role','=',3)
              // tambahan untuk top sales per bulan actual //
                ->where(DB::raw('MONTH(date_visit)'), '=', date('m'))
              ->selectRaw('count(visit_plan.id) as `TotalVisit`')
              ->selectRaw('count(take_order.id) as `TotalEc`')
              ->where('visit_plan.status_visit','=',1)
              ->join('outlet','outlet.kd_user','=','user.id')
              ->leftJoin('visit_plan','visit_plan.kd_outlet','=','outlet.kd_outlet')
              ->leftJoin('take_order','take_order.kd_visitplan','=','visit_plan.id')
              ->leftJoin('area','area.id','=','outlet.kd_area')
              ->groupBy('user.id')
              ->orderBy('TotalVisit', 'desc')
              ->get();
          
            $data['visitMonth']=DB::select(DB::raw("SELECT day(date_visiting) as dates, count(date_visiting) as jumlah_visit FROM visit_plan WHERE month(date_visiting)=month(now()) AND status_visit=1 group by day(date_visiting) order by date_visiting"));
            $result = Lava::DataTable();
            $result->addStringColumn('Visit Plan')
                    ->addNumberColumn('Jumlah Visit');
                    foreach($data['visitMonth'] as $graphVisit){
                        $result->addRow(array($graphVisit->dates,$graphVisit->jumlah_visit));
                    }
            Lava::AreaChart('VisitPlan', $result, [
                'title'     => 'Submited Visit Graph',
                'legend'    => ['position' => 'in'],
                'hAxis'     => ['title'=>'Tanggal'],
                'vAxis'     => ['title'=>'Jumlah Visit'],
                'colors'    => ['green'],
            ]);

            $data['orderMonth']=DB::select(DB::raw("SELECT day(date_order) as dates, count(date_order) as jumlah_order FROM take_order WHERE month(date_order)=month(now()) group by day(date_order) order by date_order"));
            $result = Lava::DataTable();
            $result->addStringColumn('Effective Call')
                    ->addNumberColumn('Jumlah Effective Call');
                    foreach($data['orderMonth'] as $graphVisit){
                        $result->addRow(array($graphVisit->dates,$graphVisit->jumlah_order));
                    }
            Lava::AreaChart('TakeOrder', $result, [
                'title' => 'Take Order Graph',
                'legend'    => ['position' => 'in'],
                'hAxis'     => ['title'=>'Tanggal'],
                'vAxis'     => ['title'=>'Jumlah Order'],
                'colors'    => ['blue'],
            ]);
            
           return view('dashboard.index',compact('data'));
        }
        else if(Auth::user()->kd_role == 3)
        {
            $data = $this->getSalesDashboard();
            return view('dashboard.index',compact('data'));
        }
        else
        {
            $data = $this->getAreaManagerDashboard();
            return view('dashboard.index',compact('data'));
        }
    }
   
    public function getAreaManagerDashboard()
    {
        $kode = Auth::user()->kd_area;
        $data['finishedVisit']= VisitPlan::join('outlet', 'visit_plan.kd_outlet','=','outlet.kd_outlet' )
                                ->where('visit_plan.status_visit', '=', 1)
                                 ->where(DB::raw('MONTH(date_visit)'), '=', date('m'))
                                ->where('outlet.kd_area', '=', $kode)
                                ->count();
        $data['pendingVisit'] = VisitPlan::join('outlet', 'visit_plan.kd_outlet','=','outlet.kd_outlet' )
                                ->where('status_visit', '=', 0)
                                 ->where(DB::raw('MONTH(date_visit)'), '=', date('m'))
                                ->where('outlet.kd_area', '=', $kode)
                                ->count();
        $data['effectiveCall'] = DB::table('take_order')
                                ->where('status_order', '=', 1)
                                 ->where(DB::raw('MONTH(date_visit)'), '=', date('m'))
                                ->join('visit_plan', 'visit_plan.id','=','take_order.kd_visitplan' )
                                 ->join('outlet', 'outlet.kd_outlet','=','visit_plan.kd_outlet' )
                                ->where('outlet.kd_area', '=', $kode)
                                ->count();
             $data['competitorAct'] = DB::table('photo_activity')->where('jenis_photo', 0)->where(DB::raw('MONTH(date_upload_photo)'), '=', date('m'))->count();
        $data['salesForce'] = DB::table('user')
                            ->where('kd_role', '=', 3)
                            ->where('user.kd_area', '=', $kode)
                            ->count();
        $data['regOutlet'] = DB::table('outlet')
                            ->where('reg_status', '=', 'YES')
                            ->where('outlet.kd_area', '=', $kode)
                            ->count();
        // count perbulan //                         
        $data['photoOutlet'] = DB::table('photo_activity')->where('jenis_photo', 2)->where(DB::raw('MONTH(date_upload_photo)'), '=', date('m'))->count();

        $data['top']= User::select('user.nama','area.nm_area')
        ->where('user.kd_area','=', $kode)
        ->where('user.kd_role','=','3')
        ->selectRaw('count(visit_plan.id) as `TotalVisit`')
        ->selectRaw('count(take_order.id) as `TotalEc`')
        ->where('visit_plan.status_visit','=',1)
         ->where(DB::raw('MONTH(date_visit)'), '=', date('m'))
        ->join('outlet','outlet.kd_user','=','user.id')
        ->leftJoin('visit_plan','visit_plan.kd_outlet','=','outlet.kd_outlet')
        ->leftJoin('take_order','take_order.kd_visitplan','=','visit_plan.id')
        ->leftJoin('area','area.id','=','outlet.kd_area')
        ->groupBy('user.id')
        ->orderBy('TotalVisit', 'desc')
        ->take(5)->get();

        $data['visitMonth']=DB::select(DB::raw("SELECT day(date_visiting) as dates, count(date_visiting) as jumlah_visit 
        FROM visit_plan, outlet 
        WHERE visit_plan.kd_outlet=outlet.kd_outlet
        AND outlet.kd_area = :kodeArea
        AND month(date_visiting)=month(now())
        AND status_visit=1 
        GROUP BY day(date_visiting) 
        ORDER BY date_visiting"),array('kodeArea' => $kode,));
        $result = Lava::DataTable();
        $result->addStringColumn('Visit Plan')
                ->addNumberColumn('Jumlah Visit');
                foreach($data['visitMonth'] as $graphVisit){
                    $result->addRow(array($graphVisit->dates,$graphVisit->jumlah_visit));
                }
        Lava::AreaChart('VisitPlan', $result, [
            'title'     => 'Submited Visit Graph',
            'legend'    => ['position' => 'in'],
            'hAxis'     => ['title'=>'Tanggal'],
            'vAxis'     => ['title'=>'Jumlah Visit'],
            'colors'    => ['green'],
        ]);

        $data['orderMonth']=DB::select(DB::raw("SELECT day(date_order) as dates, count(date_order) as jumlah_order 
        FROM take_order,outlet,visit_plan
        WHERE take_order.kd_visitplan=visit_plan.id
        AND visit_plan.kd_outlet = outlet.kd_outlet
        AND outlet.kd_area = :kodeArea
        AND month(date_order)=month(now())
        group by day(date_order) 
        order by date_order"),array('kodeArea' => $kode,));
        $result = Lava::DataTable();
        $result->addStringColumn('Effective Call')
                ->addNumberColumn('Jumlah Effective Call');
                foreach($data['orderMonth'] as $graphVisit){
                    $result->addRow(array($graphVisit->dates,$graphVisit->jumlah_order));
                }
        Lava::AreaChart('TakeOrder', $result, [
            'title' => 'Take Order Graph',
            'legend'    => ['position' => 'in'],
            'hAxis'     => ['title'=>'Tanggal'],
            'vAxis'     => ['title'=>'Jumlah Order'],
            'colors'    => ['blue'],
        ]);

        return $data;
    }
    
    public function getSalesDashboard()
    {
        $id = Auth::user()->id;
        // Pending Visit Plan, 
        $data['pending']= VisitPlan::join('outlet', 'visit_plan.kd_outlet','=','outlet.kd_outlet' )
                                
                                ->where('approve_visit', '=', 0)
                                ->where('status_visit', '=', 0)
                                ->where('outlet.kd_user', '=', $id)
                                ->count();
        // My Outlet
        $data['outlet'] = DB::table('outlet')
                                ->where('reg_status', '=', 'YES')
                                ->where('outlet.kd_user', '=', $id)
                                ->count();
        // My Take Orders                    
        $data['order'] = DB::table('take_order')
                                ->join('visit_plan', 'visit_plan.id','=','take_order.kd_visitplan' )
                                ->join('outlet', 'outlet.kd_outlet','=','visit_plan.kd_outlet' )
                                ->where('status_order', '=', 1)
                                ->where('outlet.kd_user', '=', $id)
                                ->count();
        // Visit Plan Today
        $data['visit'] =VisitPlan::join('outlet', 'visit_plan.kd_outlet','=','outlet.kd_outlet' )
                                ->where('approve_visit', '=', 1)
                                ->where('status_visit', '=', 2)
                                ->where('date_visit','=',date('Y-m-d'))
                                ->where('outlet.kd_user', '=', $id)
                                ->count();
 


        $data['visitMonth']=DB::select(DB::raw("SELECT day(date_visiting) as dates, count(date_visiting) as jumlah_visit 
        FROM visit_plan, outlet 
        WHERE visit_plan.kd_outlet=outlet.kd_outlet
        AND outlet.kd_user = :kodeSales
        AND month(date_visiting)=month(now())
        AND status_visit=1 
        GROUP BY day(date_visiting) 
        ORDER BY date_visiting"),array('kodeSales' => $id));
        $result = Lava::DataTable();
        $result->addStringColumn('Visit Plan')
                ->addNumberColumn('Jumlah Visit');
                foreach($data['visitMonth'] as $graphVisit){
                    $result->addRow(array($graphVisit->dates,$graphVisit->jumlah_visit));
                }
        Lava::AreaChart('VisitPlan', $result, [
            'title'     => 'Submited Visit Graph',
            'legend'    => ['position' => 'in'],
            'hAxis'     => ['title'=>'Tanggal'],
            'vAxis'     => ['title'=>'Jumlah Visit'],
            'colors'    => ['green'],
        ]);

        $data['orderMonth']=DB::select(DB::raw("SELECT day(date_order) as dates, count(date_order) as jumlah_order 
        FROM take_order,outlet,visit_plan
        WHERE take_order.kd_visitplan=visit_plan.id
        AND visit_plan.kd_outlet = outlet.kd_outlet
        AND outlet.kd_user = :kodeSales
        AND month(date_order)=month(now()) 
        group by day(date_order) 
        order by date_order"),array('kodeSales' => $id));
        $result = Lava::DataTable();
        $result->addStringColumn('Effective Call')
                ->addNumberColumn('Jumlah Effective Call');
                foreach($data['orderMonth'] as $graphVisit){
                    $result->addRow(array($graphVisit->dates,$graphVisit->jumlah_order));
                }
        Lava::AreaChart('TakeOrder', $result, [
            'title' => 'Take Order Graph',
            'legend'    => ['position' => 'in'],
            'hAxis'     => ['title'=>'Tanggal'],
            'vAxis'     => ['title'=>'Jumlah Order'],
            'colors'    => ['blue'],
        ]);

        return $data;
    }

    public function getLogout()
    {
        $this->getLogHistory('LOGOUT');
        Auth::logout();
        return redirect()->route('admin.login');
    }

    public function getChangePassword()
    {
        return view('pages.admin.changepassword');
    }
    
    public function postChangePassword(Request $request)
    {
       $user = Auth::user();
        $oldPassword = Input::get('old_password');
        $newPassword = Input::get('new_password');
        $retypePassword = Input::get('retype_password');
        if($newPassword != $retypePassword) {
            Session::flash('errorMessage', 'Re-type Password tidak sama');
            return redirect()->route('admin.getChangePassword');
        }
        if(!Hash::check($oldPassword, $user->password)) {
            Session::flash('errorMessage', 'Password salah');
            return redirect()->route('admin.getChangePassword');
        }
        $user->password = $newPassword;
        $user->save();
        Session::flash('successMessage', 'Ganti password berhasil');
        return redirect()->route('admin.dashboard');
    }
    
    public function autentikasi()
    {
        return view('auth.forgot.autentikasi');
    }

    public function sendEmailReminder(Request $request)
    {
        $email = $request->input('email_user');
        $nik = $request->input('nik');
        $telp = $request->input('telepon');

        $user = User::where('email_user', '=', $email)->first();
        if(is_null($user))
        {
             Session::flash('error_message','User Not Found');
             return view('auth.login'); 
        }
        else
        {
             if(!($user->nik == $nik && $user->telepon == $telp))
             {
                Session::flash('error_message','User Not Found');
                return view('auth.login'); 
             }
        }   
       $newPass = str_random(8);
        if($user->update(array('password' => $newPass))){
            Mail::send('email.ubahPass', ['newPass' => $newPass], function($message) use ($user)
            {
                $message->from('noreply@growth.co.id', 'Sales App');
                $message->to($user->email_user, $user->nama)->subject('GROWTH Reset Password!');
            });
        }
        Session::flash('success_message','Password baru telah dikirim ke email '.$email);
        return view('auth.login'); 
    }
}
