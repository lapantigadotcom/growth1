<?php
/*
    PT. Trikarya Teknologi Indonesia
    Tenggilis raya 127
    Office Complex Apartment Metropolis MKB 206
    Surabaya, Jawa timur, Indonesia
    Phone : +6231-8420384 / +6281235537717
    Code By : Novita Nata Wardanie
*/
namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Input;
use Excel;
use App\VisitPlan;
use App\Produk;
use App\TakeOrder;
use App\Area;
use App\Kota;
  use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\OrderRequest;
use App\Http\Controllers\AdminController;

class OrderController extends Controller
{
    
    public function formOrder()
    {
        if(Auth::user()->kd_area== 100){
             $data= DB::table('take_order')
            ->select('produk.kd_produk',
                'user.nama',
                'produk.nm_produk',
                'outlet.nm_outlet',
                'kota.nm_kota',
                'area.nm_area',
                'take_order.*')
            ->join('produk','produk.id','=','take_order.kd_produk')

            ->join('visit_plan','visit_plan.id','=','take_order.kd_visitplan')
            ->join('outlet','visit_plan.kd_outlet','=','outlet.kd_outlet')

            ->leftJoin('user', 'user.id', '=', 'outlet.kd_user')
            ->leftJoin('kota', 'kota.id', '=', 'outlet.kd_kota')
            ->leftJoin('area','area.id','=','user.kd_area')
            ->whereBetween('take_order.date_order',[date('Y-m-d',strtotime("-7 day")),date('Y-m-d',strtotime("+1 day"))])
            ->get();
           
        }
        else if(Auth::user()->kd_role == 3){
            $id =Auth::user()->id;
            $data= DB::table('take_order')
            ->select('produk.kd_produk',
                'user.nama',
                'produk.nm_produk',
                'outlet.nm_outlet',
                'kota.nm_kota',
                'area.nm_area',
                'take_order.*')
            ->join('produk','produk.id','=','take_order.kd_produk')
                        ->join('outlet','visit_plan.kd_outlet','=','outlet.kd_outlet')

            ->join('visit_plan','visit_plan.id','=','take_order.kd_visitplan')
            ->join('outlet','visit_plan.kd_outlet','=','outlet.kd_outlet')

            ->leftJoin('user', 'user.id', '=', 'outlet.kd_user')
            ->leftJoin('kota', 'kota.id', '=', 'outlet.kd_kota')
            ->leftJoin('area','area.id','=','user.kd_area')

            ->where('outlet.kd_user','=',$id)
            ->whereBetween('take_order.date_order',[date('Y-m-d',strtotime("-7 day")),date('Y-m-d',strtotime("+1 day"))])
            ->get();
        }
        else {
            $area =Auth::user()->kd_area;
            $data= DB::table('take_order')
            ->select('produk.kd_produk',
                'user.nama',
                'produk.nm_produk',
                'kota.nm_kota',
                'area.nm_area',
                'outlet.nm_outlet',
                'take_order.*')
            ->join('produk','produk.id','=','take_order.kd_produk')
            ->join('visit_plan','visit_plan.id','=','take_order.kd_visitplan')
            ->join('outlet','visit_plan.kd_outlet','=','outlet.kd_outlet')
            ->leftJoin('user', 'user.id', '=', 'outlet.kd_user')
            ->leftJoin('kota', 'kota.id', '=', 'outlet.kd_kota')
            ->leftJoin('area','area.id','=','user.kd_area')
            ->where('outlet.kd_area','=',$area)
            ->whereBetween('take_order.date_order',[date('Y-m-d',strtotime("-7 day")),date('Y-m-d',strtotime("+1 day"))])
            ->get();
        }
        return view('pages.order.order')->with('data',$data);
    }

    public function index(OrderRequest $request)
    {
        $data['dateFrom'] = $request->input('dateFrom');
        $data['dateTo'] = $request->input('dateTo');
        if(Input::get('show')) {
            if(Auth::user()->kd_area == 100) {
                $data= DB::table('take_order')
                ->select('produk.kd_produk',
                     'user.nama',
                    'produk.nm_produk',
                    'kota.nm_kota',
                    'area.nm_area',

                    'produk.nm_produk',
                    'outlet.nm_outlet',
                    'take_order.*')
               ->join('produk','produk.id','=','take_order.kd_produk')

            ->join('visit_plan','visit_plan.id','=','take_order.kd_visitplan')
            ->join('outlet','visit_plan.kd_outlet','=','outlet.kd_outlet')

           ->leftJoin('user', 'user.id', '=', 'outlet.kd_user')
            ->leftJoin('kota', 'kota.id', '=', 'outlet.kd_kota')
            ->leftJoin('area','area.id','=','user.kd_area')

                ->whereBetween('take_order.date_order',[$data['dateFrom'],$data['dateTo']])
                ->get();
            }
            else if(Auth::user()->kd_role == 3){
                $id =Auth::user()->id;
                $data= DB::table('take_order')
                ->select('produk.kd_produk',

                    'produk.nm_produk',
                    'outlet.nm_outlet',
                    'take_order.*')
                ->join('produk','produk.id','=','take_order.kd_produk')
                ->join('visit_plan','visit_plan.id','=','take_order.kd_visitplan')
                ->join('outlet','visit_plan.kd_outlet','=','outlet.kd_outlet')
                ->where('outlet.kd_user','=',$id)
                 ->whereBetween('take_order.date_order',[$data['dateFrom'],$data['dateTo']])
                ->get();
            }
            else {
                $area =Auth::user()->kd_area;
                $data= DB::table('take_order')
                ->select('produk.kd_produk',
                    'produk.nm_produk',
                    'outlet.nm_outlet',
                    'take_order.*')
                ->join('produk','produk.id','=','take_order.kd_produk')
                ->join('visit_plan','visit_plan.id','=','take_order.kd_visitplan')
                ->join('outlet','visit_plan.kd_outlet','=','outlet.kd_outlet')
                ->where('outlet.kd_area','=',$area)
                ->whereBetween('take_order.date_order',[$data['dateFrom'],$data['dateTo']])
                ->get();
            }
            return view('pages.order.index')->with('data',$data);
        } else if(Input::get('download')) {
            $this->exportTakeOrder($request);
        }
    }

    public function exportTakeOrder(OrderRequest $request)
    {
        $log = new AdminController;
        $log->getLogHistory('Download Data Take Order');
        $data['dateFrom'] = $request->input('dateFrom');
        $data['dateTo'] = $request->input('dateTo');
        $role = Auth::user()->kd_role;
        $area = Auth::user()->kd_area;
        if($area == 100)
        {
            ob_end_clean();
            ob_start();
            $order = TakeOrder::select(
                    'take_order.kd_to AS KD_ORDER',
                    'visit_plan.id AS KD_VISIT',
                    'user.nama AS NAMA_SF',
                     'produk.kd_produk as KODE_PRODUK',
                    'produk.nm_produk AS NAMA_PRODUK',
                    'outlet.kd_outlet AS KD_OUTLET',
                    'outlet.nm_outlet AS OUTLET',
                    'outlet.almt_outlet AS ALAMAT',
                    'outlet.kodepos AS KODEPOS',
                    'kota.nm_kota AS KOTA',
                    'area.nm_area AS AREA',

                    'take_order.date_order as DATE_ORDER',
                    'take_order.qty_order as QUANTITY',
                    'take_order.satuan as SATUAN')
                ->join('produk','produk.id','=','take_order.kd_produk')
 
                ->join('visit_plan','visit_plan.id','=','take_order.kd_visitplan')
                ->join('outlet','visit_plan.kd_outlet','=','outlet.kd_outlet')
              ->join('user', 'user.id', '=', 'outlet.kd_user')
                ->join('kota', 'kota.id', '=', 'outlet.kd_kota')
                ->join('area','area.id','=','user.kd_area')
                ->whereBetween('take_order.date_order',[$data['dateFrom'],$data['dateTo']])
                ->get();
            date_default_timezone_set("Asia/Jakarta");
            $d= strtotime("now");
            $time =date("d-m-Y H:i:s", $d);
            $filename = $time."_TAKE_ORDER";
            Excel::create($filename, function($excel) use($order) {
                $excel->sheet('Sheet 1', function($sheet) use($order) {
                    $sheet->fromArray($order);
                });
            })->download('xls');
        }
        else
        {
            $this->exportTakeOrderArea($area,$data['dateFrom'],$data['dateTo']);
        }
        
    }

    public function exportTakeOrderArea($area,$dateForm,$dateTo)
    {
        $area = Auth::user()->kd_area;
        ob_start();
        $order = TakeOrder::select('produk.kd_produk as KODE_PRODUK',
                'produk.nm_produk AS NAMA_PRODUK',
                'outlet.nm_outlet AS OUTLET',
                'take_order.date_order as DATE_ORDER',
                'take_order.qty_order as QUANTITY',
                'take_order.satuan as SATUAN')
            ->join('produk','produk.id','=','take_order.kd_produk')
            ->join('visit_plan','visit_plan.id','=','take_order.kd_visitplan')
            ->join('outlet','visit_plan.kd_outlet','=','outlet.kd_outlet')
            ->where('outlet.kd_area','=',$area)
            ->whereBetween('visit_plan.date_create_visit',[$dateForm,$dateTo])
            ->get();
        date_default_timezone_set("Asia/Jakarta");
        $d= strtotime("now");
        $time =date("d-m-Y H:i:s", $d);
        $filename = $time."_TAKE_ORDER";
        Excel::create($filename, function($excel) use($order) {
            $excel->sheet('Sheet 1', function($sheet) use($order) {
                $sheet->fromArray($order);
            });
        })->download('xls');
    }

    public function create()
    { 
        $role = Auth::user()->kd_role;
        $area = Auth::user()->kd_area;
        $id = Auth::user()->id;
        if($area == 100)
        {
            $data['visit'] = [''=>''] + VisitPlan::join('outlet','outlet.kd_outlet','=','visit_plan.kd_outlet')
            ->lists('outlet.nm_outlet', 'visit_plan.id')
            ->toArray();
        }
        else if($role==3)
        {
            $data['visit'] = [''=>''] + VisitPlan::join('outlet','outlet.kd_outlet','=','visit_plan.kd_outlet')
            ->where('outlet.kd_user','=',$id)
            ->lists('outlet.nm_outlet', 'visit_plan.id')
            ->toArray();
        }
        else
        {
             $data['visit'] = [''=>''] + VisitPlan::join('outlet','outlet.kd_outlet','=','visit_plan.kd_outlet')
            ->where('outlet.kd_area','=',$area)
            ->lists('outlet.nm_outlet', 'visit_plan.id')
            ->toArray();
        }
        $data['produk'] = [''=>''] + Produk::lists('nm_produk', 'id')->toArray();
        return view('pages.order.create', compact('data'));
    }

    public function store(OrderRequest $request)
    {
        TakeOrder::create($request->all());
        $log = new AdminController;
        $log->getLogHistory('Make New Order');
        return redirect()->route('admin.takeOrder.form');
    }

    public function edit($id)
   /* {
        $data ['content']= TakeOrder::find($id);
        if($area == 100)
        {
            $data['visit'] = [''=>''] + VisitPlan::join('outlet','outlet.kd_outlet','=','visit_plan.kd_outlet')
            ->lists('outlet.nm_outlet', 'visit_plan.id')
            ->toArray();
        }
        else if($role==3)
        {
            $data['visit'] = [''=>''] + VisitPlan::join('outlet','outlet.kd_outlet','=','visit_plan.kd_outlet')
            ->where('outlet.kd_user','=',$id)
            ->lists('outlet.nm_outlet', 'visit_plan.id')
            ->toArray();
        }
        else
        {
             $data['visit'] = [''=>''] + VisitPlan::join('outlet','outlet.kd_outlet','=','visit_plan.kd_outlet')
            ->where('outlet.kd_area','=',$area)
            ->lists('outlet.nm_outlet', 'visit_plan.id')
            ->toArray();
        }
        $data['produk'] = [''=>''] + Produk::lists('nm_produk', 'id')->toArray();     
        return view('pages.order.edit')->with('data',$data);
    } */

    {
        $data['visit'] = [''=>''] + VisitPlan::join('outlet','outlet.kd_outlet','=','visit_plan.kd_outlet')
        ->lists('outlet.nm_outlet', 'visit_plan.id')->toArray();
        $data['produk'] = [''=>''] + Produk::lists('nm_produk', 'id')->toArray();
        $data ['content']= TakeOrder::find($id);
        return view('pages.order.edit')->with('data',$data);
    }

    public function update(OrderRequest $request, $id)
    {
        $data = VisitPlan::find($id);
        $data->update($request->all());
        $log = new AdminController;
        $log->getLogHistory('Update Order');
        return redirect()->route('admin.takeOrder.form');
    }

    public function destroy($id)
    {
        $data = TakeOrder::find($id);
        $data->delete();
        $log = new AdminController;
        $log->getLogHistory('Delete Order');
        return redirect()->route('admin.takeOrder.form');
        
    }
 
    
}
