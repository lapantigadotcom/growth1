<?php
/*
    PT. Trikarya Teknologi Indonesia
    Tenggilis raya 127
    Office Complex Apartment Metropolis MKB 206
    Surabaya, Jawa timur, Indonesia
    Phone : +6231-8420384 / +6281235537717
    Code By : Novita Nata Wardanie
*/
namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Input;
use Validator;
use App\Outlet;
use App\Distributor;
use App\Area;
use App\Kota;
use App\Tipe;
use App\User;
use App\Konfigurasi;
use App\Http\Controllers\Controller;
use App\Http\Requests\OutletRequest;
use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use File;

class OutletController extends Controller
{
    public function index()
    {
          if(Auth::user()->kd_role == 1 || Auth::user()->kd_area == 100){
            $data = DB::table('outlet')
            ->select('outlet.kd_outlet', 
                    'outlet.kode', 
                    'outlet.nm_outlet', 
                    'outlet.almt_outlet',
                    'outlet.kodepos',
                    'outlet.nm_pic',
                    'outlet.tlp_pic',
                    'tipe.nm_tipe',
                    'area.kd_area',
                    'kota.nm_kota')
            ->leftJoin('area', 'area.id', '=', 'outlet.kd_area')
            ->leftJoin('kota', 'kota.id', '=', 'outlet.kd_kota')
            ->leftJoin('tipe', 'tipe.id', '=', 'outlet.kd_tipe')
            ->get();
            return view('pages.outlet.index', compact('data'));
        }
        else if(Auth::user()->kd_role == 3){
            $id =Auth::user()->id;
            $data = DB::table('outlet')
            ->select('outlet.kd_outlet', 
                    'outlet.kode', 
                    'outlet.nm_outlet', 
                    'outlet.almt_outlet',
                    'outlet.kodepos',
                     'outlet.nm_pic',
                    'outlet.tlp_pic',
                    'tipe.nm_tipe',
                    'area.kd_area',
                    'kota.nm_kota')
            ->where('outlet.kd_user','=',$id)
            ->leftJoin('area', 'area.id', '=', 'outlet.kd_area')
            ->leftJoin('kota', 'kota.id', '=', 'outlet.kd_kota')
            ->leftJoin('tipe', 'tipe.id', '=', 'outlet.kd_tipe')
            ->get();

            return view('pages.outlet.index', compact('data'));
        }
        else
        {
            $area =Auth::user()->kd_area;
            $data = DB::table('outlet')
            ->select('outlet.kd_outlet', 
                    'outlet.kode', 
                    'outlet.nm_outlet', 
                    'outlet.almt_outlet',
                    'outlet.kodepos',
                     'outlet.nm_pic',
                    'outlet.tlp_pic',
                    'tipe.nm_tipe',
                    'area.kd_area',
                    'kota.nm_kota')
            ->where('outlet.kd_area','=',$area)
            ->leftJoin('area', 'area.id', '=', 'outlet.kd_area')
            ->leftJoin('kota', 'kota.id', '=', 'outlet.kd_kota')
            ->leftJoin('tipe', 'tipe.id', '=', 'outlet.kd_tipe')
            ->get();

            return view('pages.outlet.index', compact('data'));
        }
    }

    public function create()
    {
        if(Auth::user()->kd_role == 1 || Auth::user()->kd_area == 100)
        {
            $dist = [''=>''] + Distributor::lists('nm_dist', 'id')->toArray();
            $areas = [''=>''] + Area::lists('kd_area', 'id')->toArray();
            $users = [''=>''] + User::where('kd_role','=','3')->lists('nama', 'id')->toArray();
            $tipe = [''=>''] + Tipe::lists('nm_tipe', 'id')->toArray();
            return view('pages.outlet.create')->with('dist',$dist)->with('areas',$areas)->with('users',$users)->with('tipe',$tipe);
        }
        else
        {
            $dist = [''=>''] + Distributor::lists('nm_dist', 'id')->toArray();
            $areas = [''=>''] + Area::where('id','=',Auth::user()->kd_area)->lists('kd_area', 'id')->toArray();
            $users = [''=>''] + User::where('kd_role','=','3')->where('kd_area','=',Auth::user()->kd_area)->lists('nama', 'id')->toArray();
            $tipe = [''=>''] + Tipe::lists('nm_tipe', 'id')->toArray();
            return view('pages.outlet.create')->with('dist',$dist)->with('areas',$areas)->with('users',$users)->with('tipe',$tipe);
        }
    }

    public function store(OutletRequest $request)
    {
         $rules = array(  
            'kode'              => 'required|unique:outlet',                      
            'nm_outlet'         => 'required|unique:outlet',    
            'kd_tipe'           => 'required',  
            'kd_user'           => 'required',
            'almt_outlet'       => 'required',  
            'kd_dist'           => 'required',
            'path_photo'        => 'required',

        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {

            $messages = $validator->messages();

            $data = $this->create();
            return $data->withErrors($validator);
        
        } else {
            $data = new Outlet;
            $konfig = Konfigurasi::first();
            $data->kd_dist=$request->input('kd_dist');
            $data->kode=$request->input('kode');
            $data->kd_kota=$request->input('kd_kota');
            $data->kd_area=$request->input('kd_area');
            $data->kd_user=$request->input('kd_user');
            $data->nm_outlet=$request->input('nm_outlet');
            $data->almt_outlet=$request->input('almt_outlet');
            $data->kd_tipe=$request->input('kd_tipe');
            $data->rank_outlet=$request->input('rank_outlet');
            $data->kodepos=$request->input('kodepos');
            $data->reg_status=$request->input('reg_status');
            $data->latitude=$request->input('latitude');
            $data->longitude=$request->input('longitude');
            $data->nm_pic=$request->input('nm_pic');
            $data->tlp_pic=$request->input('tlp_pic');
            $data->toleransi_long=$konfig->toleransi_max;
            $data->save();
            $file = $request->file('path_photo');
            $destinationPath = 'image_upload/outlet';
            $filename = date('YmdHis').'_'.$data->kd_outlet.'_'. $file->getClientOriginalName();
            $file->move($destinationPath, $filename);
            $data->update(array('foto_outlet' => $filename));
            $log = new AdminController;
            $log->getLogHistory('Make New Outlet');
            return redirect()->route('admin.outlet.index');
        }
    }

    public function show($kd_outlet)
    {
       $page = 'show';
        $data['content'] = DB::table('outlet')
        ->select('outlet.*',
                'tipe.nm_tipe',
                'area.kd_area',
                'area.nm_area',
                'kota.nm_kota',
                'user.nama',
                'distributor.nm_dist')
        ->leftJoin('area', 'area.id', '=', 'outlet.kd_area')
        ->leftJoin('kota', 'kota.id', '=', 'outlet.kd_kota')  
        ->leftJoin('distributor', 'distributor.id', '=', 'outlet.kd_dist')
        ->leftJoin('tipe', 'tipe.id', '=', 'outlet.kd_tipe')
        ->leftJoin('user', 'user.id', '=', 'outlet.kd_user')
        ->where('outlet.kd_outlet', '=', $kd_outlet)
        ->first();
       
        return view('pages.outlet.'.$page,compact('data'));
    }

    public function edit($kd_outlet)
    {
        $data['content'] = Outlet::find($kd_outlet);
        $dist = [''=>''] + Distributor::lists('nm_dist', 'id')->toArray();
        $areas = [''=>''] + Area::lists('kd_area', 'id')->toArray();
        $kodeArea = Outlet::select('kd_area')->where('kd_outlet','=',$kd_outlet)->first();
        $value = $kodeArea->kd_area;
        $users = [''=>''] + User::where('kd_role','=',3)->where('kd_area','=',$value)->lists('nama', 'id')->toArray();
        $tipe = [''=>''] + Tipe::lists('nm_tipe', 'id')->toArray();
        $city = [''=>''] + Kota::lists('nm_kota', 'id')->toArray();

        return view('pages.outlet.edit')->with('data',$data)
        ->with('dist',$dist)
        ->with('areas',$areas)
        ->with('users',$users)
        ->with('tipe',$tipe)
        ->with('city',$city);
    }

    public function update(OutletRequest $request, $kd_outlet)
    
    // {
    //      $rules = array(  
            
    //         'path_photo'        => 'required',

    //     );

    //     $validator = Validator::make(Input::all(), $rules);

    //     if ($validator->fails()) {

    //         $messages = $validator->messages();

    //         $data = $this->create();
    //         return $data->withErrors($validator);
        
    //     } else 

    {
        $data = Outlet::find($kd_outlet);
        $konfig = Konfigurasi::first();
        $data->kd_dist=$request->input('kd_dist');
        $data->kode=$request->input('kode');
        $data->kd_kota=$request->input('kd_kota');
        $data->kd_area=$request->input('kd_area');
        $data->kd_user=$request->input('kd_user');
        $data->nm_outlet=$request->input('nm_outlet');
        $data->almt_outlet=$request->input('almt_outlet');
        $data->kd_tipe=$request->input('kd_tipe');
        $data->rank_outlet=$request->input('rank_outlet');
        $data->kodepos=$request->input('kodepos');
        $data->reg_status=$request->input('reg_status');
        $data->latitude=$request->input('latitude');
        $data->longitude=$request->input('longitude');
        $data->nm_pic=$request->input('nm_pic');
        $data->tlp_pic=$request->input('tlp_pic');
        $data->toleransi_long=$konfig->toleransi_max;
        $data->save();
        $file = $request->file('path_photo');
        $destinationPath = 'image_upload/outlet';
        $filename = date('YmdHis').'_'.$data->kd_outlet.'_'. $file->getClientOriginalName();
        $file->move($destinationPath, $filename);
        $data->update(array('foto_outlet' => $filename));
        $log = new AdminController;
        $log->getLogHistory('Update Outlet with ID '.$kd_outlet);
        return redirect()->route('admin.outlet.index');
    }

     

    public function destroy($kd_outlet)
    {
        $data = Outlet::with('visitPlans', 'photoActivities')->find($kd_outlet);
        foreach($data->visitPlans as $row)
        {
            $row->delete();
        }
        foreach($data->photoActivities as $row)
        {
            $row->delete();
        }
        $data->delete();
        $log = new AdminController;
	$log->getLogHistory('Delete Outlet with ID '.$kd_outlet);
        return redirect()->route('admin.outlet.index');
        
    }
    
    public function formToleransi()
    {
        $data = Tipe::all();
        //$outlet = Outlet::first();
        //$bundel = array('data' => $data,'outlet'=>$outlet);
        return view('pages.outletManage.edit', compact('data'));
    }

    public function updateToleransi(OutletRequest $request)
    {
        $long = $request->input('toleransi_long');
        //$lat = $request->input('toleransi_lat');
        DB::table('outlet')->update(['toleransi_long' => $long]); 
        DB::table('konfigurasi')->update(['toleransi_max' => $long]);
        $log = new AdminController;
        $log->getLogHistory('Update Toleransi Outlet');
        return redirect()->route('admin.outlet.index');
    }

    
}
