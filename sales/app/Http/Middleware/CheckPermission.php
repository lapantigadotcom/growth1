<?php namespace App\Http\Middleware;

use Closure;
use Auth;
use Route;

class CheckPermission {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
	
	        $route = Route::getCurrentRoute()->getName();

	        if(Auth::user()->hasAccess($route))  
	        {
	        	return $next($request);
	        }
	        else
	        {
				return abort(401);
			}
		
	}
}
