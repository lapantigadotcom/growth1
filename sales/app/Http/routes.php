<?php

Route::get('/', function () {
    return view('welcome');
});

//Login Admin
Route::get('/admin',array('as' => 'admin.login','uses' => 'AdminController@getLogin'));
Route::post('/admin', array('as' => 'admin.auth.login','uses' => 'AdminController@postLogin'));

//Login Manager
Route::get('/manager',array('as' => 'manager.login','uses' => 'ManagerController@getLogin'));
Route::post('/manager', array('as' => 'manager.auth.login','uses' => 'ManagerController@postLogin'));

//Reset Password
Route::get('/resetpassword',array('as' => 'admin.autentikasi','uses' => 'AdminController@autentikasi'));
Route::post('/resetpassword/auth',array('as' => 'admin.reset.password','uses' => 'AdminController@sendEmailReminder'));

//REST API 
Route::group(array('prefix'=>'api'),function(){
	Route::resource('webService','WebServiceController',array('except'=>array('create','edit')));
});

//Register
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

                //AJAX
		Route::get('/ajax-call', function(){
		    $id = Input::get('kd_outlet');
		    $user = \App\Outlet::where('kd_outlet', '=', $id)
		    ->join('user','outlet.kd_user','=','user.id')
		    ->get();
		    return Response::json($user);
		});

		Route::get('/ajax-city', function(){
		    $id = Input::get('id');
		    $city = \App\Kota::where('kd_area','=',$id)
		    ->get();
		    
		    return Response::json($city);
		});

		Route::get('/ajax-area', function(){
		    $id = Input::get('id');
		    $area = \App\Area::where('id','=',$id)
		    ->get();
		    return Response::json($area);
		});

                Route::get('/ajax-sales', function(){
		    $id = Input::get('id');
		    $area = \App\User::where('kd_area','=',$id)
		    ->where('kd_role','=','3')
		    ->get();
		    return Response::json($area);
		});


Route::group(['middleware' => ['web']], function () {
    Route::group(['middleware' => ['auth','admin','acl']], function()
	{
		//Dashboard
		Route::get('/admin/dashboard', array('as' => 'admin.dashboard','uses' =>'AdminController@getDashboard'));

		//ChangesPassword
		Route::get('/admin/changepassword',array('as' => 'admin.getChangePassword', 'uses' => 'AdminController@getChangePassword'));
		Route::post('/admin/changepassword',array('as' => 'admin.postChangePassword', 'uses' => 'AdminController@postChangePassword'));

		//Logout
		Route::get('admin/logout',array('as' => 'admin.logout','uses' =>'AdminController@getLogout'));

		//Sales Force
		Route::get('/admin/sales/{id}/delete',array('as'=> 'admin.sales.delete','uses' => 'SalesController@destroy'));
		Route::resource('/admin/sales', 'SalesController');

		//User
		Route::get('/admin/user/{id}/delete',array('as'=> 'admin.user.delete','uses' => 'UserController@destroy'));
		Route::resource('/admin/user', 'UserController');

		//Area
		Route::get('/admin/area/{id}/delete',array('as'=> 'admin.area.delete','uses' => 'AreaController@destroy'));
		Route::resource('/admin/area', 'AreaController');

		//Kota
		Route::get('/admin/kota/{id}/delete',array('as'=> 'admin.kota.delete','uses' => 'KotaController@destroy'));
		Route::resource('/admin/kota', 'KotaController');

		//Produk
		Route::get('/admin/produk/{id}/delete',array('as'=> 'admin.produk.delete','uses' => 'ProdukController@destroy'));
		Route::resource('/admin/produk', 'ProdukController');

		//Distributor
		Route::get('/admin/distributor/{id}/delete',array('as'=> 'admin.distributor.delete','uses' => 'DistributorController@destroy'));
		Route::resource('/admin/distributor', 'DistributorController');

		//Outlet
		Route::get('/admin/outlet/{kd_outlet}/delete',array('as'=> 'admin.outlet.delete','uses' => 'OutletController@destroy'));
		Route::resource('/admin/outlet', 'OutletController'); 

        //Toleransi Koordinat
        Route::get('admin/outletManage', array('as' => 'admin.outletManage.index','uses' => 'OutletController@formToleransi'));
		Route::post('admin/outletManage', array('as' => 'admin.outletManage.updateToleransi','uses' => 'OutletController@updateToleransi'));
   
        //Tipe Outlet
		Route::get('/admin/tipe/{id}/delete',array('as'=> 'admin.tipe.delete','uses' => 'TipeOutletController@destroy'));
		Route::resource('/admin/tipe', 'TipeOutletController'); 

        //Visit Plan Get data
		Route::get('admin/visitPlan', array('as' => 'admin.visitPlan.form', 'uses' => 'VisitPlanController@formVisit'));
		Route::post('admin/VisitPlan', array('as' => 'admin.visitPlan.result','uses' => 'VisitPlanController@index'));	
 
        //Approval Checklist
		Route::get('/admin/visit/approved',array('as'=> 'admin.visit.editApprove','uses' => 'VisitPlanController@editApprove'));
		Route::post('/admin/visit/approved',array('as'=> 'admin.visit.approved','uses' => 'VisitPlanController@approvedVisit'));

        //Visit Monitor Get data
		Route::get('admin/visitingMonitor', array('as' => 'admin.visitingMonitor.form', 'uses' => 'VisitMonitorController@formVisit'));
		Route::post('admin/visitingMonitor', array('as' => 'admin.visitingMonitor.result','uses' => 'VisitMonitorController@index'));	
                
        //Visit Plan
		Route::get('/admin/visit/{id}/delete',array('as'=> 'admin.visit.delete','uses' => 'VisitPlanController@destroy'));
		Route::resource('/admin/visit', 'VisitPlanController');
	
        //Visiting Monitor
		Route::get('/admin/visitMonitor/{id}/delete',array('as'=> 'admin.visitMonitor.delete','uses' => 'VisitMonitorController@destroy'));
		Route::resource('/admin/visitMonitor', 'VisitMonitorController');

        //Take Order Get data
		Route::get('admin/takeOrder', array('as' => 'admin.takeOrder.form', 'uses' => 'OrderController@formOrder'));
		Route::post('admin/takeOrder', array('as' => 'admin.takeOrder.result','uses' => 'OrderController@index'));	

		//Take Order
		Route::get('/admin/order/{id}/delete',array('as'=> 'admin.order.delete','uses' => 'OrderController@destroy'));
		Route::resource('/admin/order', 'OrderController');
                
        //Photo Activity Get data
		Route::get('admin/photoAct', array('as' => 'admin.photoAct.form', 'uses' => 'PhotoActivityController@formPhoto'));
		Route::post('admin/photoAct', array('as' => 'admin.photoAct.result','uses' =>'PhotoActivityController@index'));	               
	        
        //Photo Display
		Route::get('/admin/photo/{id}/delete',array('as'=> 'admin.photo.delete','uses' => 'PhotoActivityController@destroy'));
		Route::resource('/admin/photo', 'PhotoActivityController');
     
        //Tipe Photo
		Route::get('/admin/tipephoto/{id}/delete',array('as'=> 'admin.tipephoto.delete','uses' => 'TipePhotoController@destroy'));
		Route::resource('/admin/tipephoto', 'TipePhotoController');

		//Competitor Activity
		Route::get('/admin/competitor/{id}/delete',array('as'=> 'admin.competitor.delete','uses' => 'CompetitorActivityController@destroy'));
		Route::resource('/admin/competitor', 'CompetitorActivityController');

        //Competitor
		Route::get('/admin/comp/{id}/delete',array('as'=> 'admin.comp.delete','uses' => 'CompetitorController@destroy'));
		Route::resource('/admin/comp', 'CompetitorController');

		//Report
		Route::get('admin/report/', array('as' => 'admin.report', 'uses' => 'ReportController@getReport'));

        //Report Outlet Visit per sales
		Route::get('admin/report/outletVisit', array('as' => 'admin.report.outletVisit', 'uses' => 'ReportController@outletVisit'));
		Route::post('admin/report/outletVisit', array('as' => 'admin.report.resultReport','uses' => 'ReportController@resultReport'));

        //Report Working Time per sales
		Route::get('admin/report/workingTime', array('as' => 'admin.report.workingTime', 'uses' => 'ReportController@workingTime'));
		Route::post('admin/report/workingTime', array('as' => 'admin.report.resultWorkTime','uses' => 'ReportController@resultWorkTime'));

		//Report Effective Call per sales
		Route::get('admin/report/effectiveCall', array('as' => 'admin.report.effectiveCall', 'uses' => 'ReportController@effectiveCall'));
		Route::post('admin/report/effectiveCall', array('as' => 'admin.report.resultEffectiveCall', 'uses' => 'ReportController@resultEffectiveCall'));

        //Report Photo Activity per sales
		Route::get('admin/report/photoActivity', array('as' => 'admin.report.photoActivity', 'uses' => 'ReportController@photoActivity'));
		Route::post('admin/report/photoActivity', array('as' => 'admin.report.resultPhotoActivity', 'uses' => 'ReportController@resultPhotoActivity'));
		Route::get('admin/download/reportPhoto',array('as' => 'admin.reportPhoto.download','uses' =>'ReportController@exportAllPhoto'));
		//Report Competitor Photo Activity per sales
		Route::get('admin/report/competitorActivity', array('as' => 'admin.report.competitorActivity', 'uses' => 'ReportController@competitorActivity'));
		Route::post('admin/report/competitorActivity', array('as' => 'admin.report.resultCompetitorActivity', 'uses' => 'ReportController@resultCompetitorActivity'));
		//Route::get('admin/download/reportPhoto',array('as' => 'admin.reportPhoto.download','uses' =>'ReportController@exportAllPhoto'));
		//Configuration
		Route::get('/admin/permission/{id}/delete',array('as'=> 'admin.permission.delete','uses' => 'PermissionRoleController@destroy'));
		Route::resource('/admin/permission', 'PermissionController');

		Route::get('/admin/role/{id}/delete',array('as'=> 'admin.role.delete','uses' => 'RoleController@destroy'));
		Route::resource('/admin/role', 'RoleController');

        Route::get('/admin/adminmenu/{id}/delete',array('as'=> 'admin.adminmenu.delete','uses' => 'AdminMenuController@destroy'));
		Route::resource('/admin/adminmenu', 'AdminMenuController');

		//Download
		Route::get('admin/download',array('as' => 'admin.download','uses' =>'DownloadController@index'));
		Route::get('admin/download/area',array('as' => 'admin.download.area','uses' =>'DownloadController@exportArea'));
		Route::get('admin/download/outlet',array('as' => 'admin.download.outlet','uses' =>'DownloadController@exportOutlet'));
		Route::get('admin/download/distributor',array('as' => 'admin.download.distributor','uses' =>'DownloadController@exportDistributor'));
		Route::get('admin/download/produk',array('as' => 'admin.download.produk','uses' =>'DownloadController@exportProduk'));
		Route::get('admin/download/visitPlan',array('as' => 'admin.download.visitPlan','uses' =>'DownloadController@exportVisitPlan'));
		Route::get('admin/download/visitMonitor',array('as' => 'admin.download.visitMonitor','uses' =>'DownloadController@exportVisitMonitor'));
		Route::get('admin/download/takeOrder',array('as' => 'admin.download.takeOrder','uses' =>'DownloadController@exportTakeOrder'));
		Route::get('admin/download/salesForce',array('as' => 'admin.download.salesForce','uses' =>'DownloadController@exportSalesForce'));
		Route::get('admin/download/nonSF',array('as' => 'admin.download.nonSF','uses' =>'DownloadController@exportNonSF'));
		Route::get('admin/download/salesDetail/{id}',array('as' => 'admin.download.salesDetail','uses' =>'DownloadController@exportSalesDetail'));
		
		//Upload
		Route::post('admin/upload/outlet',array('as' => 'admin.upload.outlet','uses' =>'DownloadController@uploadOutlet'));
		Route::post('admin/upload/produk',array('as' => 'admin.upload.produk','uses' =>'DownloadController@uploadProduk'));
		Route::post('admin/upload/distributor',array('as' => 'admin.upload.distributor','uses' =>'DownloadController@uploadDistributor'));
		Route::post('admin/upload/area',array('as' => 'admin.upload.area','uses' =>'DownloadController@uploadArea'));
		Route::post('admin/upload/user',array('as' => 'admin.upload.user','uses' =>'DownloadController@uploadUser'));
                
       	//My Profile
		Route::get('admin/profile/{name}',array('as'=> 'admin.profile','uses' => 'UserController@myProfile'));	

		//Edit Profile 
		Route::get('admin/editprofile/{name}',array('as'=> 'admin.edit.profile','uses' => 'UserController@editProfile'));
		Route::put('admin/editprofile/{name}',array('as'=> 'admin.edited.profile','uses' => 'UserController@updateProfile'));
	});

});
//Web Service
Route::post('/setOutlet', 'WebServiceController@setOutlet');
Route::get('/login/{username}/{password}', 'WebServiceController@getUser');
Route::get('/getFoto/{kd_outlet}', 'WebServiceController@getFoto');
Route::get('/outlet/{kd_user}', 'WebServiceController@getOutlet');
Route::get('/kota/{kd_area}', 'WebServiceController@getKota');
Route::get('/distributor/{kdArea}', 'WebServiceController@getDistributor');
Route::get('/tipe', 'WebServiceController@getTipe');
Route::get('/produk', 'WebServiceController@getProduk');
Route::get('/visit/{kd_user}', 'WebServiceController@getVisit');
Route::post('/setVisit', 'WebServiceController@setVisit');
Route::post('/submitVisit', 'WebServiceController@submitVisit');
Route::post('/setPhoto', 'WebServiceController@setPhoto');
Route::post('/setIdGCM', 'WebServiceController@setGCM');
Route::get('/getCompetitor', 'WebServiceController@getCompetitor');
Route::get('/resetPass/{email}/{nik}/{telp}', 'WebServiceController@sendEmailReminder');
Route::get('/getAll/{kd_user}/{kd_area}', 'WebServiceController@getAllData');
//WS Desktop
Route::get('/desktop/getAll', 'WSDesktop@getAllData');
Route::get('/desktop/getTop', 'WSDesktop@getTopSales');
Route::post('/desktop/setVisit', 'WSDesktop@setVisit');
Route::post('/desktop/editVisit', 'WSDesktop@editVisit');
Route::get('/desktop/delVisit/{id}', 'WSDesktop@delVisit');
Route::get('/desktop/delTakeOrder/{id}', 'WSDesktop@delTakeOrder');
Route::post('/desktop/setArea', 'WSDesktop@setArea');
Route::post('/desktop/setProduk', 'WSDesktop@setProduk');
Route::post('/desktop/editProduk', 'WSDesktop@editProduk');
Route::get('/desktop/delProduk/{id}', 'WSDesktop@delProduk');
Route::post('/desktop/setOutlet', 'WSDesktop@setOutlet');
Route::post('/desktop/editOutlet', 'WSDesktop@editOutlet');
Route::get('/desktop/delOutlet/{id}', 'WSDesktop@delOutlet');
Route::post('/desktop/setDistributor', 'WSDesktop@setDistributor');
Route::post('/desktop/editDistributor', 'WSDesktop@editDistributor');
Route::get('/desktop/delDistributor/{id}', 'WSDesktop@delDistributor');
Route::get('/desktop/getFoto/{kd_outlet}', 'WSDesktop@getFoto');
Route::get('/desktop/login/{username}/{password}', 'WSDesktop@getUser');
Route::get('/desktop/resetPass/{email}/{nik}/{telp}', 'WebServiceController@sendEmailReminder');
