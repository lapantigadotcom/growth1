<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use App\Role;
use App\PermissionRole;

class AdminMenu extends Model 
{
    //
    protected $table = 'admin_menu';
    protected $guarded = ['id'];
    protected $fillable = [
                    'parent_menu',
                    'nm_menu',
                    'route',
                    'urutan',
                    'icon',
                    'enable'
                    ];

    public $timestamps = false;

    public $_child = null;

    public function parent()
    {
        return $this->belongsTo('\App\AdminMenu', 'parent_menu', 'id');
    }

    public function child()
    {
        return $this->hasMany('\App\AdminMenu', 'parent_menu', 'id');
    }

    public function listChild()
    {
        return $this->child();
    }

    public function roles()
    {
        return $this->belongsToMany('\App\Role', 'admin_menu_role', 'kd_admin_menu', 'kd_role');
    }

    public function listRoles()
    {
        return $this->roles();
    }
}
