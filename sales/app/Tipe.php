<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Outlet;
use App\Distributor;

class Tipe extends Model
{
    //
    protected $table = 'tipe';
    protected $guarded = ['id'];
    protected $primaryKey = 'id';
    protected $fillable = ['nm_tipe'];
    public $timestamps = false;

    public function outlets()
    {
    	return $this->hasMany('App\Outlet', 'kd_tipe', 'id');
    }

    public function distributors()
    {
    	return $this->hasMany('App\Distributor', 'kd_tipe', 'id');
    }
}
