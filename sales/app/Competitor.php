<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\PhotoActivity;

class Competitor extends Model
{
    //
    protected $table = 'competitor';
    protected $guarded = ['kd_competitor'];
    public $timestamps = false;

    public function photoActivities()
    {
    	return $this->hasMany('App\PhotoActivity', 'kd_competitor', 'kd_competitor');
    }

}
