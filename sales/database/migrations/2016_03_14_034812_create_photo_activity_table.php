<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotoActivityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photo_activity', function (Blueprint $table) {
            $table->string('kd_photo',50);
            $table->primary('kd_photo');
            $table->integer('kd_user');
            $table->foreign('kd_user')->references('kd_user')->on('user');
            $table->integer('kd_outlet');
            $table->foreign('kd_outlet')->references('kd_outlet')->on('outlet');
            $table->integer('kd_competitor')->nullable();
            $table->foreign('kd_competitor')->references('kd_competitor')->on('competitor');
            $table->string('nm_photo',50);
            $table->string('jenis_photo',50);
            $table->timestamp('date_take_photo');
            $table->string('almt_comp_activity',255)->nullable();;
            $table->timestamp('date_upload_photo');
            $table->string('keterangan',255)->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('photo_activity');
    }
}
