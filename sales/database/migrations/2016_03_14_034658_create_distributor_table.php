<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistributorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distributor', function (Blueprint $table) {
            $table->string('kd_dist',20);
            $table->primary('kd_dist');
            $table->string('nm_dist',100);
            $table->string('tipe_dist',20);
            $table->string('almt_dist',255);
            $table->string('kt_dist',20);
            $table->string('telp_dist',20);
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('distributor');
    }
}
