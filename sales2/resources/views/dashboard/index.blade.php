<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Hisamitsu Panel | Dashboard</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        @include('partials.head')
        <script src="//cdn.ckeditor.com/4.5.2/standard/ckeditor.js"></script>

    </head>
<body>

<div class="container-fluid-full">
    <div class="row-fluid"> 
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="left-side sidebar-offcanvas">
        @include('partials.sidebar')
        </aside>


<div id="content" class="span10">
    <ul class="breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="{{url('/home/dashboard')}}">Home</a> 
			<i class="icon-angle-right"></i>
		</li>
		<li><a href="#">Dashboard</a></li>
	</ul>
        @if(AUth::user()->kd_role != 3)
	<div class="row-fluid sortable">        
	    <div class="box span12">
	        <div class="box-header" data-original-title>
	            <h2><i class="halflings-icon user"></i><span class="break"></span>Top 5 Sales Force This Month <b>( <?php echo date('F Y', strtotime('now'));?> )</b></h2>
	        </div>

	        <div class="box-content">                                 
                <table class="table table-striped table-bordered bootstrap-datatable ">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama Sales Force</th>
                            <th>Area</th>
                            <th>Total Visit</th>
                            <th>Effective Call</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1; ?>
		           @foreach($data['top'] as $val)
		           <tr>
		               <td>{{$i++}}</td>
		               <td>{{$val->nama}}</td>
		               <td>{{$val->nm_area}}</td>
		               <td>{{$val->TotalVisit}}</td>
		               <td>{{$val->TotalEc}}</td>
		            </tr>
		          @endforeach          
                    </tbody>
                  </table>
    		</div>
		</div>
	</div>

		<div class="row-fluid ">	
		<div class="quick-button metro green span4">
			<i class="icon-truck"></i>
			<p>Finished Visit Plan This Month <b>( <?php echo date('F Y', strtotime('now'));?> )</b></p>
			<span class="badge"> {!! $data['finishedVisit'] !!}</span> 
		</div>	
		<div class="quick-button metro red span4">
			<i class="icon-check"></i>
			<p>Pending Visit Plan This Month <b>( <?php echo date('F Y', strtotime('now'));?> )</b></p>
			<span class="badge"> {!! $data['pendingVisit'] !!}</span>
		</div>
		<div class="quick-button metro purple span4">
			<i class="icon-shopping-cart"></i>
			<p>Effective Call This Month <b>( <?php echo date('F Y', strtotime('now'));?> )</b></p>
			<span class="badge"> {!! $data['countTO'] !!}</span>
		</div>
	</div>
	<br>
	<div class="row-fluid ">	
		<div class="quick-button metro orange span4">
			<i class="icon-group"></i>
			<p>Competitor Activity This Month <b>( <?php echo date('F Y', strtotime('now'));?> )</b></p>
			<span class="badge"> {!! $data['competitorAct'] !!}</span>
		</div>
		<div class="quick-button metro yellow span4">
			<i class="icon-camera"></i>
			<p>Photo Outlet This Month <b>( <?php echo date('F Y', strtotime('now'));?> )</b></p>
			<span class="badge"> {!! $data['photoOutlet'] !!}</span>
		</div>
		<div class="quick-button metro blue span4">
			<i class="icon-home"></i>
			<p>Registered Outlet</p>
			<span class="badge">{!! $data['regOutlet'] !!}</span>
		</div>
	</div>

	
        @endif
        @if(AUth::user()->kd_role == 3)
				<div class="row-fluid">
					<div class="span3 statbox purple" onTablet="span6" onDesktop="span3">
						<div class="number">{{$data['pending']}}<i class="icon-warning-sign"></i></div>
						<div class="title">pending</div>
						<div class="footer">
							<a href="#"> Not yet approve</a>
						</div>	
					</div>
					<div class="span3 statbox green" onTablet="span6" onDesktop="span3">
						<div class="number">{{$data['outlet']}}<i class="icon-home"></i></div>
						<div class="title">outlet</div>
						<div class="footer">
							<a href="#">  My Outlet</a>
						</div>
					</div>
					<div class="span3 statbox blue noMargin" onTablet="span6" onDesktop="span3">
						<div class="number">{{ $data['visit']}}<i class="icon-truck"></i></div>
						<div class="title">visits</div>
						<div class="footer">
							<a href="#"> Visit Plan Today</a>
						</div>
					</div>
					<div class="span3 statbox yellow" onTablet="span6" onDesktop="span3">
						<div class="number">{{ $data['order'] }}<i class="icon-shopping-cart"></i></div>
						<div class="title">orders</div>
						<div class="footer">
							<a href="#"> My Take Orders</a>
						</div>
					</div>	
				</div>		
			@endif
                        <br>
			<div class="clearfix"></div>   
			
			<div class="row-fluid">     
				<div class="box span12">
					
					<div class="box-header">
						<h2><i class=" halflings-icon signal"></i><span class="break"></span>Visit Activity <?php echo "This Month " . date("m-Y"); ?></h2>
						<div class="box-icon">
 							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
 						</div>
					</div>
					<div style="display: block;" class="box-content alerts">
						<div class="content">
					    <div id="pop_div" style="height:300px"></div>
						<?= Lava::render('AreaChart', 'VisitPlan', 'pop_div') ?>
						@areachart('VisitPlan', 'pop_div')
					</div>
						 
					</div>
					
				</div>
			</div>

			<div class="row-fluid">     
				<div class="box span12">
					
					<div class="box-header">
						<h2><i class="halflings-icon tags"></i><span class="break"></span>Effective Call <?php echo "This Month " . date("m-Y"); ?></h2>
						<div class="box-icon">
 							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
 						</div>
					</div>
					<div style="display: block;" class="box-content alerts">
						<div class="content">
                          <div id="order_div" style="height:300px"></div>
				          <?= Lava::render('AreaChart', 'TakeOrder', 'pop_div') ?>
				          @areachart('TakeOrder', 'order_div')
					</div>
						 
					</div>
					
				</div>
			</div>

					 

        </div>
    </div>
</div>

 @include('partials.footer')
    </body>
</html>

