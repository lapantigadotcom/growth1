<!-- start: CSS -->
        <link id="bootstrap-style" href="{{url('css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{url('css/bootstrap-responsive.min.css')}}" rel="stylesheet">
        <link id="base-style" href="{{url('css/style.css')}}" rel="stylesheet">
        <link id="base-style-responsive" href="{{url('css/style-responsive.css')}}" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
<!-- end: CSS -->

     <div class="navbar">
        <div class="navbar-inner">
            <div class="container-fluid">
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <a class="brand" href="{!! url('admin/dashboard') !!}"><span>
                <img src="{!! url('img/hisamits_white.png') !!}" style="width:130px;height:25px;"> Admin Panel</span></a>

             <div class="nav-no-collapse header-nav">
                    <ul class="nav pull-right">
                        <li>
                        <a class="btn" href="{{ route('admin.profile',[Auth::user()->username]) }}">
                            <i class="halflings-icon white wrench"></i> Profile
                        </a>                  
                        </li>
                        <li class="dropdown hidden-phone">
                   
                            <li class="dropdown">
                                <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="halflings-icon white user"></i>  
                                    @if(Auth::check())
                                        {!! Auth::user()->nama !!}
                                    <!-- Ini cuma buat testing, biar kalo ada apa2 bisa akses masihan-->
                                    @else
                                        Guest
                                    @endif

                                    <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li class="dropdown-menu-title">
                                        <span>Account Settings</span>
                                    </li>
                                    <li><a href="{!! route('admin.getChangePassword') !!}"><i class="halflings-icon user"></i> Change Password</a></li>
                                    <li><a href=" {!! route('admin.logout') !!}"><i class="fa fa-circle text-red"></i> Log Out</a></li>
                                </ul>
                            </li>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
<!-- start: JavaScript-->
    <script src="{{url('js/jquery-1.9.1.min.js')}}"></script>
    <script src="{{url('js/jquery-migrate-1.0.0.min.js')}}"></script>   
    <script src="{{url('js/jquery-ui-1.10.0.custom.min.js')}}"></script>   
    <script src="{{url('js/modernizr.js')}}"></script>    
    <script src="{{url('js/bootstrap.min.js')}}"></script>   
    <script src="{{url('js/jquery.cookie.js')}}"></script>   
    <script src="{{url('js/fullcalendar.min.js')}}"></script>    
    <script src="{{url('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{url('js/excanvas.js')}}"></script>
    <script src="{{url('js/jquery.flot.js')}}"></script>
    <script src="{{url('js/jquery.flot.stack.js')}}"></script>
    <script src="{{url('js/jquery.flot.resize.min.js')}}"></script>       
    <script src="{{url('js/jquery.chosen.min.js')}}"></script>    
    <script src="{{url('js/jquery.uniform.min.js')}}"></script>       
    <script src="{{url('js/jquery.cleditor.min.js')}}"></script>   
    <script src="{{url('js/jquery.noty.js')}}"></script>  
    <script src="{{url('js/jquery.elfinder.min.js')}}"></script>  
    <script src="{{url('js/jquery.raty.min.js')}}"></script>  
    <script src="{{url('js/jquery.iphone.toggle.js')}}"></script>    
    <script src="{{url('js/jquery.uploadify-3.1.min.js')}}"></script>   
    <script src="{{url('js/jquery.gritter.min.js')}}"></script>   
    <script src="{{url('js/jquery.imagesloaded.js')}}"></script>    
    <script src="{{url('js/jquery.masonry.min.js')}}"></script>   
    <script src="{{url('js/jquery.knob.modified.js')}}"></script>    
    <script src="{{url('js/jquery.sparkline.min.js')}}"></script>    
    <script src="{{url('js/counter.js')}}"></script>    
    <script src="{{url('js/retina.js')}}"></script>
    <script src="{{url('js/custom.js')}}"></script>
<!-- end: JavaScript--> 



