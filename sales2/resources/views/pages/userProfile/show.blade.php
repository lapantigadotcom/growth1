<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Hisamitsu Panel | Profil : {!! $data['content']->nama !!}</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        @include('partials.head')
        <script src="//cdn.ckeditor.com/4.5.2/standard/ckeditor.js"></script>

    </head>
<body>
<div class="container-fluid-full">
    <div class="row-fluid"> 
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="left-side sidebar-offcanvas">
        @include('partials.sidebar')
        </aside>

<!-- Main content -->
<div id="content" class="span9">
  <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="{{url('/home/dashboard')}}">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">My Profile</a>
        </li>
    </ul>
        <div class="row-fluid sortable">        
                <div class="box span12">
                    <div class="box-content">
                        <div class="row-fluid">            
                            <div class="span3">
                                <h1>Foto Profile</h1>
                                @if(File::exists('userphoto/'.$data['content']->foto) and $data['content']->foto != '')
                                    <img width="300" height="300" src="{!! url('userphoto/'.$data['content']->foto) !!}" class="img-responsive" />
                                @else
                                    <img src="{!! url('/img/user.png') !!}" class="img-responsive img-thumbnail">
                                @endif
                            </div>
                                <div class="span5">
                                    <h1>Detail Profile</h1>
                                </div>
                                
               <div class="icon span3" style="float: right;">
                  <h1><i class="icon-pencil"></i><a href="{{ route('admin.edit.profile',[Auth::user()->username]) }}"> Edit Profile</a></h1>       
               </div>
                                <div class="span8">
                                    <div class="priority high"><span>Nama : {!! $data['content']->nama !!}</span></div>
                                    <div class="task high">
                                        <div class="desc">
                                            <div class="title"><i class=" halflings-icon barcode"></i> NIK</div>
                                            <div>{!! $data['content']->nik !!}</div>
                                        </div>
                                        <div class="desc">
                                            <div class="title"><i class=" halflings-icon th"></i> Area</div>
                                            <div>{!! $data['content']->nm_area !!}</div>
                                        </div>
                                        <div class="desc">
                                            <div class="title"><i class=" halflings-icon home"></i> Alamat </div>
                                            <div>{!! $data['content']->alamat !!}</div>
                                        </div>
                                        <div class="desc">
                                            <div class="title"><i class=" halflings-icon calendar"></i> Join date</div>
                                            <div>{!! $data['content']->join_date !!}</div>
                                        </div>
                                        <div class="desc">
                                            <div class="title"><i class=" halflings-icon phone"></i> Telepon </div>
                                            <div>{!! $data['content']->telepon !!}</div>
                                        </div>
                                        <div class="desc">
                                            <div class="title"><i class=" halflings-icon user"></i> User Name</div>
                                            <div>{!! $data['content']->username !!}</div>
                                        </div>
                                         <div class="desc">
                                            <div class="title"><i class=" halflings-icon envelope"></i> Email</div>
                                            <div>{!! $data['content']->email_user !!}</div>
                                        </div>
                                    </div>
                                </div>
    
                            
                        </div>
                    </div><!-- /.box-body -->                
                </div><!-- /.box -->
         </div>
         @if(Auth::user()->kd_role==3)
         <div class="row-fluid sortable"> 
            <div class="box span12">
                <div class="box-header">
                    <h2><i class="halflings-icon th-list"></i><span class="break"></span>My Report</h2>
                </div>
                    <div class="box-content">
                        <div class="row-fluid"> 
                            <a class="quick-button metro blue span3">
                                <i class="icon-truck"></i>
                                <p>Outlet visit</p>
                                <span class="badge">{{$data['outletVisit'] }}</span>
                            </a>                       
                            <a class="quick-button metro red span3">
                                <i class="icon-group"></i>
                                <p>Competitor Activity</p>

                                <!--
                                <p>Working Time hours/day</p>
                                @foreach($data['workingTime'] as $row)
                                    <span class="badge">{{ $row->working}}</span>
                                @endforeach -->
                            </a>    
                            <a class="quick-button metro green span3">
                                <i class="icon-shopping-cart"></i>
                                <p>Take Order</p>
                                <span class="badge">{{$data['takeOrder']}}</span>
                            </a>   
                            <a class="quick-button metro orange span3">
                                <i class="icon-camera"></i>
                                <p>Take Photo</p>
                                <span class="badge">{{$data['takePhoto']}}</span>
                            </a>                  
                        </div>
                    </div><!-- /.box-body -->                
                </div><!-- /.box -->
            </div>
            @endif

        </div><!-- /.box -->
    </div>
</div><!-- /.row (main row) -->


<!-- add new calendar event modal -->
@include('partials.footer')
    </body>
</html>