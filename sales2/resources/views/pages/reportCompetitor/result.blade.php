<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Hisamitsu Panel</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        @include('partials.head')
        <script src="//cdn.ckeditor.com/4.5.2/standard/ckeditor.js"></script>

    </head>
<body>
<div class="container-fluid-full">
    <div class="row-fluid"> 
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="left-side sidebar-offcanvas">
        @include('partials.sidebar')
        </aside>

        <!-- Right side column. Contains the navbar and content of the page -->
        <div id="content" class="span10">
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="{{url('/home/dashboard')}}">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="#">Report</a>  
                    <i class="icon-angle-right"></i>             
                </li>    
                 <li>
                    <a href="#">Competitor Activity Report</a>                          
                </li>    
            </ul>

            <div class="row-fluid sortable">   
                
               <div class="box span12">
                <div class="box-header" data-original-title>
                    <h2><i class="halflings-icon user"></i><span class="break"></span>Laporan Competitor Activity Per SF</h2>
                </div>
                    <div class="box-content">    
                         <div class="span12">
                             @foreach($data['sales'] as $val)
                                <h1>Sales Force : {!! $val->nama !!}</h1>
                             @endforeach
                             @foreach($data['sales'] as $val)
                                <div class="priority medium"><span>NIK: {{$val->nik}}</span></div>
                             @endforeach
                            <div class="task high">
                                 @foreach($data['area'] as $val)
                                 <div class="span3">
                                    <div class="title">Area</div>
                                    <div> {!! $val->nm_area !!}</div>
                                </div>
                                 @endforeach  
                                <div class="span3">
                                    <div class="title">Periode</div>
                                    <div>{{$data['periode']}}</div>
                                </div>
                                 
                            </div>
                        </div>
                
                    
                        <table class="table table-striped table-bordered bootstrap-datatable">
                            <thead>
                                <tr>               
                                    <th style="text-align:center;" rowspan="2" colspan="1">Kode Outlet</th>
                                    <th style="text-align:center;" rowspan="2" colspan="1">Nama Outlet</th>                                   
                                    <th style="text-align:center;" rowspan="2" colspan="1">Nama Kompetitor</th>
                                    <th style="text-align:center;" rowspan="2" colspan="1">Tanggal Take Photo</th>
                                    <th style="text-align:center;" rowspan="2" colspan="1">Tanggal Upload</th>
                                    <th style="text-align:center;" rowspan="2" colspan="1">Nama Photo</th>
                                    <th style="text-align:center;" rowspan="2" colspan="1">Link Photo</th>
                                    <th style="text-align:center;" rowspan="2" colspan="1">Photo</th>
                                    <th style="text-align:center;" rowspan="2" colspan="1">Keterangan</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data['competitorAct'] as $row)
                                <tr>                         
                                    <td style="text-align:center;">{{ $row->kd_outlet }}</td>
                                    <td style="text-align:center;">{{ $row->nm_outlet }}</td>

                                    <td style="text-align:center;">{{ $row->nm_competitor }}</td>  
                                    <td style="text-align:center;">{{ date('d-F-Y H:i:s', strtotime($row->date_take_photo))  }}</td>
                                    <td style="text-align:center;">{{ date('d-F-Y H:i:s', strtotime($row->date_upload_photo)) }}</td>
                                    <td style="text-align:center;">{{ $row->nm_photo }}</td>     
                                    <td style="text-align:center;"><a href="{!! url('image_upload/competitor/'.$row->path_photo) !!}">{{ $row->path_photo }}</a></td> 
                                    <td style="text-align:center;"><img style="max-height: 60px; max-width: 60px;" src="{!! url('image_upload/competitor/'.$row->path_photo) !!}" class="img-responsive" /></td>   
                                    <td style="text-align:center;">{{ $row->keterangan }}</td>                            
                               </tr>
                               @endforeach  
                               
                            </tbody>
                        </table>
                           
                        </div>
                    </div>
                </div>
            </div> 
        </div>  
    </div>
</div>

@include('partials.footer')
    </body>
</html>