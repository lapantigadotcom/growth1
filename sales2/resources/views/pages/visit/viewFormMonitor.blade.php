@extends('layouts.app')

@section('content')
@section('title', 'Visiting Monitor List')

<div id="content" class="span10">
	<ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="{{url('/home/dashboard')}}">Homes</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">Visit Plan</a>  
            <i class="icon-angle-right"></i>             
        </li>    
         <li>
            <a href="#">Visit Monitor List Filter</a>                          
        </li>    
    </ul>

	<div class="row-fluid sortable">
		<div class="box span12">
			<div class="box-header" data-original-title>
				<h2><i class="halflings-icon edit"></i><span class="break"></span>Filter Data Date Visit</h2>
			</div>
			    {!! Form::open(array('route' => 'admin.visitingMonitor.result', 'method' => 'POST' )) !!}
			    	@include('pages.visit.filterVisitMonitor')
			    {!! Form::close() !!}
		</div>
	</div>
        
        <div class="row-fluid sortable">        
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon check"></i><span class="break"></span>Visiting Monitor 1 Minggu Terakhir</h2>
            </div>
            <br>
                <div class="box-content">                                 
                    <table class="table table-striped table-bordered bootstrap-datatable datatable" id="result">
                        <thead>
                            <tr>  
                                <th style="text-align:center;">#</th>
                                <th style="text-align:center;">Kode Visit</th>
                                <th style="text-align:center;">Kode Outlet</th>
                                <th style="text-align:center;">Sales Force</th>             
                               <th style="text-align:center;">Nama Outlet</th>

                                <th style="text-align:center;">Alamat Outlet</th>
                                <th style="text-align:center;">Kota Outlet</th>
                                <th style="text-align:center;">Kode Pos</th>
                                <th style="text-align:center;">Actual Visit</th>
                                <th style="text-align:center;">Status Visit</th>
                                <th style="text-align:center;">Skip Order Reason</th>
                                <th style="text-align:center;">Unvisited Reason</th>

                                @if(Auth::user()->kd_role != 3)   
                                <th style="text-align:center;">Action</th>
                                @else
                                <th></th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach($data as $row)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{ $row->id }}</td>
                                <td>{{ $row->kd_outlet }}</td>
                                <td>{{ $row->nama }}</td>                     
                                <td>{{ $row->nm_outlet }}</td>
 
                                <td>{{ $row->almt_outlet }}</td>
                                <td>{{ $row->nm_kota }}</td>
                                <td>{{ $row->kodepos }}</td>
                                @if($row->date_visiting != '0000-00-00 00:00:00')
                                    <td>{{ date('d-F-Y H:i:s', strtotime($row->date_visiting)) }}</td>
                                @else
                                    <td> - </td>
                                @endif
                                @if($row->status_visit == 1)
                                    <td class="center">
                                        <span class="label label-success">Finished</span>
                                    </td>
                                @elseif($row->status_visit == 0)
                                    @if((new DateTime($row->date_visit))->diff(new DateTime("now"))->format('%a') < 3)
                                        <td class="center">
                                            <span class="label label-warning">Waiting</span>
                                        </td>
                                    @else
                                        <td class="center">
                                            <span class="label label-important">Unvisited</span>
                                        </td>
                                    @endif
                                @else
                                <td class="center">
                                    <span class="label label-warning">Pending</span>
                                </td>
                                @endif 

                                @if($row->skip_order_reason == '')
                                    <td> - </td>
                                @else
                                    <td>{{ $row->skip_order_reason }}</td> 
                                @endif 

                                <td>{{ $row->skip_reason }}</td>    

                                <td>      
                                    @if(Auth::user()->hasAccess('admin.visitMonitor.edit'))                             
                                    <a href="{!! route('admin.visitMonitor.edit',[$row->id]) !!}" class="icon-pencil"></a>  
                                    @endif 
                                    &nbsp;             
                                    @if(Auth::user()->hasAccess('admin.visitMonitor.delete'))              
                                    <a href="{!! route('admin.visitMonitor.delete',[$row->id]) !!}" onclick="return confirm('Are You Sure?')" class="icon-trash"></a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</div>	    

@endsection

@section('custom-head')
    
@endsection

@section('custom-footer')
    
@endsection
