@extends('layouts.app')

@section('content')
<div id="content" style="min-height:800px;" class="span10">  

	<ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="{{url('/home/dashboard')}}">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">Visit Plan</a>
            <i class="icon-angle-right"></i>           
        </li>
         <li>
            <a href="#">Visit Plan List</a>
            <i class="icon-angle-right"></i>
            
        </li>
        <li>
            <a href="#">Approval Visit Plan</a>
        </li>
    </ul>               
        {!! Form::model($data,array('route' =>'admin.visit.approved', 'method' => 'POST')) !!}
            @include('pages.visit.formApprove',array('submit' => 'Perbarui'))
        {!! Form::close() !!}
             
</div>
@endsection


@section('custom-head')
    
@endsection

@section('custom-footer')
    
@endsection