<div class="box-body">
	<div class="form-group">

		{!! Form::label('kd_outlet','Nama Outlet') !!}
		 <select name="kd_outlet" id="outlet">
		    <option>Select Outlet</option>
		    @foreach ($outlet as $outlets)
		      <option value="{{ $outlets->kd_outlet }}">{{ $outlets->nm_outlet}}</option>
		    @endforeach
	    </select>
            <br>
                {!! Form::label('distributor','Nama Distributor : ') !!}
                <div id="dist" name="dist"> </div>
            <br>
	    	{!! Form::label('username','Nama Sales Force : ') !!}
	    	<div id="user" name="user"> </div>
            <br> 
		
		{!! Form::label('date_visit','Tanggal dan Waktu Visit') !!}
		{!! Form::text('date_visit', '', array('id' => 'datepicker')) !!}

                {!! Form::hidden('approve_visit', '2', array('class' => 'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
	</div>

	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	<script>
	  $(function() {
	    $('#datepicker').datepicker({ dateFormat: 'yy-mm-dd' }).val();
	  });
	</script>
       <script>
	  $('#outlet').on('change', function(e) {
            console.log(e);
            var kd_outlet = e.target.value;
            var outlet = <?php echo json_encode($outlet); ?>;
            var dist = <?php echo json_encode($dist); ?>;
            var kd_dist = 0;
            //ajax
            $.getJSON('{{url('/ajax-call?kd_outlet=')}}'+kd_outlet, function (data) {
				//console.log(data);
                $('#user').empty();
              	$.each(data, function(index, userObj){
                	$('#user').append('<div>'+userObj.nama+'</div>');
 				});
            });

            $.each(outlet, function(index, outletObj){
                if(outletObj.kd_outlet == kd_outlet){
                    kd_dist = outletObj.kd_dist;
                    console.log(kd_dist);
                }
            });
            $.each(dist, function(index, distObj){
                if(distObj.id == kd_dist){
                    console.log(distObj.nm_dist);
                    $('#dist').empty();
                    $('#dist').append('<div>'+distObj.nm_dist+'</div>');
                }
            });
        });
	</script>
</div>