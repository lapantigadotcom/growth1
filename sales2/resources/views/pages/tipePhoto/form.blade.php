<div class="span3">
	<div class="box-body">
		<div class="form-group">
			{!! Form::label('nama_tipe','Nama Tipe Photo') !!}
			{!! Form::text('nama_tipe',null, array('class' => 'form-control')) !!}		

			<div class="form-group">
				@if ($errors->has())
					<div class="alert alert-error">
					  @foreach ($errors->all() as $error)
					    {!! $error !!}<br />		
					  @endforeach
					</div>
				@endif
				{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
			</div>
		
</div></div></div>		






