@extends('layouts.app')

@section('content')
<div id="content" class="span10">  
	<ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="{{url('/home/dashboard')}}">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">Photo Activity</a>
            <i class="icon-angle-right"></i> 
        </li>
         <li>
            <a href="#">Create Tipe Photo</a>
        </li>
    </ul>

    {!! Form::open(array('route' => 'admin.tipephoto.store', 'method' => 'POST')) !!}
        @include('pages.tipePhoto.form',array('submit' => 'Simpan'))
    {!! Form::close() !!}
</div>
@endsection

@section('custom-head')
    
@endsection

@section('custom-footer')
    
@endsection