<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Hisamitsu Panel | Admin Menu</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        @include('partials.head')
        <script src="//cdn.ckeditor.com/4.5.2/standard/ckeditor.js"></script>
    </head>
<body>
<div class="container-fluid-full">
    <div class="row-fluid"> 
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="left-side sidebar-offcanvas">
        @include('partials.sidebar')
        </aside>

        <!-- Right side column. Contains the navbar and content of the page -->
        <div id="content" class="span10">
        <ul class="breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="{{url('/home/dashboard')}}">Home</a> 
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a href="#">Configuration</a>
                <i class="icon-angle-right"></i>
            </li>
             <li>
                <a href="#">Admin Menu</a>
            </li>
        </ul>
        <div class="row-fluid sortable">        
            <div class="box span12">
                <div class="box-header" data-original-title>
                    <h2><i class="halflings-icon user"></i><span class="break"></span>Admin Menu List</h2>
                </div>
                <br>
                <a href="{!! route('admin.adminmenu.create') !!}" class="btn btn-primary">Tambah Admin Menu</a>
           
                    <div class="box-content">                                 
                        <table class="table table-striped table-bordered bootstrap-datatable datatable">
                            <thead>
                                <tr>               
                                    <th>Menu</th>
                                    <th>Route</th>
                                    <th>Child</th>
                                    <th>Role Pengakses</th>
                                    <th>Enable</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data['content'] as $row)
                                <tr>                          
                                    <td>{{ $row->nm_menu }}</td>
                                    @if($row->route != '')
                                        <td>{{ $row->route }}</td>
                                    @else
                                        <td><span class="label label-success">Undefined</span></td>
                                    @endif

                                    <td>
                                    <?php $tmp = array(); ?>
                                        @foreach($row->child as $v)
                                            <?php array_push($tmp, $v->nm_menu);  ?>
                                        @endforeach
                                    {!! implode(', ', $tmp) !!}
                                    </td>
                                    <td>
                                     <?php $tmp = array(); ?>
                                    	@foreach($row->roles as $val)
                                    		 <?php array_push($tmp, $val->type_role);  ?>
                                    	@endforeach
                                    {!! implode(', ', $tmp) !!}
                                    </td>
                                    @if($row->enable == 1)
                                        <td>YES</td> 
                                    @else
                                        <td>NO</td> 
                                    @endif                       
                                    <td>                                   
                                    <a href="{!! route('admin.adminmenu.edit',[$row->id]) !!}" class="icon-pencil"></a>  
                                    &nbsp;                            
                                    <a href="{!! route('admin.adminmenu.delete',[$row->id]) !!}"  onclick="Are you sure?" class="icon-trash"></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div><!-- /.row (main row) -->
    </div>
</div>

@include('partials.footer')
    </body>
</html>