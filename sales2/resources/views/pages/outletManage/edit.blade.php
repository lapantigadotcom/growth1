@extends('layouts.app')

@section('content')
@section('title', 'Outlet Management')
<div id="content" class="span10">  

    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="{{url('/home/dashboard')}}">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">Configuration</a>
            <i class="icon-angle-right"></i>           
        </li>
         <li>
            <a href="#">Outlet Management</a>
        </li>
    </ul>

    <div class="row-fluid">     
        <div class="box span12">
            <div class="box-header">
                <h2><i class="halflings-icon th"></i><span class="break"></span>Outlet Management</h2>
            </div>
                    <div class="box-content">   
                        <h1>Toleransi Jarak</h1>                    
                        {!! Form::open(array('route' => 'admin.outletManage.updateToleransi', 'method' => 'store' )) !!}
                            <div class="form-group">
                                {!! Form::label('toleransi_long','*dalam meter') !!}
                                {!! Form::text('toleransi_long',null, array('class' => 'form-control')) !!}
                                <div class="form-group">
                                    {!! Form::submit('Perbarui', array('class' => 'btn btn-primary')) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
          
                <div class="box-content">  
                  <h1>Tipe Outlet</h1>
                    @if(Auth::user()->hasAccess('admin.outlet.create'))  
                        <a href="{!! route('admin.tipe.create') !!}" class="btn btn-primary">Tambah Tipe Outlet </a>
                    @endif   
                     <table class="table table-striped table-bordered bootstrap-datatable datatable">
                        <thead>
                            <tr>                
                                <th>Tipe Outlet</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $row)
                            <tr>                 
                                <td>{{ $row->nm_tipe }}</td>
                                <td>         
                                    <a href="{!! route('admin.tipe.edit',[$row->id]) !!}" class="icon-pencil"></a>
                                    &nbsp;
                                    <a href="{!! route('admin.tipe.delete',[$row->id]) !!}" onclick="return confirm('Are You Sure')" class="icon-trash"></a>          
                                </td>
                            </tr>
                            @endforeach
                        </tbody> 
                    </table>
                </div>
         </div>
    </div>
</div>
@endsection

@section('custom-head')
    
@endsection

@section('custom-footer')
    
@endsection