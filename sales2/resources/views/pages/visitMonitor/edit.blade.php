@extends('layouts.app')

@section('content')
<div id="content" class="span10">  

	<ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="{{url('/home/dashboard')}}">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">Visit Plan</a>
            <i class="icon-angle-right"></i>           
        </li>
         <li>
            <a href="#">Visiting Monitor</a>
            <i class="icon-angle-right"></i>
            
        </li>
        <li>
            <a href="#">Edit Visiting</a>
        </li>
    </ul>               
        {!! Form::model($data['content'],array('route' => ['admin.visitMonitor.update',$data['content']->id], 'method' => 'PUT')) !!}
            @include('pages.visitMonitor.form',array('submit' => 'Perbarui'))
        {!! Form::close() !!}
             
</div>
@endsection


@section('custom-head')
    
@endsection

@section('custom-footer')
    
@endsection