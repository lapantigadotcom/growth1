<div class="span3">
	<div class="box-body">
		<div class="form-group">
			{!! Form::label('kd_area','Nama Area') !!}
			{!! Form::select('kd_area',$areas, null, array('id'=>'area','class' => 'form-control')) !!}
			{!! Form::label('kd_kota','Nama Kota') !!}
			{!! Form::select('kd_kota',$city, null, array('class' => 'form-control')) !!}
		    {!! Form::label('nm_outlet','Nama Outlet') !!}
			{!! Form::text('nm_outlet',null, array('class' => 'form-control')) !!}
			{!! Form::label('kd_outlet','Kode Outlet') !!}
 			{!! Form::text('kd_outlet',null, array('class' => 'form-control', 'disabled' => 'disabled')) !!}

			{!! Form::label('kd_user', 'Nama Sales') !!}
		    {!! Form::select('kd_user', $users, null, ['class' => 'form-control']) !!}
		    {!! Form::label('kd_dist', 'Kode Distributor') !!}
		    {!! Form::select('kd_dist', $dist, null, ['class' => 'form-control']) !!}	
</div></div></div>
<div class="span3">
	<div class="box-body">
		<div class="form-group">
			{!! Form::label('almt_outlet','Alamat') !!}
			{!! Form::textarea('almt_outlet',null, array('class' => 'form-control')) !!}
			{!! Form::label('kodepos','Kode Pos') !!}
			{!! Form::text('kodepos',null, array('class' => 'form-control')) !!}
			{!! Form::label('tipe_outlet','Tipe') !!}
			{!! Form::select('kd_tipe', $tipe, null, array('class' => 'form-control')) !!}
			{!! Form::label('nm_pic','Nama PIC') !!}
			{!! Form::text('nm_pic',null, array('class' => 'form-control')) !!}
			{!! Form::label('tlp_pic','Tlp PIC') !!}
			{!! Form::text('tlp_pic',null, array('class' => 'form-control')) !!}
		
</div></div></div>	
<div class="span3">
	<div class="box-body">
		<div class="form-group">
			{!! Form::label('reg_status','Register Status') !!}
			{!! Form::select('reg_status',[
						   'YES' => 'YES',
						   'NO' => 'NO',
						   ]) !!}			  

			{!! Form::label('rank_outlet','Rank') !!}
			{!! Form::select('rank_outlet', [
						   'A' => 'A',
						   'B' => 'B',
						   'C' => 'C',
						   'D' => 'D',
						   'E' => 'E',
						   'F' => 'F']
						) !!}  
			{!! Form::label('longitude','Longitude') !!}
			{!! Form::text('longitude',null, array('class' => 'form-control')) !!}
			{!! Form::label('latitude','Latitude') !!}
			{!! Form::text('latitude',null, array('class' => 'form-control')) !!}
			{!! Form::label('status_area','Status Area') !!}
			{!! Form::select('status_area',[
					   '1' => 'Good Coverage',
					   '0' => 'Bad Coverage',
					   ], null) !!}		<br><br>
			<div class="control-group error">
				<div class="controls">
					{!! Form::label('path_photo','Upload Foto') !!}
					{!! Form::file('path_photo',null, array('class' => 'form-control')) !!}
				 	<span class="help-inline">*) Required</span>
				</div>
			</div>
			<div class="form-group">
				{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
			</div>
		</div>
	</div>
</div>	


<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script>
	$('#area').on('change', function(e) {
        console.log(e);
        var id = e.target.value;
        //ajax
        $.getJSON('{{url('/ajax-city?id=')}}'+id, function (data) {
			//console.log(data);
            $('#city').empty();
          	$.each(data, function(index, cityObj){
            	 $('#city').append('<option value="'+cityObj.id+'">'+cityObj.nm_kota+'</option>');
				});
        });
    });
</script>






