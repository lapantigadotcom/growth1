<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Hisamitsu Panel | Outlet Name: {!! $data['content']->nm_outlet!!}</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        @include('partials.head')
        <script src="//cdn.ckeditor.com/4.5.2/standard/ckeditor.js"></script>

    </head>
<body>
<div class="container-fluid-full">
    <div class="row-fluid"> 
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="left-side sidebar-offcanvas">
        @include('partials.sidebar')
        </aside>

<!-- Main content -->
<div id="content" class="span9">
  <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="{{url('/home/dashboard')}}">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">My Profile</a>
        </li>
    </ul>
        <div class="row-fluid sortable">        
                <div class="box span12">
                    
                    <div class="box-content">
                        <div class="row-fluid">            
                            <div class="span3">
                                <h1>Profil Outlet</h1>
                                @if(File::exists('image_upload/outlet/'.$data['content']->foto_outlet) and $data['content']->foto_outlet != '')
                                    <img width="300" height="300" src="{!! url('image_upload/outlet/'.$data['content']->foto_outlet) !!}" class="img-responsive" />
                                @else
                                    <img src="{!! url('/img/outlet.png') !!}" class="img-responsive img-thumbnail">
                                @endif
                            </div>

                            <div class="span8">
                               
                                <h1>Detail </h1>
                                <div class="priority medium"><span>Nama Outlet : {!! $data['content']->nm_outlet!!}</span></div>
                          
                                <div class="task medium">
                                    <div class="span4">
                                        <div class="desc">
                                            <div class="title"><i class=" halflings-icon user"></i> Sales Force </div>
                                            <div>{!! $data['content']->nama !!}</div>
                                        </div>
                                        <div class="desc">
                                            <div class="title"><i class=" halflings-icon barcode"></i> Kode Outlet</div>
                                            <div>{!! $data['content']->kd_outlet !!}</div>
                                        </div>
                                        <div class="desc">
                                            <div class="title"><i class=" halflings-icon th"></i> Area</div>
                                            <div>{!! $data['content']->nm_area !!}  ( {!! $data['content']->kd_area !!}) </div>
                                        </div>
                                        <div class="desc">
                                            <div class="title"><i class=" halflings-icon home"></i> Alamat </div>
                                            <div>{!! $data['content']->almt_outlet !!}</div>
                                        </div>
                                        <div class="desc">
                                            <div class="title"><i class=" halflings-icon briefcase"></i> Kota </div>
                                            <div>{!! $data['content']->nm_kota !!}</div>
                                        </div>
                                        <div class="desc">
                                            <div class="title"><i class=" halflings-icon envelope"></i> Kode Pos</div>
                                            <div>{!! $data['content']->kodepos !!}</div>
                                        </div>
                                        <div class="desc">
                                            <div class="title"><i class=" halflings-icon print"></i> Distributor </div>
                                            <div>{!! $data['content']->nm_dist !!}</div>
                                        </div>
                                        <div class="desc">
                                            <div class="title"><i class="halflings-icon user"></i> Nama PIC </div>
                                            <div>{!! $data['content']->nm_pic !!}</div>
                                        </div>
                                    </div>
                                    <div class="span4">
                                         
                                         <div class="desc">
                                            <div class="title"><i class="halflings-icon phone"></i> Tlp PIC </div>
                                            <div>{!! $data['content']->tlp_pic !!}</div>
                                        </div>

                                        <div class="desc">
                                            <div class="title"><i class="halflings-icon list-alt"></i> Tipe </div>
                                            <div>{!! $data['content']->nm_tipe !!}</div>
                                        </div>
                                        <div class="desc">
                                            <div class="title"><i class=" halflings-icon star"></i> Rank </div>
                                            <div>{!! $data['content']->rank_outlet !!}</div>
                                        </div>
                                        <div class="desc">
                                            <div class="title"><i class=" halflings-icon map-marker"></i> Longitude</div>
                                            @if($data['content']->longitude != '')
                                            <div>{!! $data['content']->longitude !!}</div>
                                            @else
                                            <td><span class="label label-success">Undefined</span></td>
                                            @endif
                                        </div>
                                        <div class="desc">
                                            <div class="title"><i class=" halflings-icon map-marker"></i> Latitude</div>
                                              @if($data['content']->latitude != '')
                                            <div>{!! $data['content']->latitude!!}</div>
                                            @else
                                            <td><span class="label label-success">Undefined</span></td>
                                            @endif
                                        </div>
                                        <div class="desc">
                                            <div class="title"><i class=" halflings-icon plus"></i> Toleransi Max</div>
                                            @if($data['content']->toleransi_long != '')
                                            <div>{!! $data['content']->toleransi_long !!} Meter</div>
                                            @else
                                            <td><span class="label label-success">Undefined</span></td>
                                            @endif
                                        </div>
                                         
                                        <div class="desc">
                                            <div class="title"><i class=" halflings-icon signal"></i> Status Outlet</div>
                                            @if($data['content']->status_area == 0)
                                             <div>Bad Coverage</div> 
                                             @else
                                             <div>Good Coverage</div> 
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div><!-- /.box-body -->                
                </div><!-- /.box -->
            </div>
        </div><!-- /.box -->
    </div>
</div>

@include('partials.footer')
    </body>
</html>