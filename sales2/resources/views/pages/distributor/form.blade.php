<div class="box-body">
	<div class="form-group">
		{!! Form::label('kd_dist','Kode Distributor') !!}
		{!! Form::text('kd_dist',null, array('class' => 'form-control')) !!}
		{!! Form::label('nm_dist','Nama Distributor') !!}
		{!! Form::text('nm_dist',null, array('class' => 'form-control')) !!}
		{!! Form::label('kd_tipe','Tipe') !!}
		{!! Form::select('kd_tipe', [
						   'Distributor' => 'Distributor',
						   'Sub Distributor' => 'Sub Distributor']
						) !!} 
		{!! Form::label('almt_dist','Alamat') !!}
		{!! Form::text('almt_dist',null, array('class' => 'form-control')) !!}
		{!! Form::label('kd_kota','Kota') !!}
		{!! Form::select('kd_kota', $data['city'], null, array('class' => 'form-control')) !!}
		{!! Form::label('telp_dist','Telepon') !!}
		{!! Form::text('telp_dist',null, array('class' => 'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
	</div>
</div>