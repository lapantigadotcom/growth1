<div class="span3">
	<div class="box-body">
		<div class="form-group">
			{!! Form::hidden('kd_role', '3', array('class' => 'form-control')) !!}
			{!! Form::label('nik','NIK') !!}
			{!! Form::text('nik',null, array('class' => 'form-control')) !!}
			{!! Form::label('nama','Nama') !!}
			{!! Form::text('nama',null, array('class' => 'form-control')) !!}
			{!! Form::label('join_date','Join Date') !!}
			{!! Form::text('join_date', null, array('id' => 'datepicker')) !!}
			{!! Form::label('kd_area','Area') !!}
			{!! Form::select('kd_area', $area, null, ['class' => 'form-control']) !!}
			{!! Form::label('telepon','Telepon') !!}
			{!! Form::text('telepon',null, array('class' => 'form-control')) !!}
</div></div></div>

<div class="span3">
	<div class="box-body">
		<div class="form-group">	
		{!! Form::label('alamat','Alamat') !!}
		{!! Form::textarea('alamat',null, array('class' => 'form-control')) !!}
		{!! Form::label('email_user','Email') !!}
		{!! Form::text('email_user',null, array('class' => 'form-control')) !!}		
</div></div></div>

<div class="span3">
	<div class="box-body">
		<div class="form-group">		
		{!! Form::label('username','Username') !!}
		{!! Form::text('username',null, array('class' => 'form-control')) !!}
		{!! Form::label('password','Password') !!}
		{!! Form::password('password',null, array('class' => 'form-control')) !!}
		{!! Form::label('foto','Upload Foto') !!}
		{!! Form::file('foto',null, array('class' => 'form-control')) !!}<br><br><br>
		<div class="form-group">
		  	@if ($errors->has())
				<div class="alert alert-error">
				  @foreach ($errors->all() as $error)
				    {!! $error !!}<br />		
				  @endforeach
				</div>
			@endif
			{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
		</div>
</div></div></div>

<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script>
	$(function() {
	  $('#datepicker').datepicker({ dateFormat: 'yy-mm-dd' }).val();
	});
</script>
