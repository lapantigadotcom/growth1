<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Hisamitsu Panel | Sales Force List</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        @include('partials.head')
        <script src="//cdn.ckeditor.com/4.5.2/standard/ckeditor.js"></script>

    </head>
<body>
<div class="container-fluid-full">
    <div class="row-fluid"> 
<!-- Left side column. contains the logo and sidebar -->
<aside class="left-side sidebar-offcanvas">
@include('partials.sidebar')
</aside>

<!-- Main content -->
<div id="content" class="span10">
  <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="{{url('/home/dashboard')}}">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">User Manager</a>
            <i class="icon-angle-right"></i>
            
        </li>
        <li>
            <a href="#">Sales Force</a>
        </li>
    </ul>
    <div class="row-fluid sortable">        
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>Sales List</h2>
            </div><br>
                @if(Auth::user()->hasAccess('admin.sales.create')) 
                    <a href="{!! route('admin.sales.create') !!}" class="btn btn-primary">Tambah Sales </a>
                @endif
                <div class="box-content">                                 
                    <table class="table table-striped table-bordered bootstrap-datatable datatable">
                        <thead>
                            <tr>
                                <th>&nbsp;</th>
                                <th>NIK</th>
                                <th>Nama</th>
                                <th>Alamat</th>
                                <th>Telepon</th>
                                <th>Area</th>
                                <th>Join Date</th>
                                <th>Role</th>
                                @if(Auth::user()->kd_role != 3) 
                                   <th style="text-align:center;">Action</th>
                                @else
                                   <th style="text-align:center;"></th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                         
                            @foreach($data as $row)
                            <tr>
                                <td>
                                @if(Auth::user()->hasAccess('admin.sales.show')) 
                                    <a href="{!! route('admin.sales.show',[$row->id]) !!}" data-toggle="tooltip" data-placement="bottom" title="Detail Sales"><i class="icon-zoom-in"></a></td>
                                @endif
                                <td>{{ $row->nik }}</td>
                                <td>{{ $row->nama }}</td>
                                <td>{{ $row->alamat }}</td>
                                <td>{{ $row->telepon }}</td>
                                <td>{{ $row->kd_area }}</td>
                                <td>{{ date('d-F-Y', strtotime($row->join_date)) }}</td>
                                <td>{{ $row->type_role }}</td>
                                @if(Auth::user()->kd_role != 3)
                                    <td>         
                                    @if(Auth::user()->hasAccess('admin.sales.edit')) 
                                        <a href="{!! route('admin.sales.edit',[$row->id]) !!}" class="icon-pencil"></a>
                                    @endif
                                        &nbsp;
                                    @if(Auth::user()->hasAccess('admin.sales.delete')) 
                                        <a href="{!! route('admin.sales.delete',[$row->id]) !!}" onclick="return confirm('Are You Sure?')" class="icon-trash"></a>
                                    @endif
                                    </td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div><!-- /.row (main row) -->
</div>
</div>

@include('partials.footer')
    </body>
</html>