@extends('layouts.app')

@section('content')
<div id="content" class="span10">  
	<ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="{{url('/home/dashboard')}}">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">Master Data</a>
            <i class="icon-angle-right"></i>   
        </li>
        <li>
            <a href="#">Outlet</a>
        </li>
         <li>
            <a href="#">Edit Outlet</a>
        </li>
    </ul>
    {!! Form::model($data['content'],array('route' => ['admin.photo.update',$data['content']->id], 'method' => 'PUT','files' => true)) !!}
        @include('pages.photo.form',array('submit' => 'Perbarui'))
    {!! Form::close() !!}
</div>
@endsection


@section('custom-head')
    
@endsection

@section('custom-footer')
    
@endsection