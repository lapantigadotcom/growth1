<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Hisamitsu Panel | Photo Outlet Activity</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
            @include('partials.head')
        <script src="//cdn.ckeditor.com/4.5.2/standard/ckeditor.js"></script>

    </head>
<body>

<div class="container-fluid-full">
    <div class="row-fluid"> 
        <aside class="left-side sidebar-offcanvas">
            @include('partials.sidebar')
        </aside>
        <div id="content" class="span10">
          <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="{{url('/home/dashboard')}}">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="#">Photo Activity</a>
                    <i class="icon-angle-right"></i>
                    
                </li>
                <li>
                    <a href="#">Photo Display</a>
                </li>
            </ul>
<div class="row-fluid">     
        <div class="box span12">
            <div class="box-header">
               <h2><i class="halflings-icon user"></i><span class="break"></span>Photo List</h2>
            </div>
            <div class="box-content">
                        <div class="box-content">              
                            @if(Auth::user()->hasAccess('admin.photo.create'))  
                                <a href="{!! route('admin.photo.create') !!}" class="btn btn-primary">Tambah Photo </a>
                            @endif                   
                            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                            <thead>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>Nama Outlet</th>
                                    <th>Tipe</th>
                                    <th>Sales Force </th>
                                    <th>Nama Foto </th>
                                    <th>Date Take Foto</th>
                                    <th>Date Upload</th>
                                    <th>Foto</th>
                                    @if(Auth::user()->kd_role != 3)   
                                    <th>Action</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $row)
                                <tr>
                                    <td><a href="#"  data-toggle="tooltip" data-placement="bottom" title="Lihat Detail Photo"><i class="icon-zoom-in"></a></td>
                                    <td>{{ $row->nm_outlet }}</td>
                                    <td>{{ $row->nama_tipe }}</td>
                                    <td>{{ $row->nama }}</td>
                                    <td>{{ $row->nm_photo }}</td>
                                    <td>{{ $row->date_take_photo }}</td>
                                    <td>{{ $row->date_upload_photo }}</td>
                                    @if(File::exists('image_upload/outlet_photoact/'.$row->path_photo) and $row->path_photo != '')
                                    <td> <a href="{!! url('image_upload/outlet_photoact/'.$row->path_photo) !!}" target="_blank" title="{{ $row->nm_photo }}">
                                        <img style="max-height: 60px; max-width: 60px;" src="{!! url('image_upload/outlet_photoact/'.$row->path_photo) !!}" class="img-responsive" /></a>
                                    </td>
                                    @else
                                       <td><img style="max-height: 60px; max-width: 60px;" src="{!! url('/img/no_image_outlet_upload.png') !!}" class="img-responsive" /></td>
                                    @endif

                                    @if(Auth::user()->kd_role != 3)   
                                    <td>         
                                    @if(Auth::user()->hasAccess('admin.photo.edit'))
                                        <a href="{!! route('admin.photo.edit',[$row->id]) !!}" class="icon-pencil"></a>
                                    @endif
                                        &nbsp;
                                    @if(Auth::user()->hasAccess('admin.photo.delete'))
                                        <a href="{!! route('admin.photo.delete',[$row->id]) !!}" onclick="return confirm('Are You Sure')" class="icon-trash"></a>   
                                    @endif           
                                    </td>
                                    @endif
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>                
                </div>
            </div>
        </div>       
    </div>   
        </div>
    </div>
</div>
        @include('partials.footer')
    </body>
</html>