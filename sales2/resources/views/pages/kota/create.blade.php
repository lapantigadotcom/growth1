@extends('layouts.app')

@section('content')

<div id="content" class="span10"> 
    {!! Form::open(array('route' => 'admin.kota.store', 'method' => 'POST')) !!}
    @include('pages.kota.form',array('submit' => 'Simpan'))
    {!! Form::close() !!}
</div>

@endsection

@section('custom-head')
    
@endsection

@section('custom-footer')
    
@endsection