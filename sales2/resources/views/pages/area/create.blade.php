@extends('layouts.app')

@section('content')
	<div id="content" class="span10">
		<ul class="breadcrumb">
	        <li>
	            <i class="icon-home"></i>
	            <a href="{{url('/home/dashboard')}}">Home</a> 
	            <i class="icon-angle-right"></i>
	        </li>
	        <li>
	            <a href="#">Master Data</a>
	            <i class="icon-angle-right"></i>            
	        </li>
	        <li>
	            <a href="#">Area</a>
	              <i class="icon-angle-right"></i> 
	        </li>
	        <li>
	            <a href="#">Add Area</a>
	        </li>
    	</ul>
		    {!! Form::open(array('route' => 'admin.area.store', 'method' => 'POST')) !!}
		        @include('pages.area.form',array('submit' => 'Simpan'))
		    {!! Form::close() !!}
	</div>

@endsection

@section('custom-head')
    
@endsection

@section('custom-footer')
    
@endsection