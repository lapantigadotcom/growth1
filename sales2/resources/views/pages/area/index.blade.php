<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Hisamitsu Panel | Area List</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        @include('partials.head')
        <script src="//cdn.ckeditor.com/4.5.2/standard/ckeditor.js"></script>
    </head>
<body>
<div class="container-fluid-full">
    <div class="row-fluid">  
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="left-side sidebar-offcanvas">
        @include('partials.sidebar')
        </aside>
       
        <div id="content" class="span10">
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="{{url('/home/dashboard')}}">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="#">Master Data</a>
                    <i class="icon-angle-right"></i>    
                </li>
                <li>
                    <a href="#">Area</a>
                </li>
            </ul>

			<div class="row-fluid">		
				<div class="box span12">
					<div class="box-header">
						<h2><i class="halflings-icon th"></i><span class="break"></span>Area List</h2>
					</div>
					<div class="box-content">
						<ul class="nav tab-menu nav-tabs" id="myTab">
							<li class="active"><a href="#areaCity">Area-City</a></li>
							<li><a href="#areaOnly">Area Only</a></li>
						</ul>
						 
						<div id="myTabContent" class="tab-content">				
							<div class="tab-pane active" id="areaCity">
			                    <div class="box-content">                                 
			                        <table class="table table-striped table-bordered bootstrap-datatable datatable">
			                            <thead>
			                                <tr>
			                                    <th>Kode Area</th>                   
			                                    <th>Nama Area</th>
			                                    <th>Nama Kota</th>
			                                    @if(Auth::user()->kd_role != 3) 
                                                            <th style="text-align:center;">Action</th>
                                                            @else
                                                            <th style="text-align:center;"></th>
                                                            @endif
			                                </tr>
			                            </thead>
			                            <tbody>
			                                @foreach($data['all'] as $row)
			                                    <tr>
			                                        @if($row->kd_area != '' )
			                                            <td>{{ $row->kd_area }}</td>
			                                            <td>{{ $row->nm_area }}</td>
			                                        @else
			                                            <td><span class="label label-warning">Area belum tersedia</span></td>
			                                            <td><span class="label label-error">Unregistered</span></td>
			                                        @endif
			                                        <td>{{ $row->nm_kota }}</td>
			                                        <td>   
			                                        @if(Auth::user()->hasAccess('admin.kota.edit'))                                
			                                        <a href="{!! route('admin.kota.edit',[$row->id]) !!}" class="icon-pencil"></a>
			                                        @endif
			                                        &nbsp;
			                                        @if(Auth::user()->hasAccess('admin.kota.delete'))      
			                                        <a href="{!! route('admin.kota.delete',[$row->id]) !!}" onclick="return confirm('Are You Sure?')" class="icon-trash"> </a>
			                                        @endif
			                                        </td>
			                                    </tr>
			                                @endforeach
			                            </tbody>
			                        </table>
			                    </div><!-- /.box-body -->
							</div>
							<div class="tab-pane" id="areaOnly">
								  <div class="box-content">    
								   	@if(Auth::user()->hasAccess('admin.area.create'))            
								  	<a href="{!! route('admin.area.create') !!}" class="btn btn-primary">Tambah Area </a><br>    
								  	@endif                
			                        <table class="table table-striped table-bordered bootstrap-datatable datatable">
			                            <thead>
			                                <tr>
			                                    <th>Kode Area</th>                   
			                                    <th>Nama Area</th>
			                                    @if(Auth::user()->kd_role != 3)   
                                                            <th>Action</th>
                                                            @endif
			                                </tr>
			                            </thead>
			                            <tbody>
			                                @foreach($data['area'] as $row)
			                                    <tr>
			                                        <td>{{ $row->kd_area }}</td>
			                                        <td>{{ $row->nm_area }}</td>
			                                        <td>       
			                                        @if(Auth::user()->hasAccess('admin.area.edit'))                                  
			                                        <a href="{!! route('admin.area.edit',[$row->id]) !!}" class="icon-pencil"></a>
			                                        @endif
			                                        &nbsp;
			                                        @if(Auth::user()->hasAccess('admin.area.delete'))      
			                                        <a href="{!! route('admin.area.delete',[$row->id]) !!}" onclick="return confirm('Are You Sure?')" class="icon-trash"> </a>
			                                        @endif
			                                        </td>
			                                    </tr>
			                                @endforeach
			                            </tbody>
			                        </table>
		                    	</div>
							</div>
						</div>
					</div>
				</div>		
			</div>
        </div>
    </div>
</div>
    	@include('partials.footer')
    </body>
</html>