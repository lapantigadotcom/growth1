<div class="box-body">
	<div class="form-group">
		{!! Form::label('kd_area','Kode Area') !!}
		{!! Form::text('kd_area',null, array('class' => 'form-control')) !!}
		{!! Form::label('nm_area','Nama Area') !!}
		{!! Form::text('nm_area',null, array('class' => 'form-control')) !!}
		{!! Form::label('status_aproval','Status Approval') !!}
		{!! Form::select('status_aproval', [
				   '' => 'Select Approval Configuration',
				   '1' => 'Auto Approval',
				   '0' => 'Manual Approval'], null, ['class' => 'form-control']
				) !!}
	</div>
	<div class="form-group">
		{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
	</div>
</div>