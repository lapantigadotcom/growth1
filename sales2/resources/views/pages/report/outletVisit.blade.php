@extends('layouts.app')

@section('content')
@section('title', 'Visiting Outlet Report')
<div id="content" class="span10">
	<ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="{{url('/home/dashboard')}}">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">Report</a>  
            <i class="icon-angle-right"></i>             
        </li>    
         <li>
            <a href="#">Visiting Outlet Report</a>                          
        </li>    
    </ul>

	<div class="row-fluid sortable">
		<div class="box span12">
			<div class="box-header" data-original-title>
				<h2><i class="halflings-icon edit"></i><span class="break"></span>Input Data</h2>
				<div class="box-icon">
					<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
					<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
					<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
				</div>
			</div>
			  	{!! Form::open(array('route' => 'admin.report.resultReport', 'method' => 'store' )) !!}
			    	@include('pages.report.form',array('submit' => 'Calculate'))
			    {!! Form::close() !!}
		</div>
	</div>
</div>	    

@endsection

@section('custom-head')
    
@endsection

@section('custom-footer')
    
@endsection
