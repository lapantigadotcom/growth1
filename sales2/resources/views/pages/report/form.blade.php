<div class="box-content">
	<div class="form-horizontal">
		 <fieldset>
		 	<div class="control-group">
				<label class="control-label">Periode</label>
				  	<div class="controls">
					{!! Form::text('periode', null, array('id' => 'datepicker0')) !!}
				</div>
			</div>

			<div class="span12">				
				<div class="control-group span4">
					<label class="control-label" >Kode Area</label>
					  	<div class="controls">
						{!! Form::select('kd_area',$areas, null, array('id'=>'area','class' => 'form-control')) !!}	
					</div>
				</div>
				<div class="control-group span7">
					<label class="control-label" >Target Visit</label>
					  	<div class="controls">
						{!! Form::text('targetVisit', null, array('class' => 'form-control')) !!}	
						<span class="help-inline">*) Required</span>
					</div>
				</div>
			</div>
			
			<div class="span12">
				<div class="control-group span4">
					<label class="control-label" >Nama Sales</label>
					  	<div class="controls">
						<select id="sales" name="sales">
						  <option>Select Sales</option>
						</select>
					</div>
				</div>
				<div class="control-group span7">
					<label class="control-label" >Target EC</label>
					  	<div class="controls">
						{!! Form::text('targetEc', null, array('class' => 'form-control')) !!}	
						<span class="help-inline">*) Required</span>
					</div>
				</div>
			</div>

			<!-- M1 -->
			<div class="span12">
				<div class="span1">
					<h3>M1 : </h3>
				</div>
				<div class="control-group span4" style="margin-left: -70px;">

				 <label class="control-label">From</label>
				  	<div class="controls">
					{!! Form::text('dateFrom', null, array('id' => 'datepicker1')) !!}

				  	</div>
				</div>

				<div class="control-group span4">
				 <label class="control-label">To</label>
				  	<div class="controls">
						{!! Form::text('dateTo', null, array('id' => 'datepicker2')) !!}
				  	</div>
				</div>
			</div>

			<!-- M2-->
			<div class="span12">
				<div class="span1">
					<h3>M2 : </h3>
				</div>
				<div class="control-group span4" style="margin-left: -70px;">
				 <label class="control-label">From</label>
				  	<div class="controls">
					{!! Form::text('dateFrom2', null, array('id' => 'datepicker3')) !!}

				  	</div>
				</div>

				<div class="control-group span4">
				 <label class="control-label">To</label>
				  	<div class="controls">
						{!! Form::text('dateTo2', null, array('id' => 'datepicker4')) !!}
				  	</div>
				</div>
			</div>
	
			<!-- M3-->
			<div class="span12">
				<div class="span1">
					<h3>M3 : </h3>
				</div>
				<div class="control-group span4" style="margin-left: -70px;">
				 <label class="control-label">From</label>
				  	<div class="controls">
					{!! Form::text('dateFrom3', null, array('id' => 'datepicker5')) !!}

				  	</div>
				</div>

				<div class="control-group span4">
				 <label class="control-label">To</label>
				  	<div class="controls">
						{!! Form::text('dateTo3', null, array('id' => 'datepicker6')) !!}
				  	</div>
				</div>
			</div>

			<!-- M4-->
			<div class="span12">
				<div class="span1">
					<h3>M4 : </h3>
				</div>
				<div class="control-group span4" style="margin-left: -70px;">
				 <label class="control-label">From</label>
				  	<div class="controls">
					{!! Form::text('dateFrom4', null, array('id' => 'datepicker7')) !!}

				  	</div>
				</div>

				<div class="control-group span4">
				 <label class="control-label">To</label>
				  	<div class="controls">
						{!! Form::text('dateTo4', null, array('id' => 'datepicker8')) !!}
				  	</div>
				</div>
			</div>

			<!-- M4-->
			<div class="span12">
				<div class="span1">
					<h3>M5 : </h3>
				</div>
				<div class="control-group span4" style="margin-left: -70px;">
				 <label class="control-label">From</label>
				  	<div class="controls">
					{!! Form::text('dateFrom5', null, array('id' => 'datepicker9')) !!}

				  	</div>
				</div>

				<div class="control-group span4">
				 <label class="control-label">To</label>
				  	<div class="controls">
						{!! Form::text('dateTo5', null, array('id' => 'datepicker10')) !!}
				  	</div>
				</div>
			</div>

			<div class="control-group span5">
				  	<div class="controls">
				  	@if ($errors->has())
						<div class="alert alert-error">
						  @foreach ($errors->all() as $error)
						    {!! $error !!}<br />		
						  @endforeach
						</div>
					@endif
					{!! Form::submit('Calculate', array('class' => 'btn btn-primary', 'name' => 'calc')) !!}
                                        {!! Form::submit('Download', array('class' => 'btn btn-primary', 'name' => 'down')) !!}
				</div>
			</div>
		</fieldset>
	</div>
</div>

             
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script>
function standardPeriod() {
				var CurrentDate = new Date();
				CurrentDate.setMonth(CurrentDate.getMonth());
				
				var day = CurrentDate.getDate();
				var year = CurrentDate.getFullYear();
				var month = new Array();
				month[0] = "January";
				month[1] = "February";
				month[2] = "March";
				month[3] = "April";
				month[4] = "May";
				month[5] = "June";
				month[6] = "July";
				month[7] = "August";
				month[8] = "September";
				month[9] = "October";
				month[10] = "November";
				month[11] = "December";
				var n = month[CurrentDate.getMonth()]; 
				var today = n + "-" + year;
				
				return today;
}

$(document).ready(
  function () {
			$('#initialdate').val(standardPeriod());
  }
);
</script>
 <script>
    $(function() {
      $('#datepicker0').datepicker({ dateFormat: 'mm-yy' }).val();
      $('#datepicker1').datepicker({ dateFormat: 'yy-mm-dd' }).val();
      $('#datepicker2').datepicker({ dateFormat: 'yy-mm-dd' }).val();
      $('#datepicker3').datepicker({ dateFormat: 'yy-mm-dd' }).val();
      $('#datepicker4').datepicker({ dateFormat: 'yy-mm-dd' }).val();
      $('#datepicker5').datepicker({ dateFormat: 'yy-mm-dd' }).val();
      $('#datepicker6').datepicker({ dateFormat: 'yy-mm-dd' }).val();
      $('#datepicker7').datepicker({ dateFormat: 'yy-mm-dd' }).val();
      $('#datepicker8').datepicker({ dateFormat: 'yy-mm-dd' }).val();
      $('#datepicker9').datepicker({ dateFormat: 'yy-mm-dd' }).val();
      $('#datepicker10').datepicker({ dateFormat: 'yy-mm-dd' }).val();
    });
  </script>
 <script>
	$('#area').on('change', function(e) {
        console.log(e);
        var id = e.target.value;
        //ajax
        $.getJSON('{{url('/ajax-sales?id=')}}'+id, function (data) {
			//console.log(data);
            $('#sales').empty();
          	$.each(data, function(index, salesObj){
            	 $('#sales').append('<option value="'+salesObj.id+'">'+salesObj.nama+'</option>');
				});
        });
    });
</script>





