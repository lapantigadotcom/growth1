<div class="box-content">
	<div class="form-horizontal">
		 <fieldset>
		 	<div class="control-group">
				<label class="control-label">Periode</label>
				  	<div class="controls">
					{!! Form::text('periode', null, array('id' => 'datepicker0')) !!}
				</div>
			</div>
						
			<div class="control-group ">
				<label class="control-label" >Kode Area</label>
				  	<div class="controls">
					{!! Form::select('kd_area',$areas, null, array('id'=>'area','class' => 'form-control')) !!}	
				</div>
			</div>
			
			<div class="control-group ">
				<label class="control-label" >Nama Sales</label>
				  	<div class="controls">
					<select id="sales" name="sales">
					  <option>Select Sales</option>
					</select>
				</div>
			</div>
				
			<div class="control-group ">
				  	<div class="controls">
					{!! Form::submit('Show', array('class' => 'btn btn-primary', 'name' => 'show')) !!}
					{!! Form::submit('Download', array('class' => 'btn btn-primary', 'name' => 'down')) !!}
				</div>
			</div>

		</fieldset>
	</div>
</div>

             
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script>
    $(function() {
      $('#datepicker0').datepicker({ dateFormat: 'mm-yy'}).val();
    });
 </script>
 <script>
	$('#area').on('change', function(e) {
        console.log(e);
        var id = e.target.value;
        $.getJSON('{{url('/ajax-sales?id=')}}'+id, function (data) {
            $('#sales').empty();
          	$.each(data, function(index, salesObj){
            	 $('#sales').append('<option value="'+salesObj.id+'">'+salesObj.nama+'</option>');
				});
        });
    });
</script>
 





