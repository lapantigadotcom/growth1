<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Hisamitsu Panel | Produk List</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        @include('partials.head')
        <script src="//cdn.ckeditor.com/4.5.2/standard/ckeditor.js"></script>

    </head>
<body>
<div class="container-fluid-full">
    <div class="row-fluid"> 
<!-- Left side column. contains the logo and sidebar -->
<aside class="left-side sidebar-offcanvas">
@include('partials.sidebar')
</aside>

<!-- Main content -->
<div id="content" class="span10">
  <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="{{url('/home/dashboard')}}">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">Master Data</a>
            <i class="icon-angle-right"></i>
            
        </li>
        <li>
            <a href="#">Produk</a>
        </li>
    </ul>
    <div class="row-fluid sortable">        
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>Produk List</h2>
            </div>
                <br>
                @if(Auth::user()->hasAccess('admin.produk.create')) 
                    <a href="{!! route('admin.produk.create') !!}" class="btn btn-primary">Tambah Produk </a>
                @endif
                    <div class="box-content">                                 
                        <table class="table table-striped table-bordered bootstrap-datatable datatable">
                            <thead>
                                <tr>
                                    <th>Kode Produk</th>
                                    <th>Nama Produk</th>
                                    @if(Auth::user()->kd_role != 3) 
                                        <th style="text-align:center;">Action</th>
                                    @else
                                        <th style="text-align:center;"></th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data['content'] as $row)
                                <tr>                         
                                    <td>{{ $row->kd_produk }}</td>
                                    <td>{{ $row->nm_produk }}</td>
                                    <td>             
                                    @if(Auth::user()->hasAccess('admin.produk.edit'))                                                   
                                        <a href="{!! route('admin.produk.edit',[$row->id]) !!}" class="icon-pencil"></a> 
                                    @endif   
                                        &nbsp;      
                                    @if(Auth::user()->hasAccess('admin.produk.delete'))                     
                                        <a href="{!! route('admin.produk.delete',[$row->id]) !!}" onclick="return confirm('Are you sure?')"  class="icon-trash"></a>
                                    @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div><!-- /.box-content -->
                </div><!-- /.box -->
            </div> <!-- end row -->
        </div> <!-- end content -->
   </div> <!-- end row -->
</div> <!-- end content -->
@include('partials.footer')
    </body>
</html>