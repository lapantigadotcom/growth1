<div class="box-body">
	<div class="form-group">
		{!! Form::label('kd_produk','Kode Produk') !!}
		{!! Form::text('kd_produk',null, array('class' => 'form-control')) !!}
		{!! Form::label('nm_produk','Nama Produk') !!}
		{!! Form::text('nm_produk',null, array('class' => 'form-control')) !!}	
	</div>
	<div class="form-group">
		{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
	</div>
</div>