@extends('layouts.app')

@section('content')
<div id="content" class="span10">  
    <ul class="breadcrumb">
	        <li>
	            <i class="icon-home"></i>
	            <a href="{{url('/home/dashboard')}}">Home</a> 
	            <i class="icon-angle-right"></i>
	        </li>
	        <li>
	            <a href="#">User Manager</a>
                    <i class="icon-angle-right"></i>
	        </li>
                <li>
	            <a href="#">Non-SF</a>
                    <i class="icon-angle-right"></i>
	        </li>
                 <li>
	            <a href="#">Edit User</a>
	        </li>
	    </ul>             
    {!! Form::model($data['content'],array('route' => ['admin.user.update',$data['content']->id], 'method' => 'PUT', 'files' => true)) !!}
        @include('pages.user.form',array('submit' => 'Perbarui'))
    {!! Form::close() !!}
</div>
@endsection

@section('custom-head')
    
@endsection

@section('custom-footer')
    
@endsection