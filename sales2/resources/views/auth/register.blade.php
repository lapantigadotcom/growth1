@extends('layouts.auth')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-5 col-md-offset-3">
            <div class="panel panel-default">

            <div class="panel-body">
                    <div class="login-header">
                        <h4>DAFTAR KE SISTEM</h4>
                    </div>

                @if (isset($errors) && $errors->any())
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="alert alert-danger alert-alt">
                                <strong><i class="fa fa-bug fa-fw"></i></strong><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    <br/>
                @endif
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/register') }}">
                                    {!! csrf_field() !!}
                       <div class="form-group">
                            <div class="col-md-12">
                                <input type="text" name="name" value="" placeholder="Nama">
                            </div>
                       </div>
                       <div class="form-group">
                            <div class="col-md-12">
                                <input type="email" class="form-control" name="email" value="" placeholder="Email">
                            </div>
                       </div>
                       <div class="form-group">
                            <div class="col-md-12">
                                <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                            </div>
                       </div>
                       <div class="form-group">
                            <div class="col-md-12">
                                <input type="password" name="password_confirmation" placeholder="Re-Type Password">
                            </div>
                       </div>
                       <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">Register</button>
                            </div>
                       </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection