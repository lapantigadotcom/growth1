@extends('layouts.auth')
<!-- start: CSS -->
    <link id="bootstrap-style" href="{{url('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{url('css/bootstrap-responsive.min.css')}}" rel="stylesheet">
    <link id="base-style" href="{{url('css/style.css')}}" rel="stylesheet">
    <link id="base-style-responsive" href="{{url('css/style-responsive.css')}}" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
<!-- end: CSS -->
    
@section('content')
<div class="container-fluid-full">
        <div class="row-fluid">
                @if(Session::has('success_message'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ Session::get('success_message') }}</strong>
                    </div>
                @elseif(Session::has('error_message'))
                    <div class="alert alert-error">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ Session::get('error_message') }}</strong>
                    </div>
                @endif     
            
                <div class="login-box">
                    <div class="icons">
                        <a href="{{url('/')}}"><i class="halflings-icon home"></i></a>
                     </div>
                    
                    <center><img src="{{ asset('img/hisamitsu_logo_main.png') }}" class="img-responsive"></center>

                    {!! Form::open(['route'=>'admin.auth.login', 'method' => 'store', 'class'=>'form-horizontal']) !!}
                    <div class="input-prepend" title="Username">
                        <span class="add-on"><i class="halflings-icon user"></i></span>
                        {!! Form::text('username',null, array('class' => 'input-large span10', 'placeholder' => 'Username')) !!}
                    </div>

                    <div class="input-prepend" title="Password">
                        <span class="add-on"><i class="halflings-icon lock"></i></span>
                        {!! Form::password('password', array('class' => 'input-large span10', 'placeholder' => 'Password')) !!}
                    </div>

                    <div class="clearfix"></div>
                    <label class="remember" for="remember" >
                        <input type="checkbox" value="1" name="remember_me"> Remember Me
                    </label>
             
                    <div class="button-login">  
                        <button type="submit" class="btn btn-success">Sign In</button>
                    </div><!-- /.col -->
                    <div class="clearfix"></div>

                    <a style="margin-left:20px;" href="{{ route('admin.autentikasi') }}">Forgot Password</a>
            </div>
        </div>
         <span style="color:#fff">
            <center>&copy; 2016. PT. Hisamitsu Pharma Indonesia. All rights reserved. </center>
        </span>
</div>
@endsection