<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Hisamitsu SF Report</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        {!! HTML::style('css/bootstrap.min.css') !!} 
        <!-- font Awesome -->
        {!! HTML::style('css/font-awesome.min.css') !!} 
        <!-- Theme style -->
        {!! HTML::style('css/AdminLTE.css') !!}
        {!! HTML::style('plugins/iCheck/square/blue.css') !!}
    </head>
    
    <body class="login-page" style="background-color: #002952;border-bottom:10px solid #01993B">

        @yield('content')
        <!-- jQuery 2.0.2 -->
        <script src="{!! asset('plugins/jQuery/jQuery-2.1.4.min.js') !!}"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- Bootstrap -->
        {!! HTML::script('js/bootstrap.min.js') !!}    
        {!! HTML::script('plugins/iCheck/icheck.min.js') !!} 
        <script>
          $(function () {
            $('input').iCheck({
              checkboxClass: 'icheckbox_square-blue',
              radioClass: 'iradio_square-blue',
              increaseArea: '20%' // optional
            });
          });
        </script>

    </body>
</html>