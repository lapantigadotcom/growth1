<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Oppppsssss.....terjadi ERROR 500</title>
 <!-- start: CSS -->
  <link id="bootstrap-style" href="{{url('css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{url('css/bootstrap-responsive.min.css')}}" rel="stylesheet">
  <link id="base-style" href="{{url('css/style.css')}}" rel="stylesheet">
  <link id="base-style-responsive" href="{{url('css/style-responsive.css')}}" rel="stylesheet">
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
<!-- end: CSS -->

</head>

<body class="nav-md">
  <div class="container body">
    <div class="main_container">
      <!-- page content -->
      <div class="col-md-12">
        <div class="col-middle">
          <div class="text-center text-center">
           <h1 class="error-number"> <i class="icon-warning-sign text-danger" style="color:red"></i></h1>
           <h1 class="error-number">ERROR 500</h1>
            <h2>Internal Server Error</h2>
            <p>Opssssss.....! Error 500. Internal Server Error, Silahkan refresh halaman Anda, atau kembali ke halaman sebelumnya.
            </p>
            <div class="mid_center">
               <form>
              <div class="control-group">
                <div class="controls">
                  <div class="input-append">
                   <h1><a href="{{url('/admin/dashboard')}}"> Back to home</a><h1>
                  </div>
                </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
       <!-- end content -->
    </div>
  </div>

  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>

</body>

</html>
