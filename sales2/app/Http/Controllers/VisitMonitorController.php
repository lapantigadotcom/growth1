<?php
/*
    PT. Trikarya Teknologi Indonesia
    Tenggilis raya 127
    Office Complex Apartment Metropolis MKB 206
    Surabaya, Jawa timur, Indonesia
    Phone : +6231-8420384 / +6281235537717
    Code By : Novita Nata Wardanie
*/
namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Input;
use Excel;
use App\VisitPlan;
use App\Outlet;
use App\Kota;
use App\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\VisitPlanRequest;
use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;

class VisitMonitorController extends Controller
{
    public function formVisit()
    {
        if(Auth::user()->kd_area== 100){
            $data = VisitPlan::select('outlet.nm_outlet', 
                    'outlet.almt_outlet', 
                    'kota.nm_kota',
                    'distributor.nm_dist',
                    'outlet.kodepos',
                    'user.nama',
                    'visit_plan.*')
            ->join('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
            ->leftJoin('user', 'user.id', '=', 'outlet.kd_user')
            ->leftJoin('kota', 'kota.id', '=', 'outlet.kd_kota')
            ->leftJoin('distributor', 'distributor.id', '=', 'outlet.kd_dist')

            ->whereBetween('visit_plan.date_visit',[date('Y-m-d',strtotime("-7 day")),date('Y-m-d',strtotime("+1 day"))])
            ->get();
           return view('pages.visit.viewFormMonitor')->with('data',$data);
        }
        else if(Auth::user()->kd_role == 3){
            $id =Auth::user()->id;
            $data = VisitPlan::select('outlet.nm_outlet', 
                'outlet.almt_outlet',
                'kota.nm_kota',
                'outlet.kodepos',
                'user.nama',
                'visit_plan.*')
            ->join('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
            ->leftJoin('user', 'user.id', '=', 'outlet.kd_user')
            ->leftJoin('kota', 'kota.id', '=', 'outlet.kd_kota')
            ->where('outlet.kd_user','=',$id)
            ->whereBetween('visit_plan.date_visit',[date('Y-m-d',strtotime("-7 day")),date('Y-m-d',strtotime("+1 day"))])
            ->get();
           return view('pages.visit.viewFormMonitor')->with('data',$data);
        }
       else
       {
            $area =Auth::user()->kd_area;
            $data = VisitPlan::select('outlet.nm_outlet', 
                'outlet.almt_outlet',
                'kota.nm_kota',
                'outlet.kodepos',
                'user.nama',
                'visit_plan.*')
            ->join('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
            ->leftJoin('user', 'user.id', '=', 'outlet.kd_user')
            ->leftJoin('kota', 'kota.id', '=', 'outlet.kd_kota')
            ->where('outlet.kd_area','=',$area)
            ->whereBetween('visit_plan.date_visit',[date('Y-m-d',strtotime("-7 day")),date('Y-m-d',strtotime("+1 day"))])
            ->get();
            return view('pages.visit.viewFormMonitor')->with('data',$data);
       }
    }

    public function index(VisitPlanRequest $request)
    {
        $data['dateFrom'] = $request->input('dateFrom');
        $data['dateTo'] = $request->input('dateTo');
        if(Input::get('show')) {
            if(Auth::user()->kd_area == 100){
            $data = VisitPlan::select('outlet.nm_outlet', 
                    'outlet.almt_outlet',
                    'kota.nm_kota',
                    'outlet.kodepos',
                    'user.nama',
                    'visit_plan.*')
            ->join('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
            ->leftJoin('user', 'user.id', '=', 'outlet.kd_user')
            ->leftJoin('kota', 'kota.id', '=', 'outlet.kd_kota')
            ->whereBetween('visit_plan.date_visit',[$data['dateFrom'],$data['dateTo']])
            ->get();
            }
           else if(Auth::user()->kd_role == 3){
                $id =Auth::user()->id;
                $data = VisitPlan::select('outlet.nm_outlet', 
                    'outlet.almt_outlet',
                    'kota.nm_kota',
                    'outlet.kodepos',
                    'user.nama',
                    'visit_plan.*')
                ->join('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
                ->leftJoin('user', 'user.id', '=', 'outlet.kd_user')
                ->leftJoin('kota', 'kota.id', '=', 'outlet.kd_kota')
                ->where('outlet.kd_user','=',$id)
                ->whereBetween('visit_plan.date_visit',[$data['dateFrom'],$data['dateTo']])
                ->get();
            }
           else
           {
             $area =Auth::user()->kd_area;
             $data = VisitPlan::select('outlet.nm_outlet', 
                    'outlet.almt_outlet',
                    'kota.nm_kota',
                    'outlet.kodepos',
                    'user.nama',
                    'visit_plan.*')
            ->join('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
            ->leftJoin('user', 'user.id', '=', 'outlet.kd_user')
            ->leftJoin('kota', 'kota.id', '=', 'outlet.kd_kota')
            ->where('outlet.kd_area','=',$area)
            ->whereBetween('visit_plan.date_visit',[$data['dateFrom'],$data['dateTo']])
            ->get();
           }
           return view('pages.visitMonitor.index')->with('data',$data);
        } else if(Input::get('download')) {
            $this->downloadVisitPlanMonitor($request);
        }
    }

    public function downloadVisitPlanMonitor(VisitPlanRequest $request)
    {
        $log = new AdminController;
        $log->getLogHistory('Download Data Visit Monitor');
        $data['dateFrom'] = $request->input('dateFrom');
        $data['dateTo'] = $request->input('dateTo');
        $role = Auth::user()->kd_role;
        $area = Auth::user()->kd_area;
        if($area == 100)
        {
            ob_end_clean();
            ob_start();
            $visit = VisitPlan::select(DB::raw("
				visit_plan.id as KODE_VISIT,
				outlet.kd_outlet as KODE_OUTLET,
				outlet.nm_outlet as NAMA_OUTLET,
				outlet.almt_outlet as ALAMAT_OUTLET,
				outlet.kodepos AS KODEPOS,  
				kota.nm_kota as KOTA, 
				area.nm_area as AREA, 
				(CASE WHEN (visit_plan.status_visit = 1) THEN 'Finished' ELSE 'Unvisited' END) as STATUS_VISIT,
				visit_plan.skip_order_reason as SKIP_ORDER_REASON,
				visit_plan.skip_reason as UNVISITED_REASON,
				visit_plan.date_visit as PLAN_DATE_VISIT,
				visit_plan.date_visiting as DATE_ACTUAL_VISIT,
				user.nama as SALES,
				distributor.nm_dist as DISTRIBUTOR
				")
            )
            ->join('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
            ->leftJoin('user', 'user.id', '=', 'outlet.kd_user')
            ->leftJoin('kota', 'kota.id', '=', 'outlet.kd_kota')
            ->leftJoin('distributor', 'distributor.id', '=', 'outlet.kd_dist')
            ->leftJoin('area', 'area.id', '=', 'outlet.kd_area')
            ->whereBetween('visit_plan.date_visit',[$data['dateFrom'],$data['dateTo']])
            ->get();
            date_default_timezone_set("Asia/Jakarta");
            $d= strtotime("now");
            $time =date("d-m-Y H:i:s", $d);
            $filename = $time."_VISIT_MONITOR";
            Excel::create($filename, function($excel) use($visit) {
                $excel->sheet('Visiting Monitor', function($sheet) use($visit) {
                    $sheet->fromArray($visit);
                });
            })->download('xls');
        }
        else if(Auth::user()->kd_role == 3){
            ob_end_clean();
            ob_start();
            $id =Auth::user()->id;
            $visit = VisitPlan::select(DB::raw("
                visit_plan.id as KODE_VISIT,
                outlet.kd_outlet as KODE_OUTLET,
                outlet.nm_outlet as NAMA_OUTLET,
                outlet.almt_outlet as ALAMAT_OUTLET,
                outlet.kodepos AS KODEPOS,  
                kota.nm_kota as KOTA, 
                area.nm_area as AREA, 
                (CASE WHEN (visit_plan.status_visit = 1) THEN 'Finished' ELSE 'Unvisited' END) as STATUS_VISIT,
                visit_plan.skip_order_reason as SKIP_ORDER_REASON,
                visit_plan.skip_reason as UNVISITED_REASON,
                visit_plan.date_visit as PLAN_DATE_VISIT,
                visit_plan.date_visiting as DATE_ACTUAL_VISIT,
                user.nama as SALES,
                distributor.nm_dist as DISTRIBUTOR
                ")
            )
            ->join('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
            ->join('user', 'user.id', '=', 'outlet.kd_user')
            ->leftJoin('kota', 'kota.id', '=', 'outlet.kd_kota')
            ->leftJoin('distributor', 'distributor.id', '=', 'outlet.kd_dist')
            ->leftJoin('area', 'area.id', '=', 'outlet.kd_area')
            ->where('outlet.kd_user','=',$id)
            ->whereBetween('visit_plan.date_visit',[$data['dateFrom'],$data['dateTo']])
            ->get();
            date_default_timezone_set("Asia/Jakarta");
            $d= strtotime("now");
            $time =date("d-m-Y H:i:s", $d);
            $filename = $time."_VISIT_MONITOR";
            Excel::create($filename, function($excel) use($visit) {
                $excel->sheet('Visiting Monitor', function($sheet) use($visit) {
                    $sheet->fromArray($visit);
                });
            })->download('xls');
        }
        else
        {
            $this->downloadVisitMonitorArea($area,$data['dateFrom'],$data['dateTo']);
        }
    }

    public function downloadVisitMonitorArea($area,$dateForm,$dateTo)
    {
        ob_end_clean();
        ob_start();
        $visit = VisitPlan::select(DB::raw("
        	visit_plan.id as KODE_VISIT,
        	outlet.kd_outlet as KODE_OUTLET,
            outlet.nm_outlet as NAMA_OUTLET,
            outlet.almt_outlet as ALAMAT_OUTLET,  
            kota.nm_kota as KOTA, 
            outlet.kodepos AS KODEPOS,
            area.nm_area as AREA, 
            (CASE WHEN (visit_plan.status_visit = 1) THEN 'Finished' ELSE 'Skip' END) as STATUS_VISIT,
            visit_plan.skip_order_reason as SKIP_ORDER_REASON,
            visit_plan.skip_reason as UNVISITED_REASON,
            visit_plan.date_visit as PLAN_DATE_VISIT,
            visit_plan.date_visiting as DATE_ACTUAL_VISIT,
            user.nama as SALES,
            distributor.nm_dist as DISTRIBUTOR
            ")
        )
        ->join('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
        ->leftJoin('user', 'user.id', '=', 'outlet.kd_user')
        ->leftJoin('kota', 'kota.id', '=', 'outlet.kd_kota')
        ->leftJoin('distributor', 'distributor.id', '=', 'outlet.kd_dist')
        ->join('area', 'area.id', '=', 'outlet.kd_area')
        ->where('outlet.kd_area','=',$area)
        ->whereBetween('visit_plan.date_visit',[$dateForm,$dateTo])
        ->get();
        date_default_timezone_set("Asia/Jakarta");
        $d= strtotime("now");
        $time =date("d-m-Y H:i:s", $d);
        $filename = $time."_VISIT_MONITOR";
        Excel::create($filename, function($excel) use($visit) {
            $excel->sheet('Visiting Monitor', function($sheet) use($visit) {
                $sheet->fromArray($visit);
            });
        })->download('xls');
    }
    
    public function create()
    {
        $outlet= Outlet::all();
        return view('pages.visitMonitor.create', compact('outlet'));
    }

    public function store(VisitPlanRequest $request)
    {
        VisitPlan::create($request->all());
        return redirect()->route('admin.visitingMonitor.form');
    }

    public function edit($id)
    {
        $data['content'] = VisitPlan::find($id);
        $role = Auth::user()->kd_role;
        $area = Auth::user()->kd_area;
        $id = Auth::user()->id;
        if($area == 100)
        {
            $outlet = [''=>''] + Outlet::lists('nm_outlet', 'kd_outlet')->toArray();
        }
        else if($role==3)
        {
            $outlet = [''=>''] + Outlet::where('kd_user','=',$id)->lists('nm_outlet', 'kd_outlet')->toArray();
        }
        else
        {
             $outlet = [''=>''] + Outlet::where('kd_area','=',$area)->lists('nm_outlet', 'kd_outlet')->toArray();
        }
        return view('pages.visitMonitor.edit')->with('data',$data)->with('outlet',$outlet);
    }

    public function update(VisitPlanRequest $request, $id)
    {
        $data = VisitPlan::find($id);
        $data->update($request->all());
        $log = new AdminController;
        $log->getLogHistory('Update Visit Monitor with ID '.$id);
        return redirect()->route('admin.visitingMonitor.form');
    }

    public function destroy($id)
    {
        $data = VisitPlan::with('takeOrders')->find($id);
        foreach ($data->takeOrders as $row) {            
            $row->delete();
        }
        $data->delete(); 
        $log = new AdminController;
        $log->getLogHistory('Delete Visit Monitor with ID '.$id);
        return redirect()->route('admin.visitingMonitor.form');     
    }

    
}
