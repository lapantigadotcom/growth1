<?php
/*
    PT. Trikarya Teknologi Indonesia
    Tenggilis raya 127
    Office Complex Apartment Metropolis MKB 206
    Surabaya, Jawa timur, Indonesia
    Phone : +6231-8420384 / +6281235537717
    Code By : Novita Nata Wardanie
*/
namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Input;
use Excel;
use App\VisitPlan;
use App\Outlet;
use App\Distributor;
use App\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\VisitPlanRequest;
use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;

class VisitPlanController extends Controller
{
    public function formVisit()
    {
        if(Auth::user()->kd_area == 100){
            $data = DB::table('visit_plan')
            ->select('outlet.nm_outlet', 
                    'outlet.almt_outlet',
                    'kota.nm_kota',
                    'outlet.kodepos',
                    'user.nama',
                    'visit_plan.id',
                    'visit_plan.date_create_visit',
                    'visit_plan.date_visit',
                    'visit_plan.approve_visit')
            ->join('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
            ->join('user', 'user.id', '=', 'outlet.kd_user')
            ->join('kota', 'kota.id', '=', 'outlet.kd_kota')
            ->whereBetween('visit_plan.date_visit',[date('Y-m-d',strtotime("-7 day")),date('Y-m-d',strtotime("+1 day"))])
            ->get();
            return view('pages.visit.viewForm')->with('data',$data);
         }
        else if(Auth::user()->kd_role == 3){
            $id =Auth::user()->id;
            $data = DB::table('visit_plan')
            ->select('outlet.nm_outlet', 
                    'outlet.almt_outlet',
                    'kota.nm_kota',
                    'outlet.kodepos',
                    'user.nama',
                    'visit_plan.id',
                    'visit_plan.date_create_visit',
                    'visit_plan.date_visit',
                    'visit_plan.approve_visit')
            ->join('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
            ->join('user', 'user.id', '=', 'outlet.kd_user')
            ->join('kota', 'kota.id', '=', 'outlet.kd_kota')
            ->where('outlet.kd_user','=',$id)
            ->whereBetween('visit_plan.date_visit',[date('Y-m-d',strtotime("-7 day")),date('Y-m-d',strtotime("+1 day"))])
            ->get();
            return view('pages.visit.viewForm')->with('data',$data);
        }
        else 
        {
            $area =Auth::user()->kd_area;
            $data = DB::table('visit_plan')
            ->select('outlet.nm_outlet', 
                    'outlet.almt_outlet',
                    'kota.nm_kota',
                    'outlet.kodepos',
                    'user.nama',
                    'visit_plan.id',
                    'visit_plan.date_create_visit',
                    'visit_plan.date_visit',
                    'visit_plan.approve_visit')
            ->join('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
            ->join('user', 'user.id', '=', 'outlet.kd_user')
            ->join('kota', 'kota.id', '=', 'outlet.kd_kota')
            ->where('outlet.kd_area','=',$area)
            ->whereBetween('visit_plan.date_visit',[date('Y-m-d',strtotime("-7 day")),date('Y-m-d',strtotime("+1 day"))])
            ->get();
            return view('pages.visit.viewForm')->with('data',$data);
        }
    }

    public function index(VisitPlanRequest $request)
    {
        $data['dateFrom'] = $request->input('dateFrom');
        $data['dateTo'] = $request->input('dateTo');
        if(Input::get('show')) {
            if(Auth::user()->kd_area == 100){
            $data = DB::table('visit_plan')
            ->select('outlet.nm_outlet', 
                    'outlet.almt_outlet',
                    'kota.nm_kota',
                    'outlet.kodepos',
                    'user.nama',
                    'visit_plan.id',
                    'visit_plan.date_create_visit',
                    'visit_plan.date_visit',
                    'visit_plan.approve_visit')
            ->join('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
            ->join('user', 'user.id', '=', 'outlet.kd_user')
            ->join('kota', 'kota.id', '=', 'outlet.kd_kota')
            ->whereBetween('visit_plan.date_visit',[$data['dateFrom'],$data['dateTo']])
            ->get();
             return view('pages.visit.index')->with('data',$data);
             }
             else if(Auth::user()->kd_role == 3){
                $id =Auth::user()->id;
                $data = DB::table('visit_plan')
                ->select('outlet.nm_outlet', 
                        'outlet.almt_outlet',
                        'kota.nm_kota',
                        'outlet.kodepos',
                        'user.nama',
                        'visit_plan.id',
                        'visit_plan.date_create_visit',
                        'visit_plan.date_visit',
                        'visit_plan.approve_visit')
                ->join('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
                ->join('user', 'user.id', '=', 'outlet.kd_user')
                ->join('kota', 'kota.id', '=', 'outlet.kd_kota')
                ->where('outlet.kd_user','=',$id)
                ->whereBetween('visit_plan.date_visit',[$data['dateFrom'],$data['dateTo']])
                ->get();
                 return view('pages.visit.index')->with('data',$data);
            }
             else
            {
                $area =Auth::user()->kd_area;
                $data = DB::table('visit_plan')
                ->select('outlet.nm_outlet', 
                        'outlet.almt_outlet',
                        'kota.nm_kota',
                        'outlet.kodepos',
                        'user.nama',
                        'visit_plan.id',
                        'visit_plan.date_create_visit',
                        'visit_plan.date_visit',
                        'visit_plan.approve_visit')
                ->join('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
                ->join('user', 'user.id', '=', 'outlet.kd_user')
                ->join('kota', 'kota.id', '=', 'outlet.kd_kota')
                ->where('outlet.kd_area','=',$area)
                ->whereBetween('visit_plan.date_visit',[$data['dateFrom'],$data['dateTo']])
                ->get();
                 return view('pages.visit.index')->with('data',$data);
            }
        } else if(Input::get('download')) {
            $this-> downloadVisitPlan($request);
        }
    }

    public function downloadVisitPlan(VisitPlanRequest $request)
    {
        $log = new AdminController;
        $log->getLogHistory('Download Data Visit Plan');
        $data['dateFrom'] = $request->input('dateFrom');
        $data['dateTo'] = $request->input('dateTo');
        $role = Auth::user()->kd_role;
        $area = Auth::user()->kd_area;
        if($area == 100) { 
            ob_end_clean();
            ob_start();
            $visit = VisitPlan::select(DB::raw("outlet.kd_outlet as KODE_OUTLET,
              outlet.nm_outlet as NAMA_OUTLET,
              outlet.almt_outlet as ALAMAT_OUTLET,  
              kota.nm_kota as KOTA, 
              outlet.kodepos AS KODEPOS,
              area.nm_area as AREA, 
              (CASE WHEN (visit_plan.approve_visit = 1) THEN 'Approved' WHEN (visit_plan.approve_visit = 2) THEN 'Pending' ELSE 'Denied' END) as VISIT_APPROVAL,
              visit_plan.date_create_visit as DATE_CREATE_VISIT,
              visit_plan.date_visiting as PLAN_DATE_VISIT,
              user.nama as SALES")
            )
            ->join('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
            ->join('user', 'user.id', '=', 'outlet.kd_user')
            ->leftJoin('kota', 'kota.id', '=', 'outlet.kd_kota')
            ->join('area', 'area.id', '=', 'outlet.kd_area')
            ->whereBetween('visit_plan.date_create_visit',[$data['dateFrom'],$data['dateTo']])
            ->get();
            date_default_timezone_set("Asia/Jakarta");
            $d= strtotime("now");
            $time =date("d-m-Y H:i:s", $d);
            $filename = $time."_VISIT_PLAN";
            Excel::create($filename, function($excel) use($visit) {
                $excel->sheet('Visit Plan List', function($sheet) use($visit) {
                    $sheet->fromArray($visit);
                });
            })->download('xls');
        }
        else
        {
            
            $this->downloadVisitPlanArea($area,$data['dateFrom'],$data['dateTo']);
        }
    }

    public function downloadVisitPlanArea($area,$dateForm,$dateTo)
    {
        ob_end_clean();
        ob_start();
        $visit = VisitPlan::select(DB::raw("outlet.kode as KODE_OUTLET,
          outlet.nm_outlet as NAMA_OUTLET,
          outlet.almt_outlet as ALAMAT_OUTLET,  
          kota.nm_kota as KOTA, 
          outlet.kodepos AS KODEPOS,
          area.nm_area as AREA, 
          (CASE WHEN (visit_plan.approve_visit = 1) THEN 'Approved' WHEN (visit_plan.approve_visit = 2) THEN 'Pending' ELSE 'Denied' END) as VISIT_APPROVAL,
          visit_plan.date_create_visit as DATE_CREATE_VISIT,
          visit_plan.date_visiting as PLAN_DATE_VISIT,
          user.nama as SALES")
        )
        ->join('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
        ->join('user', 'user.id', '=', 'outlet.kd_user')
        ->leftJoin('kota', 'kota.id', '=', 'outlet.kd_kota')
        ->join('area', 'area.id', '=', 'outlet.kd_area')
        ->where('outlet.kd_area','=',$area)
        ->whereBetween('visit_plan.date_create_visit',[$dateForm,$dateTo])
        ->get();
        date_default_timezone_set("Asia/Jakarta");
        $d= strtotime("now");
        $time =date("d-m-Y H:i:s", $d);
        $filename = $time."_VISIT_PLAN";
        Excel::create($filename, function($excel) use($visit) {
            $excel->sheet('Sheet 1', function($sheet) use($visit) {
                $sheet->fromArray($visit);
            });
        })->download('xls');
    }

    public function create()
    {
        $role = Auth::user()->kd_role;
        $area = Auth::user()->kd_area;
        $id = Auth::user()->id;
        if($area == 100)
        {
            $dist=Distributor::all();
            $outlet= Outlet::all();
        }
        else if($role == 3)
        {
            $dist=Distributor::all();
            $outlet= Outlet::where('kd_user','=',$id)->get();
        }
        else
        {
            $dist=Distributor::all();
            $outlet= Outlet::where('kd_area','=',$area)->get();
        }
        return view('pages.visit.create')->with('dist',$dist)->with('outlet',$outlet);
    }

    public function store(VisitPlanRequest $request)
    {
        $log = new AdminController;
        VisitPlan::create($request->all()); 
        $log->getLogHistory('Make Visit Plan');
        return redirect()->route('admin.visitPlan.form');
    }

    public function edit($id)
    {
        $data['content'] = VisitPlan::find($id);
        $role = Auth::user()->kd_role;
        $area = Auth::user()->kd_area;
        $id = Auth::user()->id;
        if($area == 100)
        {
            $outlet = [''=>''] + Outlet::lists('nm_outlet', 'kd_outlet')->toArray();
        }
        else if($role==3)
        {
            $outlet = [''=>''] + Outlet::where('kd_user','=',$id)->lists('nm_outlet', 'kd_outlet')->toArray();
        }
        else
        {
             $outlet = [''=>''] + Outlet::where('kd_area','=',$area)->lists('nm_outlet', 'kd_outlet')->toArray();
        }
        return view('pages.visit.edit')->with('data',$data)->with('outlet',$outlet);
    }

    public function update(VisitPlanRequest $request, $id)
    {
        $data = VisitPlan::find($id);
        $data->update($request->all());
        ///////////////////////////////////////////////////////////////
        $this->sendDownStream($data,"new");
        ///////////////////////////////////////////////////////////////
        $log = new AdminController;
        $log->getLogHistory('Update Visit Plan');
        return redirect()->route('admin.visitPlan.form');
    }
    
    public function editApprove()
    {
        $user = Auth::user();
    if($user->kd_role == 6){
        $data = DB::table('visit_plan')
        ->select('outlet.nm_outlet', 
                'outlet.almt_outlet',
                'kota.nm_kota',
                'user.nama',
                'visit_plan.id',
                'visit_plan.date_create_visit',
                'visit_plan.date_visit',
                'visit_plan.approve_visit')
        ->join('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
        ->join('user', 'user.id', '=', 'outlet.kd_user')
        ->join('kota', 'kota.id', '=', 'outlet.kd_kota')
        ->where('visit_plan.approve_visit','=',2)
        ->get();
    }
    else{
        $data = DB::table('visit_plan')
        ->select('outlet.nm_outlet', 
                'outlet.almt_outlet',
                'kota.nm_kota',
                'user.nama',
                'visit_plan.id',
                'visit_plan.date_create_visit',
                'visit_plan.date_visit',
                'visit_plan.approve_visit')
        ->join('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
        ->join('user', 'user.id', '=', 'outlet.kd_user')
        ->join('kota', 'kota.id', '=', 'outlet.kd_kota')
        ->where('visit_plan.approve_visit','=',2)
        ->where('outlet.kd_area','=',$user->kd_area)
        ->get();
    }
         return view('pages.visit.editApprove')->with('data',$data);
    }

    public function approvedVisit(VisitPlanRequest $request)
    {
        $id = $request->input('approve_visit');
        $data = VisitPlan::findMany($id);
        foreach ($data as $value) 
        {
            $value->update(['approve_visit' => 1]);
            $this->sendDownStream($value,"new");
        }
        return redirect()->route('admin.visitPlan.form');
    }

    public function destroy($id)
    {
        $data = VisitPlan::with('takeOrders')->find($id);
        foreach ($data->takeOrders as $row) {            
            $row->delete();
        }
        $data->delete(); 
        $log = new AdminController;
        $log->getLogHistory('Delete Visit Plan');
        return redirect()->route('admin.visitPlan.form');    
        
    }

    public function sendDownStream($data,$status){
        $outlet = Outlet::find($data->kd_outlet);
        $user = User::find($outlet->kd_user);
        ///////////////////////////////////////////////////////////////
        //Getting api key 
        $api_key = "AIzaSyDYP-YlzfI6HZOjkaCK9lXcASvHtgFrlic"; 

        //Getting registration token we have to make it as array 
        $reg_token = $user->id_gcm;

        //Creating a message array 
        $msg = array
        (
            'message' => $data,
            'tipe' => 'VisitPlan',
            'status' => $status
        );

        //Creating a new array fileds and adding the msg array and registration token array here 
        $fields = array
        (
            'to' => $reg_token,
            'data' => $msg
        );

        //Adding the api key in one more array header 
        $headers = array
        (
            'Authorization: key=' . $api_key,
            'Content-Type: application/json'
        ); 

        //Using curl to perform http request 
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );

        //Getting the result 
        $result = curl_exec($ch );
        curl_close( $ch );
    }
}
