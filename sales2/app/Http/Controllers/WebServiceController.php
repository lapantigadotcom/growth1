<?php

namespace App\Http\Controllers;
use Hash;
use DB;
use Auth;
use Session;
use Response;
use Input;
use Mail;
use App\Exceptions\SymfonyDisplayer;
use App\User;
use App\TakeOrder;
use App\Produk;
use App\Role;
use App\Kota;
use App\Konfigurasi;
use App\Counter;
use App\Outlet;
use App\Distributor;
use App\Tipe;
use App\TipePhoto;
use App\Area;
use App\Logging;
use App\Competitor;
use App\VisitPlan;
use App\PhotoActivity;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;

class WebServiceController extends Controller
{
    
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $data=new User;
        $data->author=Input::get('username');
        $data->text=Input::get('password');
        $success=$data->save();
     
        if(!$success)
        {
                 return Response::json("error saving",500);
        }
     
            return Response::json("success",201);
    }

    public function getUser($username,$password)
    {
        $data = User::where('username', '=', $username)->first();
        if(is_null($data))
        {
             return Response::json("not found",404);
        }
        else
        {
             if(Hash::check($password,$data->password))
             {
                   return Response::json($data,200);
             }
             else
                   return Response::json("wrong password",404);
        }     
    }
    public function getFoto($kd_outlet)
    {
        $data = Outlet::where('kd_outlet', '=', $kd_outlet)->first();
        if(is_null($data)||is_null($data->foto_outlet))
        {
            $out['foto'] = "";
            return Response::json($out,201);
        }
        else
        {
            $path = 'image_upload/outlet/';
            $imagedata = file_get_contents($path.$data->foto_outlet);
            $out['foto'] = base64_encode($imagedata);
            return Response::json($out,200);
        }     
    }
    public function getArea()
    {
        $data = Area::all();
        if(is_null($data))
        {
             return Response::json("not found",404);
        }
        else
        {
             return Response::json($data,200);
        }     
    }
    public function setPhoto()
    {
        $data = new PhotoActivity;
        $data->kd_outlet = Input::get('kd_outlet');
        $data->kd_competitor = Input::get('kd_kompetitor');
        $data->kd_user = Input::get('kd_user');
        $data->nm_photo = Input::get('nm_photo');
        $data->jenis_photo = Input::get('jenis_photo');
        $data->date_take_photo = Input::get('tgl_take');
        $data->almt_comp_activity = Input::get('alamat');
        $data->keterangan = Input::get('keterangan');
        $data->date_upload_photo = Input::get('tgl_upload');
        $data->save();
        ////////////////////////////////
        if(Input::get('kd_kompetitor') != 0){
            $jenis = "_kompetitor";
            $path = 'image_upload/competitor/';
        }
        if(Input::get('kd_kompetitor') == 0){
            $jenis = "_outlet";
            $path = 'image_upload/outlet_photoact/';
        }
        $base =Input::get('foto');
        $binary = base64_decode($base);
        header('Content-Type: bitmap; charset=utf-8');

        $f = finfo_open();
        $mime_type = finfo_buffer($f, $binary, FILEINFO_MIME_TYPE);
        $mime_type = str_ireplace('image/', '', $mime_type);

        $filename =  strval($data->id).$jenis.date('_YmdHis'). '.' . $mime_type;
        $file = fopen($path . $filename, 'wb');
        if (fwrite($file, $binary)) {
            $data->update(array('path_photo' => $filename));
            fclose($file);
            $logging = new Logging;
            $logging->kd_user = Input::get("kd_user");
            $logging->description = "Upload foto ".$jenis;
            $logging->log_time = Input::get('tgl_upload');
            $logging->detail_akses = "mobile";
            $logging->save();
            $out['status'] = "success";
            return Response::json($out,201);//return FALSE;
        } else {
            $out['status'] = "Upload image failed";
            return Response::json($out,500);//return FALSE;
        }
    }

    public function setGCM()
    {
        $id = Input::get('kd_user');
        $data = User::find($id);
        $success = $data->update(['id_gcm' => Input::get('id_gcm')]);
        if($success)
        {
            $out['status'] = "success";
            return Response::json($out,201);
        }
        else
        {
            $out['status'] = "error saving";
            return Response::json($out,501);
        }
    }
    public function setOutlet()
    {
        $id = -1;
        $data=new Outlet;
        $kota = Kota::where("id",Input::get('kd_kota'))->first();
        if(!Input::has('nm_outlet'))
        {
            $out['status'] = "no data sent";
            return Response::json($out,500);
        }
        $konfig = Konfigurasi::first();
        $data->kd_dist=Input::get('kd_dist');
        $data->kd_kota=Input::get('kd_kota');
        $data->kd_area=$kota->kd_area;
        $data->kd_user=Input::get('kd_user');
        $data->nm_outlet=Input::get('nm_outlet');
        $data->almt_outlet=Input::get('almt_outlet');
        $data->kd_tipe=Input::get('kd_tipe');
        $data->rank_outlet=Input::get('rank_outlet');
        $data->kodepos=Input::get('telp_outlet');
        $data->reg_status=Input::get('reg_status');
        $data->latitude=Input::get('latitude');
        $data->longitude=Input::get('longitude');
        $data->nm_pic=Input::get('nama_pic');
        $data->tlp_pic=Input::get('tlp_pic');
        $data->toleransi_long=$konfig->toleransi_max;
        $success = $data->save();
        $path = 'image_upload/outlet/';
        $base =Input::get('foto');
        $binary = base64_decode($base);
        header('Content-Type: bitmap; charset=utf-8');

        $f = finfo_open();
        $mime_type = finfo_buffer($f, $binary, FILEINFO_MIME_TYPE);
        $mime_type = str_ireplace('image/', '', $mime_type);

        $filename =  strval($data->kd_outlet).date('_YmdHis'). '.' . $mime_type;
        $file = fopen($path . $filename, 'wb');
        if (fwrite($file, $binary)) {
            $data->update(array('foto_outlet' => $filename));
            fclose($file);
        }
        $logging = new Logging;
        $logging->kd_user = Input::get("kd_user");
        $logging->description = "Register outlet ".Input::get("nm_outlet");
        $logging->log_time = Input::get('tgl_upload');
        $logging->detail_akses = "mobile";
        $logging->save();
        /*$path = 'image_upload/outlet/';
        //$base = $_REQUEST['image'];
        $base =Input::get('image');
        $binary = base64_decode($base);
        //$binary = base64_decode(urldecode($base));
        header('Content-Type: bitmap; charset=utf-8');

        $f = finfo_open();
        $mime_type = finfo_buffer($f, $binary, FILEINFO_MIME_TYPE);
        $mime_type = str_ireplace('image/', '', $mime_type);

        $filename =  strval($data->kd_outlet). '.' . $mime_type;
        $file = fopen($path . $filename, 'wb');
        if (fwrite($file, $binary)) {
            $data->update(array('foto_outlet' => $filename));
            $success = 1;
            //$data->save();
        } else {
            $out['status'] = "Upload image failed";
            return Response::json($out,500);//return FALSE;
        }
        fclose($file);    */
///////////////////////////////////////////////////////
        if(Input::get('take_order') == 1)
        {
            $vp=new VisitPlan;
            $vp->kd_outlet=$data->kd_outlet;
            $vp->approve_visit=1;
            $vp->status_visit=0;
            $vp->date_visit = Input::get('tgl_upload');
            $success = $vp->save();
            $id = $vp->id;
            $logging = new Logging;
            $logging->kd_user = Input::get("kd_user");
            $logging->description = "Register visit plan to outlet ".Input::get("nm_outlet");
            $logging->log_time = Input::get('tgl_upload');
            $logging->detail_akses = "mobile";
            $logging->save();
        }
///////////////////////////////////////////////////////
        if(!$success)
        {
            $out['status'] = "error saving";
            return Response::json($out,501);
        }
        $out['status'] = "success";
        $out['id'] = $data->kd_outlet;
        $out['kd_visit'] = $id;
        $out['toleransi'] = $konfig->toleransi_max;
        return Response::json($out,201);
    }
    public function setVisit()
    {
        $data=new VisitPlan;
        if(!Input::has('kd_outlet'))
        {
            $out['status'] = "no data sent";
            return Response::json($out,500);
        }
		$outlet = Outlet::where('kd_outlet','=',Input::get('kd_outlet'))->first();
        $area = Area::where('id','=',$outlet->kd_area)->first();
        $data->kd_outlet=Input::get('kd_outlet');
        $data->date_visit=Input::get('date_visit');
        $data->date_create_visit=Input::get('date_create_visit');
        if($area->status_aproval == 0)
                $data->approve_visit=Input::get('approve_visit');
        else if($area->status_aproval == 1)
                $data->approve_visit=1;
        $data->status_visit = 0;
        $success=$data->save();
             
        if(!$success)
        {
            $out['status'] = "error saving";
            return Response::json($out,501);
        }
            $logging = new Logging;
            $logging->kd_user = Input::get("kd_user");
            $logging->description = "Register visit plan to outlet ".Outlet::where("kd_outlet",Input::get("kd_outlet"))->first()->nm_outlet;
            $logging->log_time = Input::get('tgl_upload');
            $logging->detail_akses = "mobile";
            $logging->save();
            if($area->status_aproval == 0)
                $out['id'] = 0;
        	else if($area->status_aproval == 1)
                $out['id'] = $data->id;
            $out['status'] = "success";
            return Response::json($out,201);
    }
    public function submitVisit()
    {
        $user = VisitPlan::where("id",Input::get('kd_visit'))->first();
        $new_user_data = array("status_visit"=>Input::get('status_visit'),"date_visiting"=>Input::get('date_visiting'),
            "skip_order_reason"=>Input::get('skip_order_reason'));
        $success = $user->update($new_user_data);
        $success1 = 1;
        if(Input::get('counter')!=0){
            $counter = Input::get('counter');
        }
        else{
            $counter_to = Counter::where("id",1)->first();
            $counter = $counter_to->counter;
            $counter_to->update(array('counter' => $counter+1));
        }
        if(Input::get('kd_produk')!=0){
            $produk = new TakeOrder;
            $produk->id = Input::get('id');
            $produk->kd_to = "to_".strval($counter);
            $produk->kd_visitplan = Input::get('kd_visit');
            $produk->kd_produk = Input::get('kd_produk');
            $produk->qty_order = Input::get('qty_order');
            $produk->satuan = Input::get('satuan');
            $produk->date_order = Input::get('date_order');
            $produk->status_order = Input::get('status_order');
            $success1=$produk->save();
        }
        if(!($success && $success1))
        {
            $out['status'] = "error saving takeorder";
            return Response::json($out,501);
        }
        $logging = new Logging;
        $logging->kd_user = Input::get("kd_user");
        $logging->description = "Submit visit plan to outlet ".Outlet::where("kd_outlet",$user->kd_outlet)->first()->nm_outlet;
        $logging->log_time = Input::get('date_visiting');
        $logging->detail_akses = "mobile";
        $logging->save();
        $out['status'] = "success";
        $out['counter'] = $counter;
        return Response::json($out,201);
    }
    public function getAllData($kd_user,$kd_area)
    {
        $kota = Kota::where('kd_area','=',$kd_area)->get();
        $competitor = Competitor::all();
        $outlet = Outlet::where('kd_user','=',$kd_user)->get();
        $tipePhoto = TipePhoto::all();
        /*$distributor = Distributor::select('distributor.id','distributor.kd_dist','distributor.kd_tipe','distributor.kd_kota','distributor.nm_dist','distributor.almt_dist','distributor.telp_dist')
        ->join('kota','kota.id','=','distributor.kd_kota')->where('kota.kd_area','=',$kd_area)
        ->get();*/
        $distributor = Distributor::all();
        $tipe = Tipe::all();
        if(is_null($outlet))
        {
             $visits = "";
        }
        else
        {
            $visits = array();
            foreach($outlet as $outlet1)
            {
                $visit=VisitPlan::select('visit_plan.id','visit_plan.kd_outlet','visit_plan.date_visit','visit_plan.date_create_visit','visit_plan.approve_visit','visit_plan.status_visit','visit_plan.date_visiting','visit_plan.skip_order_reason','visit_plan.skip_reason')
                ->join('outlet','outlet.kd_outlet','=','visit_plan.kd_outlet')->where('outlet.kd_outlet','=',$outlet1->kd_outlet)->where('approve_visit','=',1)->get();
                foreach($visit as $vis)
                {
                    array_push($visits,$vis);
                }
            }
        }
        $produk = Produk::all();
        $dataUser = array("kota"=>$kota,"competitor"=>$competitor,"outlet"=>$outlet,"distributor"=>$distributor,
            "tipe"=>$tipe,"visitplan"=>$visits,"produk"=>$produk,"tipe_photo"=>$tipePhoto);
        return Response::json($dataUser,200);
    }
    public function getKota($kd_area)
    {
        //$kota = Kota::where('id','=',$kdKota)->first();
        //if(is_null($kota))
        //{
        //     return Response::json("your city is not registered",404);
        //}
        $data = Kota::where('kd_area','=',$kd_area)->get();
        if(is_null($data))
        {
             return Response::json("not found",404);
        }
        else
        {
             return Response::json($data,200);
        }     
    }
    public function getCompetitor()
    {
        $data = Competitor::all();
        if(is_null($data))
        {
             return Response::json("not found",404);
        }
        else
        {
             return Response::json($data,200);
        }     
    }
    public function getOutlet($kdUser)
    {
        $data = Outlet::where('kd_user','=',$kdUser)->get();
        if(is_null($data))
        {
             return Response::json("not found",404);
        }
        else
        {
             return Response::json($data,200);
        }     
    }
    public function getDistributor($kdArea)
    {
        //$kotaUser = Kota::where('id','=',$kdKota)->first();
        //if(is_null($kotaUser))
        //{
        //     return Response::json("your city is not registered",404);
        //}
        $data=Distributor::select('distributor.id','distributor.kd_dist','distributor.kd_tipe','distributor.kd_kota','distributor.nm_dist','distributor.almt_dist','distributor.telp_dist')
        ->join('kota','kota.id','=','distributor.kd_kota')->where('kota.kd_area','=',$kdArea)
        ->get();
        if(is_null($data))
        {
             return Response::json("not found",404);
        }
        else
        {
             return Response::json($data,200);
        }     
    }
    public function getTipe()
    {
        $data = Tipe::all();
        if(is_null($data))
        {
             return Response::json("not found",404);
        }
        else
        {
             return Response::json($data,200);
        }     
    }
    public function getVisit($kdUser)
    {
        $data = Outlet::where('kd_user','=',$kdUser)->get();
        if(is_null($data))
        {
             return Response::json("not found",404);
        }
        else
        {
            $visits = array();
            foreach($data as $outlet)
            {
                $visit=VisitPlan::select('visit_plan.id','visit_plan.kd_outlet','visit_plan.date_visit','visit_plan.date_create_visit','visit_plan.approve_visit','visit_plan.status_visit','visit_plan.date_visiting','visit_plan.skip_order_reason','visit_plan.skip_reason')
                ->join('outlet','outlet.kd_outlet','=','visit_plan.kd_outlet')->where('outlet.kd_outlet','=',$outlet->kd_outlet)->where('approve_visit','=',1)->get();
                foreach($visit as $vis)
                {
                    array_push($visits,$vis);
                }
            }

            return Response::json($visits,200);
        }     
    }
    public function getProduk()
    {
        $data = Produk::all();
        if(is_null($data))
        {
             return Response::json("not found",404);
        }
        else
        {
            return Response::json($data,200);
        }     
    }
    public function sendEmailReminder($email,$nik,$telp)
    {
        $user = User::where('email_user', '=', $email)->first();
        if(is_null($user))
        {
             $out['status'] = "not found";
             return Response::json($out,201);
        }
        else
        {
             if(!($user->nik == $nik && $user->telepon == $telp))
             {
                   $out['status'] = "not found";
                   return Response::json($out,201);
             }
        }     
        $newPass = str_random(8);
        if($user->update(array('password' => $newPass))){
            Mail::send('email.ubahPass', ['newPass' => $newPass], function($message) use ($user)
            {
                $message->from('sales@hisamitsu.co.id', 'Hisamitsu SF Report');
                $message->to($user->email_user, $user->nama)->subject('Reset Password!');
            });
        }
        $out['status'] = "success";
        return Response::json($out,201);
    }
}