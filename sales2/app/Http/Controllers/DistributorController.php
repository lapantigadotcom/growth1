<?php
/*
    PT. Trikarya Teknologi Indonesia
    Tenggilis raya 127
    Office Complex Apartment Metropolis MKB 206
    Surabaya, Jawa timur, Indonesia
    Phone : +6231-8420384 / +6281235537717
    Code By : Novita Nata Wardanie
*/
namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use App\Distributor;
use App\Kota;
use App\Tipe;
use App\Http\Controllers\Controller;
use App\Http\Requests\DistributorRequest;
use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;

class DistributorController extends Controller
{
    public function index()
    {
        $data = DB::table('distributor')
        ->select('distributor.id',
                'distributor.kd_dist', 
                'distributor.nm_dist', 
                'distributor.almt_dist',
                'distributor.kd_tipe',
                'kota.nm_kota',
                'distributor.telp_dist')
        ->join('kota', 'kota.id', '=', 'distributor.kd_kota')
        ->get();
        return view('pages.distributor.index', compact('data'));
    }

    public function create()
    {       
        $data['city'] = [''=>''] + Kota::lists('nm_kota', 'id')->toArray();
        return view('pages.distributor.create', compact('data'));
    }

    public function store(DistributorRequest $request)
    {
        Distributor::create($request->all());
        $log = new AdminController;
	$log->getLogHistory('Make New Distributor');
        return redirect()->route('admin.distributor.index');
    }

    public function edit($id)
    {
        $data['content'] = Distributor::find($id);
        $data['city']= [''=>''] + Kota::lists('nm_kota', 'id')->toArray();
        return view('pages.distributor.edit', compact('data'));
    }

    public function update(DistributorRequest $request, $id)
    {
        $data = Distributor::find($id);
        $data->update($request->all());
        $log = new AdminController;
	$log->getLogHistory('Update Distributor with ID '.$id);
        return redirect()->route('admin.distributor.index');
    }

    public function destroy($id)
    {
        $data = Distributor::find($id);
        $data->delete();
        $log = new AdminController;
	$log->getLogHistory('Delete Distributor with ID '.$id);
        return redirect()->route('admin.distributor.index');    
        
    }

    
}
