<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\PhotoActivity;

class TipePhoto extends Model
{
    //
    protected $table = 'tp_photo';
    protected $guarded = ['id'];
    protected $primaryKey = 'id';
    protected $fillable = ['nama_tipe'];
    public $timestamps = false;

    public function photoActivities()
    {
    	return $this->hasMany('App\PhotoActivity', 'jenis_photo', 'id');
    }
}
