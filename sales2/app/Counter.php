<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Counter extends Model
{
    //
    protected $table = 'to_counter';
    protected $guarded = ['id'];
    public $timestamps = false;
	protected $fillable = ['counter'];
}
