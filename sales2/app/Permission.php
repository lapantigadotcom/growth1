<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use App\Role;
use App\PermissionRole;

class Permission extends Model 
{
    //
    protected $table = 'permission';
    protected $guarded = ['id'];
    protected $fillable = [
                    'nm_permission',
                    'enable'
                    ];

    public $timestamps = false;

    public function permissionRoles()
    {
        return $this->belongsToMany('App\Role', 'permission_role', 'kd_permission', 'kd_role');
    }

    public function listRoles()
    {
        return $this->group();
    }
}
