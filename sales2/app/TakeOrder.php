<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\VisitPlan;
use App\Produk;

class TakeOrder extends Model
{
    protected $table = 'take_order';
    protected $guarded = ['id'];
    protected $fillable = ['kd_visitplan', 'kd_produk', 'qty_order', 'satuan','date_order','status_order'];

    public $timestamps = false;

    public function products()
    {
    	return $this->belongsTo('App\Produk', 'kd_produk', 'id');
    }

    public function visitPlans()
    {
    	return $this->belongsTo('App\VisitPlan', 'kd_visitplan', 'id');
    }
}
