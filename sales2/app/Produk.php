<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\TakeOrder;

class Produk extends Model
{
    //
    protected $table = 'produk';
    protected $guarded = 'id';
    protected $fillable = ['kd_produk','nm_produk'];
    public $timestamps = false;

    public function takeOrders()
    {
    	return $this->hasMany('App\TakeOrder', 'kd_produk', 'id');
    }
}
