// var elixir = require('laravel-elixir');


//  |--------------------------------------------------------------------------
//  | Elixir Asset Management
//  |--------------------------------------------------------------------------
//  |
//  | Elixir provides a clean, fluent API for defining some basic Gulp tasks
//  | for your Laravel application. By default, we are compiling the Sass
//  | file for our application, as well as publishing vendor resources.
//  |
 

// elixir(function(mix) {
//     mix.sass('app.scss');
// });


var gulp = require('gulp');
var replace = require('gulp-replace');
var rename = require("gulp-rename");

gulp.task('default', function() {
  gulp.src(['public/doodle/dist/css/font-awesome.min.css'])
    .pipe(replace(/\.fa-(.*):before \{\n\t(.*)\n\}/g, '.fa-$1 { \&:before { $2 } }'))
    .pipe(replace(/\[class\^="fa-"\], \[class\*=" fa-"\]/, '.fa'))
    .pipe(replace(/url\('fonts\//g, "url('@{icon-font-path}/"))
    .pipe(rename("my-icons.less"))
    .pipe(gulp.dest('./'));
});