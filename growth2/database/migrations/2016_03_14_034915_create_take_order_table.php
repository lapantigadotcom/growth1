<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTakeOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('take_order', function (Blueprint $table) {
            $table->increments('ke_order');
            $table->primary('kd_order');
            $table->integer('kd_visitplan');
            $table->foreign('kd_visitplan')->references('kd_visitplan')->on('visit_plan');
            $table->string('kd_produk',20);
            $table->foreign('kd_produk')->references('kd_produk')->on('produk');
            $table->integer('qty_order');
            $table->string('satuan',50);
            $table->timestamp('date_order');
            $table->smallInteger('status_order');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('take_order');
    }
}
