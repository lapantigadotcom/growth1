<?php

return [


    'driver' => 'smtp',
    'host' => 'palapa5.lazeon.com',
    'port' => 587,
    'from' => ['address' => null, 'name' => null],
    'encryption' => null,
    'username' => env('MAIL_USERNAME'),
    'password' => env('MAIL_PASSWORD'),
    'sendmail' => '/usr/sbin/sendmail -bs',
];
