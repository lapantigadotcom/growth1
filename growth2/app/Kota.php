<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Area;

class Kota extends Model
{
    //
    protected $table = 'kota';
    protected $guarded = ['id'];
    public $timestamps = false;

    public function areas()
    {
    	return $this->belongsTo('App\Area', 'kd_area', 'id');
    }
}
