<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class DistributorRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'kd_dist' => 'required',
			'kd_tipe' => 'required',
			'kd_kota' => 'required',
			'nm_dist' => 'required',
			'almt_dist' => 'required',
			'telp_dist' => 'required'
		];
	}

}
