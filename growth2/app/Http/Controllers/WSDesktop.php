<?php

namespace App\Http\Controllers;
use Hash;
use DB;
use Auth;
use Session;
use Response;
use Input;
use Mail;
use App\Exceptions\SymfonyDisplayer;
use App\User;
use App\TakeOrder;
use App\Produk;
use App\Role;
use App\Kota;
use App\Konfigurasi;
use App\Counter;
use App\Outlet;
use App\Distributor;
use App\Tipe;
use App\TipePhoto;
use App\Area;
use App\Logging;
use App\Competitor;
use App\VisitPlan;
use App\PhotoActivity;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;

class WSDesktop extends Controller
{
    
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function getAllData()
    {
    	$area = Area::all();
        $competitor = Competitor::all();
        $distributor = Distributor::all();
        $konfigurasi = Konfigurasi::all();
        $kota = Kota::all();
        $logging = Logging::all();
        $outlet = Outlet::all();
        $photo = PhotoActivity::all();
        $produk = Produk::all();
        $takeorder = TakeOrder::all();
        $tipe = Tipe::all();
        $tipePhoto = TipePhoto::all();
        $user = User::all();
        $visitplan = VisitPlan::all();
        $dataUser = array("area"=>$area,"competitor"=>$competitor,"distributor"=>$distributor,"konfigurasi"=>$konfigurasi,"kota"=>$kota,"logging"=>$logging,"outlet"=>$outlet,"photo"=>$photo,"produk"=>$produk,"takeorder"=>$takeorder,"tipe"=>$tipe,"tipe_photo"=>$tipePhoto,"user"=>$user,"visitplan"=>$visitplan);
        return Response::json($dataUser,200);
    }
    public function getTopSales()
    {
		$data['top']= DB::table('user')
		->select('user.nama','area.nm_area')
		->where('user.kd_role','=',3)
		// tambahan untuk top sales per bulan actual //
		->where(DB::raw('MONTH(date_visit)'), '=', date('m'))
		->selectRaw('count(visit_plan.id) as `TotalVisit`')
		->selectRaw('count(take_order.id) as `TotalEc`')
		->where('visit_plan.status_visit','=',1)
		->join('outlet','outlet.kd_user','=','user.id')
		->leftJoin('visit_plan','visit_plan.kd_outlet','=','outlet.kd_outlet')
		->leftJoin('take_order','take_order.kd_visitplan','=','visit_plan.id')
		->leftJoin('area','area.id','=','outlet.kd_area')
		->groupBy('user.id')
		->orderBy('TotalVisit', 'desc')
		->take(5)->get();
		return Response::json($data,200);
    }
    public function setVisit()
    {
    	$data=new VisitPlan;
        if(!Input::has('kd_outlet'))
        {
            $out['status'] = "no data sent";
            $out['id'] = 0;
            return Response::json($out,500);
        }
        $data->kd_outlet=Input::get('kd_outlet');
        $data->date_visit=Input::get('date_visit');
        $data->date_create_visit=Input::get('date_create_visit');
        $data->approve_visit=Input::get('approve_visit');
        $data->status_visit = Input::get('status_visit');
        $success=$data->save();
             
        if(!$success)
        {
            $out['status'] = "error saving";
            $out['id'] = 0;
            return Response::json($out,501);
        }
        $logging = new Logging;
        $logging->kd_user = 0;
        $logging->description = "Register visit plan to outlet ".Outlet::where("kd_outlet",Input::get("kd_outlet"))->first()->nm_outlet;
        $logging->log_time = Input::get('date_create_visit');
        $logging->detail_akses = "desktop";
        $logging->save();
        $out['status'] = "success";
        $out['id'] = $data->id;
        return Response::json($out,201);
    }
    public function editVisit()
    {
        $data = VisitPlan::where('id','=',Input::get('id'))->first();
        $data->kd_outlet=Input::get('kd_outlet');
        $data->date_visit=Input::get('date_visit');
        $data->date_create_visit=Input::get('date_create_visit');
        $data->approve_visit=Input::get('approve_visit');
        $data->status_visit = Input::get('status_visit');
        $data->date_visiting = Input::get('date_visiting');
        $data->skip_reason = Input::get('skip_reason');
        $data->save();
        $out['status'] = "success";
        $out['id'] = $data->id;
        return Response::json($out,201);
    }
    public function delVisit($id)
    {
        VisitPlan::where('id','=',$id)->delete();
        $out['status'] = "success";
        $out['id'] = $id;
        return Response::json($out,201);
    }
    public function setArea()
    {
    	$data = new Area;
    	$data->kd_area = Input::get('kd_area');
    	$data->nm_area = Input::get('nm_area');
    	$data->save();
    	$out['status'] = "success";
        $out['id'] = $data->id;
        return Response::json($out,201);
    }
    public function setProduk()
    {
        $data = new Produk;
        $data->kd_produk = Input::get('kd_produk');
        $data->nm_produk = Input::get('nm_produk');
        $data->save();
        $out['status'] = "success";
        $out['id'] = $data->id;
        return Response::json($out,201);
    }
    public function editProduk()
    {
        $data = Produk::where('id','=',Input::get('id'))->first();
        $data->kd_produk = Input::get('kd_produk');
        $data->nm_produk = Input::get('nm_produk');
        $data->save();
        $out['status'] = "success";
        $out['id'] = $data->id;
        return Response::json($out,201);
    }
    public function delProduk($id)
    {
        Produk::where('id','=',$id)->delete();
        $out['status'] = "success";
        $out['id'] = $id;
        return Response::json($out,201);
    }
    public function setOutlet()
    {
        $id = -1;
        $data=new Outlet;
        $kota = Kota::where("id",Input::get('kd_kota'))->first();
        if(!Input::has('nm_outlet'))
        {
            $out['status'] = "no data sent";
            return Response::json($out,500);
        }
        $konfig = Konfigurasi::first();
        $data->kd_dist=Input::get('kd_dist');
        $data->kd_kota=Input::get('kd_kota');
        $data->kd_area=$kota->kd_area;
        $data->kd_user=Input::get('kd_user');
        $data->nm_outlet=Input::get('nm_outlet');
        $data->almt_outlet=Input::get('almt_outlet');
        $data->kd_tipe=Input::get('kd_tipe');
        $data->rank_outlet=Input::get('rank_outlet');
        $data->kodepos=Input::get('kodepos');
        $data->reg_status=Input::get('reg_status');
        $data->latitude=Input::get('latitude');
        $data->longitude=Input::get('longitude');
        $data->nm_pic=Input::get('nm_pic');
        $data->tlp_pic=Input::get('tlp_pic');
        $data->toleransi_long=$konfig->toleransi_max;
        $success = $data->save();
        $path = 'image_upload/outlet/';
        $base =Input::get('foto_outlet');
        $binary = base64_decode($base);
        header('Content-Type: bitmap; charset=utf-8');

        $f = finfo_open();
        $mime_type = finfo_buffer($f, $binary, FILEINFO_MIME_TYPE);
        $mime_type = str_ireplace('image/', '', $mime_type);

        $filename =  strval($data->kd_outlet).date('_YmdHis'). '.' . $mime_type;
        $file = fopen($path . $filename, 'wb');
        if (fwrite($file, $binary)) {
            $data->update(array('foto_outlet' => $filename));
            fclose($file);
        }
        $logging = new Logging;
        $logging->kd_user = Input::get("kd_user");
        $logging->description = "Register outlet ".Input::get("nm_outlet");
        $logging->log_time = date('_YmdHis');
        $logging->detail_akses = "desktop";
        $logging->save();
        if(!$success)
        {
            $out['status'] = "error saving";
            return Response::json($out,501);
        }
        $out['status'] = "success";
        $out['id'] = $data->kd_outlet;
        $out['foto'] = $data->foto_outlet;
        $out['toleransi'] = $konfig->toleransi_max;
        return Response::json($out,201);
    }
    public function editOutlet()
    {
    	$data=Outlet::where('kd_outlet','=',Input::get('kd_outlet'))->first();
        $kota = Kota::where("id",Input::get('kd_kota'))->first();
        $konfig = Konfigurasi::first();
        $data->kd_dist=Input::get('kd_dist');
        $data->kd_kota=Input::get('kd_kota');
        $data->kd_area=$kota->kd_area;
        $data->kd_user=Input::get('kd_user');
        $data->nm_outlet=Input::get('nm_outlet');
        $data->almt_outlet=Input::get('almt_outlet');
        $data->kd_tipe=Input::get('kd_tipe');
        $data->rank_outlet=Input::get('rank_outlet');
        $data->kodepos=Input::get('kodepos');
        $data->reg_status=Input::get('reg_status');
        $data->latitude=Input::get('latitude');
        $data->longitude=Input::get('longitude');
        $data->nm_pic=Input::get('nm_pic');
        $data->tlp_pic=Input::get('tlp_pic');
        $data->toleransi_long=$konfig->toleransi_max;
        $success = $data->save();
        if($data->foto_outlet != Input::get('foto_outlet'))
        {
        	$path = 'image_upload/outlet/';
	        $base =Input::get('foto_outlet');
	        $binary = base64_decode($base);
	        header('Content-Type: bitmap; charset=utf-8');

	        $f = finfo_open();
	        $mime_type = finfo_buffer($f, $binary, FILEINFO_MIME_TYPE);
	        $mime_type = str_ireplace('image/', '', $mime_type);

	        $filename =  strval($data->kd_outlet).date('_YmdHis'). '.' . $mime_type;
	        $file = fopen($path . $filename, 'wb');
	        if (fwrite($file, $binary)) {
	            $data->update(array('foto_outlet' => $filename));
	            fclose($file);
	        }
        }
        $logging = new Logging;
        $logging->kd_user = Input::get("kd_user");
        $logging->description = "Update outlet ".Input::get("nm_outlet");
        $logging->log_time = date('_YmdHis');
        $logging->detail_akses = "desktop";
        $logging->save();
        if(!$success)
        {
            $out['status'] = "error saving";
            return Response::json($out,501);
        }
        $out['status'] = "success";
        $out['id'] = $data->kd_outlet;
        $out['foto'] = $data->foto_outlet;
        $out['toleransi'] = $konfig->toleransi_max;
        return Response::json($out,201);
    }
    public function delOutlet($id)
    {
    	Outlet::where('kd_outlet','=',$id)->delete();
        $out['status'] = "success";
        $out['id'] = $id;
        return Response::json($out,201);
    }
    public function setDistributor()
    {
        $data = new Distributor;
        $data->kd_dist = Input::get('Kd_dist');
        $data->kd_tipe = Input::get('Kd_tipe');
        $data->kd_kota = Input::get('Kd_kota');
        $data->nm_dist = Input::get('Nm_dist');
        $data->almt_dist = Input::get('Almt_dist');
        $data->telp_dist = Input::get('Telp_dist');
        $data->save();
        $out['status'] = "success";
        $out['id'] = $data->id;
        return Response::json($out,201);
    }
    public function editDistributor()
    {
        $data = Distributor::where('id','=',Input::get('Id'))->first();
        $data->kd_dist = Input::get('Kd_dist');
        $data->kd_tipe = Input::get('Kd_tipe');
        $data->kd_kota = Input::get('Kd_kota');
        $data->nm_dist = Input::get('Nm_dist');
        $data->almt_dist = Input::get('Almt_dist');
        $data->telp_dist = Input::get('Telp_dist');
        $data->save();
        $out['status'] = "success";
        $out['id'] = $data->id;
        return Response::json($out,201);
    }
    public function delDistributor($id)
    {
        Distributor::where('id','=',$id)->delete();
        $out['status'] = "success";
        $out['id'] = $id;
        return Response::json($out,201);
    }
    public function getFoto($kd_outlet)
    {
        $data = Outlet::where('kd_outlet', '=', $kd_outlet)->first();
        if(is_null($data)||is_null($data->foto_outlet))
        {
            $out['foto'] = "";
            return Response::json($out,201);
        }
        else
        {
            $path = 'image_upload/outlet/';
            $imagedata = file_get_contents($path.$data->foto_outlet);
            $out['status'] = "success";
            $out['foto'] = base64_encode($imagedata);
            return Response::json($out,200);
        }     
    }
    public function getUser($username,$password)
    {
        $data = User::where('username', '=', $username)->first();
        if(is_null($data))
        {
            $out['status'] = "not found";
            $out['user'] = $data;
            return Response::json($out,200);
        }
        else
        {
             if(Hash::check($password,$data->password))
             {
                $out['status'] = "success";
                $out['user'] = $data;
                return Response::json($out,200);
             }
             else
             {
                $out['status'] = "wrong password";
                $out['user'] = $data;
                return Response::json($out,200);
             }
        }     
    }
    public function delTakeOrder($id)
    {
    	$data = TakeOrder::where('id','=',$id)->first();
    	if(is_null($data)){
    		$out['status'] = "not found";
            return Response::json($out,200);
    	}
    	else
    	{
    		$data->delete();
	        $out['status'] = "success";
	        $out['id'] = $id;
	        return Response::json($out,200);
    	}
    }
}