<?php
/*
    PT. Trikarya Teknologi Indonesia
    Tenggilis raya 127
    Office Complex Apartment Metropolis MKB 206
    Surabaya, Jawa timur, Indonesia
    Phone : +6231-8420384 / +6281235537717
    Code By : Novita Nata Wardanie
*/
namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Input;
use Validator;
use App\TipePhoto;
use File;
use App\Http\Controllers\Controller;
use App\Http\Requests\PhotoRequest;
use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;

class TipePhotoController extends Controller
{
    public function index()
    {
        $data = TipePhoto::all();
        return view('pages.tipePhoto.index', compact('data'));
    }

    public function create()
    {
        return view('pages.tipePhoto.create');
    }

    public function store(PhotoRequest $request)
    {
        TipePhoto::create($request->all());
        Session::flash('create', 'Tipe Photo baru berhasil ditambahkan');
        return redirect()->route('admin.tipephoto.index');
    }

    public function edit($id)
    {
        $data['content'] = TipePhoto::find($id);
        return view('pages.tipePhoto.edit')->with('data',$data);
    }

    public function update(PhotoRequest $request, $id)
    {
        $photoAct = TipePhoto::find($id);       
        $photoAct->update($request->all());
        Session::flash('update', 'Tipe Photo berhasil diperbaharui');
        return redirect()->route('admin.tipephoto.index');
    }

    public function destroy($id)
    {
        $data = TipePhoto::find($id);
        $data->delete();
        Session::flash('delete', 'Tipe Photo berhasil dihapus');
        return redirect()->route('admin.photo.index'); 
    }
}
