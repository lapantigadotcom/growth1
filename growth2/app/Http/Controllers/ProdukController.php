<?php
/*
    PT. Trikarya Teknologi Indonesia
    Tenggilis raya 127
    Office Complex Apartment Metropolis MKB 206
    Surabaya, Jawa timur, Indonesia
    Phone : +6231-8420384 / +6281235537717
    Code By : Novita Nata Wardanie
*/
namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use App\Produk;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProdukRequest;
use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;

class ProdukController extends Controller
{
    public function index()
    {
        $data['content'] = Produk::all();
        return view('pages.produk.index', compact('data'));
    }

    public function create()
    {
        return view('pages.produk.create');
    }

    public function store(ProdukRequest $request)
    {
        Produk::create($request->all());
        $log = new AdminController;
        $log->getLogHistory('Make New Product');
        Session::flash('create', 'Produk baru berhasil ditambahkan');
        return redirect()->route('admin.produk.index');
    }

    public function edit($id)
    {
        $data['content'] = Produk::find($id);
        return view('pages.produk.edit', compact('data'));
    }

    public function update(ProdukRequest $request, $id)
    {
        $data = Produk::find($id);
        $data->update($request->all());
        $log = new AdminController;
        $log->getLogHistory('Update Product with ID '.$id);
        Session::flash('update', 'Produk berhasil diperbaharui');
        return redirect()->route('admin.produk.index');
    }

    public function destroy($id)
    {
        $data = Produk::with('takeOrders')->find($id);
        foreach($data->takeOrders as $row)
        {
            $row->delete();
        }
        $data->delete();
        $log = new AdminController;
        $log->getLogHistory('Delete Product with ID '.$id);
        Session::flash('delete', 'Produk berhasil dihapus');
        return redirect()->route('admin.produk.index');
        
    }

    
}
