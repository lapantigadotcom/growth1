<?php
/*
    PT. Trikarya Teknologi Indonesia
    Tenggilis raya 127
    Office Complex Apartment Metropolis MKB 206
    Surabaya, Jawa timur, Indonesia
    Phone : +6231-8420384 / +6281235537717
    Code By : Novita Nata Wardanie
*/
namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use App\Area;
use App\Kota;
use App\Http\Controllers\Controller;
use App\Http\Requests\KotaRequest;
use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;

class KotaController extends Controller
{
    public function index()
    {
        //$data['content'] = Area::all();
        $products = DB::table('area')->join('kota', 'kota.kd_area', '=', 'area.id')->get();
        return view('pages.area.index', compact('products'));
    }

    public function create()
    {
        $data = Area::all();
        $areas = [''=>''] + Area::lists('nm_area', 'id')->toArray();
         return view('pages.kota.create', compact('areas', 'data'));
    }

    public function store(KotaRequest $request)
    {
        Kota::create($request->all());
        $log = new AdminController;
        $log->getLogHistory('Make New Kota');
        Session::flash('create', 'Kota baru berhasil ditambahkan');
        return redirect()->route('admin.area.index');
    }

    public function edit($id)
    {
        $data['content'] = Kota::find($id);
        $data['area'] = Area::all();
        return view('pages.kota.edit', compact('data'));
    }

    public function update(KotaRequest $request, $id)
    {
        $data = Kota::find($id);
        $data->update($request->all());
        $log = new AdminController;
        $log->getLogHistory('Update Kota with ID '.$id);
        Session::flash('update', 'Kota berhasil diperbaharui');
        return redirect()->route('admin.area.index');
    }

    public function destroy($id)
    {
        $data =Kota::find($id);
        $data->delete();
        $log = new AdminController;
        $log->getLogHistory('Delete Kota with ID '.$id);
        Session::flash('delete', 'Kota berhasil dihapus');
        return redirect()->route('admin.area.index');
    }

    
}
