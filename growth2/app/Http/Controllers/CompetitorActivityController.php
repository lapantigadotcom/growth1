<?php
/*
    PT. Trikarya Teknologi Indonesia
    Tenggilis raya 127
    Office Complex Apartment Metropolis MKB 206
    Surabaya, Jawa timur, Indonesia
    Phone : +6231-8420384 / +6281235537717
    Code By : Novita Nata Wardanie
*/
namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Input;
use Validator;
use App\PhotoActivity;
use App\Outlet;
use App\Competitor;
use App\User;
use App\TipePhoto;
use File;
use App\Http\Controllers\Controller;
use App\Http\Requests\PhotoRequest;
use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;

class CompetitorActivityController extends Controller
{
    public function index()
    {
         $data['competitorAct'] = PhotoActivity::select('photo_activity.*',
            'competitor.nm_competitor','outlet.nm_outlet',
            'user.nama'
          )
        ->leftJoin('user','user.id','=','photo_activity.kd_user')
        ->leftJoin('outlet','outlet.kd_outlet','=','photo_activity.kd_outlet')
        ->leftJoin('competitor','competitor.id','=','photo_activity.kd_competitor')
        ->where('photo_activity.kd_competitor','!=',0)
        ->orWhere('photo_activity.kd_outlet','=',0)
        ->groupBy('photo_activity.id')
        ->get();
 
        $data['competitor'] = Competitor::all();
        return view('pages.competitor.index', compact('data'));
    }

    public function create()
    {
        $outlets = [''=>''] + Outlet::lists('nm_outlet', 'kd_outlet')->toArray();
        $competitors = [''=>''] + Competitor::lists('nm_competitor', 'id')->toArray();
        $users = [''=>''] + User::where('kd_role','=','3')->lists('nama', 'id')->toArray();
        $tipe = [''=>''] + TipePhoto::lists('nama_tipe', 'id')->toArray();
        return view('pages.competitor.create')->with('outlets',$outlets)->with('competitors',$competitors)->with('users',$users)->with('tipe',$tipe);
        //return view('pages.competitor.create')->with('competitors',$competitors)->with('users',$users);
    }

    public function store(PhotoRequest $request)
    {
        $rules = array(
            'nm_photo'         => 'required',    
            'path_photo'       => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {

            $messages = $validator->messages();

            $outlets = [''=>''] + Outlet::lists('nm_dist', 'id')->toArray();
            $competitors = [''=>''] + Competitor::lists('kd_area', 'id')->toArray();
            $users = [''=>''] + User::where('kd_role','=','3')->lists('nama', 'id')->toArray();

            return view('pages.competitor.create')
            ->with('outlets',$outlets)
            ->with('competitors',$competitors)
            ->with('users',$users)
            ->withErrors($validator);

        } else {
            $photoAct = new PhotoActivity;

            $file = $request->file('path_photo');
            $destinationPath = 'image_upload/competitor';
            $filename = str_random(6).'_'.$file->getClientOriginalName();
            $file->move($destinationPath, $filename);
            $outlet = Outlet::where('kd_outlet','=',$request->input('kd_outlet'))->first();
            $photoAct->kd_outlet = 0;
            $photoAct->kd_competitor = $request->input('kd_competitor');
            $photoAct->kd_user = $outlet->kd_user;
            $photoAct->nm_photo = $request->input('nm_photo');
            $photoAct->jenis_photo = $request->input('jenis_photo');
            $photoAct->date_take_photo = $request->input('date_take_photo');
            $photoAct->date_upload_photo = $request->input('date_upload_photo');
            $photoAct->almt_comp_activity = $request->input('almt_comp_activity');
            $photoAct->keterangan = $request->input('keterangan');
            $photoAct->path_photo = $filename;
            $photoAct->save();        
            $log = new AdminController;
            $log->getLogHistory('Make Competitor Activity');
            Session::flash('create', 'Competitor Activity baru berhasil ditambahkan');
            return redirect()->route('admin.competitor.index');
        } 
    }

    public function edit($id)
    {
        $data['content'] = PhotoActivity::find($id);
        $outlets = [''=>''] + Outlet::lists('nm_outlet', 'kd_outlet')->toArray();
        $competitors = [''=>''] + Competitor::lists('nm_competitor', 'id')->toArray();
        $users = [''=>''] + User::where('kd_role','=','3')->lists('nama', 'id')->toArray();
        $tipe = [''=>''] + TipePhoto::lists('nama_tipe', 'id')->toArray();
        return view('pages.competitor.edit')->with('data',$data)->with('outlets',$outlets)->with('competitors',$competitors)->with('users',$users)->with('tipe',$tipe);
    }

    public function update(PhotoRequest $request, $id)
    {
      $photoAct = PhotoActivity::find($id); 
        
        if(Input::hasFile('path_photo')){
            File::delete('image_upload/competitor/'.$photoAct->path_photo); 
            $file = $request->file('path_photo');
            $destinationPath = 'image_upload/competitor';
            $filename = str_random(6).'_'.$file->getClientOriginalName();
            $file->move($destinationPath, $filename);

            $photoAct->kd_competitor = $request->input('kd_competitor');
            $photoAct->kd_outlet = $request->input('kd_outlet');
            $photoAct->nm_photo = $request->input('nm_photo');
            $photoAct->jenis_photo = $request->input('jenis_photo');
            $photoAct->date_take_photo = $request->input('date_take_photo');
            $photoAct->date_upload_photo = $request->input('date_upload_photo');
            $photoAct->almt_comp_activity = $request->input('almt_comp_activity');
            $photoAct->keterangan = $request->input('keterangan');
            $photoAct->path_photo = $filename;
            $photoAct->save();        
            
            $log = new AdminController;
            $log->getLogHistory('Update Competitor Activity with ID '.$id);
            Session::flash('update', 'Competitor Activity berhasil diperbaharui');
            return redirect()->route('admin.competitor.index');
        }
        else
        {
            $photoAct->update($request->all());
            $log = new AdminController;
            $log->getLogHistory('Update Competitor Activity with ID '.$id);
            Session::flash('update', 'Competitor Activity berhasil diperbaharui');
            return redirect()->route('admin.competitor.index');
        }
    }

    public function destroy($id)
    {
        $data = PhotoActivity::find($id);
        $data->delete();
        $log = new AdminController;
        $log->getLogHistory('Delete Competitor Activity with ID '.$id);
        Session::flash('delete', 'Competitor Activity berhasil dihapus');
        return redirect()->route('admin.competitor.index');
    }
}
