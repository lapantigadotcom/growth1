<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Carbon\Carbon;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('partials.sidebar', function ($view) {
            $icons = ['fa fa-leaf', 'fa fa-bar-chart','fa fa-calendar', 'fa fa-camera', 'fa fa-user', 'fa fa-book', 'fa fa-cogs', 'fa fa-download'];

            $view->with(compact('icons'));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $current_time = Carbon::now()->format('l, j F Y');
        View::share('current_time', $current_time);
    }
}
