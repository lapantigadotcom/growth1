		<!-- Left Sidebar Menu -->
		{{-- <div class="fixed-sidebar-left">
			<ul class="nav navbar-nav side-nav nicescroll-bar">
				<li class="navigation-header">
					<span>Main</span> 
					<i class="zmdi zmdi-more"></i>
				</li>

	            @foreach(Auth::user()->getEffectiveMenu() as $idx => $menu)
	                @if(count($menu->_child) > 0)           
		            <li>
						<a href="javascript:void(0);" data-toggle="collapse" data-target="#menu{{$idx}}"><div class="pull-left"><i class="zmdi zmdi-map mr-20"></i><span class="right-nav-text">{{ $menu->nm_menu}}</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
						<ul id="menu{{$idx}}" class="collapse collapse-level-1">
							@foreach($menu->_child as $child)
							<li>
								<a href="{!! $child->route==''?'':route($child->route) !!}"><i class="{{ $child->icon }}"></i><span class="fa fa-arrow-right">&nbsp;
								{{ $child->nm_menu}}
								</span></a>
							</li>
							@endforeach
						</ul>
					</li>
					@else
					<li>
						<a href="{{ $menu->route==''?'':route($menu->route) }}"><div class="pull-left"><i class="fa fa-leaf mr-20"></i><span class="right-nav-text">{{ $menu->nm_menu }}</span></div><div class="clearfix"></div></a>
					</li>
					@endif
	            @endforeach
				
			</ul>
		</div> --}}
		<!-- /Left Sidebar Menu -->
		
		

		<!-- Left Sidebar Menu -->
		<div class="fixed-sidebar-left">
			<ul class="nav navbar-nav side-nav nicescroll-bar">
				<li class="navigation-header">
					<span>Main</span> 
					<i class="zmdi zmdi-more"></i>
				</li>
				<li>
					<a href="{{url('/admin/dashboard')}}"><div class="pull-left"><i class="zmdi zmdi-view-dashboard mr-20"></i><span class="right-nav-text">Dashboard</span></div><div class="pull-right"></div><div class="clearfix"></div></a>
				</li>
				<li>
					<a href="javascript:void(0);" data-toggle="collapse" data-target="#master_dr"><div class="pull-left"><i class="zmdi zmdi-chart mr-20"></i><span class="right-nav-text">Master Data </span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
					<ul id="master_dr" class="collapse collapse-level-1">
						<li>
							<a href="{{url('/admin/area')}}">Area</a>
						</li>
						<li>
							<a href="{{url('/admin/outlet')}}">Outlet</a>
						</li>
						<li>
							<a href="{{url('/admin/distributor')}}">Distributor</a>
						</li>
						<li>
							<a href="{{url('/admin/produk')}}">Produk</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="javascript:void(0);" data-toggle="collapse" data-target="#visit_dr"><div class="pull-left"><i class="zmdi zmdi-calendar-note mr-20"></i><span class="right-nav-text">Visit Plan </span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
					<ul id="visit_dr" class="collapse collapse-level-1">
						<li>
							<a href="{{url('/admin/visit/create')}}">Create Visit Plan</a>
						</li>
						<li>
							<a href="{{url('/admin/visitPlan')}}">Visit Plan List</a>
						</li>
						<li>
							<a href="{{url('/admin/visitingMonitor')}}">Visiting Monitor</a>
						</li>
						<li>
							<a href="{{url('/admin/takeOrder')}}">Take Order List</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="javascript:void(0);" data-toggle="collapse" data-target="#photo_dr"><div class="pull-left"><i class="zmdi zmdi-camera mr-20"></i><span class="right-nav-text">Photo Activity </span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
					<ul id="photo_dr" class="collapse collapse-level-1">
						<li>
							<a href="{{url('/admin/photoAct')}}">Photo Display</a>
						</li>
						<li>
							<a href="{{url('/admin/competitor')}}">Competitor Activity</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="javascript:void(0);" data-toggle="collapse" data-target="#user_dr"><div class="pull-left"><i class="zmdi zmdi-account mr-20"></i><span class="right-nav-text">User Manager </span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
					<ul id="user_dr" class="collapse collapse-level-1">
						<li>
							<a href="{{url('/admin/sales')}}">Sales Force</a>
						</li>
						<li>
							<a href="{{url('/admin/user')}}">Non SF</a>
						</li>
						<li>
							<a href="{{url('/admin/user/create')}}">Add User</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="{{url('/admin/report')}}"><div class="pull-left"><i class="zmdi zmdi-book mr-20"></i><span class="right-nav-text">Report</span></div><div class="pull-right"></div><div class="clearfix"></div></a>
				</li>
				<li>
					<a href="javascript:void(0);" data-toggle="collapse" data-target="#config_dr"><div class="pull-left"><i class="zmdi zmdi-settings mr-20"></i><span class="right-nav-text">Configuration </span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
					<ul id="config_dr" class="collapse collapse-level-1">
						<li>
							<a href="{{url('/admin/role')}}">Role Management</a>
						</li>
						<li>
							<a href="{{url('/admin/adminmenu')}}">Add Menu</a>
						</li>
						<li>
							<a href="{{url('/admin/permission')}}">Permission</a>
						</li>
						<li>
							<a href="{{url('/admin/outletManage')}}">Outlet Management</a>
						</li>
						<li>
							<a href="{{url('/admin/tipephoto')}}">Tipe Photo</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="{{url('/admin/download')}}"><div class="pull-left"><i class="zmdi zmdi-download mr-20"></i><span class="right-nav-text">Export/Import File</span></div><div class="pull-right"></div><div class="clearfix"></div></a>
				</li>
				{{-- @foreach($icons as $idx => $icon)
					<li>
						<a href="{{url('/admin/download')}}"><div class="pull-left"><i class="{{ $icon }} mr-20"></i><span class="right-nav-text">Icon</span></div><div class="pull-right"></div><div class="clearfix"></div></a>
					</li>
				@endforeach --}}

			</ul>
		</div>
		<!-- /Left Sidebar Menu -->
