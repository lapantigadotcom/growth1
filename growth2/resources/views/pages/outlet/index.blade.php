@extends('layouts.master')

@section('title', 'Outlet List')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">outlet management</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li><a href="#"><span>Master Data</span></a></li>
                            <li class="active"><span>Outlet</span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->

                <!-- Row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-primary card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-light"><i class="fa fa-building-o"></i>&nbsp;&nbsp;Outlet List</h6>
                                </div>
                                <div class="pull-right">
                                    @if(Auth::user()->hasAccess('admin.download.outlet'))  
                                            <a href="{!! route('admin.download.outlet') !!}" class="btn btn-default" style="background: white;"><i class="fa fa-download">&nbsp;&nbsp;DOWNLOAD</i></a>
                                    @endif
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="table-wrap">
                                    
                                        @if(Auth::user()->hasAccess('admin.outlet.create')) 
                                            <a href="{!! route('admin.outlet.create') !!}" class="btn btn-primary"><i class="fa fa-plus">&nbsp;&nbsp;Tambah Outlet</i></a>
                                        @endif

                                        <div class="table-responsive">
                                            <table class="table datable_1 table-hover table-bordered display mb-30">
                                                <thead>
                                                    <tr>
                                                        <th><center>Nama Outlet</center></th>
                                                        <th><center>Alamat</center></th>
                                                        {{-- <th><center>Kodepos</center></th> --}}
                                                        <th><center>Kota</center></th>
                                                        <th><center>Area</center></th>
                                                        <th><center>Tipe</center></th>
                                                        <th><center>Nama PIC</center></th>
                                                        <th><center>Tlp PIC</center></th>
                                                        @if(Auth::user()->kd_role != 3) 
                                                            <th><center>Action</center></th>
                                                        @else
                                                            <th></th>
                                                        @endif
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($data as $row)
                                                        <tr>
                                                            <td>{{ $row->nm_outlet }}</td>
                                                            <td>{{ $row->almt_outlet }}</td>
                                                            {{-- <td>{{ $row->kodepos }}</td> --}}
                                                            <td>{{ $row->nm_kota }}</td>
                                                            <td>{{ $row->kd_area }}</td>
                                                            <td>{{ $row->nm_tipe }}</td>
                                                            <td>{{ $row->nm_pic }}</td>
                                                            <td>{{ $row->tlp_pic }}</td>
                                                            @if(Auth::user()->kd_role != 3)   
                                                                <td>    
                                                                    <center>
                                                                        <div class="button-list">
                                                                            <a href="{!! route('admin.outlet.show',[$row->kd_outlet]) !!}" data-toggle="tooltip" title="Detail Outlet" class="btn btn-primary btn-sm btn-icon-anim btn-square mr-0" style="display: unset;"><i class="fa fa-search-plus" style="font-size: 14px;"></i></a>
                                                                            @if(Auth::user()->hasAccess('admin.outlet.edit'))
                                                                                <a href="{!! route('admin.outlet.edit',[$row->kd_outlet]) !!}" data-toggle="tooltip" title="Edit" class="btn btn-success btn-sm btn-icon-anim btn-square mr-0" style="display: unset;"><i class="fa fa-pencil" style="font-size: 14px;"></i></a>
                                                                            @endif
                                                                            @if(Auth::user()->hasAccess('admin.outlet.delete'))      
                                                                                <a href="{!! route('admin.outlet.delete',[$row->kd_outlet]) !!}" id="delete" data-toggle="tooltip" title="Delete" class="btn btn-danger btn-sm btn-icon-anim btn-square mr-0" style="display: unset;"><i class="fa fa-trash" style="font-size: 14px;"></i></a>
                                                                            @endif
                                                                        </div>
                                                                    </center>        
                                                                </td>
                                                            @endif
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <!-- /Row -->

            
                @include('partials.footer')

            </div>
        </div>
        <!-- /Main Content -->

        @include('partials.sweetalert')

        @push('message')
            @include('partials.toastr')
        @endpush

@endsection