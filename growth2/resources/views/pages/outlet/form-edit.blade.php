<h3><span class="number"><i class="fa fa-map-marker txt-black"></i></span><span class="head-font capitalize-font">maps</span></h3>
    <fieldset>
    <!--CREDIT CART PAYMENT-->
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <div class="form-group">
                <label class="control-label">Lokasi Outlet</label>
                <input type="text" id="pac-input" style="width: 300px; height: 35px; text-align: center;">
            </div>
            <div class="form-group">
                <div id="map-canvas" style="width:100%;height:500px;"></div>
            </div>
            <div class="form-group">
                {!! Form::label('latitude','Latitude', array('class' => 'control-label mb-10')) !!}
                {!! Form::text('latitude',null, array('id' => 'lat', 'class' => 'form-control', 'readonly')) !!}
            </div>
            <div class="form-group">
                {!! Form::label('longitude','Longitude', array('class' => 'control-label mb-10')) !!}
                {!! Form::text('longitude',null, array('id' => 'lng', 'class' => 'form-control', 'readonly')) !!}
            </div>
        </div>
    </div>
    <!--CREDIT CART PAYMENT END-->
</fieldset>

<h3><span class="number"><i class="fa fa-building-o txt-black"></i></span><span class="head-font capitalize-font">outlet</span></h3>
<fieldset>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-wrap">
                <div class="form-group">
                    {!! Form::label('kd_dist', 'Outlet Distributor', array('class' => 'control-label mb-10')) !!}
                    {!! Form::select('kd_dist', $dist, null, array('class' => 'selectpicker', 
                    'data-style' => 'form-control btn-default btn-outline', 'required')) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('kd_area', 'Outlet Area', array('class' => 'control-label mb-10')) !!}
                    {!! Form::select('kd_area',$areas, null, array('id' => 'id_area', 'class' => 'selectpicker', 
                    'data-style' => 'form-control btn-default btn-outline', 'required')) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('kd_kota', 'Kota', array('class' => 'control-label mb-10')) !!}
                    <select id="city" name="kd_kota" class="form-control">
                      <option value=""></option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-wrap">
                <div class="form-group">
                    {!! Form::label('kd_user', 'Registered Sales Force', array('class' => 'control-label mb-10')) !!}
                    {!! Form::select('kd_user', $users, null, array('class' => 'selectpicker', 
                    'data-style' => 'form-control btn-default btn-outline')) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('nm_outlet','Nama Outlet', array('class' => 'control-label mb-10')) !!}
                    {!! Form::text('nm_outlet',null, array('class' => 'form-control')) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('kode','Kode Outlet', array('class' => 'control-label mb-10')) !!}
                    {!! Form::text('kode',null, array('class' => 'form-control')) !!}
                </div>
            </div>
        </div>
    </div>
</fieldset>

<h3><span class="number"><i class="fa fa-location-arrow txt-black"></i></span><span class="head-font capitalize-font">alamat</span></h3>
<fieldset>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-wrap">
                <div class="form-group">
                    {!! Form::label('nm_pic','Nama PIC', array('class' => 'control-label mb-10')) !!}
                    {!! Form::text('nm_pic',null, array('class' => 'form-control')) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('tlp_pic','Telepon PIC', array('class' => 'control-label mb-10')) !!}
                    {!! Form::text('tlp_pic',null, array('class' => 'form-control')) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('kodepos','Kode Pos', array('class' => 'control-label mb-10')) !!}
                    {!! Form::text('kodepos',null, array('class' => 'form-control')) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('almt_outlet','Alamat', array('class' => 'control-label mb-10')) !!}
                    {!! Form::textarea('almt_outlet',null, array('rows'=>'3', 'class' => 'form-control')) !!}
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="from-wrap">
                <div class="form-group">
                    {!! Form::label('kd_tipe','Tipe Outlet', array('class' => 'control-label mb-10')) !!}
                    {!! Form::select('kd_tipe',$tipe, null, array('class' => 'selectpicker', 
                    'data-style' => 'form-control btn-default btn-outline', 'required')) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('rank_outlet','Rank Outlet', array('class' => 'control-label mb-10')) !!}
                    {!! Form::select('rank_outlet', [
		                       'A' => 'A',
		                       'B' => 'B',
		                       'C' => 'C',
		                       'D' => 'D',
		                       'E' => 'E',
		                       'F' => 'F',
		                    ], null, array('class' => 'selectpicker', 'data-style' => 'form-control btn-default btn-outline')) !!} 
                </div>
                <div class="form-group">
                    {!! Form::label('reg_status','Register Status', array('class' => 'control-label mb-10')) !!}
                    {!! Form::select('reg_status', [
                                   'YES' => 'YES',
                                   'NO' => 'NO',
                                   ], null, array('class' => 'selectpicker', 'data-style' => 'form-control btn-default btn-outline')) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('status_area','Status Area', array('class' => 'control-label mb-10')) !!}
                    {!! Form::select('status_area',[
		                   '1' => 'Good Coverage',
		                   '0' => 'Bad Coverage',
		                   ], null, array('class' => 'selectpicker', 'data-style' => 'form-control btn-default btn-outline')) !!}   
                </div>
            </div>
        </div>
    </div>
</fieldset>

<h3><span class="number"><i class="fa fa-camera-retro txt-black"></i></span><span class="head-font capitalize-font">foto</span></h3>
<fieldset>
    <div class="row">
        <div class="col-sm-12">
            <div class="col-md-6 col-md-offset-3">
                <div class="form-wrap">
                    <div class="form-group">
                        {!! Form::label('foto','Upload Foto', array('class' => 'control-label mb-10')) !!}
                        <input type="file" id="input-file-now-custom-2" name="path_photo" class="dropify" data-height="300" data-default-file="{{ asset('/image_upload/outlet/'.$data['content']->foto_outlet) }}" style="height: inherit;"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</fieldset>