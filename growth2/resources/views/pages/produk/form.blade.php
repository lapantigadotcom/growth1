<div class="col-xs-6 col-sm-4 col-sm-offset-4">
	<div class="form-group">
		{!! Form::label('kd_produk','Kode Produk', array('class' => 'control-label mb-10')) !!}
		{!! Form::text('kd_produk',null, array('class' => 'form-control', 'required')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('nm_produk','Nama Produk', array('class' => 'control-label mb-10')) !!}
		{!! Form::text('nm_produk',null, array('class' => 'form-control', 'required')) !!}	
	</div>
	@include('partials.errors')
	<div class="form-group">
		{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
	</div>
</div>