@extends('layouts.master')

@section('title', 'Order List')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">Visit Plan management</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li><a href="#"><span>Take Order</span></a></li>
                            <li class="active"><span>Result</span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->

                <!-- Row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-primary card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-light"><i class="fa fa-user"></i>&nbsp;&nbsp;Take Order List</h6>
                                </div>

                                @include('partials.panel')

                                <div class="clearfix"></div>
                            </div>
                            <div id="collapse_1" class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="table-wrap">
                        
                                        @if(Auth::user()->hasAccess('admin.order.create'))  
                                            <a href="{!! route('admin.order.create') !!}" class="btn btn-primary"><i class="fa fa-plus">&nbsp;&nbsp;Tambah Take Order</i></a>
                                        @endif
                                     
                                        <div class="table-responsive">
                                            <table class="table datable_1 table-hover table-bordered display mb-30">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align:center;">Nama SF</th>
                                                        <th style="text-align:center;">Kode Order</th>
                                                        <th style="text-align:center;">Kode Visit</th>
                                                        <th style="text-align:center;">Nama Outlet</th>
                                                        <th style="text-align:center;">Kota Outlet</th>
                                                        <th style="text-align:center;">Nama Produk</th>
                                                        <th style="text-align:center;">Jumlah Order</th>
                                                        <th style="text-align:center;">Satuan</th>
                                                        <th style="text-align:center;">Date Order</th>
                                                        <th style="text-align:center;">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($data as $row)
                                                        <tr>
                                                            <td>{{ $row->nama }}
                                                            <td>{{ $row->kd_to }}
                                                            <td>{{ $row->kd_visitplan }}</td>
                                                            <td>{{ $row->nm_outlet }}</td>
                                                            <td>{{ $row->nm_kota }}
                                                            <td>{{ $row->nm_area }}
                                                            <td>{{ $row->nm_produk}}</td>
                                                            <td>{{ $row->qty_order }}</td>   
                                                            <td>{{ $row->satuan }}</td>
                                                            <td>{{ date('d-F-Y H:i:s', strtotime($row->date_order)) }}</td>
                                                            <td>
                                                                <center>
                                                                    <div class="button-list">
                                                                        @if(Auth::user()->hasAccess('admin.order.edit'))
                                                                            <a href="{!! route('admin.order.edit',[$row->id]) !!}" data-toggle="tooltip" title="Edit" class="btn btn-success btn-sm btn-icon-anim btn-square mr-5" style="display: unset;"><i class="fa fa-pencil" style="font-size: 14px;"></i></a>
                                                                        @endif
                                                                        @if(Auth::user()->hasAccess('admin.order.delete'))      
                                                                            <a href="{!! route('admin.order.delete',[$row->id]) !!}" id="delete" data-toggle="tooltip" title="Delete" class="btn btn-danger btn-sm btn-icon-anim btn-square mr-5" style="display: unset;"><i class="fa fa-trash" style="font-size: 14px;"></i></a>
                                                                        @endif
                                                                    </div>
                                                                </center>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <!-- /Row -->
            
                @include('partials.footer')

            </div>
        </div>
        <!-- /Main Content -->

        @include('partials.sweetalert')

        @push('message')
            @include('partials.toastr')
        @endpush
        
@endsection