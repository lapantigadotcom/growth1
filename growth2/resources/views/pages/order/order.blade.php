@extends('layouts.master')

@section('link', '<link rel="stylesheet" href="/doodle/vendors/bower_components/summernote/dist/summernote.css" />
    <link href="/doodle/vendors/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" type="text/css"/>
    <link href="/doodle/vendors/bower_components/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet" type="text/css"/>
  <link href="/doodle/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>')

@section('title', 'Take Order List')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">Take Order management</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li><a href="#"><span>Take Order</span></a></li>
                            <li class="active"><span>Take Order List Filter</span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->

                <!-- Row -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-primary card-view panel-refresh">
                                <div class="refresh-container">
                                    <div class="la-anim-1"></div>
                                </div>
                                <div class="panel-heading">
                                    <div class="pull-left">
                                        <h6 class="panel-title txt-light"><i class="fa fa-filter"></i>&nbsp;&nbsp;Filter Data Date Order</h6>
                                    </div>
                                    @include('partials.panel')
                                    <div class="clearfix"></div>
                                </div>
                                <div id="collapse_1" class="panel-wrapper collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-wrap">

                                                    {!! Form::open(array('route' => 'admin.takeOrder.result', 'method' => 'POST', 'data-toggle' => 'validator', 'role' => 'form', 'class' => 'form-horizontal')) !!}
                                                        @include('pages.order.formFilter')
                                                    {!! Form::close() !!}

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- /Row -->

                <!-- Row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-primary card-view panel-refresh">
                            <div class="refresh-container">
                                <div class="la-anim-1"></div>
                            </div>
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-light"><i class="fa fa-shopping-cart"></i>&nbsp;&nbsp;Take Order 1 Minggu Terakhir</h6>
                                </div>

                                @include('partials.panel2')

                                <div class="clearfix"></div>
                            </div>
                            <div id="collapse_2" class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="table-wrap">
                                        <div class="table-responsive">
                                            <table class="table datable_1 table-hover display pb-30" >
                                                <thead>
                                                    <tr>
                                                        <th style="text-align:center;">NAMA SF</th>
                                                        <th style="text-align:center;">Kode Order</th>
                                                        <th style="text-align:center;">Kode Visit</th>
                                                        <th style="text-align:center;">Nama Outlet</th>
                                                        <th style="text-align:center;">Kota Outlet</th>
                                                        <th style="text-align:center;">Area Outlet</th>
                                                        <th style="text-align:center;">Nama Produk</th>
                                                        <th style="text-align:center;">Jumlah Order</th>
                                                        <th style="text-align:center;">Satuan</th>
                                                        <th style="text-align:center;">Date Order</th>
                                                        <th style="text-align:center;">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($data as $row)
                                                        <tr>
                                                            <td>{{ $row->nama }}
                                                            <td>{{ $row->kd_to }}
                                                            <td>{{ $row->kd_visitplan }}</td>
                                                            <td>{{ $row->nm_outlet }}</td>
                                                            <td>{{ $row->nm_kota }}
                                                            <td>{{ $row->nm_area }}
                                                            <td>{{ $row->nm_produk}}</td>
                                                            <td>{{ $row->qty_order }}</td>   
                                                            <td>{{ $row->satuan }}</td>
                                                            <td>{{ date('d-F-Y H:i:s', strtotime($row->date_order)) }}</td>
                                                            <td>
                                                                <center>
                                                                    <div class="button-list">
                                                                        
                                                                    </div>
                                                                        @if(Auth::user()->hasAccess('admin.order.edit'))
                                                                            <a href="{!! route('admin.order.edit',[$row->id]) !!}" data-toggle="tooltip" title="Edit" class="btn btn-success btn-sm btn-icon-anim btn-square mr-5" style="display: unset;"><i class="fa fa-pencil" style="font-size: 14px;"></i></a>
                                                                        @endif
                                                                        &nbsp;
                                                                        @if(Auth::user()->hasAccess('admin.order.delete'))      
                                                                            <a href="{!! route('admin.order.delete',[$row->id]) !!}" id="delete" data-toggle="tooltip" title="Delete" class="btn btn-danger btn-sm btn-icon-anim btn-square mr-5" style="display: unset;"><i class="fa fa-trash" style="font-size: 14px;"></i></a>
                                                                        @endif
                                                                </center>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <!-- /Row -->
            
                @include('partials.footer')

            </div>
        </div>
        <!-- /Main Content -->

        @include('partials.sweetalert')

        @push('message')
            @include('partials.toastr')
        @endpush
        
        <!-- Moment JavaScript -->
        <script type="text/javascript" src="/doodle/vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
            
        <!-- Bootstrap Colorpicker JavaScript -->
        <script src="/doodle/vendors/bower_components/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
        <!-- Bootstrap Datetimepicker JavaScript -->
        <script type="text/javascript" src="/doodle/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

        <script type="text/javascript">
            $(function () {
                $('.datetimepicker1').datetimepicker({
                    format: 'YYYY-MM-DD'
                });
            });
        </script>




@endsection