@extends('layouts.master')

@section('title', 'Create Toleransi')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">Outlet management</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/home/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li><a href="#"><span>Configuration</span></a></li>
                            <li><a href="{{url('/admin/outletManage')}}"><span>Outlet Management</span></a></li>
                            <li class="active"><span>Toleransi</span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->

                <!-- Row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-primary card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-light"><i class="fa fa-wpforms"></i>&nbsp;&nbsp;Outlet Management</h6>
                                </div>
                                @include('partials.panel')
                                <div class="clearfix"></div>
                            </div>
                            <div id="collapse_1" class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-wrap">
                                                <div class="col-xs-6 col-sm-4 col-sm-offset-4">
                                                    {!! Form::open(array('route' => 'admin.outletManage.updateToleransi', 'method' => 'store', 'data-toggle' => 'validator', 'role' => 'form')) !!}
                                                        <div class="form-group">
                                                            {!! Form::label('toleransi_long','Toleransi', array('class' => 'control-label mb-10')) !!}
                                                            {!! Form::text('toleransi_long',null, array('class' => 'form-control', 'required')) !!}
                                                            <span><p class="text-info mb-10">(*) dalam meter</p></span>
                                                        </div>
                                                        <div class="form-group">
                                                            {!! Form::submit('Perbarui', array('class' => 'btn btn-primary')) !!}
                                                        </div>
                                                    {!! Form::close() !!}
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Row -->

                <!-- Row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-primary card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-light"><i class="fa fa-building-o"></i>&nbsp;&nbsp;List Tipe Outlet</h6>
                                </div>
                                @include('partials.panel2')
                                <div class="clearfix"></div>
                            </div>
                            <div id="collapse_2" class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="table-wrap">
                                        <div class="table-responsive">
                                            <table class="table table-hover datable_1 table-bordered display mb-30">
                                                <thead>
                                                    <tr>
                                                        <th>Tipe Outlet</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($data as $row)
                                                    <tr>
                                                        <td>{{ $row->nm_tipe }}</td>
                                                        <td>
                                                            <center>
                                                                <a href="{!! route('admin.tipe.edit',[$row->id]) !!}" class="btn btn-success btn-sm btn-icon-anim btn-square" style="display: unset;"><i class="fa fa-pencil" style="font-size: 14px;"></i></a>
                                                                &nbsp;   
                                                                <a href="{!! route('admin.tipe.delete',[$row->id]) !!}" id="delete" class="btn btn-danger btn-sm btn-icon-anim btn-square" style="display: unset;"><i class="fa fa-trash" style="font-size: 14px;"></i></a>
                                                            </center>

                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <!-- /Row -->

                @include('partials.footer')

            </div>
        </div>
        <!-- /Main Content -->

        @include('partials.sweetalert')


@endsection