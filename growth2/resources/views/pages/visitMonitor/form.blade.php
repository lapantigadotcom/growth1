<div class="col-xs-6 col-sm-4 col-sm-offset-4">
	<div class="form-group">
		{!! Form::label('kd_outlet','Nama Outlet', array('class' => 'control-label mb-10')) !!}
		{!! Form::select('kd_outlet', $outlet, null, array('class' => 'selectpicker', 
		'data-style' => 'data-style="form-control btn-default btn-outline', 'required')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('status_visit','Status Visiting', array('class' => 'control-label mb-10')) !!}
		{!! Form::select('status_visit', [
				   '' => 'Select Status Visiting',
				   '1' => 'YES',
				   '0' => 'NO'], null, array('class' => 'selectpicker', 'data-style' => 'form-control btn-default btn-outline')
				) !!}
	</div>
	<div class="form-group">
        {!! Form::label('date_visit','Tanggal Visit Plan', array('class' => 'control-label mb-10')) !!}
		{!! Form::text('date_visit', date('d-F-Y H:i:s', strtotime($data['content']->date_visit)), array('class' => 'form-control', 'disabled' => 'disabled')) !!}
	</div>
	<div class="form-group">
        <div class="control-group success">
			<div class="controls">
			    {!! Form::label('date_visiting','Tanggal Visiting (Aktual)', array('class' => 'control-label mb-10')) !!}
			    <div class='input-group date' id='datetimepicker1'>
	                <input type='text' name="date_visiting" class="form-control" />
	                <span class="input-group-addon">
	                    <span class="glyphicon glyphicon-calendar"></span>
	                </span>
	            </div>
	            <span><p class="text-success mb-10">Di isi jika Status Visit "YES"</p></span>
            </div>
		</div>
	
		<div class="control-group error">
		<div class="controls">
			{!! Form::label('skip_reason','Alasan Skip Visit', array('class' => 'control-label mb-10')) !!}
			{!! Form::text('skip_reason', null, array('class' => 'form-control')) !!}
		 	<span><p class="text-info mb-10">Di isi jika Status Visit "NO"</p></span>
		</div>
	</div>

	<div class="form-group">
		{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
	</div>
</div>

	<!-- Moment JavaScript -->
	<script type="text/javascript" src="/doodle/vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
		
	<!-- Bootstrap Colorpicker JavaScript -->
	<script src="/doodle/vendors/bower_components/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
	<!-- Bootstrap Datetimepicker JavaScript -->
	<script type="text/javascript" src="/doodle/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

	<script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker({
            	format: 'YYYY-MM-DD HH:mm:ss'
            });
        });
    </script>




