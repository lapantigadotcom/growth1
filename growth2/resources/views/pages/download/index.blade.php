@extends('layouts.master')

@section('link', '
    <link href="/doodle/vendors/bower_components/dropify/dist/css/dropify.min.css" rel="stylesheet" type="text/css"/>')

@section('title', 'Export/Import File')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">Export/Import  management</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/home/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li class="active"><span>Export/Import File</span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->

                <!-- Row -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-primary card-view panel-refresh">
                                <div class="refresh-container">
                                    <div class="la-anim-1"></div>
                                </div>
                                <div class="panel-heading">
                                    <div class="pull-left">
                                        <h6 class="panel-title txt-light"><i class="fa fa-download"></i>&nbsp;&nbsp;Download List</h6>
                                    </div>
                                    
                                    @include('partials.panel')

                                    <div class="clearfix"></div>
                                </div>
                                <div id="collapse_1" class="panel-wrapper collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                            
                                            <div class="col-md-2">
                                                <div class="panel panel-default card-view pa-0">
                                                    <div  class="panel-wrapper collapse in">
                                                        <div  class="panel-body pa-0">
                                                            <div class="sm-data-box bg-green">
                                                                <div class="container-fluid">
                                                                    <div class="row">
                                                                        <a href="{{url('admin/download/area')}}">
                                                                            <div class="text-center data-wrap-right">
                                                                                <i class="fa fa-globe txt-light data-right-rep-icon"></i>
                                                                            </div>
                                                                            <div class="text-center">
                                                                                <span class="txt-light block pa-0 pb-10">
                                                                                    <strong>Area</strong>
                                                                                </span>
                                                                            </div>
                                                                        </a>
                                                                    </div>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="panel panel-default card-view pa-0">
                                                    <div  class="panel-wrapper collapse in">
                                                        <div  class="panel-body pa-0">
                                                            <div class="sm-data-box bg-red">
                                                                <div class="container-fluid">
                                                                    <div class="row">
                                                                        <a href="{{url('admin/download/outlet')}}">
                                                                            <div class="text-center data-wrap-right">
                                                                                <i class="fa fa-home txt-light data-right-rep-icon"></i>
                                                                            </div>
                                                                            <div class="text-center">
                                                                                <span class="txt-light block pa-0 pb-10">
                                                                                    <strong>Outlet</strong>
                                                                                </span>
                                                                            </div>
                                                                        </a>
                                                                    </div>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="panel panel-default card-view pa-0">
                                                    <div  class="panel-wrapper collapse in">
                                                        <div  class="panel-body pa-0">
                                                            <div class="sm-data-box bg-blue">
                                                                <div class="container-fluid">
                                                                    <div class="row">
                                                                        <a href="{{url('admin/download/distributor')}}">
                                                                            <div class="text-center data-wrap-right">
                                                                                <i class="fa fa-truck txt-light data-right-rep-icon"></i>
                                                                            </div>
                                                                            <div class="text-center">
                                                                                <span class="txt-light block pa-0 pb-10">
                                                                                    <strong>Distributor</strong>
                                                                                </span>
                                                                            </div>
                                                                        </a>
                                                                    </div>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="panel panel-default card-view pa-0">
                                                    <div  class="panel-wrapper collapse in">
                                                        <div  class="panel-body pa-0">
                                                            <div class="sm-data-box bg-yellow">
                                                                <div class="container-fluid">
                                                                    <div class="row">
                                                                        <a href="{{url('admin/download/produk')}}">
                                                                            <div class="text-center data-wrap-right">
                                                                                <i class="fa fa-barcode txt-light data-right-rep-icon"></i>
                                                                            </div>
                                                                            <div class="text-center">
                                                                                <span class="txt-light block pa-0 pb-10">
                                                                                    <strong>Produk</strong>
                                                                                </span>
                                                                            </div>
                                                                        </a>
                                                                    </div>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="panel panel-default card-view pa-0">
                                                    <div  class="panel-wrapper collapse in">
                                                        <div  class="panel-body pa-0">
                                                            <div class="sm-data-box bg-blue">
                                                                <div class="container-fluid">
                                                                    <div class="row" style="background: teal">
                                                                        <a href="{{url('admin/download/visitPlan')}}">
                                                                            <div class="text-center data-wrap-right">
                                                                                <i class="fa fa-calendar txt-light data-right-rep-icon"></i>
                                                                            </div>
                                                                            <div class="text-center">
                                                                                <span class="txt-light block pa-0 pb-10">
                                                                                    <strong>Visit Plan</strong>
                                                                                </span>
                                                                            </div>
                                                                        </a>
                                                                    </div>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="panel panel-default card-view pa-0">
                                                    <div  class="panel-wrapper collapse in">
                                                        <div  class="panel-body pa-0">
                                                            <div class="sm-data-box bg-green">
                                                                <div class="container-fluid">
                                                                    <div class="row" style="background: darkslategrey">
                                                                        <a href="{{url('admin/download/visitMonitor')}}">
                                                                            <div class="text-center data-wrap-right">
                                                                                <i class="fa fa-check txt-light data-right-rep-icon"></i>
                                                                            </div>
                                                                            <div class="text-center">
                                                                                <span class="txt-light block pa-0 pb-10">
                                                                                    <strong>Visit Monitor</strong>
                                                                                </span>
                                                                            </div>
                                                                        </a>
                                                                    </div>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="panel panel-default card-view pa-0">
                                                    <div  class="panel-wrapper collapse in">
                                                        <div  class="panel-body pa-0">
                                                            <div class="sm-data-box bg-pink">
                                                                <div class="container-fluid">
                                                                    <div class="row">
                                                                        <a href="{{url('admin/download/takeOrder')}}">
                                                                            <div class="text-center data-wrap-right">
                                                                                <i class="fa fa-shopping-cart txt-light data-right-rep-icon"></i>
                                                                            </div>
                                                                            <div class="text-center">
                                                                                <span class="txt-light block pa-0 pb-10">
                                                                                    <strong>Take Order</strong>
                                                                                </span>
                                                                            </div>
                                                                        </a>
                                                                    </div>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="panel panel-default card-view pa-0">
                                                    <div  class="panel-wrapper collapse in">
                                                        <div  class="panel-body pa-0">
                                                            <div class="sm-data-box bg-yellow">
                                                                <div class="container-fluid">
                                                                    <div class="row" style="background: blueviolet">
                                                                        <a href="{{url('admin/download/salesForce')}}">
                                                                            <div class="text-center data-wrap-right">
                                                                                <i class="fa fa-group txt-light data-right-rep-icon"></i>
                                                                            </div>
                                                                            <div class="text-center">
                                                                                <span class="txt-light block pa-0 pb-10">
                                                                                    <strong>Sales Force</strong>
                                                                                </span>
                                                                            </div>
                                                                        </a>
                                                                    </div>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="panel panel-default card-view pa-0">
                                                    <div  class="panel-wrapper collapse in">
                                                        <div  class="panel-body pa-0">
                                                            <div class="sm-data-box bg-dark">
                                                                <div class="container-fluid">
                                                                    <div class="row" style="background: brown">
                                                                        <a href="{{url('admin/download/nonSF')}}">
                                                                            <div class="text-center data-wrap-right">
                                                                                <i class="fa fa-user txt-light data-right-rep-icon"></i>
                                                                            </div>
                                                                            <div class="text-center">
                                                                                <span class="txt-light block pa-0 pb-10">
                                                                                    <strong>Non SF</strong>
                                                                                </span>
                                                                            </div>
                                                                        </a>
                                                                    </div>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- /Row -->

                <!-- Row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-primary card-view panel-refresh">
                            <div class="refresh-container">
                                <div class="la-anim-1"></div>
                            </div>
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-light"><i class="fa fa-upload"></i>&nbsp;&nbsp;Upload Data</h6>
                                </div>

                                <div class="pull-right">
                                    <div class="tab-struct custom-tab-1">
                                        <ul role="tablist" class="nav nav-tabs" id="myTabs_9">
                                            <li class="active" role="presentation">
                                                <a aria-expanded="true" data-toggle="tab" role="tab" id="home_tab_9" href="#outlet"><p class="panel-title txt-light">Outlet</p></a>
                                            </li>
                                            <li role="presentation" >
                                                <a data-toggle="tab" id="profile_tab_1" role="tab" href="#user" aria-expanded="false"><p class="panel-title txt-light">User</p></a>
                                            </li>
                                            <li role="presentation" >
                                                <a data-toggle="tab" id="profile_tab_2" role="tab" href="#distributor" aria-expanded="false"><p class="panel-title txt-light">Distributor</p></a>
                                            </li>
                                            <li role="presentation" >
                                                <a data-toggle="tab" id="profile_tab_3" role="tab" href="#produk" aria-expanded="false"><p class="panel-title txt-light">Produk</p></a>
                                            </li>
                                            <li role="presentation" >
                                                <a data-toggle="tab" id="profile_tab_4" role="tab" href="#area" aria-expanded="false"><p class="panel-title txt-light">Area</p></a>
                                            </li>
                                        </ul>
                                    </div> 
                                </div>

                                <div class="clearfix"></div>
                            </div>
                            <div class="tab-content" id="myTabContent_12">
                                <div id="outlet" class="tab-pane fade active in" role="tabpanel">
                                    <div class="panel-wrapper collapse in">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-wrap">
                                                        <div class="col-xs-6 col-sm-4 col-sm-offset-4">
                                                            {!! Form::open(array('route' => 'admin.upload.outlet', 'method' => 'POST',  'files' => true, 'data-toggle' => 'validator', 'role' => 'form')) !!}
                                                            {!! Form::label('fileUpload','Upload User.xls', array('class' => 'control-label mb-10')) !!}
                                                                <input type="file" id="input-file-now" name="fileUpload" class="dropify" required="" />
                                                                <center>
                                                                    <div class="form-group pa-20 pb-10 pl-0 pr-0">
                                                                        <button class="btn btn-lg btn-success btn-anim">
                                                                            <i class="fa fa-upload"></i><span class="btn-text">Upload</span>
                                                                        </button>
                                                                    </div>
                                                                </center>
                                                            {!! Form::close() !!}
                                                            @include('partials.errors')
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="user" class="tab-pane fade in" role="tabpanel">
                                    <div class="panel-wrapper collapse in">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-wrap">
                                                        <div class="col-xs-6 col-sm-4 col-sm-offset-4">
                                                            {!! Form::open(array('route' => 'admin.upload.user', 'method' => 'POST',  'files' => true, 'data-toggle' => 'validator', 'role' => 'form')) !!}
                                                            {!! Form::label('fileUpload','Upload User.xls', array('class' => 'control-label mb-10')) !!}
                                                                <input type="file" id="input-file-now" name="fileUpload" class="dropify"/>
                                                                <center>
                                                                    <div class="form-group pa-20 pb-10 pl-0 pr-0">
                                                                        <button class="btn btn-lg btn-success btn-anim">
                                                                            <i class="fa fa-upload"></i><span class="btn-text">Upload</span>
                                                                        </button>
                                                                    </div>
                                                                </center>
                                                            {!! Form::close() !!}
                                                            @include('partials.errors')
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="distributor" class="tab-pane fade in" role="tabpanel">
                                    <div class="panel-wrapper collapse in">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-wrap">
                                                        <div class="col-xs-6 col-sm-4 col-sm-offset-4">
                                                            {!! Form::open(array('route' => 'admin.upload.distributor', 'method' => 'POST',  'files' => true, 'data-toggle' => 'validator', 'role' => 'form')) !!}
                                                            {!! Form::label('fileUpload','Upload Distributor.xls', array('class' => 'control-label mb-10')) !!}
                                                                <input type="file" id="input-file-now" name="fileUpload" class="dropify"/>
                                                                <center>
                                                                    <div class="form-group pa-20 pb-10 pl-0 pr-0">
                                                                        <button class="btn btn-lg btn-success btn-anim">
                                                                            <i class="fa fa-upload"></i><span class="btn-text">Upload</span>
                                                                        </button>
                                                                    </div>
                                                                </center>
                                                            {!! Form::close() !!}
                                                            @include('partials.errors')
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="produk" class="tab-pane fade in" role="tabpanel">
                                    <div class="panel-wrapper collapse in">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-wrap">
                                                        <div class="col-xs-6 col-sm-4 col-sm-offset-4">
                                                            {!! Form::open(array('route' => 'admin.upload.produk', 'method' => 'POST',  'files' => true, 'data-toggle' => 'validator', 'role' => 'form')) !!}
                                                            {!! Form::label('fileUpload','Upload Produk.xls', array('class' => 'control-label mb-10')) !!}
                                                                <input type="file" id="input-file-now" name="fileUpload" class="dropify"/>
                                                                <center>
                                                                    <div class="form-group pa-20 pb-10 pl-0 pr-0">
                                                                        <button class="btn btn-lg btn-success btn-anim">
                                                                            <i class="fa fa-upload"></i><span class="btn-text">Upload</span>
                                                                        </button>
                                                                    </div>
                                                                </center>
                                                            {!! Form::close() !!}
                                                            @include('partials.errors')
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="area" class="tab-pane fade in" role="tabpanel">
                                    <div class="panel-wrapper collapse in">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-wrap">
                                                        <div class="col-xs-6 col-sm-4 col-sm-offset-4">
                                                            {!! Form::open(array('route' => 'admin.upload.area', 'method' => 'POST',  'files' => true, 'data-toggle' => 'validator', 'role' => 'form')) !!}
                                                            {!! Form::label('fileUpload','Upload Area.xls', array('class' => 'control-label mb-10')) !!}
                                                                <input type="file" id="input-file-now" name="fileUpload" class="dropify"/>
                                                                <center>
                                                                    <div class="form-group pa-20 pb-10 pl-0 pr-0">
                                                                        <button class="btn btn-lg btn-success btn-anim">
                                                                            <i class="fa fa-upload"></i><span class="btn-text">Upload</span>
                                                                        </button>
                                                                    </div>
                                                                </center>
                                                            {!! Form::close() !!}
                                                            @include('partials.errors')
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>  
                    </div>
                </div>
                <!-- /Row -->
            
                @include('partials.footer')

            </div>
        </div>
        <!-- /Main Content -->

        <script type="text/javascript">
        $(document).ready(function() {
            $('#upload').bind("click",function() 
            { 
                var upVal = $('#fileUp').val(); 
                if(upVal=='') 
                { 
                    alert("Empty input file");
                } 
            }); 
        });
        </script> 

@endsection

@section('script', '
    <!-- Bootstrap dropify JavaScript -->
    <script src="/doodle/vendors/bower_components/dropify/dist/js/dropify.min.js"></script>
    <!-- Form Flie Upload Data JavaScript -->
    <script src="/doodle/dist/js/form-file-upload-data.js"></script>')