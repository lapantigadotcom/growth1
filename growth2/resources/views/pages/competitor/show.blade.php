<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>GROWTH | {!! $data['content']->nm_outlet !!}</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        @include('partials.head')
        <script src="//cdn.ckeditor.com/4.5.2/standard/ckeditor.js"></script>

    </head>
<body>
<div class="container-fluid-full">
    <div class="row-fluid"> 
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="left-side sidebar-offcanvas">
        @include('partials.sidebar')
        </aside>
<!-- Main content -->
<div id="content" class="span9">
  <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="{{url('/home/dashboard')}}">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">Master Data</a>
            <i class="icon-angle-right"></i>
            
        </li>
        <li>
            <a href="#">Outlet</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">Detail Outlet</a>
        </li>
    </ul>
        <div class="row-fluid sortable">        
                <div class="box span10">
                    <div class="box-header">
                        <h2><i class="halflings-icon th-list"></i><span class="break"></span>{!! $data['content']->nm_outlet !!}</h2>
                    </div>
                    
                    <div class="box-content">
                        <div class="row-fluid">            
                              <div class="span3">
                                <img src="{!! url('/img/outlet.png') !!}" class="img-responsive img-thumbnail">                             
                              </div>
                              <div class="span4">    
                                <p>Nama Outlet : {!! $data['content']->nm_outlet !!}</p>
                                <p>Alamat  : {!! $data['content']->almt_outlet !!}</p>
                                <p>Area : {!! $data['content']->kd_area !!}</p>
                                <p>Kota : {!! $data['content']->nm_kota !!}</p>
                                <p>Telepon : {!! $data['content']->kodepos !!}</p>
                                <p>Tipe Outlet : {!! $data['content']->nm_tipe !!}</p>  
                                <p>Distributor : {!! $data['content']->nm_dist !!}</p>     
                                <p>Rank Outlet : {!! $data['content']->rank_outlet !!}</p>
                                <p>Register Outlet : {!! $data['content']->reg_status !!}</p>  
                                <p>Longitude : {!! $data['content']->longitude !!}</p> 
                                <p>Latitude : {!! $data['content']->latitude !!}</p>                        
                              </div>
                        </div>
                    </div><!-- /.box-body -->                
                </div><!-- /.box -->
        </div>

            </div>
        </div><!-- /.row (main row) -->
    </div>
</div>

<!-- add new calendar event modal -->
@include('partials.footer')
    </body>
</html>