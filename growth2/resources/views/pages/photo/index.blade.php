@extends('layouts.master')

@section('title', 'Photo Outlet Activity')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">Photo Activity management</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li><a href="#"><span>Photo Activity</span></a></li>
                            <li class="active"><span>Add Photo Display</span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->

                <!-- Row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-primary card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-light"><i class="fa fa-camera"></i>&nbsp;&nbsp;Photo Activity List</h6>
                                </div>

                                @include('partials.panel')

                                <div class="clearfix"></div>
                            </div>
                            <div id="collapse_1" class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="table-wrap">
                        
                                        @if(Auth::user()->hasAccess('admin.photo.create'))  
                                            <a href="{!! route('admin.photo.create') !!}" class="btn btn-primary"><i class="fa fa-plus">&nbsp;&nbsp;Tambah Photo</i></a>
                                        @endif

                                        <div class="table-responsive">
                                            <table class="table datable_1 table-hover table-bordered display mb-30">
                                                <thead>
                                                    <tr>
                                                        <th><center>View</center></th>
                                                        <th><center>Nama Outlet</center></th>
                                                        <th><center>Tipe</center></th>
                                                        <th><center>Sales Force</center></th>
                                                        <th><center>Nama Foto</center></th>
                                                        <th><center>Date Take Foto</center></th>
                                                        <th><center>Date Upload</center></th>
                                                        <th><center>Foto</center></th>
                                                        @if(Auth::user()->kd_role != 3)   
                                                        <th><center>Action</center></th>
                                                        @endif
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($data as $row)
                                                        <tr>
                                                            <td><center><a href="#"  data-toggle="tooltip" data-placement="bottom" title="Lihat Detail Photo"><i class="fa fa-search-plus"></a></center></td>
                                                            <td>{{ $row->nm_outlet }}</td>
                                                            <td>{{ $row->nama_tipe }}</td>
                                                            <td>{{ $row->nama }}</td>
                                                            <td>{{ $row->nm_photo }}</td>
                                                            <td>{{ $row->date_take_photo }}</td>
                                                            <td>{{ $row->date_upload_photo }}</td>
                                                            @if(File::exists('image_upload/outlet_photoact/'.$row->path_photo) and $row->path_photo != '')
                                                            <td> <a href="{!! url('image_upload/outlet_photoact/'.$row->path_photo) !!}" target="_blank" title="{{ $row->nm_photo }}">
                                                                <img style="max-height: 60px; max-width: 60px;" src="{!! url('image_upload/outlet_photoact/'.$row->path_photo) !!}" class="img-responsive" /></a>
                                                            </td>
                                                            @else
                                                               <td><img style="max-height: 60px; max-width: 60px;" src="{!! url('/img/no_image_outlet_upload.png') !!}" class="img-responsive" /></td>
                                                            @endif

                                                            @if(Auth::user()->kd_role != 3)   
                                                            <td>
                                                                <center>
                                                                    <div class="button-list">
                                                                        @if(Auth::user()->hasAccess('admin.photo.edit'))
                                                                            <a href="{!! route('admin.photo.edit',[$row->id]) !!}" data-toggle="tooltip" title="Edit" class="btn btn-success btn-sm btn-icon-anim btn-square mr-5" style="display: unset;"><i class="fa fa-pencil" style="font-size: 14px;"></i></a>
                                                                        @endif
                                                                        @if(Auth::user()->hasAccess('admin.photo.delete'))      
                                                                            <a href="{!! route('admin.photo.delete',[$row->id]) !!}" id="delete" data-toggle="tooltip" title="Delete" class="btn btn-danger btn-sm btn-icon-anim btn-square mr-5" style="display: unset;"><i class="fa fa-trash" style="font-size: 14px;"></i></a>
                                                                        @endif
                                                                    </div>
                                                                </center>         
                                                            </td>
                                                            @endif
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <!-- /Row -->
            
                @include('partials.footer')

            </div>
        </div>
        <!-- /Main Content -->

        @include('partials.sweetalert')

        @push('message')
            @include('partials.toastr')
        @endpush

@endsection