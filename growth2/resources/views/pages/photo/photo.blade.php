@extends('layouts.master')

@section('link', '<link rel="stylesheet" href="/doodle/vendors/bower_components/summernote/dist/summernote.css" />
    <link href="/doodle/vendors/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" type="text/css"/>
    <link href="/doodle/vendors/bower_components/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet" type="text/css"/>
  <link href="/doodle/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>')

@section('title', 'Photo Display List')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">Photo Activity management</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li><a href="#"><span>Photo Activity</span></a></li>
                            <li class="active"><span>Photo Display List Filter</span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->

                <!-- Row -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-primary card-view panel-refresh">
                                <div class="refresh-container">
                                    <div class="la-anim-1"></div>
                                </div>
                                <div class="panel-heading">
                                    <div class="pull-left">
                                        <h6 class="panel-title txt-light"><i class="fa fa-filter"></i>&nbsp;&nbsp;Filter Data Date Photo Display</h6>
                                    </div>

                                    @include('partials.panel')

                                    <div class="clearfix"></div>
                                </div>
                                <div id="collapse_1" class="panel-wrapper collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-wrap">

                                                    {!! Form::open(array('route' => 'admin.photoAct.result', 'method' => 'POST', 'data-toggle' => 'validator', 'role' => 'form', 'class' => 'form-horizontal')) !!}
                                                        @include('pages.photo.formFilter')
                                                    {!! Form::close() !!}

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- /Row -->

                <!-- Row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-primary card-view panel-refresh">
                            <div class="refresh-container">
                                <div class="la-anim-1"></div>
                            </div>
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-light"><i class="fa fa-camera"></i>&nbsp;&nbsp;Photo Display 1 Minggu Terakhir</h6>
                                </div>

                                @include('partials.panel2')

                                <div class="clearfix"></div>
                            </div>
                            <div id="collapse_2" class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="table-wrap">
                                        <div class="table-responsive">
                                            <table class="table datable_1 table-hover table-bordered display mb-30">
                                                <thead>
                                                    <tr>
                                                        <th>Nama Outlet</th>
                                                        <th>Tipe</th>
                                                        <th>Sales Force </th>
                                                        <th>Nama Foto </th>
                                                        <th>Note</th>
                                                        <th>Date Take Foto</th>
                                                        <th>Date Upload</th>
                                                        <th>Foto</th>
                                                        @if(Auth::user()->kd_role != 3)   
                                                            <th>Action</th>
                                                        @endif
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($data as $row)
                                                        <tr>
                                                            <td>{{ $row->nm_outlet }}</td>
                                                            <td>{{ $row->nama_tipe }}</td>
                                                            <td>{{ $row->nama }}</td>
                                                            <td>{{ $row->nm_photo }}</td>
                                                            <td>{{ $row->keterangan }}</td>
                                                            <td>{{ date('d-F-Y H:i:s', strtotime($row->date_take_photo)) }}</td>
                                                            <td>{{ date('d-F-Y H:i:s', strtotime($row->date_upload_photo)) }}</td>
                                                            @if(File::exists('image_upload/outlet_photoact/'.$row->path_photo) and $row->path_photo != '')
                                                            <td> <a href="{!! url('image_upload/outlet_photoact/'.$row->path_photo) !!}" target="_blank" title="{{ $row->nm_photo }}">
                                                                <img style="max-height: 60px; max-width: 60px;" src="{!! url('image_upload/outlet_photoact/'.$row->path_photo) !!}" class="img-responsive" /></a>
                                                            </td>
                                                            @else
                                                               <td><img style="max-height: 60px; max-width: 60px;" src="{!! url('/img/no_image_outlet_upload.png') !!}" class="img-responsive" /></td>
                                                            @endif

                                                            @if(Auth::user()->kd_role != 3)   
                                                            <td>
                                                                <center>
                                                                    @if(Auth::user()->hasAccess('admin.photo.edit'))
                                                                            <a href="{!! route('admin.photo.edit',[$row->id]) !!}" class="btn btn-success btn-sm btn-icon-anim btn-square" style="display: unset;"><i class="fa fa-pencil" style="font-size: 14px;"></i></a>
                                                                        @endif
                                                                        &nbsp;
                                                                        @if(Auth::user()->hasAccess('admin.photo.delete'))      
                                                                            <a href="{!! route('admin.photo.delete',[$row->id]) !!}" id="delete" class="btn btn-danger btn-sm btn-icon-anim btn-square" style="display: unset;"><i class="fa fa-trash" style="font-size: 14px;"></i></a>
                                                                        @endif
                                                                </center>               
                                                            </td>
                                                            @endif
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <!-- /Row -->
            
                @include('partials.footer')

            </div>
        </div>
        <!-- /Main Content -->

        @include('partials.sweetalert')

        @push('message')
            @include('partials.toastr')
        @endpush

        <!-- Moment JavaScript -->
        <script type="text/javascript" src="/doodle/vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
            
        <!-- Bootstrap Colorpicker JavaScript -->
        <script src="/doodle/vendors/bower_components/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
        <!-- Bootstrap Datetimepicker JavaScript -->
        <script type="text/javascript" src="/doodle/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

        <script type="text/javascript">
            $(function () {
                $('.datetimepicker1').datetimepicker({
                    format: 'YYYY-MM-DD'
                });
            });
        </script>


@endsection