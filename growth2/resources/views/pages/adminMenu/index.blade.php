@extends('layouts.master')

@section('title', 'Admin Menu')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">Admin Menu Management</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li><a href="#"><span>Configuration</span></a></li>
                            <li class="active"><span>Admin Menu</span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->

                <!-- Row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-primary card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-light"><i class="fa fa-user"></i>&nbsp;&nbsp;Admin Menu List</h6>
                                </div>
                                @include('partials.panel')
                                <div class="clearfix"></div>
                            </div>
                            <div id="collapse_1" class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="table-wrap">
                                        
                                        <a href="{!! route('admin.adminmenu.create') !!}" class="btn btn-primary"><i class="fa fa-plus">&nbsp;&nbsp;Tambah Admin Menu</i></a>
                                      
                                        <div class="table-responsive">
                                            <table class="table datable_1 table-hover table-bordered display mb-30">
                                                <thead>
                                                    <tr>
                                                        <th><center>Menu</center></th>
                                                        <th><center>Route</center></th>
                                                        <th><center>Child</center></th>
                                                        <th><center>Role Pengakses</center></th>
                                                        <th><center>Enable</center></th>
                                                        <th><center>Action</center></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($data['content'] as $row)
                                                        <tr>
                                                            <td>{{ $row->nm_menu }}</td>
                                                            @if($row->route != '')
                                                                <td>{{ $row->route }}</td>
                                                            @else
                                                                <td><span class="label label-success">Undefined</span></td>
                                                            @endif

                                                            <td>
                                                            <?php $tmp = array(); ?>
                                                                @foreach($row->child as $v)
                                                                    <?php array_push($tmp, $v->nm_menu);  ?>
                                                                @endforeach
                                                            {!! implode(', ', $tmp) !!}
                                                            </td>
                                                            <td>
                                                             <?php $tmp = array(); ?>
                                                                @foreach($row->roles as $val)
                                                                     <?php array_push($tmp, $val->type_role);  ?>
                                                                @endforeach
                                                            {!! implode(', ', $tmp) !!}
                                                            </td>
                                                            @if($row->enable == 1)
                                                                <td>YES</td> 
                                                            @else
                                                                <td>NO</td> 
                                                            @endif                       
                                                            <td>
                                                            <center>
                                                                <div class="button-list">                     
                                                                    <a href="{!! route('admin.adminmenu.edit',[$row->id]) !!}" data-toggle="tooltip" title="Edit" class="btn btn-success btn-sm btn-icon-anim btn-square mr-5" style="display: unset;"><i class="fa fa-pencil" style="font-size: 10px;"></i></a>                
                                                                    <a href="{!! route('admin.adminmenu.delete',[$row->id]) !!}"  id="delete" data-toggle="tooltip" title="Delete" class="btn btn-danger btn-sm btn-icon-anim btn-square mr-5" style="display: unset;"><i class="fa fa-trash" style="font-size: 10px;"></i></a>
                                                                </div>
                                                            </center>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <!-- /Row -->
            
                @include('partials.footer')

            </div>
        </div>
        <!-- /Main Content -->

        @include('partials.sweetalert')

        @push('message')
            @include('partials.toastr')
        @endpush
        
@endsection