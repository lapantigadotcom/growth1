@extends('layouts.master')

@section('title', 'Report dashboard')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">Report dashboard</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li class="active"><span>Report </span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->

                <!-- Row -->
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="panel panel-default card-view pa-0">
                            <div  class="panel-wrapper collapse in">
                                <div  class="panel-body pa-0">
                                    <div class="sm-data-box bg-green">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <a href="{!! route('admin.report.outletVisit') !!}">
                                                    <div class="col-md-12 text-center pa-25 pb-0 data-wrap-right">
                                                        <i class="fa fa-truck txt-light data-right-rep-icon"></i>
                                                    </div>
                                                    <div class="col-md-12 text-center pa-0 pb-30">
                                                        <span class="txt-light block font-22">
                                                            <strong>Laporan Outlet Visit Per SF</strong>
                                                        </span>
                                                    </div>
                                                </a>
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="panel panel-default card-view pa-0">
                            <div  class="panel-wrapper collapse in">
                                <div  class="panel-body pa-0">
                                    <div class="sm-data-box bg-yellow">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <a href="{!! route('admin.report.workingTime') !!}">
                                                    <div class="col-md-12 text-center pa-25 pb-0 data-wrap-right">
                                                        <i class="fa fa-clock-o txt-light data-right-rep-icon"></i>
                                                    </div>
                                                    <div class="col-md-12 text-center pa-0 pb-30">
                                                        <span class="txt-light block font-22">
                                                            <strong>Laporan Working Time Per SF</strong>
                                                        </span>
                                                    </div>
                                                </a>
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="panel panel-default card-view pa-0">
                            <div  class="panel-wrapper collapse in">
                                <div  class="panel-body pa-0">
                                    <div class="sm-data-box bg-red">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <a href="{!! route('admin.report.effectiveCall') !!}">
                                                    <div class="col-md-12 text-center pa-25 pb-0 data-wrap-right">
                                                        <i class="fa fa-check-square-o txt-light data-right-rep-icon"></i>
                                                    </div>
                                                    <div class="col-md-12 text-center pa-0 pb-30">
                                                        <span class="txt-light block font-22">
                                                            <strong>Laporan Effective Call Sales</strong>
                                                        </span>
                                                    </div>
                                                </a>
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="panel panel-default card-view pa-0">
                            <div  class="panel-wrapper collapse in">
                                <div  class="panel-body pa-0">
                                    <div class="sm-data-box bg-blue">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <a href="{!! route('admin.report.photoActivity') !!}">
                                                    <div class="col-md-12 text-center pa-25 pb-0 data-wrap-right">
                                                        <i class="fa fa-camera txt-light data-right-rep-icon"></i>
                                                    </div>
                                                    <div class="col-md-12 text-center pa-0 pb-30">
                                                        <span class="txt-light block font-22">
                                                            <strong>Laporan Photo Activity</strong>
                                                        </span>
                                                    </div>
                                                </a>
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="panel panel-default card-view pa-0">
                            <div  class="panel-wrapper collapse in">
                                <div  class="panel-body pa-0">
                                    <div class="sm-data-box bg-pink">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <a href="{!! route('admin.report.competitorActivity') !!}">
                                                    <div class="col-md-12 text-center pa-25 pb-0 data-wrap-right">
                                                        <i class="fa fa-search txt-light data-right-rep-icon"></i>
                                                    </div>
                                                    <div class="col-md-12 text-center pa-0 pb-30">
                                                        <span class="txt-light block font-22">
                                                            <strong>Laporan Competitor Activity</strong>
                                                        </span>
                                                    </div>
                                                </a>
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="panel panel-default card-view pa-0">
                            <div  class="panel-wrapper collapse in">
                                <div  class="panel-body pa-0">
                                    <div class="sm-data-box bg-green">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <a href="{!! route('admin.report.outletVisit') !!}">
                                                    <div class="col-md-12 text-center pa-25 pb-0 data-wrap-right">
                                                        <i class="zmdi zmdi-male-female txt-light data-right-rep-icon"></i>
                                                    </div>
                                                    <div class="col-md-12 text-center pa-0 pb-25">
                                                        <span class="txt-light block counter">
                                                            <span class="counter-anim">Laporan Outlet Visit Per SF</span>
                                                        </span>
                                                    </div>
                                                </a>
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> --}}

                </div>
                <!-- /Row -->
            
                @include('partials.footer')

            </div>
        </div>
        <!-- /Main Content -->

@endsection