<div class="form-group">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-3">
            </div>
            <div class="col-sm-3">
                <label class="control-label mb-10">From</label>
                <div class='input-group date datetimepicker1'>
                    <input type='text' name="dateFrom" class="form-control" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
               </div>    
            </div>
            <div class="col-sm-3">
                <label class="control-label mb-10">To</label>
                <div class='input-group date datetimepicker1'>
                    <input type='text' name="dateTo" class="form-control" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>  
            </div>
            <div class="col-sm-3">
            </div>
        </div>
    </div>  
</div>
<div class="form-group">
	<center>
		{!! Form::submit('Show', array('name'=>'show','class' => 'btn btn-success')) !!}
		@if(Auth::user()->hasAccess('admin.download.visitPlan'))        
			{!! Form::submit('Download', array('name'=>'download','class' => 'btn btn-primary')) !!}
		@endif
	</center>
</div>