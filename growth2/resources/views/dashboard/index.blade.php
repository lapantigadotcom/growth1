<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title>GROWTH Sales Force Monitoring Application | Dashboard</title>
	<meta name="description" content="Doodle is a Dashboard & Admin Site Responsive Template by hencework." />
	<meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Doodle Admin, Doodleadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
	<meta name="author" content="hencework"/>
	
	<!-- Favicon -->
	<link rel="shortcut icon" href="/doodle/favicon.ico">
	<link rel="icon" href="/doodle/favicon.ico" type="image/x-icon">

	<!-- Data table CSS -->
	<link href="/doodle/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>

	<!-- Custom CSS -->
	<link href="/doodle/dist/css/style.css" rel="stylesheet" type="text/css">

	<!-- jQuery -->
    <script src="/doodle/vendors/bower_components/jquery/dist/jquery.min.js"></script>
</head>

<body>
	<!--Preloader-->
	<div class="preloader-it">
		<div class="la-anim-1"></div>
	</div>
	<!--/Preloader-->
    <div class="wrapper theme-2-active box-layout pimary-color-blue">

		@include('partials.head')

		@include('partials.sidebar')
       
       	<!-- Main Content -->
		<div class="page-wrapper">
            <div class="container-fluid">
				
				<!-- Title -->
				<div class="row heading-bg">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
						<h5 class="txt-dark">dashboard page</h5>
					</div>
					<!-- Breadcrumb -->
					<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
						<ol class="breadcrumb">
							<li><a href="{{url('/home/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
							<li class="active"><span>Dashboard</span></li>
						</ol>
					</div>
					<!-- /Breadcrumb -->
				</div>
				<!-- /Title -->

				@if(AUth::user()->kd_role != 3)

				<div class="row">
					<!-- Basic Table -->
					<div class="col-sm-12">
						<div class="panel panel-primary card-view">
							<div class="panel-heading">
								<div class="pull-left">
									<h6 class="panel-title txt-light"><i class="fa fa-user"></i>&nbsp;&nbsp;Top 5 Sales Force This Month <b>( <?php echo date('F Y', strtotime('now'));?> )</b></h6>
								</div>

								@include('partials.panel')

								<div class="clearfix"></div>
							</div>
							<div id="collapse_1" class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="table-wrap ">
										<div class="table-responsive">
											<table class="table table-striped table-bordered mb-0">
												<thead>
													<tr>
														<th>#</th>
														<th>Nama Sales Force</th>
														<th>Area</th>
														<th>Total Visit</th>
														<th>Effective Call</th>
													</tr>
												</thead>
												<tbody>
													<?php $i = 1; ?>
		          									@foreach($data['top'] as $val)
													<tr>
														<td>{{$i++}}</td>
										               	<td>{{$val->nama}}</td>
										               	<td>{{$val->nm_area}}</td>
										               	<td>{{$val->TotalVisit}}</td>
										               	<td>{{$val->TotalEc}}</td>
												  	</tr>
												  	@endforeach
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /Basic Table -->
				</div>

				<br>

				<!-- Row -->
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="panel panel-default card-view pa-0">
                            <div  class="panel-wrapper collapse in">
                                <div  class="panel-body pa-0">
                                    <div class="sm-data-box bg-blue">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-md-6 text-center pa-25 pb-0 pr-0 data-wrap-right">
                                                    <i class="fa fa-truck txt-light data-right-rep-icon pull-right"></i>
                                                </div>
                                                <div class="col-md-6 text-center pa-25 pb-0 pr-0 pl-20 data-wrap-right">
                                                    <span class="txt-light block counter">
                                                    	<span class="counter-anim pull-left">{!! $data['finishedVisit'] !!}</span>
                                                    </span>
                                                </div>
                                                <div class="col-md-12 text-center pa-0 pb-40">
                                                    <span class="txt-light block font-16">
                                                    	<strong>Finished Visit Plan This Month ( <?php echo date('F Y', strtotime('now'));?> )</strong>
                                                    </span>
                                                </div>
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="panel panel-default card-view pa-0">
                            <div  class="panel-wrapper collapse in">
                                <div  class="panel-body pa-0">
                                    <div class="sm-data-box bg-red">
                                        <div class="container-fluid">
                                            <div class="row">
                                            	<div class="col-md-6 text-center pa-25 pb-0 pr-0 data-wrap-right">
                                                    <i class="fa fa-check txt-light data-right-rep-icon pull-right"></i>
                                                </div>
                                                <div class="col-md-6 text-center pa-25 pb-0 pr-0 pl-20 data-wrap-right">
                                                    <span class="txt-light block counter">
                                                    	<span class="counter-anim pull-left">{!! $data['pendingVisit'] !!}</span>
                                                    </span>
                                                </div>
                                                <div class="col-md-12 text-center pa-0 pb-40">
                                                    <span class="weight-500 txt-light block font-16">
                                                     	<strong>Pending Visit Plan This Month ( <?php echo date('F Y', strtotime('now'));?> )</strong>
                                                    </span>
                                                </div>
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="panel panel-default card-view pa-0">
                            <div  class="panel-wrapper collapse in">
                                <div  class="panel-body pa-0">
                                    <div class="sm-data-box bg-yellow">
                                        <div class="container-fluid">
                                            <div class="row">
                                            	<div class="col-md-6 text-center pa-25 pb-0 pr-0 data-wrap-right">
                                                    <i class="fa fa-shopping-cart txt-light data-right-rep-icon pull-right"></i>
                                                </div>
                                                <div class="col-md-6 text-center pa-25 pb-0 pr-0 pl-20 data-wrap-right">
                                                    <span class="txt-light block counter">
                                                    	<span class="counter-anim pull-left">{!! $data['effectiveCall'] !!}</span>
                                                    </span>
                                                </div>
                                                <div class="col-md-12 text-center pa-0 pb-40">
                                                    <span class="txt-light block font-16">
                                                     	<strong>Effective Call This Month ( <?php echo date('F Y', strtotime('now'));?> )</strong>
                                                    </span>
                                                </div>
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="panel panel-default card-view pa-0">
                            <div  class="panel-wrapper collapse in">
                                <div  class="panel-body pa-0">
                                    <div class="sm-data-box bg-pink">
                                        <div class="container-fluid">
                                            <div class="row">
                                            	<div class="col-md-6 text-center pa-25 pb-0 pr-0 data-wrap-right">
                                                    <i class="fa fa-group txt-light data-right-rep-icon pull-right"></i>
                                                </div>
                                                <div class="col-md-6 text-center pa-25 pb-0 pr-0 pl-20 data-wrap-right">
                                                    <span class="txt-light block counter">
                                                    	<span class="counter-anim pull-left">{!! $data['competitorAct'] !!}</span>
                                                    </span>
                                                </div>
                                                <div class="col-md-12 text-center pa-0 pb-40">
                                                    <span class="txt-light block font-16">
                                                    	<strong>Competitor Activity This Month ( <?php echo date('F Y', strtotime('now'));?> )</strong>
                                                    </span>
                                                </div>
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="panel panel-default card-view pa-0">
                            <div  class="panel-wrapper collapse in">
                                <div  class="panel-body pa-0">
                                    <div class="sm-data-box bg-green">
                                        <div class="container-fluid">
                                            <div class="row">
                                            	<div class="col-md-6 text-center pa-25 pb-0 pr-0 data-wrap-right">
                                                    <i class="fa fa-camera txt-light data-right-rep-icon pull-right"></i>
                                                </div>
                                                <div class="col-md-6 text-center pa-25 pb-0 pr-0 pl-20 data-wrap-right">
                                                    <span class="txt-light block counter">
                                                    	<span class="counter-anim pull-left">{!! $data['photoOutlet'] !!}</span>
                                                    </span>
                                                </div>
                                                <div class="col-md-12 text-center pa-0 pb-40">
                                                    <span class="txt-light block font-16">
                                                       	<strong>Photo Outlet This Month ( <?php echo date('F Y', strtotime('now'));?> )</strong>
                                                    </span>
                                                </div>
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="panel panel-default card-view pa-0">
                            <div  class="panel-wrapper collapse in">
                                <div  class="panel-body pa-0">
                                    <div class="sm-data-box bg-dark">
                                        <div class="container-fluid">
                                            <div class="row" style="background: teal">
                                            	<div class="col-md-6 text-center pa-25 pb-0 pr-0 data-wrap-right">
                                                    <i class="fa fa-home txt-light data-right-rep-icon pull-right"></i>
                                                </div>
                                                <div class="col-md-6 text-center pa-25 pb-0 pr-0 pl-20 data-wrap-right">
                                                    <span class="txt-light block counter">
                                                    	<span class="counter-anim pull-left">{!! $data['regOutlet'] !!}</span>
                                                    </span>
                                                </div>
                                                <div class="col-md-12 text-center pa-0 pb-40">
                                                    <span class="txt-light block font-16">
                                                    	<strong>Registered Outlet ( <?php echo date('F Y', strtotime('now'));?> )</strong>
                                                    </span>
                                                </div>
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Row -->

				@endif

				<br>
				
				<div class="row">
					<!-- Basic Table -->
					<div class="col-sm-12">
						<div class="panel panel-primary card-view">
							<div class="panel-heading">
								<div class="pull-left">
									<h6 class="panel-title txt-light"><i class="fa fa-signal"></i>&nbsp;&nbsp;Visit Activity <?php echo "This Month " . date("m-Y"); ?></h6>
								</div>

								@include('partials.panel2')
								
								<div class="clearfix"></div>

								<div id="collapse_2" class="panel-wrapper collapse in">
                                    <div class="panel-body">
                                    	<div style="display: block;" class="box-content alerts">
											<div class="content">
											    <div id="pop_div" style="height:300px"></div>
												<?= Lava::render('AreaChart', 'VisitPlan', 'pop_div'); ?>
												@areachart('VisitPlan', 'pop_div')
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /Basic Table -->
				</div>

				<br>

				<div class="row">
					<!-- Basic Table -->
					<div class="col-sm-12">
						<div class="panel panel-primary card-view">
							<div class="panel-heading">
								<div class="pull-left">
									<h6 class="panel-title txt-light"><i class="fa fa-tags"></i>&nbsp;&nbsp;Effective Call <?php echo "This Month " . date("m-Y"); ?></h6>
								</div>

								@include('partials.panel3')
								
								<div class="clearfix"></div>

								<div id="collapse_3" class="panel-wrapper collapse in">
                                    <div class="panel-body">
                                    	<div style="display: block;" class="box-content alerts">
											<div class="content">
											    <div id="order_div" style="height:300px"></div>
												<?= Lava::render('AreaChart', 'TakeOrder', 'pop_div'); ?>
												@areachart('TakeOrder', 'order_div')
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /Basic Table -->
				</div>
				
			
				@include('partials.footer')

			</div>
		</div>
        <!-- /Main Content -->

    </div>
    <!-- /#wrapper -->
	
	<!-- JavaScript -->
	
    <!-- Bootstrap Core JavaScript -->
    <script src="/doodle/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    
	<!-- Data table JavaScript -->
	<script src="/doodle/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
	<script src="/doodle/dist/js/dataTables-data.js"></script>
	
	<!-- Slimscroll JavaScript -->
	<script src="/doodle/dist/js/jquery.slimscroll.js"></script>

	<!-- Progressbar Animation JavaScript -->
	<script src="/doodle/vendors/bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
	<script src="/doodle/vendors/bower_components/jquery.counterup/jquery.counterup.min.js"></script>

	<!-- Owl JavaScript -->
	<script src="/doodle/vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>

	<!-- Fancy Dropdown JS -->
	<script src="/doodle/dist/js/dropdown-bootstrap-extended.js"></script>
	
	<!-- Switchery JavaScript -->
	<script src="/doodle/vendors/bower_components/switchery/dist/switchery.min.js"></script>

	<!-- Init JavaScript -->
	<script src="/doodle/dist/js/init.js"></script>


</body>

</html>
