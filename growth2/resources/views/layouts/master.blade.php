<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title>GROWTH Sales Force Monitoring Application | @yield('title')</title>
	<meta name="description" content="Doodle is a Dashboard & Admin Site Responsive Template by hencework." />
	<meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Doodle Admin, Doodleadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
	<meta name="author" content="hencework"/>
	
	<!-- Favicon -->
	<link rel="shortcut icon" href="/doodle/favicon.ico">
	<link rel="icon" href="/doodle/favicon.ico" type="image/x-icon">

	<!-- bootstrap-select CSS -->
	<link href="{{ asset("/doodle/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.min.css")}}" rel="stylesheet" type="text/css"/>
	<!-- Data table CSS -->
	<link href="{{ asset("/doodle/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css")}}" rel="stylesheet" type="text/css"/>
	<!--Toast CSS -->
	<link href="{{ asset("/doodle/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css")}}" rel="stylesheet" type="text/css">
	<!--Sweetalert CSS -->
	<link href="{{ asset("/doodle/vendors/sweetalert2/sweetalert2.min.css")}}" rel="stylesheet" type="text/css">

	@yield('link')

	<!-- Custom CSS -->
	<link href="{{ asset("/doodle/dist/css/style.css")}}" rel="stylesheet" type="text/css">
	<!-- jQuery -->
    <script src="{{ asset("/doodle/vendors/bower_components/jquery/dist/jquery.min.js")}}"></script>
    

</head>

<body>
	<!--Preloader-->
	<div class="preloader-it">
		<div class="la-anim-1"></div>
	</div>
	<!--/Preloader-->
    <div class="wrapper theme-2-active box-layout pimary-color-blue">

		@include('partials.head')

		@include('partials.sidebar')

		@yield('content')

    </div>
    <!-- /#wrapper -->
	
	<!-- JavaScript -->
	
    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset("/doodle/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js") }}"></script>
    <script src="{{ asset("/doodle/vendors/bower_components/bootstrap-validator/dist/validator.min.js") }}"></script>
    
    @yield('script')

	<!-- Data table JavaScript -->
	<script src="{{ asset("/doodle/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js") }}"></script>
	<script src="{{ asset("/doodle/dist/js/dataTables-data.js") }}"></script>
	
	<!-- Slimscroll JavaScript -->
	<script src="{{ asset("/doodle/dist/js/jquery.slimscroll.js") }}"></script>
	<!-- Owl JavaScript -->
	<script src="{{ asset("/doodle/vendors/bower_components/owl.carousel/dist/owl.carousel.min.js") }}"></script>
	<!-- Switchery JavaScript -->
	<script src="{{ asset("/doodle/vendors/bower_components/switchery/dist/switchery.min.js") }}"></script>
	<!-- Morris Charts JavaScript -->
    <script src="{{ asset("/doodle/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js") }}"></script>
	<!-- Sweet-Alert  -->
	<script src="{{ asset("/doodle/vendors/sweetalert2/sweetalert2.min.js") }}"></script>
	<!-- Bootstrap Select JavaScript -->
	<script src="{{ asset("/doodle/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js") }}"></script>
	<!-- Fancy Dropdown JS -->
	<script src="{{ asset("/doodle/dist/js/dropdown-bootstrap-extended.js") }}"></script>
	<!-- Init JavaScript -->
	<script src="{{ asset("/doodle/dist/js/init.js") }}"></script>

	@stack('message')

</body>

</html>
