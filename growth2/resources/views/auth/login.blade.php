@extends('layouts.auth')

@section('title', 'Login')

@section('content')

  		<div class="wrapper pa-0">
			<header class="sp-header">
				<div class="sp-logo-wrap pull-left">
					<a href="index.html">
						<img class="brand-img mr-10" src="/doodle/dist/img/logo.png" alt="brand"/>
						<span class="brand-text">growth</span>
					</a>
				</div>
				<div class="form-group mb-0 pull-right">
					<span class="inline-block pr-10">Don't have an account?</span>
					<a class="inline-block btn btn-info btn-rounded btn-outline" href="/auth/register">Sign Up</a>
				</div>
				<div class="clearfix"></div>
			</header>

			<!-- Main Content -->
			<div class="page-wrapper pa-0 ma-0 auth-page">
				<div class="container-fluid">
					<!-- Row -->
					<div class="table-struct full-width full-height">
						<div class="table-cell vertical-align-middle auth-form-wrap">
							<div class="auth-form  ml-auto mr-auto no-float">
								<div class="row">
									<div class="col-sm-12 col-xs-12">
										<div class="mb-30">
											<h3 class="text-center txt-dark mb-10">Sign in to Growth</h3>
											<h6 class="text-center nonecase-font txt-grey">Enter your details below</h6>
										</div>	
										<div class="form-wrap">

											{!! Form::open(['route'=>'admin.auth.login', 'method' => 'store', 'class'=>'form-horizontal']) !!}
												<div class="form-group">
													<label class="control-label mb-10" for="Username">Username</label>
													{!! Form::text('username',null, array('class' => 'form-control', 'placeholder' => 'Enter Username')) !!}
												</div>
												<div class="form-group">
													<label class="pull-left control-label mb-10" for="Password">Password</label>
													<a class="capitalize-font txt-primary block mb-10 pull-right font-12" href="{{ route('admin.autentikasi') }}">forgot password ?</a>
													<div class="clearfix"></div>
													{!! Form::password('password', array('class' => 'form-control', 'placeholder' => 'Enter Password')) !!}
												</div>
												
												<div class="form-group">
													<div class="checkbox checkbox-primary pr-10 pull-left">
														<input id="checkbox_2" type="checkbox" value="1" name="remember_me">
														<label for="checkbox_2"> Keep me logged in</label>
													</div>
													<div class="clearfix"></div>
												</div>
												@include('partials.errors')
												<div class="form-group text-center">
													<button type="submit" class="btn btn-info btn-rounded">sign in</button>
												</div>
											{!! Form::close() !!}

										</div>
									</div>	
								</div>
							</div>
						</div>
					</div>
					<!-- /Row -->	
				</div>
				
			</div>
			<!-- /Main Content -->

		</div>
    	<!-- /#wrapper -->


@endsection
